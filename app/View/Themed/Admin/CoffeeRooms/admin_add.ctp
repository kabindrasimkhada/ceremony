<?php
echo $this->element('navigation', array('items' => 'tab8')); ?>	
<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane active" id="tab1">
        <?php echo $this->Form->create('CoffeeRoom',array(
                                                                'class'=>'form-horizontal',
                                                                'novalidate' => true
                                                            )
                                                        );
        ?>
        <div class="form-group">
            <div class="col-sm-12">
                <table class="table-bordered table">
                    <tr>
                        <td><?php echo __('Consumpties'); ?></td>
                        <td><?php echo __('Omschrijving'); ?></td>
                        <td width="50" class="text-center"><a href="#"><span class="glyphicon glyphicon-plus"></span></a></td>
                    </tr>
							<?php 
								if(isset($dynamicDatas) && !empty($dynamicDatas)):
									foreach($dynamicDatas as $data):
							?>
                                        <tr>
                                            <td><input id="CoffeeRoomNumber" class="form-control" type="text" name="data[CoffeeRoom][number][]" value = "<?php echo $data['number'];?>"></td>
                                            <td colspan="2"><input id="CoffeeRoomNumber" class="form-control" type="text" name="data[CoffeeRoom][letter][]" value = "<?php echo $data['letter'];?>"></td>
                                        </tr>
        				 	<?php 
        				 			 endforeach;
        				 			endif;
        				    ?>
                    <!--<tr>
                            <td>Consumpties</td>
                            <td>Omschrijving</td>
                            <td class="text-center"><a href="#" class="text-danger"><span class="glyphicon glyphicon-remove"></span></a></td>
                    </tr>-->
                </table>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2"><?php echo __('Aantal personen'); ?></label>
            <div class="col-sm-1">
						<?php echo $this->Form->input('no_of_people',
                                                            array(
																'class'=>'form-control',
																'div'=>false,
																'label'=>false,
                                                                'type'=>'text'
															)
										);
						?>
            </div>
       <!--  </div>
        <div class="form-group"> -->
            <label class="control-label col-sm-2"><?php echo __('Familietafel aantal'); ?></label>
            <div class="col-sm-1">
						<?php echo $this->Form->input('family_tbl_no',
                                    array(
                                            'class'=>'form-control',
                                            'div'=>false,
                                            'label'=>false,
                                            'type'=>'text'
                                        )
                                    );
                         ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2"><?php echo __('Moment stilte/gebed'); ?></label>
            <div class="col-sm-4">
						<?php echo $this->Form->input('moment_of_silence',
										array(  
                                                'options'=>array('Nee','Ja'),
                                                'before'=>'<label class="radio-inline">',
                                                'type' => 'radio',
                                                'class' => 'radio-inline',
                                                'legend' => false,
                                                'div' => false,
                                                'fieldset' => false,
                                                'separator'=>'</label><label class="radio-inline">',
                                                'after' => '</label>',
                                                //'class' => 'radioGroup7',
                                                'required' => false,
                                                'default'=>'',
                                        )
								);
			 			?>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2"><?php echo __('Gereserveerd'); ?></label>
            <div class="col-sm-4">
					<?php echo $this->Form->input('reserved',
										array(  
                                                'options'=>array('Nee','Ja'),
                                                'before'=>'<label class="radio-inline">',
                                                'type' => 'radio',
                                                'class' => 'radio-inline',
                                                'legend' => false,
                                                'div' => false,
                                                'fieldset' => false,
                                                'separator'=>'</label><label class="radio-inline">',
                                                'after' => '</label>',
                                                //'class' => 'radioGroup7',
                                                'required' => false,
                                                'default'=>'',
											)
										 );
			 		?>
            </div>
        </div>
        <h1 class="title-large font-light"><?php echo __('Koffietafel'); ?></h1>
        <div class="form-group">
            <label class="control-label col-sm-2"><?php echo __('Gereserveerd'); ?></label>
            <div class="col-sm-4">
						<?php echo $this->Form->input('coffee_reserved',array(
                                         'options'=>$reserved_types,
                                         'class'=>'form-control',
                                         'div'=>false,
                                         'label'=>false,
                                         'empty'=>'Selecteer',
                                         'default'=>'0',
                                   	 ));
						?>

            </div>
            <label class="control-label col-sm-1"><?php echo __('Elders'); ?></label>
            <div class="col-sm-4">
						<?php echo $this->Form->input('elders',array(
																	  'class'=>'form-control',
																	  'div'=>false,
																	  'label'=>false
																	)
													);
						?>
            </div>
        </div>
        <div class="actions">
            <div class="pull-right">
						<?php echo $this->Form->submit(__('Opslaan'),array(
																			'class'=>'btn btn-primary'
																		  )
														);
						?>
            </div>
        </div>
			<?php echo $this->Form->end(); ?>
    </div>
</div>

<div id="bottom-nav">
 	<?php echo $this->element('bottom_nav'); ?>
</div>


