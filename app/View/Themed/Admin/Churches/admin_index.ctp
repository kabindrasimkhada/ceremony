<div class="row Church">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <i class="glyphicon glyphicon-list-alt"></i>
                <h3 class="box-title"><?php echo __('Church List'); ?></h3>                                    
            </div><!-- /.box-header -->
            <!-- Flash Msg -->
            <?php echo $this->Session->flash(); ?>           
            <?php //echo $this->Session->flash('auth'); ?>            <!--/Flash Msg -->

            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                            <th><?php echo $this->Paginator->sort('chruch_name', __('kerk naam')); ?></th>
                            <th><?php echo $this->Paginator->sort('location','locatie'); ?></th>
                            <th><?php echo $this->Paginator->sort('created',__('aangemaakt')); ?></th>
                            <th class="actions"><?php echo __('acties'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($churches as $chruch): ?>
                                <tr>
                                    <td><?php echo h($chruch['Church']['id']); ?>&nbsp;</td>
                                    <td><?php echo h($chruch['Church']['chruch_name']); ?>&nbsp;</td>
                                     <td><?php echo h($chruch['Church']['location']); ?>&nbsp;</td>
                                    <td><?php echo h($chruch['Church']['created']); ?>&nbsp;</td>
                                    <td class="actions">
                                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $chruch['Church']['id'])); ?>
                                        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $chruch['Church']['id']), null, __('Are you sure you want to delete # %s?', $chruch['Church']['id'])); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                    </tbody>
                    <tfoot>
                         <tr>
                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                            <th><?php echo $this->Paginator->sort('chruch_name', __('kerk naam')); ?></th>
                            <th><?php echo $this->Paginator->sort('location','locatie'); ?></th>
                            <th><?php echo $this->Paginator->sort('created',__('aangemaakt')); ?></th>
                            <th class="actions"><?php echo __('acties'); ?></th>
                        </tr>
                    </tfoot>
                </table>
                 <?php
                    echo $this->Html->link(__('toevoegen'), 
                                                array('action' => 'add'),
                                                array('class'=>'btn btn-primary addbtn'));
                 ?>
                <?php echo $this->element('pagination'); ?>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<!--End Church div-->
