<?php
echo $this->element('navigation', array('items' => 'tab1'));
?>	
<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane active" id="tab1">
        <h1 class="title-large font-light"><?php echo __('Gegevens Opdrachtgever'); ?></h1>
        <?php echo $this->Form->create('ClientData', array('class' => 'form-horizontal')); ?>
        <div class="form-group">
            <label class="control-label col-sm-2"><?php echo __('Geslachtsnaam'); ?></label>
            <div class="col-sm-4">
                <?php
                echo $this->Form->input('genus', array(
                    'class' => 'form-control',
                    'div' => false,
                    'label' => false,
                    'required' => false
                        )
                );
                ?>
            </div>
            <label class="control-label col-sm-2"><?php echo __('Voorvoegsels'); ?></label>
            <div class="col-sm-4">
                <?php
                echo $this->Form->input('genus_prefix', array(
                    'class' => 'form-control',
                    'div' => false,
                    'label' => false,
                    'required' => false
                        )
                );
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2"><?php echo __('Voornamen'); ?></label>
            <div class="col-sm-4">
                <?php
                echo $this->Form->input('first_name', array(
                    'class' => 'form-control',
                    'div' => false,
                    'label' => false,
                    'required' => false,
                        )
                );
                ?>
            </div>
            <label class="control-label col-sm-2"><?php echo __('Roepnaam'); ?></label>
            <div class="col-sm-4">
                <?php
                echo $this->Form->input('nickname', array(
                    'class' => 'form-control',
                    'div' => false,
                    'label' => false,
                    'required' => false,
                        )
                );
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2"><?php echo __('Partnernaam'); ?></label>
            <div class="col-sm-4">
                <?php
                echo $this->Form->input('partner_name', array(
                    'class' => 'form-control',
                    'div' => false,
                    'label' => false,
                    'required' => false,
                        )
                );
                ?>
            </div>
            <label class="control-label col-sm-2"><?php echo __('Voorvoegsels'); ?></label>
            <div class="col-sm-4">
                <?php
                echo $this->Form->input('partner_prefix', array(
                    'class' => 'form-control',
                    'div' => false,
                    'label' => false,
                    'required' => false,
                        )
                );
                ?>
            </div>
        </div>
        <!-- <div class="form-group">
            <label class="control-label col-sm-2"><?php echo __('Naamgebruik'); ?></label>
            <div class="col-sm-4">
        <?php
        echo $this->Form->input('use_name', array(
            'class' => 'form-control',
            'div' => false,
            'label' => false,
            'required' => false,
                )
        );
        ?>
            </div>
        </div> -->
        <div class="form-group">
            <label class="control-label col-sm-2"><?php echo __('Geslacht'); ?></label>
            <div class="col-sm-4">
                <?php
                echo $this->Form->input('sex', array(
                    'options' => array(
                        'Man',
                        'Vrouw',
                    ),
                    'class' => 'form-control',
                    'div' => false,
                    'label' => false,
                    'required' => false,
                    'empty' => 'Selecteer',
                    'default' => ""
                        )
                );
                ?>
            </div>
            <label class="control-label col-sm-2"><?php echo __('BSN'); ?></label>
            <div class="col-sm-4">
                <?php
                echo $this->Form->input('BSN', array(
                    'class' => 'form-control',
                    'div' => false,
                    'label' => false,
                    'required' => false,
                        )
                );
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2"><?php echo __('Geboortedatum'); ?></label>
            <div class="col-sm-5">
                <div class="input-group"  data-date="12-02-2012" data-date-format="dd-mm-yyyy">
                    <?php
                    echo $this->Form->input('birthdate', array(
                        'class' => 'form-control datepicker-control',
                        'div' => false,
                        'label' => false,
                        'required' => false,
                        'type' => 'text'
                            )
                    );
                    ?>
                    <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
                </div>
            </div>
            <label class="control-label col-sm-2"><?php echo __('Geboorteplaats'); ?></label>
            <div class="col-sm-3">
                <?php
                echo $this->Form->input('birthplace', array(
                    'class' => 'form-control',
                    'div' => false,
                    'label' => false,
                    'required' => false,
                        )
                );
                ?>
            </div>
        </div>
        <div class="form-group message">
            <label class="control-label col-sm-2"><?php echo __('Postcode'); ?></label>
            <div class="col-sm-5">
                <div class="input-group" >
                    <?php
                    echo $this->Form->input('zip_code', array(
                        'class' => 'form-control postcode',
                        'div' => false,
                        'label' => false,
                        'required' => false,
                            )
                    );
                    ?>
                    <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
                </div>
            </div>
            <label class="control-label col-sm-2"><?php echo __('Nummer'); ?></label>
            <div class="col-sm-1">
                <?php
                echo $this->Form->input('number', array(
                    'class' => 'form-control',
                    'div' => false,
                    'label' => false,
                    'required' => false,
                    'type' => 'text'
                        )
                );
                ?>
            </div>
            <label class="control-label col-sm-1"><?php echo __('Letter'); ?></label>
            <div class="col-sm-1">
                <?php
                echo $this->Form->input('letter', array(
                    'class' => 'form-control',
                    'div' => false,
                    'label' => false,
                    'required' => false,
                        )
                );
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2"><?php echo __('Straat'); ?></label>
            <div class="col-sm-5">
                <?php
                echo $this->Form->input('street', array(
                    'class' => 'form-control street',
                    'div' => false,
                    'label' => false,
                    'required' => false,
                        )
                );
                ?>
            </div>
            <label class="control-label col-sm-2"><?php echo __('Plaats (/land)'); ?></label>
            <div class="col-sm-3">
                <?php
                echo $this->Form->input('location', array(
                    'class' => 'form-control place',
                    'div' => false,
                    'label' => false,
                    'required' => false,
                        )
                );
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2"><?php echo __('Telefoon'); ?></label>
            <div class="col-sm-2">
                <?php
                echo $this->Form->input('phone', array(
                    'class' => 'form-control',
                    'div' => false,
                    'label' => false,
                    'required' => false,
                        )
                );
                ?>
            </div>
            <label class="control-label col-sm-1"><?php echo __('Mobiel'); ?></label>
            <div class="col-sm-2">
                <?php
                echo $this->Form->input('mobile', array(
                    'class' => 'form-control',
                    'div' => false,
                    'label' => false,
                    'required' => false,
                    'type' => 'text'
                        )
                );
                ?>
            </div>
            <label class="control-label col-sm-2"><?php echo __('E-mail adres'); ?></label>
            <div class="col-sm-3">
                <?php
                echo $this->Form->input('email', array(
                    'class' => 'form-control',
                    'div' => false,
                    'label' => false,
                    'required' => false,
                        )
                );
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2"><?php echo __('Relatie tot overledene'); ?></label>
            <div class="col-sm-5">
                <?php
                echo $this->Form->input('relationship_to_deceased', array(
                    'class' => 'form-control',
                    'div' => false,
                    'label' => false,
                    'required' => false,
                        )
                );
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2"><?php echo __('Afwijkend factuur adres'); ?></label>
            <div class="col-sm-1">
                <?php
                echo $this->Form->input('invoice', array(
                    'placeholder' => __('Afwijkend factuur adres'),
                    'type' => 'checkbox',
                    'label' => false,
                    'class' => 'form-control invoice_add',
                    'div' => false
                ));
                ?>
            </div>
        </div><!-- /.box-body -->
        
        
        <div id="invocie_address" style="display:none"> 
            <div class="form-group">
                <label class="control-label col-sm-2"><?php echo __('Naam'); ?></label>
                <div class="col-sm-4">
                    <?php
                    echo $this->Form->input('BillingAddress.name', array(
                        'class' => 'form-control',
                        'div' => false,
                        'label' => false
                            )
                    );
                    ?>
                </div>
                <label class="control-label col-sm-1"><?php echo __('Geslacht'); ?></label>
                <div class="col-sm-2">
                    <?php
                    echo $this->Form->input('BillingAddress.sex', array(
                        'options' => array(
                            'Man' => 'Man',
                            'Vrouw' => 'Vrouw'
                        ),
                        'class' => 'form-control',
                        'div' => false,
                        'label' => false,
                        'empty' => 'Selecteer',
                        'default' => ""
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2"><?php echo __('Relatie tot ovl.'); ?></label>
                <div class="col-sm-3">
                    <?php
                    echo $this->Form->input('BillingAddress.relation', array(
                        'class' => 'form-control',
                        'div' => false,
                        'label' => false
                            )
                    );
                    ?>
                </div>
            </div>	
            <div class="form-group">	
                <label class="control-label col-sm-2"><?php echo __('Telefoon'); ?></label>
                <div class="col-sm-2">
                    <?php
                    echo $this->Form->input('BillingAddress.phone', array(
                        'class' => 'form-control',
                        'div' => false,
                        'label' => false
                            )
                    );
                    ?>
                </div>
                <label class="control-label col-sm-1"><?php echo __('Mobiel'); ?></label>
                <div class="col-sm-2">
                    <?php
                    echo $this->Form->input('BillingAddress.mobile', array(
                        'class' => 'form-control',
                        'div' => false,
                        'label' => false,
                        'type' => 'text'
                            )
                    );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2"><?php echo __('Postcode'); ?></label>
                <div class="col-sm-2">
                    <div class="input-group">
                        <?php
                        echo $this->Form->input('BillingAddress.zip_code', array(
                            'class' => 'form-control postcode',
                            'div' => false,
                            'label' => false,
                            'type' => 'text'
                                )
                        );
                        ?>
                        <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
                    </div>
                </div>
                <label class="control-label col-sm-1"><?php echo __('Nummer'); ?></label>
                <div class="col-sm-1">
                    <?php
                    echo $this->Form->input('BillingAddress.number', array(
                        'class' => 'form-control',
                        'div' => false,
                        'label' => false,
                        'type' => 'text'
                            )
                    );
                    ?>
                </div>
            </div>	
            <div class="form-group">	
                <label class="control-label col-sm-2"><?php echo __('Letter'); ?></label>
                <div class="col-sm-2">
                    <?php
                    echo $this->Form->input('BillingAddress.letter', array(
                        'class' => 'form-control',
                        'div' => false,
                        'label' => false
                            )
                    );
                    ?>
                </div>

                <label class="control-label col-sm-1"><?php echo __('Toev.'); ?></label>
                <div class="col-sm-1">
                    <?php
                    echo $this->Form->input('BillingAddress.add', array(
                        'class' => 'form-control',
                        'div' => false,
                        'label' => false
                            )
                    );
                    ?>
                </div>
                <label class="control-label col-sm-1"><?php echo __('Aand.'); ?></label>
                <div class="col-sm-1">
                    <?php
                    echo $this->Form->input('BillingAddress.tightening', array(
                        'class' => 'form-control',
                        'div' => false,
                        'label' => false
                            )
                    );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2"><?php echo __('Straat'); ?></label>
                <div class="col-sm-5">
                    <?php
                    echo $this->Form->input('BillingAddress.street', array(
                        'class' => 'form-control street',
                        'div' => false,
                        'label' => false
                            )
                    );
                    ?>
                </div>
            </div>
            <div class="form-group">	
                <label class="control-label col-sm-2"><?php echo __('Plaats (/land)'); ?></label>
                <div class="col-sm-3">
                    <?php
                    echo $this->Form->input('BillingAddress.location', array(
                        'class' => 'form-control place',
                        'div' => false,
                        'label' => false
                            )
                    );
                    ?>
                </div>
            </div>
        </div>
        <div class="actions col-sm-12">
            <div class="pull-right">
                <?php echo $this->Form->submit(__('Opslaan'), array('class' => 'btn btn-primary')); ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>	
</div>


<div id="bottom-nav">
    <?php echo $this->element('bottom_nav'); ?>
</div>

 <?php echo $this->fetch('scriptBottom'); ?>
<script>
jQuery(function() {    
    invoiceTrigger();    
});    
    
jQuery('.invoice_add').change(function(){
    invoiceTrigger();
});
// To add the invoice different 

function invoiceTrigger(){
    if(jQuery('#ClientDataInvoice').is(':checked')){ //if checked
        jQuery("#invocie_address").show();
    }else{ //if not checked
        jQuery("#invocie_address").hide();
    }     
}
</script>
<?php  $this->end(); ?>
    