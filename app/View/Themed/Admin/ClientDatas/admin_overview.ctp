<?php echo $this->element('navigation', array('items' => 'tab10')); ?>	
<?php ob_start(); ?>
<?php echo $this->Form->create('ClientData', array('class' => 'form-horizontal')); ?>
<br>
<div class="form-group">
    <div class="col-sm-4">
        <?php
        echo $this->Form->input('Client.status', array(
            'options' =>
            array(
                __('Drafts'),
                __('orders'),
                __('Quotes')
            ),
            'class' => 'form-control',
            'div' => false,
            'label' => false,
            'empty' => 'Selecteer',
            'default' => ""
                )
        );
        ?>
    </div>
    <div class="actions col-sm-2">
        <div>
            <?php
            echo $this->Form->submit(__('Opslaan'), array(
                'class' => 'btn btn-primary btn-block'
                    )
            );
            ?>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>

<h2><?php echo __('Overview'); ?></h2>
<?php
$map = array(
    'genus' => __('Geslachtsnaam'),
    'genus_prefix' => __('Voorvoegsels'),
    'first_name' => __('Voornamen'),
    'nickname' => __('Roepnaam'),
    'partner_name' => __('Partnernaam'),
    'partner_prefix' => __('Voorvoegsels'),
    'use_name' => __('Naamgebruik'),
    'sex' => __('Geslacht'),
    'BSN' => __('BSN'),
    'brithdate' => __('Geboortedatum'),
    'birthdate' => __('Geboortedatum'),
    'birthplace' => __('Geboorteplaats'),
    'zip_code' => __('Postcode'),
    'number' => __('Nummer'),
    'letter' => __('Letter'),
    'add' => __('Toev'),
    'tightening' => __('Aand'),
    'street' => __('Straat'),
    'location' => __('Plaats (/land)'),
    'belief' => __('Levensovertuiging'),
    'phone' => __('Telefoon'),
    'mobile' => __('Mobiel'),
    'email' => __('E-mail adres'),
    'age' => __('Leeftijd'),
    'martial_status' => __('Burgerlijkestaat'),
    'father_name' => __('Naam vader'),
    'mother_name' => __('Naam moeder'),
    'wedding_booklet_included' => __('Trouwboekje meegenomen'),
    'extract_death_certificate' => __('Uitreksel overlijdensakte'),
    'name_of_declarant' => __('Aangifte gedaan door (Naam aangever)'),
    'funerals' => __('Uitvaartverzekering'),
    'forwarded_data_to' => __('Gegevens doorsturen naar'),
    'relationship_to_deceased' => __('Relatie tot overledene'),
    'crematorium' => __('Crematorium'),
    'part_of' => __('Onderdeel van'),
    'assignment_for' => __('Opdracht voor'),
    'ceromony_date' => __('Plechtigheidsdatum'),
    'theater' => __('Aula'),
    'start_time' => __('Aanvangstijd'),
    'duration' => __('Tijdsduur'),
    'no_of_people' => __('Aantal personen'),
    'att_info' => __('Aanv. info'),
    'kistreg_nr' => __('Kistreg.nr.'),
    'parish' => __('Parochie'),
    'evening_vigil' => __('Avondwake'),
    'funeral_chapel' => __('Gebruik rouwkapel'),
    'ceromony_date' => __('Plechtigheid datum'),
    'ceromony_time' => __('Plechtigheid tijd'),
    'monthly_thoughts' => __('Maandelijkse gedachten'),
    'cemetry' => __('Begraafplaats'),
    'grave_number' => __('Grafnummer'),
    'grafrechten' => __('Grafrechten'),
    'complement_to_grafrechten' => __('Grafrechten aanvullen tot'),
    'extends_to_ten_yrs' => __('Grafrechten verlengen 10 jaar'),
    'remove_monuments' => __('Monument verwijderen'),
    'grave_digging' => __('Graf delven/dichten'),
    'funeral_company_type' => __('Gegevens uitvaartonderneming'),
    'funeral_company' => __('Uitvaartonderneming'),
    'funeral_caregiver' => __('Uitvaartverzorger'),
    'relation' => __('Relatie tot ovl.'),
    'reception' => __('Ontvangst'),
    'goodbye_say' => __('Vóór de afscheidsdienst afscheid nemen?'),
    'accompanied_by' => __('Binnen begeleiden door'),
    'cross_post' => __('Kruis plaatsen?'),
    'predecessor' => __('Voorganger'),
    'last_to_leave' => __('Familie als laatste de aula te verlaten?'),
    'floral' => __('Bloemen'),
    'ramainder' => __('Overige'),
    'reserve_seats' => __('Plaatsen reserveren'),
    'reserve_spaces' => __('Parkeerplaatsen reserveren'),
    'music_name' => __('Muziek'),
    'image_recording' => __('Wenst u beeldopname/PowerPoint te presenteren?'),
    'cd_dvd_recording' => __('Wenst u een CD of DVD opname?'),
    'live_music' => __('Live muziek'),
    'others' => __('Overige'),
    'neutral_backgrund_music' => __('neutrale achtergrond muziek'),
    'no_of_people' => __('Aantal personen'),
    'family_tbl_no' => __('Familietafel aantal'),
    'moment_of_silence' => __('Moment stilte/gebed'),
    'reserved' => __('Gereserveerd'),
    'coffee_reserved' => __('Koffietafel Gereserveerd'),
    'assignment_faxed' => __('Opdracht gefaxt'),
    'deathdate' => __('Overlijdensdatum'),
    'deathplace' => __('Overlijdensplaats'),
    'pointoftime' => __('Tijdstip'),
    'name' => __('naam'),
    'Music' => __('Muziek'),
    'advertisement' => __('Advertentie'),
    'advertisement_price ' => __('Advertentie  Prijs'),
    'gratitude ' => __('Dankbetuiging'),
    'gratitude_price ' => __('Overlijdensdatum'),
    'casket_adornment' => __('Kistversiering'),
    'casket_adornment_price' => __('Kistversiering Prijs'),
    'miscellaneous ' => __('Diversen'),
    'miscellaneous_price ' => __('Diversen Prijs'),
    'booklet ' => __('Boekje Dag lieve'),
    'booklet_price ' => __('Boekje Dag lieve Prijs'),
    'guide_after_death' => __('Gids na overlijden'),
    'forwarded_data_by' => __('doorgezonden gegevens door'),
    'file_no' => __('Dossier nr.'),
    'family_compensation' => __('Familievergoeding'),
    'forwarded_by_status' => __('doorgezonden gegevens door'),
    'funeral_status' => __('doorgezonden gegevens door'),
    'reserve_seats_status' => __('reserve zetels statuut'),
    'reserve_space_status' => __('Parkeerplaatsen reserveren statuut'),
);

$notRequired = array('id', 'created', 'modified', 'ClientDatas', 'music_id', 'ceromony_data_id', 'chruch_data_id', 'cemetry_data_id', 'ClientData', 'CeromonyData ', 'ChruchData', 'ChruchData', 'CemetryData');
$yesNo = array('wedding_booklet_included', 'extract_death_certificate', 'funerals', 'forwarded_data_to', 'evening_vigil', 'funeral_chapel', 'monthly_thoughts', 'extends_to_ten_yrs', 'remove_monuments', 'grave_digging', 'assignment_faxed', 'goodbye_say', 'accompanied_by', 'predecessor', 'last_to_leave', 'image_recording', 'live_music', 'image_recording', 'live_music', 'moment_of_silence', 'reserved', 'cross_post',
    'neutral_backgrund_music'
);
$filters = array(' ', null);
?>	
<?php if (isset($Clientdatas) && !empty($Clientdatas)): ?>
    <h4 class="title-large font-light"><?php echo __('Opdrachtgever'); ?></h4>		
    <table class="table table-bordered table-striped table-condensed table-hover">
        <?php
        foreach ($Clientdatas as $key => $data):
            if ($key == 'status') {
                continue;
            }
            ?>
            <?php if (!in_array($key, $notRequired) && !is_array($data) && !empty($data)): ?>
                <tr>
                    <th><?php echo $map[$key]; ?></th>
                    <?php if (!in_array($key, $yesNo)) { ?>
                        <td><?php echo h($data); ?>&nbsp;</td>
                    <?php } else {
                        ?>
                        <td><?php echo $data ? __('Yes') : __('No'); ?>&nbsp;</td>

                    <?php } ?>     
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>	

    </table>

<?php endif; ?>

<?php if (isset($Deceasesdatas) && !empty($Deceasesdatas)): ?>		
    <h1 class="title-large font-light"><?php echo __('Voor de uitvaart van'); ?></h1>
    <table class="table table-bordered table-striped table-condensed table-hover">
        <?php
        foreach ($Deceasesdatas as $key => $data):
            if ($key == 'location') {
                continue;
            }
            ?>
            <?php if (!in_array($key, $notRequired) && !is_array($data) && !empty($data)): ?>
                <tr>
                    <th><?php echo $map[$key]; ?></th>
                    <?php if (!in_array($key, $yesNo)) { ?>
                        <td><?php echo h($data); ?>&nbsp;</td>
                    <?php } else {
                        ?>
                        <td><?php echo $data ? __('Yes') : __('No'); ?>&nbsp;</td>

                    <?php } ?>     
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>	

    </table>
    
    <?php if(isset($Deceasesdatas['location'])){ ?>
    <h1 class="title-large font-light"><?php echo __('Overleden te'); ?></h1>
    <table class="table table-bordered table-striped table-condensed table-hover">
        <?php 
        foreach ($Deceasesdatas as $key => $data):
            if ($key == 'genus' || $key == 'first_name') {
                continue;
            }
            ?>
            <?php if (!in_array($key, $notRequired) && !is_array($data) && !empty($data)): ?>
                <tr>
                    <th><?php echo $map[$key]; ?></th>
                    <?php if (!in_array($key, $yesNo)) { ?>
                        <td><?php echo h($data); ?>&nbsp;</td>
                    <?php } else {
                        ?>
                        <td><?php echo $data ? __('Yes') : __('No'); ?>&nbsp;</td>

                    <?php } ?>     
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>	

    </table>
    <?php } ?>

<?php endif; ?>	

<?php if (isset($CeromonyInfodatas) && !empty($CeromonyInfodatas)): ?>
    <h4 class="title-large font-light"><?php echo __('Gegevens plechtigheid'); ?></h4>
    <table class="table table-bordered table-striped table-condensed table-hover">
        <?php
        foreach ($CeromonyInfodatas as $key => $data):
            if ($key == 'status') {
                continue;
            }
            ?>
            <?php if (!in_array($key, $notRequired) && !is_array($data) && !empty($data)): ?>
                <tr>
                    <th><?php echo $map[$key]; ?></th>
                    <?php if (!in_array($key, $yesNo)) { ?>
                        <td><?php echo h($data); ?>&nbsp;</td>
                    <?php } else {
                        ?>
                        <td><?php echo $data ? __('Yes') : __('No'); ?>&nbsp;</td>

                    <?php } ?>     
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>	
        <?php
        if (isset($chruchDatas) && !empty($chruchDatas)):
            foreach ($ceromonyDatas as $key => $data):
                if ($key == 'status') {
                    continue;
                }
                ?>
                <?php if (!in_array($key, $notRequired)): ?>
                    <tr>
                        <th><?php echo $map[$key]; ?></th>
                        <?php if (!in_array($key, $yesNo)) { ?>
                            <td><?php echo h($data); ?>&nbsp;</td>
                        <?php } else {
                            ?>
                            <td><?php echo $data ? __('Yes') : __('No'); ?>&nbsp;</td>

                        <?php } ?>     
                    </tr>
                <?php endif; ?>
                <?php
            endforeach;
        endif;
        ?>	
        <?php
        if (isset($chruchDatas) && !empty($chruchDatas)):
            foreach ($chruchDatas as $key => $data):
                if ($key == 'status') {
                    continue;
                }
                ?>
                <?php if (!in_array($key, $notRequired)): ?>
                    <tr>
                        <th><?php echo $map[$key]; ?></th>
                        <?php if (!in_array($key, $yesNo)) { ?>
                            <td><?php echo h($data); ?>&nbsp;</td>
                        <?php } else {
                            ?>
                            <td><?php echo $data ? __('Yes') : __('No'); ?>&nbsp;</td>

                        <?php } ?>     
                    </tr>
                <?php endif; ?>
                <?php
            endforeach;
        endif;
        ?>	

        <?php
        if (isset($cemetryDatas) && !empty($cemetryDatas)):
            foreach ($cemetryDatas as $key => $data):
                if ($key == 'status') {
                    continue;
                }
                ?>
                <?php if (!in_array($key, $notRequired)): ?>
                    <tr>
                        <th><?php echo $map[$key]; ?></th>
                        <?php if (!in_array($key, $yesNo)) { ?>
                            <td><?php echo h($data); ?>&nbsp;</td>
                        <?php } else {
                            ?>
                            <td><?php echo $data ? __('Yes') : __('No'); ?>&nbsp;</td>

                        <?php } ?>     
                    </tr>
                <?php endif; ?>
                <?php
            endforeach;
        endif;
        ?>	
    </table>

<?php endif; ?>

<?php if (isset($Entrepreneurdatas) && !empty($Entrepreneurdatas)): ?>
    <h1 class="title-large font-light"><?php echo __('Gegevens uitvaartonderneming'); ?></h1>
    <table class="table table-bordered table-striped table-condensed table-hover">
        <?php
        foreach ($Entrepreneurdatas as $key => $data):
            if ($key == 'status') {
                continue;
            }
            ?>
            <?php if (!in_array($key, $notRequired) && !is_array($data) && !empty($data)): ?>
                <tr>
                    <th><?php echo $map[$key]; ?></th>
                    <?php if (!in_array($key, $yesNo)) { ?>
                        <td><?php echo h($data); ?>&nbsp;</td>
                    <?php } else {
                        ?>
                        <td><?php echo $data ? __('Yes') : __('No'); ?>&nbsp;</td>

                    <?php } ?>     
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>	
    </table>	

<?php endif; ?>

<?php if (isset($BillingAddressdatas) && !empty($BillingAddressdatas) && isset($BillingAddressdatas['name'])): ?>
    <h1 class="title-large font-light"><?php echo __('Factuuradres'); ?></h1>
    <table class="table table-bordered table-striped table-condensed table-hover">
        <?php
        foreach ($BillingAddressdatas as $key => $data):
            if ($key == 'status') {
                continue;
            }
            ?>
            <?php if (!in_array($key, $notRequired) && !is_array($data) && !empty($data)): ?>
                <tr>
                    <th><?php echo $map[$key]; ?></th>
                    <?php if (!in_array($key, $yesNo)) { ?>
                        <td><?php echo h($data); ?>&nbsp;</td>
                    <?php } else {
                        ?>
                        <td><?php echo $data ? __(__('Yes')) : __('No'); ?>&nbsp;</td>

                    <?php } ?>     
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>	
    </table>

<?php endif; ?>	

<?php if (isset($FarewellServicedatas) && !empty($FarewellServicedatas)): ?>
    <h1 class="title-large font-light"><?php echo __('Bijzonderheden afscheidsdienst'); ?></h1>
    <table class="table table-bordered table-striped table-condensed table-hover">
        <?php
        foreach ($FarewellServicedatas as $key => $data):
            if ($key == 'status') {
                continue;
            }
            ?>
            <?php if (!in_array($key, $notRequired) && !is_array($data) && !empty($data)): ?>
                <tr>
                    <th><?php echo $map[$key]; ?></th>
                    <?php if (!empty($data) || $data == 0): ?>
                        <?php if (!in_array($key, $yesNo)) { ?>
                            <td><?php echo h($data); ?>&nbsp;</td>
                        <?php } else {
                            ?>
                            <td><?php echo $data ? __(__('Yes')) : __('No'); ?>&nbsp;</td>

                        <?php } ?>   
                    <?php endif; ?>	      
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>	
    </table>

<?php endif; ?>	

<?php if (isset($PictureSounddatas) && !empty($PictureSounddatas)): ?>
    <h1 class="title-large font-light"><?php echo __('Wensen beeld en geluid'); ?></h1>
    <table class="table table-bordered table-striped table-condensed table-hover">
        <?php
        foreach ($PictureSounddatas as $key => $data):
            if ($key == 'status') {
                continue;
            }
            ?>
            <?php if (!in_array($key, $notRequired) && !is_array($data) && !empty($data)): ?>
                <tr>
                    <th><?php echo $map[$key]; ?></th>
                    <?php if (!empty($data) || $data == 0): ?>
                        <?php if (!in_array($key, $yesNo)) { ?>
                            <td><?php echo h($data); ?>&nbsp;</td>
                        <?php } else {
                            ?>
                            <td><?php echo $data ? __('Yes') : __('No'); ?>&nbsp;</td>

                        <?php } ?>   
                    <?php endif; ?>	      
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>	
    </table>

<?php endif; ?>	
<?php if (!empty($musics) && is_array($musics)): //debug($musics);die;?>	
    <table class="table table-bordered table-striped table-condensed table-hover">
        <tr>
            <th>Muziek</th>
            <td>
                <table>
                    <?php foreach ($musics as $music): ?>
                        <tr><td><?php echo $music; ?></td></tr>
                    <?php endforeach; ?>
                </table>
            </td>
        </tr>
    </table>	

<?php endif; ?>	

<?php if (isset($CoffeeRoomdatas) && !empty($CoffeeRoomdatas)): ?>
    <h1 class="title-large font-light"><?php echo __('Wensen condoleanceruimte / koffiekamer'); ?></h1>
    <table class="table table-bordered table-striped table-condensed table-hover">
        <?php
        foreach ($CoffeeRoomdatas as $key => $data):
            if ($key == 'status') {
                continue;
            }
            ?>
            <?php if (!in_array($key, $notRequired) && !is_array($data) && !empty($data)): ?>
                <tr>
                    <th><?php echo $map[$key]; ?></th>
                    <?php if (!empty($data) || $data == 0): ?>
                        <?php if (!in_array($key, $yesNo)) { ?>
                            <td><?php echo h($data); ?>&nbsp;</td>
                        <?php } else {
                            ?>
                            <td><?php echo $data ? __('Yes') : __('No'); ?>&nbsp;</td>

                        <?php } ?>   
                    <?php endif; ?>	      
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>	
    </table>

<?php endif; ?>	


<!-- tab 9 -->
<?php
$Miscellaneousdatas_map = array(
    'conffin_name' => __('Naam'),
    'price' => __('Prijs'),
    'letter_name' => __('Rouwbrieven Naam'),
    'card_type' => __('Kaart'),
    'map_type' => __('Kaart'),
    'number' => __('Aantal'),
    'remarks' => __('Bijzonderheden'),
    'name' => __('Naam'),
    'name_with_price' => __('Prijs met naam'),
    'name_without_price' => __('Prijs zonder naam'),
    'price_per_piece' => __('Prijs per stuk'),
    'days' => __('Aantal staandagen'),
    'per_price' => __('Prijs per stuk'),
    'total_price' => __('totale prijs'),
    'funerals_car_from' => __('Rouw auto van'),
    'funerals_car_to' => __('Rouw auto van Naar'),
    'price_netherland' => __('Prijs Nederland'),
    'advertisement_price' => __('Advertentie Prijs'),
    'gratitude' => __('Dankbetuiging'),
    'gratitude_price' => __('Dankbetuiging Prijs'),
    'miscellaneous_price' => __('Diversen Prijs'),
    'booklet' => __('Boekje Dag lieve'),
    'booklet_price' => __('Boekje Dag lieve Prijs'),
    'guide_after_death_price' => __('Gids na overlijden Prijs'),
    'cremation_price' => __('crematie Prijs'),
    'funeral_price' => __('uitvaart Prijs'),
    'wage_price' => __('Verzorging uitvaart en ondernemersloon Prijs'),
    'advertisement' => __('Advertentie'),
    'casket_adornment' => __('Kistversiering'),
    'casket_adornment_price' => __('Kistversiering Prijs'),
    'miscellaneous' => __('Diversen'),
    'guide_after_death' => __('Gids na overlijden'),
    'price_per_piecename' => __('Misboekjes naam'),
    'price_per_pieceprice' => __('Prijs per stuk'),
    'price_per_piecenumber' => __('Misboekjes Aantal'),
    'some_days' => __('Aantal staandagen'),
    'prayer_category_type' => __('Gedachtenisprentjes categorie'),
    'type' => __('Bloemist'),
    'price_per_piecename' => __('Prijs per stuk'),
    'price_per_piecenumber' => __('Aantal'),
    'some_days' => __('Aantal staandagen'),
);
$notRequired = array(
    'coffins_data_id',
    'funeral_letters_data_id',
    'misboekjes_data_id',
    'stamp_id',
    'commemoration_id ',
    'homeobaring_id ',
    'hearse_id ',
    'carriers_data_id',
    'id',
    'prayer_cards_data_id',
    'coffee_ticket_id',
    'hospitals_data_id',
    'rouwcentrums_data_id',
    'care_decease_id',
    'arrangement_id',
    'florist_id',
    'created',
    'modified',
    'commemoration_id',
    'homeobaring_id',
    'hearse_id',
    'arrangement_data_id',
    'care_deceases_data_id',
    'miscellaneouse_id'
);
$price_key = array(
    'price',
    'funeral_price',
    'wage_price',
    'cremation_price',
    'booklet_price',
    'miscellaneous_price',
    'price_netherland',
    'total_price',
    'per_price',
    'price_per_piece',
    'name_without_price',
    'name_with_price',
    'guide_after_death_price',
    'casket_adornment_price',
    'advertisement_price',
    'gratitude_price',
    'price_per_piecename'
);
?>	

<?php if(!empty($coffinsData) || !empty($funeralLettersData) || !empty($PrayerCardsDatas) || !empty($misboekjesData) || !empty($coffeeTicket) || !empty($stamp) || !empty($commemoration) || !empty($hospitalsData) || !empty($rouwcentrum) || !empty($careDecease) || !empty($homeobaring) || !empty($hearse) || !empty($carriersData) || !empty($arrangements)  || !empty($Miscellaneousdatas)) :?>
<table class="table table-bordered table-striped table-condensed table-hover">	
    <tr>
        <th><?php echo __('Waarbij geleverd en verricht'); ?></th>
        <th><?php echo __('Aantal'); ?></th>
        <th><?php echo __('Prijs'); ?></th>
    </tr>
    
    <?php if (isset($coffinsData) && !empty($coffinsData) && isset($coffinsData['conffin_name'])): ?>
    <tr>
        <td><?php echo $coffinsData['conffin_name'];?></td>
        <td></td>
        <td><?php echo $this->App->formatPriceValue($coffinsData['price']);?></td>
    </tr>	 
    <?php endif; ?>
    
    <?php if (isset($funeralLettersData) && !empty($funeralLettersData) && isset($funeralLettersData['letter_name'])): ?>
    <tr>
        <td><?php echo $funeralLettersData['letter_name'];?></td>
        <td><?php echo $funeralLettersData['number'];?></td>
        <td><?php echo $this->App->formatPriceValue($funeralLettersData['price']);?></td>
    </tr>	 
    <?php endif; ?>

    <?php if (isset($PrayerCardsDatas) && !empty($PrayerCardsDatas) && isset($PrayerCardsDatas['prayer_category_type']) && isset($PrayerCardsDatas['card_type'])): ?>
        <tr>
            <td><?php echo $PrayerCardsDatas['prayer_category_type'].', '.$PrayerCardsDatas['card_type'];;?></td>
            <td><?php echo $PrayerCardsDatas['number'];?></td>
            <td><?php echo $this->App->formatPriceValue($PrayerCardsDatas['price']);?></td>
        </tr>	 
    <?php endif; ?>

    <?php if (isset($misboekjesData) && !empty($misboekjesData) && isset($misboekjesData['price_per_piecename'])): ?>        
        <tr>
            <td><?php echo $misboekjesData['price_per_piecename'];?></td>
            <td><?php echo $misboekjesData['price_per_piecenumber'];?></td>
            <td><?php echo $this->App->formatPriceValue($misboekjesData['total_price']);?></td>
        </tr>	 
    <?php endif; ?>
        
    <?php if (isset($coffeeTicket) && !empty($coffeeTicket) && isset($coffeeTicket['name'])): ?>
        <tr>
            <td><?php echo $coffeeTicket['name'];?></td>
            <td><?php echo $coffeeTicket['number'];?></td>
            <td><?php echo $this->App->formatPriceValue($coffeeTicket['price']);?></td>
        </tr>	
    <?php endif; ?>

    <?php if (isset($stamp) && !empty($stamp) && isset($stamp['price_netherland'])): ?>
        <tr>
            <td><?php echo __('Postzegels');?></td>
            <td><?php echo $stamp['number'];?></td>
            <td><?php echo $this->App->formatPriceValue($stamp['price']);?></td>
        </tr>	
    <?php endif; ?>

    <?php if (isset($commemoration) && !empty($commemoration) && isset($commemoration['price_per_piece'])): ?>
        <tr>
            <td><?php echo __('Condoleanceregister');?></td>
            <td><?php echo $commemoration['number'];?></td>
            <td><?php echo $this->App->formatPriceValue($commemoration['price']);?></td>
        </tr>	
    <?php endif; ?>
        
    <?php if (isset($hospitalsData) && !empty($hospitalsData) && isset($hospitalsData['hospital_name'])): ?>
        <tr>
            <td><?php echo __('Mortuarium ziekenhuis / verpleeghuis').', '.$hospitalsData['hospital_name'];?></td>
            <td></td>
            <td><?php echo $this->App->formatPriceValue($hospitalsData['price']);?></td>
        </tr>	
    <?php endif; ?>

    <?php if (isset($rouwcentrum) && !empty($rouwcentrum) && isset($rouwcentrum['name'])): ?>
        <tr>
            <td><?php echo __('Rouwcentrum').', '.$rouwcentrum['name'];?></td>
            <td><?php echo $rouwcentrum['some_days'].' '; echo ($rouwcentrum['some_days'] >1)?__('dagen'):__('dag');?></td>
            <td><?php echo $this->App->formatPriceValue($rouwcentrum['price']);?></td>
        </tr>	
    <?php endif; ?>

    <?php if (isset($careDecease) && !empty($careDecease) && isset($careDecease['name'])): ?>
        <tr>
            <td><?php echo $careDecease['name'];?></td>
            <td></td>
            <td><?php echo $this->App->formatPriceValue($careDecease['price']);?></td>
        </tr>
    <?php endif; ?>
        
    <?php if (isset($homeobaring) && !empty($homeobaring) && isset($homeobaring['name'])): ?>
        <tr>
            <td><?php echo __('Thuisopbaring').', '.$homeobaring['name'];?></td>
            <td><?php echo $homeobaring['days'].' '; echo ($homeobaring['days'] >1)?__('dagen'):__('dag');?></td>
            <td><?php echo $this->App->formatPriceValue($homeobaring['per_price']);?></td>
        </tr>
    <?php endif; ?>

    <?php if (isset($hearse) && !empty($hearse) && isset($hearse['price'])): ?>
        <tr>
            <td>
                <?php echo __('Rouwauto');?><br/>
                    <?php if(isset($hearse['funerals_car_from'])){ ?>
                <?php echo $Miscellaneousdatas_map['funerals_car_from'].' : '.$hearse['funerals_car_from'];?><br/>
                <?php } ?>
                
                <?php if(isset($hearse['funerals_car_to'])) { ?>
                    <?php echo $Miscellaneousdatas_map['funerals_car_to'].' : '.$hearse['funerals_car_to'];?>
                                
                <?php } ?>
            </td>
            <td></td>
            <td><?php echo $this->App->formatPriceValue($hearse['price']);?></td>
        </tr>
    <?php endif; ?>
        
    <?php if (isset($carriersData) && !empty($carriersData) && isset($carriersData['name'])): ?>
        <tr>            
            <td><?php echo $carriersData['name'];?></td>
            <td><?php echo (!isset($carriersData['number']))?$carriersData['number']:0;?></td>
            <td><?php echo $this->App->formatPriceValue($carriersData['price']);?></td>
        </tr>
    <?php endif; ?>
        
    <?php if (isset($arrangements) && !empty($arrangements)): ?>
        <?php foreach ($arrangements as $arrangement): ?>
            <?php if(!empty($arrangement['type']) || $arrangement['type'] != 0){ ?>
                <tr>            
                    <td><?php echo __('Bloemstuk').', '.$arrangement['type'];?></td>
                    <td></td>
                    <?php if(!isset($without_price)){ ?>
                        <td><?php echo $this->App->formatPriceValue($arrangement['price']);?></td>
                    <?php } ?>
                </tr>
            <?php } elseif(empty($arrangement['type']) && $arrangement['price'] != 0) { ?>
                
                <tr>            
                    <td><?php echo __('Bloemstuk');?></td>
                    <td></td>
                    <?php if(!isset($without_price)){ ?>
                        <td><?php echo $this->App->formatPriceValue($arrangement['price']);?></td>
                    <?php } ?>
                </tr>
                
            <?php }?>
        <?php endforeach; ?>		 
    <?php endif; ?>

        
        
    <?php     
    $miscellaneous_data = array(
        'advertisement',
        'gratitude',
        'casket_adornment',
        'miscellaneous',
        'booklet',
        'guide_after_death'
    );
    $miscellaneous_data_price = array(  
        'cremation_price',
        'funeral_price',
        'wage_price'
    );
    ?>        
    <?php if (isset($Miscellaneousdatas) && !empty($Miscellaneousdatas)): ?>
        <?php foreach ($miscellaneous_data as $data): ?>
            <?php if (isset($Miscellaneousdatas[$data]) && !empty($Miscellaneousdatas[$data])): ?>
                <tr>
                    <td>
                        <?php echo $Miscellaneousdatas_map[$data].', '.$Miscellaneousdatas[$data];?>
                    </td>
                    <td></td>
                    <td><?php echo $this->App->formatPriceValue($Miscellaneousdatas[$data.'_price']);?></td>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>	
                
                
        <?php foreach ($miscellaneous_data_price as $data): ?>
            <?php if (isset($Miscellaneousdatas[$data]) && !empty($Miscellaneousdatas[$data])): ?>
                <tr>
                    <td><?php echo $Miscellaneousdatas_map[$data];?></td>
                    <td></td>
                    <td><?php echo $this->App->formatPriceValue($Miscellaneousdatas[$data]);?></td>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>	
                
    <?php endif; ?>	   
                
                
    <?php if (isset($Miscellaneousdatas['total_price']) && !empty($Miscellaneousdatas['total_price'])): ?>    
        <?php echo '<tr><td>'.__('Door u te betalen').'</td><td></td><td>'.$this->App->formatPriceValue($Miscellaneousdatas['total_price']).'</td></tr>';?>
    <?php endif; ?>
        
     
</table>
<?php endif; ?> 
<?php ob_end_flush(); ?>
