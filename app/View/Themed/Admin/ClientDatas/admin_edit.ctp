<!-- Tab panes -->
	<div class="tab-content">
		<div class="tab-pane active" id="tab1">
			<h1 class="title-large font-light"><?php echo __('Gegevens Opdrachtgever'); ?></h1>
			<?php echo $this->Form->create('ClientData'); ?>
	 			<div class="form-group">
					<label class="control-label col-sm-2">Geslachtsnaam</label>
					<div class="col-sm-4">
							<?php echo $this->Form->input('Geslachtsnaam','',array('class'=>'form-control')); ?>
					</div>
					<label class="control-label col-sm-2">Voorvoegsels</label>
					<div class="col-sm-4">
						<?php echo $this->Form->input('Voorvoegsels',array('class'=>'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2">Voornamen</label>
					<div class="col-sm-4">
						<?php echo $this->Form->input('Voornamen',array('class'=>'form-control')); ?>
					</div>
					<label class="control-label col-sm-2">Roepnaam</label>
					<div class="col-sm-4">
						<?php echo $this->Form->input('Roepnaam',array('class'=>'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2">Partnernaam</label>
					<div class="col-sm-4">
						<?php echo $this->Form->input('Partnernaam',array('class'=>'form-control')); ?>
					</div>
					<label class="control-label col-sm-2">Voorvoegsels</label>
					<div class="col-sm-4">
						<?php echo $this->Form->input('Naamgebruik',array('class'=>'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2">Naamgebruik</label>
					<div class="col-sm-4">
						<?php echo $this->Form->input('Naamgebruik'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2">Geslacht</label>
					<div class="col-sm-4">
						<?php echo $this->Form->input('Geslacht',array(
                                         'options'=>array('Man','Vrouw'),
                                   	 ));
						?>
					</div>
					<label class="control-label col-sm-2">BSN</label>
					<div class="col-sm-4">
						<?php echo $this->Form->input('BSN'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2">Geboortedatum</label>
					<div class="col-sm-5">
						<div class="input-group"  data-date="12-02-2012" data-date-format="dd-mm-yyyy">
							<input class="form-control  datepicker-control" type="text">
							<?php echo $this->Form->input('birthdate'); ?>
							<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
						</div>
					</div>
					<label class="control-label col-sm-2">Geboorteplaats</label>
					<div class="col-sm-3">
						<?php echo $this->Form->input('Geboorteplaats'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2">Postcode</label>
					<div class="col-sm-5">
						<div class="input-group" >
							<?php echo $this->Form->input('Postcode'); ?>
							<div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
						</div>
					</div>
					<label class="control-label col-sm-2">Nummer</label>
					<div class="col-sm-1">
						<?php echo $this->Form->input('Nummer'); ?>
					</div>
					<label class="control-label col-sm-1">Letter</label>
					<div class="col-sm-1">
						<?php echo $this->Form->input('Letter'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2">Straat</label>
					<div class="col-sm-5">
						<?php echo $this->Form->input('street'); ?>
					</div>
					<label class="control-label col-sm-2">Plaats (/land)</label>
					<div class="col-sm-3">
						<?php echo $this->Form->input('location'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2">Telefoon</label>
					<div class="col-sm-2">
						<?php echo $this->Form->input('phone'); ?>
					</div>
					<label class="control-label col-sm-1">Mobiel</label>
					<div class="col-sm-2">
						<?php echo $this->Form->input('mobile'); ?>
					</div>
					<label class="control-label col-sm-2">E-mail adres</label>
					<div class="col-sm-3">
						<?php echo $this->Form->input('email'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2">Relatie tot overledene</label>
					<div class="col-sm-5">
						<?php echo $this->Form->input('relationship_to_deceased'); ?>
				</div>
				<div class="actions">
					<div class="pull-right">
						<button type="submit" class="btn btn-primary">Opslaan</button>
					</div>
				</div>
		
<?php echo $this->Form->end(__('Submit')); ?>
</div>

