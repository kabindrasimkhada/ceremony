<div class="col-md-12 ">
    <div class="box box-primary users">
        <div class="box-header">
            <h3 class="box-title"><?php echo __('uitvaartverzorger'); ?></h3>
        </div><!-- /.box-header -->
        <?php echo $this->element('validate_message') ?>
        <!-- form start -->
        <?php echo $this->Form->create('Undertaker', array('inputDefaults' => array(
                    'label' => false,
                     'div' => false
                     )
                  )
             );
        ?>
        <div class="box-body">
            <?php echo $this->Form->input('id'); ?>
            <div class="form-group">
                  <label for="name"><?php echo __('naam') ?></label>
                <?php
                echo $this->Form->input('name', array(
                    'placeholder' => __('undertaker name'),
                    'class' => 'form-control'
                ));
                ?>
            </div><!-- /.box-body -->
           <div class="box-footer">
            <input type="submit" value=<?php echo __('Opslaan')?> class="btn btn-primary">
            <?php echo $this->Html->link(__('Cancel'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div><!-- /.box -->


