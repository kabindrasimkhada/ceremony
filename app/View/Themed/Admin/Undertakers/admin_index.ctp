<div class="row Church">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <i class="glyphicon glyphicon-list-alt"></i>
                <h3 class="box-title"><?php echo __('uitvaartverzorger'); ?></h3>                                    
            </div><!-- /.box-header -->
            <!-- Flash Msg -->
            <?php echo $this->Session->flash(); ?>           
            <?php //echo $this->Session->flash('auth'); ?>            <!--/Flash Msg -->

            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                            <th><?php echo $this->Paginator->sort('name', __('name')); ?></th>
                            <th><?php echo $this->Paginator->sort('created',__('aangemaakt')); ?></th>
                            <th class="actions"><?php echo __('acties'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($undertakers as $undertaker): ?>
                            <tr>
                                <td><?php echo h($undertaker['Undertaker']['id']); ?>&nbsp;</td>
                                <td><?php echo h($undertaker['Undertaker']['name']); ?>&nbsp;</td>
                                <td><?php echo h($undertaker['Undertaker']['created']); ?>&nbsp;</td>
                                <td class="actions">
                                <?php echo $this->Html->link('<span class="fa fa-edit fa-fw"></span> '.__('Bewerken'), array('action' => 'edit', $undertaker['Undertaker']['id']),array( 'class' => 'btn btn-sm btn-default', 'escape' => false)
                    ); ?>
                                <?php echo $this->Form->postLink('<span class="fa fa-trash-o fa-fw"></span> '.__('verwijderen'), array('action' => 'delete', $undertaker['Undertaker']['id']),array( 'class' => 'btn btn-sm btn-default', 'escape' => false)
                    , __('Are you sure you want to delete # %s?', $undertaker['Undertaker']['id'])); ?>
                            </td>
                            </tr>
                        <?php endforeach; ?>
                    <tfoot>
                         <tr>
                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                            <th><?php echo $this->Paginator->sort('name', __('name')); ?></th>
                            <th><?php echo $this->Paginator->sort('created',__('aangemaakt')); ?></th>
                            <th class="actions"><?php echo __('acties'); ?></th>
                        </tr>
                    </tfoot>
                </table>
                 <?php
                    echo $this->Html->link(__('toevoegen'), 
                                                array('action' => 'add'),
                                                array('class'=>'btn btn-primary addbtn'));
                 ?>
                <?php echo $this->element('pagination'); ?>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<!--End Church div-->
