<?php echo $this->element('navigation', array('items' => 'tab2')); ?>	
<div class="tab-pane" id="tab2">
		<h1 class="title-large font-light"><?php echo __('Persoonsgegevens overledene'); ?></h1>
          <?php echo $this->Form->create('Decease',array(
          											'class'=>'form-horizontal'
          											)
          								);
           ?>


              <div class="form-group">
				<label class="control-label col-sm-2"><?php echo __('Geslachtsnaam'); ?></label>
				<div class="col-sm-4">
					<?php echo $this->Form->input('genus',array(
																'class'=>'form-control',
																'div'=>false,
																'label'=>false,
																'required' => false,
																)
															);
				    ?>
				</div>
				<label class="control-label col-sm-2"><?php echo __('Voorvoegsels'); ?></label>
				<div class="col-sm-4">
					<?php echo $this->Form->input('genus_prefix',array(
																'class'=>'form-control',
																'div'=>false,
																'label'=>false,
																'required' => false,
																)
															);
				    ?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2"><?php echo __('Voornamen'); ?></label>
			  <div class="col-sm-4">
				<?php echo $this->Form->input('first_name',array(
                                                                'class'=>'form-control',
                                                                'div'=>false,
                                                                'label'=>false,
                                                                'required' => false,
                                                                )
                                                        );
				?>
			  </div>	
			</div>	
			<div class="form-group">
				<label class="control-label col-sm-2"><?php echo __('Roepnaam'); ?></label>
				<div class="col-sm-4">
					<?php echo $this->Form->input('nickname',array(
                                'class'=>'form-control',
                                'div'=>false,
                                'label'=>false,
                                'required' => false,
                                )
                        );
				    ?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2"><?php echo __('Partnernaam'); ?></label>
				<div class="col-sm-4">
					<?php echo $this->Form->input('partner_name',array(
			                    'class'=>'form-control',
			                    'div'=>false,
			                    'label'=>false,
			                    'required' => false,
			                    )
			            );
				    ?>
				</div>
				<label class="control-label col-sm-2"><?php echo __('Voorvoegsels'); ?></label>
				<div class="col-sm-4">
					<?php echo $this->Form->input('partner_prefix',array(
	                            'class'=>'form-control',
	                            'div'=>false,
	                            'label'=>false,
	                            'required' => false,
	                            )
	                    );
				    ?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2"><?php echo __('Naamgebruik'); ?></label>
				<div class="col-sm-4">
<?php echo $this->Form->input('use_name',array(
                                'class'=>'form-control',
                                'div'=>false,
                                'label'=>false,
                                'required' => false,
                                )
                        );
				    ?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2"><?php echo __('Geslacht'); ?></label>
				<div class="col-sm-4">
					<?php echo $this->Form->input('sex',
							array(
                                 	'options'=>array(
	                                'Man' => 'Man',
	                                'Vrouw' => 'Vrouw',
                                ),
                             'class'=>'form-control',
                             'div'=>false,
                             'label'=>false,
                             'required' => false,
                             'empty'=>'Selecteer',
                             
                       	 ));
					?>
				</div>
				<label class="control-label col-sm-2"><?php echo __('BSN'); ?></label>
				<div class="col-sm-4">
						<?php echo $this->Form->input('BSN',array(
		                            'class'=>'form-control',
		                            'div'=>false,
		                            'label'=>false,
		                            'required' => false,
		                            )
		                    );
					    ?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2"><?php echo __('Geboortedatum'); ?></label>
				<div class="col-sm-4">
					<div class="input-group"  data-date="12-02-2012" data-date-format="dd-mm-yyyy">
						<?php echo $this->Form->input('birthdate',array(
                                    'class'=>'form-control datepicker-control',
                                    'div'=>false,
                                    'label'=>false,
                                    'required' => false,
                                    'type'=>'text'
                                    )
                            );
						?>
						<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
					</div>
				</div>
				<label class="control-label col-sm-2"><?php echo __('Overlijdensdatum'); ?></label>
				<div class="col-sm-4">
					<div class="input-group" data-date="12-02-2012" data-date-format="dd-mm-yyyy">
						<?php echo $this->Form->input('deathdate',
												array(
				                                    'class'=>'form-control',
				                                    'div'=>false,
				                                    'label'=>false,
				                                    'required' => false,
				                                    'type'=>'text'
				                                    )
				                    ); 
						?>
						<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
					</div>
				</div>
			</div>	
			<div class="form-group">
				<label class="control-label col-sm-2"><?php echo __('Tijdstip'); ?></label>
				<div class="col-sm-4">
					<?php echo $this->Form->input('pointoftime',array(
																'class'=>'form-control',
																'div'=>false,
																'label'=>false,
																'required' => false,
																'type'=>'text'
																)
														    );
					 ?>
				
				</div>
				<label class="control-label col-sm-2"><?php echo __('Geboorteplaats'); ?></label>
				<div class="col-sm-4">
						<?php echo $this->Form->input('birthplace',array(
																	'class'=>'form-control',
																	'div'=>false,
																	'label'=>false,
																	'required' => false,
																	)
																);
					    ?>
				</div>
		    </div>	
			<div class="form-group">
				
				<!-- <label class="control-label col-sm-2"><?php echo __('Overlijdensplaats'); ?></label>
				<div class="col-sm-2">
					<?php echo $this->Form->input('deathplace',array(
																'class'=>'form-control',
																'div'=>false,
																'label'=>false,
																'required' => false,
																)
															);
				    ?>
				</div> -->
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2"><?php echo __('Postcode'); ?></label>
				<div class="col-sm-4">
					<div class="input-group" >
						<?php echo $this->Form->input('zip_code',array(
																	'class'=>'form-control postcode',
																	'div'=>false,
																	'label'=>false,
																	'required' => false,
																	)
																);
					    ?>
						<div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
					</div>
				</div>
				<label class="control-label col-sm-1"><?php echo __('Nummer'); ?></label>
				<div class="col-sm-1">
					<?php echo $this->Form->input('number',array(
																'class'=>'form-control',
																'div'=>false,
																'label'=>false,
																'required' => false,
																 'type' => 'text'
																)
															);
				    ?>
				</div>
				<label class="control-label col-sm-1"><?php echo __('Letter'); ?></label>
				<div class="col-sm-1">
				<?php echo $this->Form->input('letter',array(
															'class'=>'form-control',
															'div'=>false,
															'label'=>false,
															'required' => false,
															)
														);
			    ?>
				</div>
				<!-- <label class="control-label col-sm-1"><?php echo __('Toev.'); ?></label>
				<div class="col-sm-1">
					<?php echo $this->Form->input('add',array(
																'class'=>'form-control',
																'div'=>false,
																'label'=>false,
																'required' => false,
																)
															);
				    ?>
				</div> -->
				<label class="control-label col-sm-1"><?php echo __('Aand.'); ?></label>
				<div class="col-sm-1">
					<?php echo $this->Form->input('tightening',array(
																'class'=>'form-control',
																'div'=>false,
																'label'=>false,
																'required' => false,
																)
															);
				    ?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2"><?php echo __('Straat'); ?></label>
				<div class="col-sm-4">
					<?php echo $this->Form->input('street',array(
																'class'=>'form-control street',
																'div'=>false,
																'label'=>false,
																'required' => false,
																)
															);
				    ?>
				</div>
				<label class="control-label col-sm-2"><?php echo __('Plaats (/land)'); ?></label>
				<div class="col-sm-4">
					<?php echo $this->Form->input('location',array(
																'class'=>'form-control place',
																'div'=>false,
																'label'=>false,
																'required' => false,
																)
															);
				    ?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2"><?php echo __('Levensovertuiging'); ?></label>
				<div class="col-sm-4">
					<?php echo $this->Form->input('belief',array(
																'class'=>'form-control',
																'div'=>false,
																'label'=>false,
																'required' => false,
																)
															);
				    ?>
				</div>
				<label class="control-label col-sm-2"><?php echo __('Leeftijd'); ?></label>
				<div class="col-sm-4">
					<?php echo $this->Form->input('age',array(
																'class'=>'form-control',
																'div'=>false,
																'label'=>false,
																'required' => false,
																 'type' => 'text'
																)
															);
				    ?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2"><?php echo __('Burgerlijkestaat'); ?></label>
				<div class="col-sm-4">
				 <?php echo $this->Form->input('martial_status',
								array(
									 'options'=>
									 	array(
									 		'Gehuwd' => 'Gehuwd',
						 					'Ongehuwd' => 'Ongehuwd',
			                 				'Geregistreerd Partner'=>'Geregistreerd Partner'
					                 		),
					                 'class'=>'form-control',
					                 'div'=>false,
					                 'label'=>false,
					                 'required' => false,
					                 'empty'=>'Selecteer',
		                            )	
								);
				?>
					
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2"><?php echo __('Naam vader'); ?></label>
				<div class="col-sm-4">
					<?php echo $this->Form->input('father_name',array(
																'class'=>'form-control',
																'div'=>false,
																'label'=>false,
																'required' => false,
																)
															);
				    ?>
				</div>
				<label class="control-label col-sm-2"><?php echo __('Naam moeder'); ?></label>
				<div class="col-sm-4">
					<?php echo $this->Form->input('mother_name',array(
																'class'=>'form-control',
																'div'=>false,
																'label'=>false,
																'required' => false,
																)
															);
				    ?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2"><?php echo __('Trouwboekje meegenomen'); ?></label>
				<div class="col-sm-4">
				<!--<?php echo $this->Form->input('wedding_booklet_included',array(
                                     'options'=>array('Nee','Ja',
                                     ),
                                     'class'=>'form-control',
                                     'div'=>false,
                                     'label'=>false,
                                     'required' => false,
                                     'empty'=>'Selecteer',
                                     'default'=>'',
                               	 )
							);
					?> -->
					<?php echo $this->Form->input('wedding_booklet_included',
									array(  
											'options'=>array('Nee','Ja'),
											'before'=>'<label class="radio-inline">',
											'type' => 'radio',
											'class' => 'radio-inline',
											'legend' => false,
											'div' => false,
											'fieldset' => false,
											'separator'=>'</label><label class="radio-inline">',
											'after' => '</label>',
											//'class' => 'radioGroup8',
											'required' => false,
											'default'=>'',
										)
									 );
		 			?>		
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2"><?php echo __('Uitreksel overlijdensakte'); ?></label>
				<div class="col-sm-4">
					<?php echo $this->Form->input('extract_death_certificate',
										array(  
												'options'=>array('Nee','Ja (+ €12,50)'),
												'before'=>'<label class="radio-inline">',
												'type' => 'radio',
												'class' => 'radio-inline',
												'legend' => false,
												'div' => false,
												'fieldset' => false,
												'separator'=>'</label><label class="radio-inline">',
												'after' => '</label>',
												//'class' => 'radioGroup8',
												'required' => false,
												'default'=>'',
											)
										 );
			 		?>		
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2"><?php echo __('Aangifte gedaan door (Naam aangever)'); ?></label>
				<div class="col-sm-4">
					<?php echo $this->Form->input('name_of_declarant',array(
																			 'class'=>'form-control',
																			 'div'=>false,
																			 'label'=>false,
																			 'required' => false,
																		    )
																		); 
				    ?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2"><?php echo __('Uitvaartverzekering'); ?></label>
				<div class="col-sm-2">
				  
					<?php echo $this->Form->input('funeral_status',
						array(  
								'options'=>array('Nee','Ja'),
								'before'=>'<label class="radio-inline">',
								'type' => 'radio',
								'class' => 'radio-inline',
								'legend' => false,
								'div' => false,
								'fieldset' => false,
								'separator'=>'</label><label class="radio-inline">',
								'after' => '</label>',
								'class' => 'radioGroup1',
								'required' => false,
								'default'=>'',
							)
						 );
			 		?>

					<!-- <label class="radio-inline">
						<input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
						Nee </label>
					<label class="radio-inline">
						<input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option1">
						Ja </label> -->
				</div>
				<div class="col-sm-6 dataradioGroup1">
					<?php echo $this->Form->input('funerals',array(
																'class'=>'form-control',
																'div'=>false,
																'label'=>false,
																'required' => false,
																)
															);
				    ?>
				</div>
			</div>
			<div class="form-group ">
				<label class="control-label col-sm-2"><?php echo __('Gegevens doorsturen naar'); ?></label>
				<div class="col-sm-2 radio-inside">
					 <?php echo $this->Form->input('forwarded_by_status',
						array(
								'options'=>array('Gemeenten','Parochies'),
								'before'=>'<label class="radio-inline">',
								'type' => 'radio',
								'class' => 'radio-inline',
								'legend' => false,
								'div' => false,
								'fieldset' => false,
								 'separator'=>'</label><label class="radio-inline">',
							 	//'separator'=>'</label><label>',
							 	'after' => '</label>',
								'class' => 'group2',
								'required' => false,
								/*'default'=>'',*/
							)
						 );
			 		?>
			</div>
				<div class="data1">
					<div class="col-sm-3 form-select">
						<?php echo $this->Form->input('forwarded_data_to',array(
                                         				 'options'=>$forward_to_data,
                                        				 'class'=>'form-control',
                                        				 'div'=>false,
                                        				 'label'=>false,
                                        				 'required' => false,
                                        				 'empty'=>'Selecteer',
                                     					'default'=>'0',
                                   				 )
						    				  );
						?>
					  <?php echo $this->Form->input('forwarded_data_to',array(
                                     					 'options'=>$forward_to_data,
                                        				 'class'=>'form-control',
                                        				 'div'=>false,
                                        				 'label'=>false,
                                        				 'required' => false,
                                        				 'empty'=>'Selecteer',
                                     					 'default'=>'0',
                               	 )
					        );
					?>
					</div>
					<div class="col-sm-3 form-select">
						<?php echo $this->Form->input('forwarded_data_by',array(
                                        		 'options'=>$forward_by_data,
		                                         'class'=>'form-control',
		                                         'div'=>false,
		                                         'label'=>false,
		                                         'required' => false,
		                                         'empty'=>'Selecteer',
                                     			 'default'=>'0',
                               	 )
						       );
						?>

					<?php echo $this->Form->input('forwarded_data_by',array(
                                    			 'options'=>$forward_by_data,
		                                         'class'=>'form-control',
		                                         'div'=>false,
		                                         'label'=>false,
		                                         'required' => false,
		                                         'empty'=>'Selecteer',
                                     			'default'=>'',
                               	 ));
					?>
						
					</div>
				
			
				<!-- <div class="col-sm-2 col-sm-offset-2">
					 <?php echo $this->Form->input('forwarded_by_status',
						array('options'=>array('Parochies'),
								'before'=>'<label class="radio-inline">',
								'type' => 'radio',
								'class' => 'radio-inline',
								'legend' => false,
								'div' => false,
								'fieldset' => false,
								// 'separator'=>'</label><label class="radio-inline">',
								'after' => '</label>',
								'name' => 'group2',
								'class' => 'group2',
								'required' => false,

							)
						 );
			 		?>
				</div> -->
			

				
					
				
			
					
		</div>		
		</div> 
			<div class="actions">
				<div class="pull-right">
					<?php echo $this->Form->submit(__('Opslaan'),array('class'=>'btn btn-primary'));?>
				</div>
			</div>
<?php echo $this->Form->end(); ?>
</div>
<div id="bottom-nav">
	<?php echo $this->element('bottom_nav'); ?>
</div>