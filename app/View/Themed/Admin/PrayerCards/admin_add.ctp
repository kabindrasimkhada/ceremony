<?php
/** 
 *
 * PHP 5
 *  
 * CakeAdmin CakePHP Plugin
 *
 * Copyright 2014, Babish Shrestha (Prabesh Shrestha)
 *                 Kathmandu, Nepal
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @Copyright 2014, Babish Shrestha (Prabesh Shrestha)
 * @link      http://prabeshshrestha.com.np
 * @package   plugins.cakeadmin.config.schema
 * @license   MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?><!-- users form elements -->
<div class="col-md-12 ">
    <div class="box box-primary users">
        <div class="box-header">
            <h3 class="box-title"><?php echo __('Voeg prentje'); ?></h3>
        </div><!-- /.box-header -->
        <?php echo $this->element('validate_message') ?>
        <!-- form start -->
        <?php echo $this->Form->create('PrayerCard', array('inputDefaults' => array('label' => false, 'div' => false))); ?>
        <div class="box-body">
             <div class="form-group">
                  <label for="name"><?php echo __('Selecteer prentje naam') ?></label>
                <?php
                echo $this->Form->input('prayer_card_category_id', array(
                    'options'=>$prayerCardCategories,
                    'placeholder' => __('Selecteer Voeg prentje'),
                    'class' => 'form-control',
                    'empty' => 'Selecteer',
                    'default' => 0
                ));
                ?>
            </div><!-- /.box-body -->
             <div class="form-group">
              <label for="lastname"><?php echo __('kaart 
soort') ?></label>
                <?php
                echo $this->Form->input('card', array(
                    'placeholder' => __('kaart'),
                    'class' => 'form-control'
                ));
                ?>
            </div><!-- /.box-body -->
            <div class="form-group">
              <label for="lastname"><?php echo __('prijs') ?></label>
                <?php
                echo $this->Form->input('price', array(
                    'placeholder' => __('prijs'),
                    'class' => 'form-control'
                ));
                ?>
            </div><!-- /.box-body --> 
            
        <div class="box-footer">
            <input type="submit" value=<?php echo __('Opslaan')?> class="btn btn-primary">
            <?php echo $this->Html->link(__('Cancel'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div><!-- /.box -->
</div>
<!-- End users form elements -->




