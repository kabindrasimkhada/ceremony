<?php echo $this->element('navigation', array('items' => 'tab9')); ?>	
<!-- Tab panes -->
<div class="tab-pane" id="tab9">
		<h1 class="font-light title-large"><?php echo __('en voorlopige kostenopgave'); ?></h1>
					<?php echo $this->Form->create('Miscellaneouse',array(
																			'class'=>'form-horizontal',
																			'novalidate' => true,
																			
																		)
														             );
				    ?>
				   <?php echo $this->element('forms/miscellaneous/coffin');?>
				   <?php echo $this->element('forms/miscellaneous/funeral_letter');?>
				   <?php echo $this->element('forms/miscellaneous/prayer_card');?>
				   <?php echo $this->element('forms/miscellaneous/misboekjes');?>
				   <?php echo $this->element('forms/miscellaneous/coffee_tickets');?>
				   <?php echo $this->element('forms/miscellaneous/stamp');?>
				   <?php echo $this->element('forms/miscellaneous/condoleanceregister');?>
				   <?php echo $this->element('forms/miscellaneous/hospital_nursing');?>
				   <?php echo $this->element('forms/miscellaneous/rouwcentrum');?>
				   <?php echo $this->element('forms/miscellaneous/care_deceased');?>
				   <?php echo $this->element('forms/miscellaneous/home_opbaring');?>
				   <?php echo $this->element('forms/miscellaneous/hearse');?>
				   <?php echo $this->element('forms/miscellaneous/carrier');?>
				   <?php echo $this->element('forms/miscellaneous/advertisement');?>
				   <?php echo $this->element('forms/miscellaneous/gratitude');?>
				   <?php echo $this->element('forms/miscellaneous/casket');?>
				   <?php echo $this->element('forms/miscellaneous/arrangement');?>
				   <?php echo $this->element('forms/miscellaneous/others');?>
					
					<div class="actions">
						<div class="pull-right">
							<?php echo $this->Form->submit(__('Opslaan'),array('class'=>'btn btn-primary'));?>
						</div>
					</div>
				<?php echo $this->Form->end(); ?>
	</div>
 <div id="bottom-nav">
 	<?php echo $this->element('bottom_nav'); ?>
 </div>	


