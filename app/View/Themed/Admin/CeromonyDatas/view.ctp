<div class="ceromonyDatas view">
<h2><?php echo __('Ceromony Data'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($ceromonyData['CeromonyData']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Client Datas'); ?></dt>
		<dd>
			<?php echo $this->Html->link($ceromonyData['ClientDatas']['id'], array('controller' => 'client_datas', 'action' => 'view', $ceromonyData['ClientDatas']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Crematorium'); ?></dt>
		<dd>
			<?php echo h($ceromonyData['CeromonyData']['crematorium']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Part Of'); ?></dt>
		<dd>
			<?php echo h($ceromonyData['CeromonyData']['part_of']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Assignment For'); ?></dt>
		<dd>
			<?php echo h($ceromonyData['CeromonyData']['assignment_for']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ceromony Date'); ?></dt>
		<dd>
			<?php echo h($ceromonyData['CeromonyData']['ceromony_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Theater'); ?></dt>
		<dd>
			<?php echo h($ceromonyData['CeromonyData']['theater']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start Time'); ?></dt>
		<dd>
			<?php echo h($ceromonyData['CeromonyData']['start_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Duration'); ?></dt>
		<dd>
			<?php echo h($ceromonyData['CeromonyData']['duration']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('No Of People'); ?></dt>
		<dd>
			<?php echo h($ceromonyData['CeromonyData']['no_of_people']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Att Info'); ?></dt>
		<dd>
			<?php echo h($ceromonyData['CeromonyData']['att_info']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Kistreg Nr'); ?></dt>
		<dd>
			<?php echo h($ceromonyData['CeromonyData']['kistreg_nr']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($ceromonyData['CeromonyData']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($ceromonyData['CeromonyData']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ceromony Data'), array('action' => 'edit', $ceromonyData['CeromonyData']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ceromony Data'), array('action' => 'delete', $ceromonyData['CeromonyData']['id']), null, __('Are you sure you want to delete # %s?', $ceromonyData['CeromonyData']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Ceromony Datas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ceromony Data'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Client Datas'), array('controller' => 'client_datas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client Datas'), array('controller' => 'client_datas', 'action' => 'add')); ?> </li>
	</ul>
</div>
