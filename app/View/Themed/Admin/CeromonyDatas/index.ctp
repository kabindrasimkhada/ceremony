<div class="ceromonyDatas index">
	<h2><?php echo __('Ceromony Datas'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('client_datas_id'); ?></th>
			<th><?php echo $this->Paginator->sort('crematorium'); ?></th>
			<th><?php echo $this->Paginator->sort('part_of'); ?></th>
			<th><?php echo $this->Paginator->sort('assignment_for'); ?></th>
			<th><?php echo $this->Paginator->sort('ceromony_date'); ?></th>
			<th><?php echo $this->Paginator->sort('theater'); ?></th>
			<th><?php echo $this->Paginator->sort('start_time'); ?></th>
			<th><?php echo $this->Paginator->sort('duration'); ?></th>
			<th><?php echo $this->Paginator->sort('no_of_people'); ?></th>
			<th><?php echo $this->Paginator->sort('att_info'); ?></th>
			<th><?php echo $this->Paginator->sort('kistreg_nr'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($ceromonyDatas as $ceromonyData): ?>
	<tr>
		<td><?php echo h($ceromonyData['CeromonyData']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($ceromonyData['ClientDatas']['id'], array('controller' => 'client_datas', 'action' => 'view', $ceromonyData['ClientDatas']['id'])); ?>
		</td>
		<td><?php echo h($ceromonyData['CeromonyData']['crematorium']); ?>&nbsp;</td>
		<td><?php echo h($ceromonyData['CeromonyData']['part_of']); ?>&nbsp;</td>
		<td><?php echo h($ceromonyData['CeromonyData']['assignment_for']); ?>&nbsp;</td>
		<td><?php echo h($ceromonyData['CeromonyData']['ceromony_date']); ?>&nbsp;</td>
		<td><?php echo h($ceromonyData['CeromonyData']['theater']); ?>&nbsp;</td>
		<td><?php echo h($ceromonyData['CeromonyData']['start_time']); ?>&nbsp;</td>
		<td><?php echo h($ceromonyData['CeromonyData']['duration']); ?>&nbsp;</td>
		<td><?php echo h($ceromonyData['CeromonyData']['no_of_people']); ?>&nbsp;</td>
		<td><?php echo h($ceromonyData['CeromonyData']['att_info']); ?>&nbsp;</td>
		<td><?php echo h($ceromonyData['CeromonyData']['kistreg_nr']); ?>&nbsp;</td>
		<td><?php echo h($ceromonyData['CeromonyData']['created']); ?>&nbsp;</td>
		<td><?php echo h($ceromonyData['CeromonyData']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $ceromonyData['CeromonyData']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $ceromonyData['CeromonyData']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $ceromonyData['CeromonyData']['id']), null, __('Are you sure you want to delete # %s?', $ceromonyData['CeromonyData']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Ceromony Data'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Client Datas'), array('controller' => 'client_datas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client Datas'), array('controller' => 'client_datas', 'action' => 'add')); ?> </li>
	</ul>
</div>
