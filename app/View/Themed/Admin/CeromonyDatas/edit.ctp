<div class="ceromonyDatas form">
<?php echo $this->Form->create('CeromonyData'); ?>
	<fieldset>
		<legend><?php echo __('Edit Ceromony Data'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('client_datas_id');
		echo $this->Form->input('crematorium');
		echo $this->Form->input('part_of');
		echo $this->Form->input('assignment_for');
		echo $this->Form->input('ceromony_date');
		echo $this->Form->input('theater');
		echo $this->Form->input('start_time');
		echo $this->Form->input('duration');
		echo $this->Form->input('no_of_people');
		echo $this->Form->input('att_info');
		echo $this->Form->input('kistreg_nr');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('CeromonyData.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('CeromonyData.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Ceromony Datas'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Client Datas'), array('controller' => 'client_datas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client Datas'), array('controller' => 'client_datas', 'action' => 'add')); ?> </li>
	</ul>
</div>
