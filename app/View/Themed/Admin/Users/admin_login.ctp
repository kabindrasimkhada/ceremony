<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="form-box" id="login-box">
    <div class="header"><?php echo __('Sign In') ?></div>
    <?php echo $this->Form->create('User', array('inputDefaults' => array('label' => false, 'div' => false))) ?>

    <div class="body bg-gray">
    <?php  
        echo $this->Session->flash();
        if ($referer != '/') {
            echo $this->Session->flash('auth');
          
        }else{
            CakeSession::delete('Message.auth');
            }
        ?>
        <div class="form-group">
            <?php
            echo $this->Form->input('username', array(
                'placeholder' => __('Username'),
                'class' => 'form-control'
            ));
            ?>
        </div>
        <div class="form-group">
            <?php
            echo $this->Form->input('password', array(
                'placeholder' => __('Password'),
                'class' => 'form-control'
            ));
            ?>
        </div>          
        <div class="form-group">
            <input type="checkbox" name="remember_me"/> <?php echo __('Gegevens onthouden');?>
        </div>
    </div>
    <div class="footer">  
      <?php echo $this->Form->submit(__('Sign in'), array('class' => 'btn btn-primary btn-block','style'=>'margin-right:10px;')); ?>
   </div>
<?php echo $this->Form->end(); ?>

</div>
