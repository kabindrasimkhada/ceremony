<?php
    
    $dashboardIcons = array(
                               array(
                                    'label' => __('Nieuw'),
                                    'link' => Router::url(array('controller' => 'ClientDatas', 'action' => 'add')),
                                    'icon' => 'ion-bag',
                                    'boxColor' => 'bg-aqua',
                                    'moreInfo' => __('nieuw')
                               ),
                             array(
                                    'label' => __('Gebruiker Mangement'),
                                    'link' => Router::url(array('controller' => 'users', 'action' => 'index')),
                                    'icon' => 'ion-person-add',
                                    'boxColor' => 'bg-green',
                                    'moreInfo' => __('gebruiker mangement')
                                ),  
                             /*array(
                                    'label' => __('Instellingen'),
                                    'link' => Router::url(array('controller' => 'users', 'action' => 'index')),
                                    'icon' => 'ion-settings',
                                    'boxColor' => 'bg-yellow',
                                    'moreInfo' => __('instellingen')
                                ),  */
                             array(
                                    'label' => __('concepten'),
                                    'link' => Router::url(array('controller' => 'clients', 'action' => 'drafts')),
                                    'icon' => 'ion-folder',
                                    'boxColor' => 'bg-aqua',
                                    'moreInfo' =>  __('concepten')
                                ),  
                              array(
                                    'label' => __('offertes'),
                                    'link' => Router::url(array('controller' => 'clients', 'action' => 'quotes')),
                                    'icon' => 'ion-bag',
                                    'boxColor' => 'bg-green',
                                    'moreInfo' =>  __('offertes')
                                ),  
                              array(
                                    'label' => __('orders'),
                                    'link' => Router::url(array('controller' => 'clients', 'action' => 'orders')),
                                    'icon' => 'ion-stats-bars',
                                    'boxColor' => 'bg-yellow',
                                    'moreInfo' => __('orders')
                                ),  
                    );

     
?>


  <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="row user">
               <!-- Main content -->
                <section class="content">
                 
                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                   <?php foreach($dashboardIcons as $dashboardIcon): ?>
                     <a href=" <?php echo $dashboardIcon['link'];?>">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box  <?php echo $dashboardIcon['boxColor'];?>">
                                <div class="inner">
                                    <h4>
                                       <?php echo $dashboardIcon['label'];?> 
                                    </h4>
                                    <p>
                                        <?php echo $dashboardIcon['moreInfo'];?> 
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion <?php echo $dashboardIcon['icon'];?>"></i>
                                </div>
                                <a href=" <?php echo $dashboardIcon['link'];?> " class="small-box-footer">
                                  <?php echo  __('Meer informatie');?> <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                     </a>   
                   <?php endforeach; ?>     
                  </div><!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->