<?php
/**
 *
 * PHP 5
 *  
 * CakeAdmin CakePHP Plugin
 *
 * Copyright 2014, Babish Shrestha (Prabesh Shrestha)
 *                 Kathmandu, Nepal
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @Copyright 2014, Babish Shrestha (Prabesh Shrestha)
 * @link      http://prabeshshrestha.com.np
 * @package   plugins.cakeadmin.config.schema
 * @license   MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?><!--Begin users tables-->
<div class="row users">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <i class="glyphicon glyphicon-list-alt"></i>
                <h3 class="box-title"><?php echo __('Users List'); ?></h3>                                    
            </div><!-- /.box-header -->
            <!-- Flash Msg -->
            <?php echo $this->Session->flash(); ?>           
            <?php //echo $this->Session->flash('auth'); ?>            <!--/Flash Msg -->

            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('id',__('id')); ?></th>
                            <th><?php echo $this->Paginator->sort('username',__('gebruikersnaam')); ?></th>
                            <th><?php echo $this->Paginator->sort('first_name', __('Full Name')); ?></th>
                            <th><?php echo $this->Paginator->sort('e-mail', __('e-mail')); ?></th>
                            <th><?php echo $this->Paginator->sort('created',__('aangemaakt')); ?></th>
                            <th class="actions"><?php echo __('acties'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($users as $user): ?>
                            <tr>
                                <td><?php echo h($user['User']['id']); ?>&nbsp;</td>
                                <td><?php echo h($user['User']['username']); ?>&nbsp;</td>
                                <td><?php echo h($user['User']['first_name']) . " " . h($user['User']['last_name']); ?>&nbsp;</td>
                                <td><?php echo h($user['User']['email']); ?></td>
                                <td><?php echo h($user['User']['created']); ?>&nbsp;</td>
                                <td class="actions">
                                    <?php
                                        echo $this->Html->link('<span class="fa fa-edit fa-fw"></span> '.__('Bewerken |'), array('action' => 'editUsers', $user['User']['id']),array( 'class' => 'btn btn-sm btn-default', 'escape' => false));
                                   
                                    ?>
                                    <?php echo $this->Form->postLink('<span class="fa fa-trash-o fa-fw"></span> '.__('verwijderen'), array('action' => 'delete', $user['User']['id'],__('Are you sure you want to delete # %s?', $user['User']['id'])),array( 'class' => 'btn btn-sm btn-default', 'escape' => false)); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th><?php echo $this->Paginator->sort('id',__('id')); ?></th>
                            <th><?php echo $this->Paginator->sort('username',__('gebruikersnaam')); ?></th>
                            <th><?php echo $this->Paginator->sort('first_name', __('Full Name')); ?></th>
                            <th><?php echo $this->Paginator->sort('e-mail', __('e-mail')); ?></th>
                            <th><?php echo $this->Paginator->sort('created',__('aangemaakt')); ?></th>
                            <th class="actions"><?php echo __('acties'); ?></th>
                        </tr>
                    </tfoot>
                </table>
                <?php
                    echo $this->Html->link(__('toevoegen'), 
                                                array('action' => 'addUsers'),
                                                array('class'=>'btn btn-primary addbtn'));
                 ?>
                <?php echo $this->element('pagination'); ?>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<!--End users div-->




