<?php
/**
 *
 * PHP 5
 *  
 * CakeAdmin CakePHP Plugin
 *
 * Copyright 2014, Babish Shrestha (Prabesh Shrestha)
 *                 Kathmandu, Nepal
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @Copyright 2014, Babish Shrestha (Prabesh Shrestha)
 * @link      http://prabeshshrestha.com.np
 * @package   plugins.cakeadmin.config.schema
 * @license   MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?><!-- users form elements -->
<div class="col-md-12 ">
    <div class="box box-primary users">
        <div class="box-header">
            <h3 class="box-title"><?php echo __('Admin Change Password'); ?></h3>
        </div><!-- /.box-header -->
        <?php echo $this->element('validate_message') ?>
        <!-- form start -->
        <?php echo $this->Form->create('User', array('inputDefaults' => array('label' => false, 'div' => false))); ?>
        <div class="box-body">
            <div class="form-group">
                <label for="username"><?php echo __('Old Password') ?></label>
                <?php
                echo $this->Form->input('old_password', array('type'=>'password',
                    'placeholder' => __('Old Passwrod'),
                    'class' => 'form-control'
                ));
                ?>
            </div><!-- /.box-body -->
            <div class="form-group">
                <label for="password"><?php echo __('New Password') ?></label>
                <?php
                echo $this->Form->input('password', array('type'=>'password',
                    'placeholder' => __('New Password'),
                    'class' => 'form-control'
                ));
                ?>
            </div><!-- /.box-body -->
            <div class="form-group">
                <label for="password"><?php echo __('Confirm Password') ?></label>
                <?php
                echo $this->Form->input('confirm_password', array('type'=>'password',
                    'placeholder' => __('Confirm Password'),
                    'class' => 'form-control'
                ));
                ?>
            </div>

        </div><!-- /.box-body -->

        <div class="box-footer">
            <input type="submit" value=<?php echo __('Save')?> class="btn btn-primary">
            <?php echo $this->Html->link(__('Cancel'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div><!-- /.box -->
</div>
<!-- End users form elements -->




