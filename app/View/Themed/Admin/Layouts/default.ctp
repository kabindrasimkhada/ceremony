<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <!--Mobile first-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--IE Compatibility modes-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css(array('bootstrap.min', 'cleanadmin', 'font-awesome.min', 'layout'));
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        //echo $this->Html->script(array('js/vendor/jquery-1.10.2.min.js'));
        echo $this->Html->css(
                array(
                    'bootstrap.min.css',
                    //'extra/datepicker.css',
                    'extra/main.css',
                    'ionicons.min.css',
                    'jquery-ui.min.css',
                    'jquery-ui.structure.min.css',
                    'jquery-ui.theme.min.css',
                    'classic',
                    'classic.date'
                )
        );
        //echo $this->Html->script('js/vendor/modernizr-2.6.2.min.js');
        echo $this->Html->script(array('jquery.min.js',
            'modernizr-build.min.js', 
            'jquery-ui.min.js',
            'main.js'
            ));
        ?>
        <?php
        if (isset($language) && $language != '') {
            echo $this->Html->script(array('lang.nl'), array('fullBase' => true));
        }
        ?> 
        <script type="text/javascript">
            var webpath = '<?php echo $this->webroot; ?>';
            var language = '<?php echo $language; ?>';
            var action = '<?php echo $this->action; ?>';
            var priceUrl = '<?php
        echo Router::url(
                array(
                    'controller' => 'miscellaneouses',
                    'action' => 'priceByAjax',
                    'admin' => true
                )
        );
        ?>';
            var cartUrl = '<?php
        echo Router::url(
                array(
                    'controller' => 'miscellaneouses',
                    'action' => 'findCardType',
                    'admin' => true
                )
        );
        ?>';
            var dynamicLocationGeneartor = '<?php
        echo Router::url(
                array(
                    'controller' => 'clientDatas',
                    'action' => 'dynamicLocationGeneartor',
                    'admin' => true
                )
        );
        ?>';
        </script>
    </head>
    <body class="skin-blue">
        <!-- Header ELement -->
<?php echo $this->element('header'); ?>
        <!-- End Header ELement -->

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left Menu ELement -->
        <?php echo $this->element('left_menu'); ?>
            <!-- End Left Menu ELement -->
            <!-- breadcrumb -->
<?php echo $this->element('breadcrumb'); ?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">                
                <!-- Main content -->
                <section class="content">
            <?php echo $this->Session->flash(); ?>
<?php echo $this->fetch('content'); ?>
                </section><!-- /.content -->


            </aside><!-- /.right-side -->

        </div><!-- ./wrapper -->

<?php
echo $this->Html->script(array(
    'jquery.validate.min.js',
    'cleanadmin.js',
    'main.js',
));
?>


        <!-- footer navigation buttons -->
        <?php //echo $this->element('footer');?>
        <?php
        echo $this->Html->script(
                array(
                    'js/bootstrap.min.js',
                    'js/picker',
                    'js/picker.date',
                    'js/legacy.js',
                    'js/plugins.js',
                    'js/main.js',
                )
        );
        ?>
        <?php echo $this->fetch('scriptBottom'); ?>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. --> 
        <script>
            (function(b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function() {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-XXXXX-X');
            ga('send', 'pageview');
        </script>
    </body>
</html>
