<div class="row prayer_card">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <i class="glyphicon glyphicon-list-alt"></i>
                <h3 class="box-title"><?php echo __('Prayer Card List'); ?></h3>                                    
            </div><!-- /.box-header -->
            <!-- Flash Msg -->
            <?php echo $this->Session->flash(); ?>           
            <?php //echo $this->Session->flash('auth'); ?>            <!--/Flash Msg -->

            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                            <th><?php echo $this->Paginator->sort('name', __('Prayer Card Name')); ?></th>
                            <th><?php echo $this->Paginator->sort('map_type', __('Map Type')); ?></th>
                            <th><?php echo $this->Paginator->sort('created',__('aangemaakt')); ?></th>
                            <th class="actions"><?php echo __('Actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($prayerCategories as $prayerCategory)://debug($prayerCategories);die; ?>
                            <tr>
                                <td><?php echo h($prayerCategory['PrayerCategory']['id']); ?>&nbsp;</td>
                                <td><?php echo h($prayerCategory['PrayerCategory']['name']); ?>&nbsp;</td>
                                <td><?php echo h($prayerCategory['PrayerCategory']['map_type']); ?>&nbsp;</td>
                                <td><?php echo h($prayerCategory['PrayerCategory']['created']); ?>&nbsp;</td>
                               <td class="actions">
                                    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $prayerCategory['PrayerCategory']['id'])); ?>
                                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $prayerCategory['PrayerCategory']['id']), null, __('Are you sure you want to delete # %s?', $prayerCategory['PrayerCategory']['id'])); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                            <th><?php echo $this->Paginator->sort('name', __('Prayer Card Name')); ?></th>
                            <th><?php echo $this->Paginator->sort('map_type', __('Map Type')); ?></th>
                            <th><?php echo $this->Paginator->sort('created',__('aangemaakt')); ?></th>
                            <th class="actions"><?php echo __('Actions'); ?></th>
                        </tr>
                    </tfoot>
                </table>
                <?php
                    echo $this->Html->link(__('toevoegen'), 
                                                array('action' => 'add'),
                                                array('class'=>'btn btn-primary addbtn'));
                 ?>
                <?php echo $this->element('pagination'); ?>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<!--End Prayer Card div-->
