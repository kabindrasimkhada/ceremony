<div class="col-md-12 ">
    <div class="box box-primary users">
        <div class="box-header">
            <h3 class="box-title"><?php echo __('Edit Prayer Card'); ?></h3>
        </div><!-- /.box-header -->
		<?php echo $this->element('validate_message') ?>
        <!-- form start -->
        <?php echo $this->Form->create('PrayerCategory', array('inputDefaults' => array('label' => false, 'div' => false))); ?>
        <div class="box-body">
        	<?php echo $this->Form->input('id'); ?>
            <div class="form-group">
                  <label for="name"><?php echo __('Prayer Card Name') ?></label>
                <?php
                echo $this->Form->input('name', array(
                    'placeholder' => __('Prayer Card Name'),
                    'class' => 'form-control'
                ));
                ?>
            </div><!-- /.box-body -->
            <div class="form-group">
              <label for="lastname"><?php echo __('Map Type') ?></label>
                <?php
                echo $this->Form->input('map_type', array(
                    'placeholder' => __('Map type'),
                    'class' => 'form-control'
                ));
                ?>
            </div><!-- /.box-body -->
            
        <div class="box-footer">
            <input type="submit" value=<?php echo __('Save')?> class="btn btn-primary">
            <?php echo $this->Html->link(__('Cancel'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div><!-- /.box -->


