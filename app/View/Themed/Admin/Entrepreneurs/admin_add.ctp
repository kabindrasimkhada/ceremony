<?php echo $this->element('navigation', array('items' => 'tab4')); ?>	
<!-- Tab panes -->
<div class="tab-content">
	<div class="tab-pane active" id="tab4">
			<h1 class="title-large font-light"><?php echo __('Gegevens uitvaartonderneming'); ?></h1>
			<?php echo $this->Form->create('Entrepreneur',array(
																'class'=>'form-horizontal',
																'novalidate' => true
																)
															);
			?>
				<div class="form-group">
					<div class="col-sm-6">
						<?php echo $this->Form->input('funeral_company_type',
							array(  
									'options'=>array('Meeuws','Verspeek'),
									'before'=>'<label class="radio-inline">',
									'type' => 'radio',
									'class' => 'radio-inline',
									'legend' => false,
									'div' => false,
   									'fieldset' => false,
   									 'separator'=>'</label><label class="radio-inline">',
   									'after' => '</label>',
								)
							 );
				 		?>
					
					</div>
				</div>
				<hr>
				<h3 class="font-light"><?php echo __('Zelf invullen'); ?></h3>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Uitvaartonderneming'); ?></label>
					<div class="col-sm-5">
						<?php echo $this->Form->input('funeral_company',array(
																		'class'=>'form-control undertakerName',
																		'div'=>false,
																		'label'=>false
																		)
																);
						?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Uitvaartverzorger'); ?></label>
					<div class="col-sm-5">
						<?php echo $this->Form->input('funeral_caregiver',array('class'=>'form-control','div'=>false,'label'=>false)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Postcode'); ?></label>
					<div class="col-sm-2">
						<div class="input-group">
							<?php echo $this->Form->input('zip_code',
																	array(
																			'class'=>'form-control postcode',
																			'div'=>false,
																			'label'=>false,
																			'type' => 'text'
																	)
																);
							 ?>
							<div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
						</div>
					</div>
					<label class="control-label col-sm-1"><?php echo __('Nummer'); ?></label>
					<div class="col-sm-1">
						<?php echo $this->Form->input('number',
																array(
																			'class'=>'form-control',
																			'div'=>false,
																			'label'=>false,
																			'type' => 'text'
																	)
																);
						?>
					</div>
					<label class="control-label col-sm-1"><?php echo __('Letter'); ?></label>
					<div class="col-sm-1">
						<?php echo $this->Form->input('letter',array(
																	 'class'=>'form-control',
																	 'div'=>false,
																	 'label'=>false
																	)
													); 
						?>
					</div>
					<label class="control-label col-sm-1"><?php echo __('Toev.'); ?></label>
					<div class="col-sm-1">
						<?php echo $this->Form->input('add',array(
																	'class'=>'form-control',
																	'div'=>false,
																	'label'=>false
																)
													);
						 ?>
					</div>
					<label class="control-label col-sm-1"><?php echo __('Aand.'); ?></label>
					<div class="col-sm-1">
						<?php echo $this->Form->input('tightening',array(
																		'class'=>'form-control',
																		'div'=>false,
																		'label'=>false
																	)
													); 
					 ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Straat'); ?></label>
					<div class="col-sm-5">
						<?php echo $this->Form->input('street',array(
																	 'class'=>'form-control street',
																	 'div'=>false,
																	 'label'=>false
																	)
													);
						 ?>
					</div>
					<label class="control-label col-sm-2"><?php echo __('Plaats (/land)'); ?></label>
					<div class="col-sm-3">
						<?php echo $this->Form->input('location',array(
																		'class'=>'form-control place',
																		'div'=>false,
																		'label'=>false
																	)
													);
						 ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Telefoon'); ?></label>
					<div class="col-sm-2">
						<?php echo $this->Form->input('phone',array(
																	'class'=>'form-control',
																	'div'=>false,
																	'label'=>false
																	)
													);
					    ?>
					</div>
					<label class="control-label col-sm-1"><?php echo __('Mobiel'); ?></label>
					<div class="col-sm-2">
						<?php echo $this->Form->input('mobile',
																array(
																			'class'=>'form-control',
																			'div'=>false,
																			'label'=>false,
																			'type' => 'text'
																	)
																);
						?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Dossier nr.');?></label>
					<div class="col-sm-2">
						<?php echo $this->Form->input('file_no',array(
																	'class'=>'form-control',
																	'div'=>false,
																	'label'=>false,
																	'type' => 'text'
																	)
													);
					    ?>
					</div>
					<label class="control-label col-sm-2"><?php echo __('Familievergoeding');?></label>
					<div class="col-sm-3">
						<?php echo $this->Form->input('family_compensation',
																array(
																	'class'=>'form-control',
																	'div'=>false,
																	'label'=>false,
																	'type' => 'text'
																	)
																);
						?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('E-mail adres');?></label>
					<div class="col-sm-5">
						<?php echo $this->Form->input('email',array(
																	'class'=>'form-control',
																	'div'=>false,
																	'label'=>false
																  )
													);
						 ?>
					</div>
				</div>
				<div class="actions">
					<div class="pull-right">
						<?php echo $this->Form->submit(__('Opslaan'),array('class'=>'btn btn-primary'));?>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
		</div>
</div>
<!-- end of 4th tab -->	
 <div id="bottom-nav">
 	<?php echo $this->element('bottom_nav'); ?>
 </div>