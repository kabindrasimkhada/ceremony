<div class="cemetryDatas view">
<h2><?php echo __('Cemetry Data'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($cemetryData['CemetryData']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Client Datas'); ?></dt>
		<dd>
			<?php echo $this->Html->link($cemetryData['ClientDatas']['id'], array('controller' => 'client_datas', 'action' => 'view', $cemetryData['ClientDatas']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cemetry'); ?></dt>
		<dd>
			<?php echo h($cemetryData['CemetryData']['cemetry']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Grave Number'); ?></dt>
		<dd>
			<?php echo h($cemetryData['CemetryData']['grave_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Grafrechten'); ?></dt>
		<dd>
			<?php echo h($cemetryData['CemetryData']['grafrechten']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Complement To Grafrechten'); ?></dt>
		<dd>
			<?php echo h($cemetryData['CemetryData']['complement_to_grafrechten']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Extends To Ten Yrs'); ?></dt>
		<dd>
			<?php echo h($cemetryData['CemetryData']['extends_to_ten_yrs']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remove Monuments'); ?></dt>
		<dd>
			<?php echo h($cemetryData['CemetryData']['remove_monuments']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Grave Digging'); ?></dt>
		<dd>
			<?php echo h($cemetryData['CemetryData']['grave_digging']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($cemetryData['CemetryData']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($cemetryData['CemetryData']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Cemetry Data'), array('action' => 'edit', $cemetryData['CemetryData']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Cemetry Data'), array('action' => 'delete', $cemetryData['CemetryData']['id']), null, __('Are you sure you want to delete # %s?', $cemetryData['CemetryData']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Cemetry Datas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cemetry Data'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Client Datas'), array('controller' => 'client_datas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client Datas'), array('controller' => 'client_datas', 'action' => 'add')); ?> </li>
	</ul>
</div>
