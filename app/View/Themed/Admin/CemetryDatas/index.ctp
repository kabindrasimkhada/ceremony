<div class="cemetryDatas index">
	<h2><?php echo __('Cemetry Datas'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('client_datas_id'); ?></th>
			<th><?php echo $this->Paginator->sort('cemetry'); ?></th>
			<th><?php echo $this->Paginator->sort('grave_number'); ?></th>
			<th><?php echo $this->Paginator->sort('grafrechten'); ?></th>
			<th><?php echo $this->Paginator->sort('complement_to_grafrechten'); ?></th>
			<th><?php echo $this->Paginator->sort('extends_to_ten_yrs'); ?></th>
			<th><?php echo $this->Paginator->sort('remove_monuments'); ?></th>
			<th><?php echo $this->Paginator->sort('grave_digging'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($cemetryDatas as $cemetryData): ?>
	<tr>
		<td><?php echo h($cemetryData['CemetryData']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($cemetryData['ClientDatas']['id'], array('controller' => 'client_datas', 'action' => 'view', $cemetryData['ClientDatas']['id'])); ?>
		</td>
		<td><?php echo h($cemetryData['CemetryData']['cemetry']); ?>&nbsp;</td>
		<td><?php echo h($cemetryData['CemetryData']['grave_number']); ?>&nbsp;</td>
		<td><?php echo h($cemetryData['CemetryData']['grafrechten']); ?>&nbsp;</td>
		<td><?php echo h($cemetryData['CemetryData']['complement_to_grafrechten']); ?>&nbsp;</td>
		<td><?php echo h($cemetryData['CemetryData']['extends_to_ten_yrs']); ?>&nbsp;</td>
		<td><?php echo h($cemetryData['CemetryData']['remove_monuments']); ?>&nbsp;</td>
		<td><?php echo h($cemetryData['CemetryData']['grave_digging']); ?>&nbsp;</td>
		<td><?php echo h($cemetryData['CemetryData']['created']); ?>&nbsp;</td>
		<td><?php echo h($cemetryData['CemetryData']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $cemetryData['CemetryData']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $cemetryData['CemetryData']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $cemetryData['CemetryData']['id']), null, __('Are you sure you want to delete # %s?', $cemetryData['CemetryData']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Cemetry Data'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Client Datas'), array('controller' => 'client_datas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client Datas'), array('controller' => 'client_datas', 'action' => 'add')); ?> </li>
	</ul>
</div>
