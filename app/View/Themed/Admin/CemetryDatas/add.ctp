<div class="cemetryDatas form">
<?php echo $this->Form->create('CemetryData'); ?>
	<fieldset>
		<legend><?php echo __('Add Cemetry Data'); ?></legend>
	<?php
		echo $this->Form->input('client_datas_id');
		echo $this->Form->input('cemetry');
		echo $this->Form->input('grave_number');
		echo $this->Form->input('grafrechten');
		echo $this->Form->input('complement_to_grafrechten');
		echo $this->Form->input('extends_to_ten_yrs');
		echo $this->Form->input('remove_monuments');
		echo $this->Form->input('grave_digging');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<