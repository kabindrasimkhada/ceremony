<div class="cemetryDatas form">
<?php echo $this->Form->create('CemetryData'); ?>
	<fieldset>
		<legend><?php echo __('Edit Cemetry Data'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('client_datas_id');
		echo $this->Form->input('cemetry');
		echo $this->Form->input('grave_number');
		echo $this->Form->input('grafrechten');
		echo $this->Form->input('complement_to_grafrechten');
		echo $this->Form->input('extends_to_ten_yrs');
		echo $this->Form->input('remove_monuments');
		echo $this->Form->input('grave_digging');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('CemetryData.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('CemetryData.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Cemetry Datas'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Client Datas'), array('controller' => 'client_datas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client Datas'), array('controller' => 'client_datas', 'action' => 'add')); ?> </li>
	</ul>
</div>
