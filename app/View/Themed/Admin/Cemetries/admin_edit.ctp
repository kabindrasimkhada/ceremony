<div class="col-md-12 ">
    <div class="box box-primary users">
        <div class="box-header">
            <h3 class="box-title"><?php echo __('Bewerken begraafplaats'); ?></h3>
        </div><!-- /.box-header -->
        <?php echo $this->element('validate_message') ?>
        <!-- form start -->
        <?php echo $this->Form->create('Cemetry', array('inputDefaults' => array('label' => false, 'div' => false))); ?>
       	<?php echo $this->Form->input('id'); ?>
        <div class="box-body">
            <div class="form-group">
                  <label for="firstname"><?php echo __('begraafplaats Name') ?></label>
                <?php
                echo $this->Form->input('cemetry_name', array(
                    'placeholder' => __('begraafplaats naam'),
                    'class' => 'form-control'
                ));
                ?>
            </div><!-- /.box-body -->
            <div class="form-group">
              <label for="lastname"><?php echo __('graf nummer') ?></label>
                <?php
                echo $this->Form->input('grave_number', array(
                    'placeholder' => __('graf nummer'),
                    'class' => 'form-control'
                ));
                ?>
            </div><!-- /.box-body -->
             <div class="form-group">
              <label for="lastname"><?php echo __('locatie') ?></label>
                <?php
                echo $this->Form->input('location', array(
                    'placeholder' => __('locatie'),
                    'class' => 'form-control'
                ));
                ?>
            </div><!-- /.box-body -->
            
        <div class="box-footer">
            <input type="submit" value=<?php echo __('Opslaan')?> class="btn btn-primary">
            <?php echo $this->Html->link(__('Cancel'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div><!-- /.box -->
</div>
<!-- End users form elements -->
