<?php echo $this->element('navigation', array('items' => 'tab3')); ?>	
<!-- Tab panes -->
<div class="tab-content">
	<div class="tab-pane active" id="tab4">
			
		<?php echo $this->Form->create('CeromonyInfo',array(
															'class'=>'form-horizontal',
															'novalidate' => true
		                                                    )
		                                                   );
		?>	
		         <?php echo $this->element('forms/ceromony/ceromony_data');?>
				 <?php echo $this->element('forms/ceromony/chruch');?>
				 <?php echo $this->element('forms/ceromony/cemetry');?>
				<?php echo $this->element('forms/ceromony/crematorium');?>
				<div class="actions">
					<div class="pull-right">
						<?php echo $this->Form->submit(__('Opslaan'),array('class'=>'btn btn-primary'));?>
				   </div>
			   </div>
		<?php echo $this->Form->end(); ?>
   </div>
</div>
<!-- end of 3rd tab -->
 <div id="bottom-nav">
 	<?php echo $this->element('bottom_nav'); ?>
 </div>

