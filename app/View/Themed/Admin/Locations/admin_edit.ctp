<?php
/** 
 *
 * PHP 5
 *  
 * CakeAdmin CakePHP Plugin
 *
 * Copyright 2014, Babish Shrestha (Prabesh Shrestha)
 *                 Kathmandu, Nepal
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @Copyright 2014, Babish Shrestha (Prabesh Shrestha)
 * @link      http://prabeshshrestha.com.np
 * @package   plugins.cakeadmin.config.schema
 * @license   MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?><!-- users form elements -->
<div class="col-md-12 ">
    <div class="box box-primary users">
        <div class="box-header">
            <h3 class="box-title"><?php echo __('Bewerken Locatie'); ?></h3>
        </div><!-- /.box-header -->
        <?php echo $this->element('validate_message') ?>
        <!-- form start -->
        <?php echo $this->Form->create('Location', array('inputDefaults' => array('label' => false, 'div' => false))); ?>
            <div class="box-body">
                <div class="form-group">
                      <label for="firstname"><?php echo __('Locatie naam') ?></label>
                    <?php
                    echo $this->Form->input('name', array(
                        'placeholder' => __('Locatie naam'),
                        'class' => 'form-control'
                    ));
                    ?>
                </div><!-- /.box-body -->
                 <div class="form-group">
                  <label for="lastname"><?php echo __('plaats') ?></label>
                    <?php
                    echo $this->Form->input('place', array(
                        'placeholder' => __('plaats'),
                        'class' => 'form-control'
                    ));
                    ?>
                </div><!-- /.box-body -->
                
            <div class="box-footer">
                <input type="submit" value=<?php echo __('Opslaan')?> class="btn btn-primary">
                <?php echo $this->Html->link(__('Annuleren'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </div><!-- /.box -->
</div>
<!-- End users form elements -->




