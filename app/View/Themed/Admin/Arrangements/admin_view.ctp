<div class="arrangements view">
<h2><?php echo __('Arrangement'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($arrangement['Arrangement']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Arrangement Type'); ?></dt>
		<dd>
			<?php echo h($arrangement['Arrangement']['arrangement_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price'); ?></dt>
		<dd>
			<?php echo h($arrangement['Arrangement']['price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($arrangement['Arrangement']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($arrangement['Arrangement']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Arrangement'), array('action' => 'edit', $arrangement['Arrangement']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Arrangement'), array('action' => 'delete', $arrangement['Arrangement']['id']), null, __('Are you sure you want to delete # %s?', $arrangement['Arrangement']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Arrangements'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Arrangement'), array('action' => 'add')); ?> </li>
	</ul>
</div>
