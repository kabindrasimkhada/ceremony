<div class="col-md-12 ">
    <div class="box box-primary users">
        <div class="box-header">
            <h3 class="box-title"><?php echo __('Add Arrangement'); ?></h3>
        </div><!-- /.box-header -->
        <?php echo $this->element('validate_message') ?>
        <!-- form start -->
        <?php echo $this->Form->create('Arrangement', array('inputDefaults' => array('label' => false, 'div' => false))); ?>
       <?php  echo $this->Form->input('id'); ?>
        <div class="box-body">
            <div class="form-group">
                  <label for="name"><?php echo __('Arrangement Type') ?></label>
                <?php
                echo $this->Form->input('arrangement_type', array(
                    'placeholder' => __('Arrangement Type'),
                    'class' => 'form-control'
                ));
                ?>
            </div><!-- /.box-body -->
             <div class="form-group">
              <label for="lastname"><?php echo __('Price') ?></label>
                <?php
                echo $this->Form->input('price', array(
                    'placeholder' => __('Price'),
                    'class' => 'form-control'
                ));
                ?>
            </div><!-- /.box-body -->
            
        <div class="box-footer">
            <input type="submit" value=<?php echo __('Save')?> class="btn btn-primary">
            <?php echo $this->Html->link(__('Cancel'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div><!-- /.box -->
</div>
<!-- End users form elements -->




