<div class="row Arrangement">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <i class="glyphicon glyphicon-list-alt"></i>
                <h3 class="box-title"><?php echo __('Arrangement List'); ?></h3>                                    
            </div><!-- /.box-header -->
            <!-- Flash Msg -->
            <?php echo $this->Session->flash(); ?>           
            <?php //echo $this->Session->flash('auth'); ?>            <!--/Flash Msg -->

            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                            <th><?php echo $this->Paginator->sort('arrangement_type', __('Arrangement Type')); ?></th>
                            <th><?php echo $this->Paginator->sort('price',__('prijs')); ?></th>
                            <th><?php echo $this->Paginator->sort('created',__('aangemaakt')); ?></th>
                            <th class="actions"><?php echo __('Actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                 <?php foreach ($arrangements as $arrangement): ?>
    <tr>
        <td><?php echo h($arrangement['Arrangement']['id']); ?>&nbsp;</td>
        <td><?php echo h($arrangement['Arrangement']['arrangement_type']); ?>&nbsp;</td>
        <td><?php echo h($arrangement['Arrangement']['price']); ?>&nbsp;</td>
        <td><?php echo h($arrangement['Arrangement']['created']); ?>&nbsp;</td>
        <td class="actions">
            <?php echo $this->Html->link('<span class="fa fa-edit fa-fw"></span> '.__('Bewerken'), array('action' => 'edit', $arrangement['Arrangement']['id']),array( 'class' => 'btn btn-sm btn-default', 'escape' => false)
); ?>
            <?php echo $this->Form->postLink('<span class="fa fa-trash-o fa-fw"></span> '.__('verwijderen'), array('action' => 'delete', $arrangement['Arrangement']['id']),array( 'class' => 'btn btn-sm btn-default', 'escape' => false)
, __('Are you sure you want to delete # %s?', $arrangement['Arrangement']['id'])); ?>
        </td>
    </tr>
<?php endforeach; ?>
                    </tbody>
                    <tfoot>
                         <tr>
                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                            <th><?php echo $this->Paginator->sort('arrangement_type', __('Arrangement Type')); ?></th>
                            <th><?php echo $this->Paginator->sort('price',__('prijs')); ?></th>
                            <th><?php echo $this->Paginator->sort('created',__('aangemaakt')); ?></th>
                            <th class="actions"><?php echo __('Actions'); ?></th>
                        </tr>
                    </tfoot>
                </table>
                <?php echo $this->element('pagination'); ?>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<!--End Arrangement div-->
