<?php echo $this->element('navigation', array('items' => 'tab7')); ?>	
<!-- Tab panes -->
	<div class="tab-pane" id="tab7">
			<h1 class="title-large font-light"><?php echo __('Wensen beeld en geluid'); ?></h1>
			<?php echo $this->Form->create('PictureSound',array(
																'class'=>'form-horizontal',
																'novalidate' => true
															)
														);
			?>
				<h4 class="font-normal"><?php echo __('Muziek:'); ?></h4>
				<?php for($i=1;$i<=8;$i++){?>
				<div class="form-group">
					<label class="col-sm-1 control-label"><?php echo $i; ?></label>
					<div class="col-sm-6">
						<?php echo $this->Form->input("Music.music_name[$i]",array(
																					'class'=>'form-control',
																					'div'=>false,
																					'label'=>false
																				 )
													  );
						?>
					</div>
				</div>
			<?php }?>
				
				<div class="form-group">
					<label class="control-label col-sm-4"><?php echo __('Wenst u beeldopname/PowerPoint te presenteren?:'); ?></label>
					<div class="col-sm-3">
					<!-- 	<?php echo $this->Form->input('image_recording',array(
                                         'options'=>array('Ja','Nee',
                                         ),
                                         'class'=>'form-control',
                                         'div'=>false,
                                         'label'=>false
                                   	 )
								);
						?> -->
						<?php echo $this->Form->input('image_recording',
									array(  
											'options'=>array('Nee','Ja'),
											'before'=>'<label class="radio-inline">',
											'type' => 'radio',
											'class' => 'radio-inline',
											'legend' => false,
											'div' => false,
											'fieldset' => false,
											'separator'=>'</label><label class="radio-inline">',
											'after' => '</label>',
											'class' => 'radioGroup8',
											'required' => false,
											'default'=>'',
										)
									 );
		 				?>	
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-4"><?php echo __('Wenst u een CD of DVD opname?:'); ?></label>
					<div class="col-sm-3">
						<?php echo $this->Form->input('cd_dvd_recording',array(
                                         		'options'=>$recording_types ,
                                                'class'=>'form-control',
                                         		'div'=>false,
                                         		'label'=>false,
                                         		'empty'=>'Selecteer',
                                             	'selected'=>'0',
                                   	 ));
						?>
					
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-4"><?php echo __('Live muziek'); ?></label>
					<div class="col-sm-3">
					<!-- 	<?php echo $this->Form->input('live_music',array(
								                                         'options'=>array('Ja','Nee',
								                                         ),
								                                         'class'=>'form-control',
								                                         'div'=>false,
								                                         'label'=>false
				                                   	 )
													);
						?> -->
						<?php echo $this->Form->input('live_music',
									array(  
											'options'=>array('Nee','Ja'),
											'before'=>'<label class="radio-inline">',
											'type' => 'radio',
											'class' => 'radio-inline',
											'legend' => false,
											'div' => false,
											'fieldset' => false,
											'separator'=>'</label><label class="radio-inline">',
											'after' => '</label>',
											'class' => 'radioGroup8',
											'required' => false,
											'default'=>'',
										)
									 );
		 				?>	
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label"><?php echo __('Overige:'); ?></label>
					<div class="col-sm-6">
						<?php echo $this->Form->input('others',array(
																	 'class'=>'form-control',
																	 'div'=>false,
																	 'label'=>false
																	)
													);
						 ?>
					</div>
				</div>
				<div class="actions">
					<div class="pull-right">
						<?php echo $this->Form->submit(__('Opslaan'),array('class'=>'btn btn-primary'));?>
					</div>
				</div>
	<?php echo $this->Form->end(); ?>
</div>
 <div id="bottom-nav">
 	<?php echo $this->element('bottom_nav'); ?>
 </div>


