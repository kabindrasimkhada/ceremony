<div class="row Carrier">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <i class="glyphicon glyphicon-list-alt"></i>
                <h3 class="box-title"><?php echo __('thuis Opbaring lijst'); ?></h3>                                    
            </div><!-- /.box-header -->
            <!-- Flash Msg -->
            <?php echo $this->Session->flash(); ?>           
            <?php //echo $this->Session->flash('auth'); ?>            <!--/Flash Msg -->

            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                       <tr>
                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                            <th><?php echo $this->Paginator->sort('name', __('naam')); ?></th>
                            <th><?php echo $this->Paginator->sort('price',__('prijs')); ?></th>
                            <th><?php echo $this->Paginator->sort('created',__('aangemaakt')); ?></th>
                            <th class="actions"><?php echo __('acties'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
 <?php foreach ($homebarings as $homebaring): ?>
            <tr>
                <td><?php echo h($homebaring['Homebaring']['id']); ?>&nbsp;</td>
                <td><?php echo h($homebaring['Homebaring']['name']); ?>&nbsp;</td>
                <td><?php echo h($this->App->formatPriceValue($homebaring['Homebaring']['price'])); ?>&nbsp;</td>
                <td><?php echo h($homebaring['Homebaring']['created']); ?>&nbsp;</td>
                <td class="actions">
                    <?php echo $this->Html->link('<span class="fa fa-edit fa-fw"></span> '.__('Bewerken'), array('action' => 'edit', $homebaring['Homebaring']['id']),array( 'class' => 'btn btn-sm btn-default', 'escape' => false)); ?>
                    <?php echo $this->Form->postLink('<span class="fa fa-trash-o fa-fw"></span> '.__('verwijderen'), array('action' => 'delete', $homebaring['Homebaring']['id']),array( 'class' => 'btn btn-sm btn-default', 'escape' => false), __('Are you sure you want to delete # %s?', $homebaring['Homebaring']['id'])); ?>
                </td>
        </tr>
<?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                            <th><?php echo $this->Paginator->sort('name', __('naam')); ?></th>
                            <th><?php echo $this->Paginator->sort('price',__('prijs')); ?></th>
                            <th><?php echo $this->Paginator->sort('created',__('aangemaakt')); ?></th>
                            <th class="actions"><?php echo __('acties'); ?></th>
                        </tr>
                    </tfoot>
                </table>
                 <?php
                    echo $this->Html->link(__('toevoegen'), 
                                                array('action' => 'add'),
                                                array('class'=>'btn btn-primary addbtn'));
                 ?>
                <?php echo $this->element('pagination'); ?>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<!--End Carrier div-->
