<div class="billingAddresses view">
<h2><?php echo __('Billing Address'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($billingAddress['BillingAddress']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Client Datas'); ?></dt>
		<dd>
			<?php echo $this->Html->link($billingAddress['ClientDatas']['id'], array('controller' => 'client_datas', 'action' => 'view', $billingAddress['ClientDatas']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($billingAddress['BillingAddress']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sex'); ?></dt>
		<dd>
			<?php echo h($billingAddress['BillingAddress']['sex']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Relation'); ?></dt>
		<dd>
			<?php echo h($billingAddress['BillingAddress']['relation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Zip Code'); ?></dt>
		<dd>
			<?php echo h($billingAddress['BillingAddress']['zip_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Number'); ?></dt>
		<dd>
			<?php echo h($billingAddress['BillingAddress']['number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Letter'); ?></dt>
		<dd>
			<?php echo h($billingAddress['BillingAddress']['letter']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Add'); ?></dt>
		<dd>
			<?php echo h($billingAddress['BillingAddress']['add']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tightening'); ?></dt>
		<dd>
			<?php echo h($billingAddress['BillingAddress']['tightening']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Street'); ?></dt>
		<dd>
			<?php echo h($billingAddress['BillingAddress']['street']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Location'); ?></dt>
		<dd>
			<?php echo h($billingAddress['BillingAddress']['location']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($billingAddress['BillingAddress']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($billingAddress['BillingAddress']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Billing Address'), array('action' => 'edit', $billingAddress['BillingAddress']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Billing Address'), array('action' => 'delete', $billingAddress['BillingAddress']['id']), null, __('Are you sure you want to delete # %s?', $billingAddress['BillingAddress']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Billing Addresses'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Billing Address'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Client Datas'), array('controller' => 'client_datas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client Datas'), array('controller' => 'client_datas', 'action' => 'add')); ?> </li>
	</ul>
</div>
