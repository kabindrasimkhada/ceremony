<div class="billingAddresses form">
<?php echo $this->Form->create('BillingAddress'); ?>
	<fieldset>
		<legend><?php echo __('Edit Billing Address'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('client_datas_id');
		echo $this->Form->input('name');
		echo $this->Form->input('sex');
		echo $this->Form->input('relation');
		echo $this->Form->input('zip_code');
		echo $this->Form->input('number');
		echo $this->Form->input('letter');
		echo $this->Form->input('add');
		echo $this->Form->input('tightening');
		echo $this->Form->input('street');
		echo $this->Form->input('location');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('BillingAddress.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('BillingAddress.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Billing Addresses'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Client Datas'), array('controller' => 'client_datas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client Datas'), array('controller' => 'client_datas', 'action' => 'add')); ?> </li>
	</ul>
</div>
