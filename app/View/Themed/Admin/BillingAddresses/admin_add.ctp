<?php echo $this->element('navigation', array('items' => 'tab5')); ?>	
<!-- Tab panes -->
<div class="tab-pane" id="tab5">
    <h1 class="title-large font-light"><?php echo __('Factuuradres'); ?></h1>
    <?php
    echo $this->Form->create('BillingAddress', array(
        'class' => 'form-horizontal',
        'novalidate' => true
            )
    );
    ?>	
    <div class="form-group">
        <label class="control-label col-sm-2"><?php echo __('Naam'); ?></label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->input('name', array(
                'class' => 'form-control',
                'div' => false,
                'label' => false
                    )
            );
            ?>
        </div>
        <label class="control-label col-sm-1"><?php echo __('Geslacht'); ?></label>
        <div class="col-sm-2">
            <?php
            echo $this->Form->input('sex', array(
                'options' => array(
                    'Man' => 'Man',
                    'Vrouw' => 'Vrouw'
                ),
                'class' => 'form-control',
                'div' => false,
                'label' => false,
                'empty' => 'Selecteer',
                'default' => ""
            ));
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2"><?php echo __('Relatie tot ovl.'); ?></label>
        <div class="col-sm-3">
            <?php
            echo $this->Form->input('relation', array(
                'class' => 'form-control',
                'div' => false,
                'label' => false
                    )
            );
            ?>
        </div>
    </div>	
    <div class="form-group">	
        <label class="control-label col-sm-2"><?php echo __('Telefoon'); ?></label>
        <div class="col-sm-2">
            <?php
            echo $this->Form->input('phone', array(
                'class' => 'form-control',
                'div' => false,
                'label' => false
                    )
            );
            ?>
        </div>
        <label class="control-label col-sm-1"><?php echo __('Mobiel'); ?></label>
        <div class="col-sm-2">
            <?php
            echo $this->Form->input('mobile', array(
                'class' => 'form-control',
                'div' => false,
                'label' => false,
                'type' => 'text'
                    )
            );
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2"><?php echo __('Postcode'); ?></label>
        <div class="col-sm-2">
            <div class="input-group">
                <?php
                echo $this->Form->input('zip_code', array(
                    'class' => 'form-control postcode',
                    'div' => false,
                    'label' => false,
                    'type' => 'text'
                        )
                );
                ?>
                <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
            </div>
        </div>
        <label class="control-label col-sm-1"><?php echo __('Nummer'); ?></label>
        <div class="col-sm-1">
            <?php
            echo $this->Form->input('number', array(
                'class' => 'form-control',
                'div' => false,
                'label' => false,
                'type' => 'text'
                    )
            );
            ?>
        </div>
    </div>	
    <div class="form-group">	
        <label class="control-label col-sm-2"><?php echo __('Letter'); ?></label>
        <div class="col-sm-2">
            <?php
            echo $this->Form->input('letter', array(
                'class' => 'form-control',
                'div' => false,
                'label' => false
                    )
            );
            ?>
        </div>

        <label class="control-label col-sm-1"><?php echo __('Toev.'); ?></label>
        <div class="col-sm-1">
            <?php
            echo $this->Form->input('add', array(
                'class' => 'form-control',
                'div' => false,
                'label' => false
                    )
            );
            ?>
        </div>
        <label class="control-label col-sm-1"><?php echo __('Aand.'); ?></label>
        <div class="col-sm-1">
<?php
echo $this->Form->input('tightening', array(
    'class' => 'form-control',
    'div' => false,
    'label' => false
        )
);
?>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2"><?php echo __('Straat'); ?></label>
        <div class="col-sm-5">
<?php
echo $this->Form->input('street', array(
    'class' => 'form-control street',
    'div' => false,
    'label' => false
        )
);
?>
        </div>
    </div>
    <div class="form-group">	
        <label class="control-label col-sm-2"><?php echo __('Plaats (/land)'); ?></label>
        <div class="col-sm-3">
<?php
echo $this->Form->input('location', array(
    'class' => 'form-control place',
    'div' => false,
    'label' => false
        )
);
?>
        </div>
    </div>
    <div class="actions">
        <div class="pull-right">
<?php echo $this->Form->submit(__('Opslaan'), array('class' => 'btn btn-primary')); ?>
        </div>
    </div>
<?php echo $this->Form->end(); ?>

</div>
<!-- end of 5th tab -->	
<div id="bottom-nav">
<?php echo $this->element('bottom_nav'); ?>
</div>