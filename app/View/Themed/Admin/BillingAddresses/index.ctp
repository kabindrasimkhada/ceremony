<div class="billingAddresses index">
	<h2><?php echo __('Billing Addresses'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('client_datas_id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('sex'); ?></th>
			<th><?php echo $this->Paginator->sort('relation'); ?></th>
			<th><?php echo $this->Paginator->sort('zip_code'); ?></th>
			<th><?php echo $this->Paginator->sort('number'); ?></th>
			<th><?php echo $this->Paginator->sort('letter'); ?></th>
			<th><?php echo $this->Paginator->sort('add'); ?></th>
			<th><?php echo $this->Paginator->sort('tightening'); ?></th>
			<th><?php echo $this->Paginator->sort('street'); ?></th>
			<th><?php echo $this->Paginator->sort('location'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($billingAddresses as $billingAddress): ?>
	<tr>
		<td><?php echo h($billingAddress['BillingAddress']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($billingAddress['ClientDatas']['id'], array('controller' => 'client_datas', 'action' => 'view', $billingAddress['ClientDatas']['id'])); ?>
		</td>
		<td><?php echo h($billingAddress['BillingAddress']['name']); ?>&nbsp;</td>
		<td><?php echo h($billingAddress['BillingAddress']['sex']); ?>&nbsp;</td>
		<td><?php echo h($billingAddress['BillingAddress']['relation']); ?>&nbsp;</td>
		<td><?php echo h($billingAddress['BillingAddress']['zip_code']); ?>&nbsp;</td>
		<td><?php echo h($billingAddress['BillingAddress']['number']); ?>&nbsp;</td>
		<td><?php echo h($billingAddress['BillingAddress']['letter']); ?>&nbsp;</td>
		<td><?php echo h($billingAddress['BillingAddress']['add']); ?>&nbsp;</td>
		<td><?php echo h($billingAddress['BillingAddress']['tightening']); ?>&nbsp;</td>
		<td><?php echo h($billingAddress['BillingAddress']['street']); ?>&nbsp;</td>
		<td><?php echo h($billingAddress['BillingAddress']['location']); ?>&nbsp;</td>
		<td><?php echo h($billingAddress['BillingAddress']['created']); ?>&nbsp;</td>
		<td><?php echo h($billingAddress['BillingAddress']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $billingAddress['BillingAddress']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $billingAddress['BillingAddress']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $billingAddress['BillingAddress']['id']), null, __('Are you sure you want to delete # %s?', $billingAddress['BillingAddress']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Billing Address'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Client Datas'), array('controller' => 'client_datas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client Datas'), array('controller' => 'client_datas', 'action' => 'add')); ?> </li>
	</ul>
</div>
