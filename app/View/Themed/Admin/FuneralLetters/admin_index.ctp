<div class="row Care Decease">
    <div class="col-xs-12">
        <div class="box box-primary">
            <?php echo $this->Html->link(__('Add'), array('action' => 'add')); ?>
            <div class="box-header">
                <i class="glyphicon glyphicon-list-alt"></i>
                <h3 class="box-title"><?php echo __('Funeral Letters List'); ?></h3>                                    
            </div><!-- /.box-header -->
            <!-- Flash Msg -->
            <?php echo $this->Session->flash(); ?>           
            <?php //echo $this->Session->flash('auth'); ?>            <!--/Flash Msg -->

            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                            <th><?php echo $this->Paginator->sort('letter_name', __('begrafenis brief naam')); ?></th>
                            <th><?php echo $this->Paginator->sort('created',__('aangemaakt')); ?></th>
                            <th class="actions"><?php echo __('acties'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($funeralLetters as $funeralLetter): ?>
                        <tr>
                            <td><?php echo h($funeralLetter['FuneralLetter']['id']); ?>&nbsp;</td>
                            <td><?php echo h($funeralLetter['FuneralLetter']['letter_name']); ?>&nbsp;</td>
                            <td><?php echo h($funeralLetter['FuneralLetter']['created']); ?>&nbsp;</td>
                            <td class="actions">
                                <?php echo $this->Html->link('<span class="fa fa-edit fa-fw"></span> '.__('Edit'), array('action' => 'edit', $funeralLetter['FuneralLetter']['id']),array( 'class' => 'btn btn-sm btn-default', 'escape' => false)); ?>
                                <?php echo $this->Form->postLink('<span class="fa fa-edit fa-fw"></span> '.__('Delete'), array('action' => 'delete', $funeralLetter['FuneralLetter']['id']),array( 'class' => 'btn btn-sm btn-default', 'escape' => false), __('Are you sure you want to delete # %s?', $funeralLetter['FuneralLetter']['id'])); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                         <tr>
                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                            <th><?php echo $this->Paginator->sort('letter_name', __('begrafenis brief naam')); ?></th>
                            <th><?php echo $this->Paginator->sort('created',__('aangemaakt')); ?></th>
                            <th class="actions"><?php echo __('acties'); ?></th>
                        </tr>
                    </tfoot>
                </table>
                 <?php
                    echo $this->Html->link(__('toevoegen'), 
                                                array('action' => 'add'),
                                                array('class'=>'btn btn-primary addbtn'));
                 ?>
                <?php echo $this->element('pagination'); ?>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<!--End Care Decease div-->
