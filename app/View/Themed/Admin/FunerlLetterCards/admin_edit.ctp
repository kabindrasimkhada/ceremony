<div class="col-md-12 ">
    <div class="box box-primary users">
        <div class="box-header">
            <h3 class="box-title"><?php echo __('Bewerken kaart'); ?></h3>
        </div><!-- /.box-header -->
        <?php echo $this->element('validate_message') ?>
        <!-- form start -->
        <?php echo $this->Form->create('FunerlLetterCard', array('inputDefaults' => array('label' => false, 'div' => false))); ?>
        <div class="box-body">
           <?php echo $this->Form->input('id'); ?>
            <div class="form-group">
                  <label for="name"><?php echo __('Selecteer begrafenis brief naam') ?></label>
                <?php
                    echo $this->Form->input('funeral_letter_id', array(
                        'options'=>$funeralLetters,
                        'placeholder' => __('begrafenis brief naam'),
                        'class' => 'form-control',
                        'empty' => 'Selecteer',
                        'default' => 0
                    ));
                ?>
            </div><!-- /.box-body -->
            <div class="form-group">
              <label for="lastname"><?php echo __('kaarttype') ?></label>
                <?php
                echo $this->Form->input('card', array(
                    'placeholder' => __('kaart'),
                    'class' => 'form-control'
                ));
                ?>
            </div><!-- /.box-body -->
           
             <div class="form-group">
              <label for="lastname"><?php echo __('prijs') ?></label>
                <?php
                echo $this->Form->input('price', array(
                    'placeholder' => __('prijs'),
                    'class' => 'form-control'
                ));
                ?>
            </div><!-- /.box-body --> 
        <div class="box-footer">
            <input type="submit" value=<?php echo __('Opslaan')?> class="btn btn-primary">
            <?php echo $this->Html->link(__('Cancel'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div><!-- /.box -->


