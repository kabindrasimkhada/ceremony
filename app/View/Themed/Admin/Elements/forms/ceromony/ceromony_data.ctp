<h1 class="title-large font-light"><?php echo __('gegevens afscheid'); ?></h1>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Crematorium'); ?></label>
					<div class="col-sm-4">
						<?php echo $this->Form->input('CeromonyData.crematorium',array(
																					'class'=>'form-control',
																					'div'=>false,
																					'label'=>false,
																					'required'=>false
																				 )
													                          ); 
					?>
					</div>
				</div>
			  <!-- <div class="form-group">
					<label class="control-label col-sm-2">Onderdeel van</label>
					<div class="col-sm-4">
						<?php echo $this->Form->input('CeromonyData.part_of',array(
																					'class'=>'form-control',
																					'div'=>false,
																					'label'=>false,
																					'required'=>false
																				 )
													                          ); 
					?>
					</div>
				</div> -->
				<!-- <div class="form-group">
					<label class="control-label col-sm-2">Opdracht voor</label>
					<div class="col-sm-2">
						<?php echo $this->Form->input('CeromonyData.assignment_for',array(
																					'class'=>'form-control',
																					'div'=>false,
																					'label'=>false,'
																					 required'=>false
																				 )
													                          ); 
					?>
					</div>
				</div> -->
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Datum van afscheid');?></label>
					<div class="col-sm-4">
						<div class="input-group"  data-date="12-02-2012" data-date-format="yyyy-mm-dd">
							<?php echo $this->Form->input('CeromonyData.ceromony_date',
																				array(
																					'class' =>'form-control datepicker-control',
																					'div'=>false,
																					'label'=>false,
																					'required'=>false,
																					'type'=>'text'
																					)
	                                                                            );
                            ?>
							<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Aula'); ?></label>
					<div class="col-sm-2">
						    <?php echo $this->Form->input('CeromonyData.theater',array(
																						'class' =>'form-control',
																						'div'=>false,
																						'label'=>false,
																						'required'=>false,
																						'type'=>'text'
																						  
																						)
                                                                                    );
                            ?>   
					</div>
					<label class="control-label col-sm-2"><?php echo __('Aanvangstijd'); ?></label>
					<div class="col-sm-2">
						<div class="input-group">
							<?php echo $this->Form->input('CeromonyData.start_time',array(
																						'class' =>'form-control',
																						'div'=>false,
																						'label'=>false,
																						'required'=>false,
																						'type'=>'text'
																						  
																						)
                                                                                    );
                            ?>
							<span class="input-group-addon">[uu:mm]</span> </div>
					</div>
					<label class="control-label col-sm-1"><?php echo __('Tijdsduur'); ?></label>
					<div class="col-sm-2">
						<div class="input-group">
							<?php echo $this->Form->input('CeromonyData.duration',array(
																						'class' =>'form-control',
																						'div'=>false,
																						'label'=>false,
																						'required'=>false,
																						'type'=>'text'
																						  
																						)
                                                                                    );
                            ?>

							<span class="input-group-addon"><?php echo __('min'); ?></span> </div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Aantal personen'); ?></label>
					<div class="col-sm-2">
						<?php echo $this->Form->input('CeromonyData.no_of_people',array(
																					'class'=>'form-control',
																					'div'=>false,
																					'label'=>false,'
																					 required'=>false,
																					 'type' => 'text'
																				 )
													                          ); 
					?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Aanv. info'); ?></label>
					<div class="col-sm-2">
						<?php echo $this->Form->input('CeromonyData.att_info',array(
																					'class'=>'form-control',
																					'div'=>false,
																					'label'=>false,'
																					 required'=>false
																				 )
													                          ); 
					?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Kistreg.nr.'); ?></label>
					<div class="col-sm-2">
						<?php echo $this->Form->input('CeromonyData.kistreg_nr',array(
																					'class'=>'form-control',
																					'div'=>false,
																					'label'=>false,
																					'required'=>false,
																					'type' => 'text'
																				 )
													                          ); 
					?>
					</div>
				</div>
