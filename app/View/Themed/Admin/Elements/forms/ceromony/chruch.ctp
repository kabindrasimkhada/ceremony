<h1 class="title-large font-light"><?php echo __('Kerk'); ?></h1>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Parochie'); ?></label>
					 <?php if(!empty($churches) && isset($churches)):?>
					<div class="col-sm-2">
					<?php echo $this->Form->input('ChruchData.parish',
															array(
                                                                'options'=>$churches,
                                                                'class'=>'form-control',
                                                                'div'=>false,
                                                                'required'=>false,
                                                                'label'=>false,
                                                                'empty'=>'Selecteer',
                                                                'default'=>'0',
                                                                ));
                                        ?>
					</div>	
					<?php else: ?>
					<div class="col-sm-2">
					    <?php echo  $this->Html->link('Plz insert Chruches',array(
                                                            'controller'=>'churches',
                                                            'action'=>'add',
                                                            'admin'=>true)
                                );
    ?>
					 </div>
					<?php endif;?>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Avondwake'); ?></label>
					<div class="col-sm-2">
					<?php echo $this->Form->input('ChruchData.evening_vigil',
									array(  
											'options'=>array('Nee','Ja'),
											'before'=>'<label class="radio-inline">',
											'type' => 'radio',
											'class' => 'radio-inline',
											'legend' => false,
											'div' => false,
											'fieldset' => false,
											'separator'=>'</label><label class="radio-inline">',
											'after' => '</label>',
											//'class' => 'radioGroup8',
											'required' => false,
											'default'=>'',
										)
									 );
		 			?>		
						
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Gebruik rouwkapel'); ?></label>
					<div class="col-sm-2">
					<?php echo $this->Form->input('ChruchData.funeral_chapel',
									array(  
											'options'=>array('Nee','Ja'),
											'before'=>'<label class="radio-inline">',
											'type' => 'radio',
											'class' => 'radio-inline',
											'legend' => false,
											'div' => false,
											'fieldset' => false,
											'separator'=>'</label><label class="radio-inline">',
											'after' => '</label>',
											//'class' => 'radioGroup8',
											'required' => false,
											'default'=>'',
										)
									 );
		 			?>	
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Datum van afscheid'); ?></label>
					<div class="col-sm-4">
						<div class="input-group"  data-date="12-02-2012" data-date-format="yyyy-mm-dd">
							<?php echo $this->Form->input('ChruchData.ceromony_date',
								                    array(
								                            'class' =>'form-control',
								                            'div'=>false,
								                            'label'=>false,
								                            'required'=>false,
								                            'type'=>'text'
								                        )
										);
                            ?>
							<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
						</div>
					</div>
					<label class="control-label col-sm-2"><?php echo __('Plechtigheid tijd'); ?></label>
					<div class="col-sm-2">
						<div class="input-group">
							<?php echo $this->Form->input('ChruchData.ceromony_time',array(
                                                'class'=>'form-control',
                                                'div'=>false,'
                                                required'=>false,
                                                'label'=>false,
                                                'type'=>'text'
                                                )
                                     );
						 ?>
						    <span class="input-group-addon"><?php echo __('[uu:mm]'); ?></span>
					   </div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Maandelijkse gedachten'); ?></label>
					<div class="col-sm-2">
					<?php echo $this->Form->input('ChruchData.monthly_thoughts',
									array(  
											'options'=>array('Nee','Ja'),
											'before'=>'<label class="radio-inline">',
											'type' => 'radio',
											'class' => 'radio-inline',
											'legend' => false,
											'div' => false,
											'fieldset' => false,
											'separator'=>'</label><label class="radio-inline">',
											'after' => '</label>',
											//'class' => 'radioGroup8',
											'required' => false,
											'default'=>'',
										)
									 );
		 			?>	
					</div>
				</div>