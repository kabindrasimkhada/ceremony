<h1 class="title-large font-light"><?php echo __('Crematorium');?> </h1>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Opdracht gefaxt');?></label>
					<div class="col-sm-2">
					<!-- 	<?php echo $this->Form->input('assignment_faxed',array(
                                         'options'=>array('Nee','Ja'),
                                         'class'=>'form-control',
                                         'div'=>false,
                                         'label'=>false,
                                         'empty'=>'Selecteer',
                                         'default'=>''
                                   	 )
								);
						?> -->
						<?php echo $this->Form->input('assignment_faxed',
									array(  
											'options'=>array('Nee','Ja'),
											'before'=>'<label class="radio-inline">',
											'type' => 'radio',
											'class' => 'radio-inline',
											'legend' => false,
											'div' => false,
											'fieldset' => false,
											'separator'=>'</label><label class="radio-inline">',
											'after' => '</label>',
											'class' => 'radioGroup8',
											'required' => false,
											'default'=>'',
										)
									 );
		 				?>	
					</div>
				</div>