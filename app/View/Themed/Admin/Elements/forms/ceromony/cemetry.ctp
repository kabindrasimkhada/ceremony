<h1 class="title-large font-light"><?php echo __('Begraafplaats/kerkhof'); ?></h1>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Begraafplaats'); ?></label>
					<div class="col-sm-2">
						 <?php if(!empty($cemetries) && isset($cemetries)):?>
						<?php echo $this->Form->input('CemetryData.cemetry',
										array(
											'options'=>$cemetries,
                                      	    'class'=>'form-control',
                                     	    'div'=>false,
                                     	    'label'=>false,
                                     	    'empty'=>'Selecteer',
                                 			'default'=>'0',
	                                   	   )   
	                                 );
						?>
					 <?php else: ?>
					    <?php $this->Html->link('Plz insert Cemetries',array('controller'=>'cemetries','action'=>'add','admin'=>true));?>
					 <?php endif; ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Grafnummer'); ?></label>
					<div class="col-sm-2">
						<?php echo $this->Form->input('CemetryData.grave_number',
																	array(
																			'class'=>'form-control',
																			'div'=>false,
																			'label'=>false,
																			'type' => 'text'
																		)
															);
					 ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Grafrechten'); ?></label>
					<div class="col-sm-2">
						<?php echo $this->Form->input('CemetryData.grafrechten',array(
                                         'options'=>array('Enkel','Dubbel',
                                         ),
                                         'class'=>'form-control',
                                         'div'=>false,
                                         'label'=>false,
                                         'empty'=>'Selecteer',
                                       )
								);
						?>
						
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Grafrechten aanvullen tot'); ?></label>
					<div class="col-sm-2">
						<?php echo $this->Form->input('CemetryData.complement_to_grafrechten',array('class'=>'form-control','div'=>false,'label'=>false)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Grafrechten verlengen 10 jaar'); ?></label>
					<div class="col-sm-2">
						<?php echo $this->Form->input('CemetryData.extends_to_ten_yrs',
										array(  
												'options'=>array('Nee','Ja'),
												'before'=>'<label class="radio-inline">',
												'type' => 'radio',
												'class' => 'radio-inline',
												'legend' => false,
												'div' => false,
												'fieldset' => false,
												'separator'=>'</label><label class="radio-inline">',
												'after' => '</label>',
												//'class' => 'radioGroup7',
												'required' => false,
												'default'=>'',
											)
										 );
			 			?>

					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Monument verwijderen'); ?></label>
					<div class="col-sm-2">
					<?php echo $this->Form->input('CemetryData.remove_monuments',
										array(  
												'options'=>array('Nee','Ja'),
												'before'=>'<label class="radio-inline">',
												'type' => 'radio',
												'class' => 'radio-inline',
												'legend' => false,
												'div' => false,
												'fieldset' => false,
												'separator'=>'</label><label class="radio-inline">',
												'after' => '</label>',
												//'class' => 'radioGroup8',
												'required' => false,
												'default'=>'',
											)
										 );
			 			?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo __('Graf delven/dichten'); ?></label>
					<div class="col-sm-2">
					<!-- 	<?php echo $this->Form->input('CemetryData.grave_digging',array(
                                         'options'=>array('Nee','Ja'),
                                         'class'=>'form-control',
                                         'div'=>false,
                                         'label'=>false,
                                         'empty'=>'Selecteer'
                                   	 )
								);
						?>
 -->					
 					<?php echo $this->Form->input('CemetryData.grave_digging',
									array(  
											'options'=>array('Nee','Ja'),
											'before'=>'<label class="radio-inline">',
											'type' => 'radio',
											'class' => 'radio-inline',
											'legend' => false,
											'div' => false,
											'fieldset' => false,
											'separator'=>'</label><label class="radio-inline">',
											'after' => '</label>',
											//'class' => 'radioGroup8',
											'required' => false,
											'default'=>'',
										)
									 );
		 			?>		
					</div>
				</div>