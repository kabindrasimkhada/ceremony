<h4 class="font-bold title-section text-primary"><?php echo __('Misboekjes'); ?></h4>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Prijs per stuk'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('MisboekjesData.price_per_piece',
												array(
												'class'=>'form-control',
												'div'=>false,
												'label'=>false,
												'readonly' => 'readonly',
												'type' => 'text'
												)
											); 
							?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Naam'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('MisboekjesData.price_per_piecename',
		                                     array(
		                                     		'options'=>$bookejDatas,
			                                        'class'=>'form-control',
			                                        'div'=>false, 
			                                        'label'=>false,
													'empty'=>'Selecteer',
													'default'=>'1',
													'type'=>'select'
		                                        )
		                             	  );
						   ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Aantal'); ?></label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('MisboekjesData.price_per_piecenumber',
											array(
													'class'=>'form-control',
													'div'=>false,
													'label'=>false,
													'type' => 'text'
										)
									);
						?>
						</div>
						<label class="col-sm-1 control-label"><?php echo __('Prijs'); ?></label>
						<div class="col-sm-2">
						<?php echo $this->Form->input('MisboekjesData.total_price',
    								array(
    										'class'=>'form-control',
    										'div'=>false,
    										'label'=>false,
    										'readonly' => 'readonly',
    										'type' => 'text'
										)
									);
					    ?>
						</div>
					</div>