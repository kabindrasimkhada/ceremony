<h4 class="font-bold title-section text-primary"><?php echo __('Mortuarium ziekenhuis / verpleeghuis'); ?></h4>
					<div class="form-group">
						<label class="col-sm-2 control-label">Naam</label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('HospitalsData.hospital_name',array(
                                         'options'=>$hospitalsDatas,
                                         'class'=>'form-control',
                                         'div'=>false,
                                         'label'=>false,
                                         'empty'=>'Selecteer',
                                         'default'=>'1',
                                   	 ));
						?>
							
						</div>
						<label class="col-sm-1 control-label"><?php echo __('Prijs'); ?></label>
						<div class="col-sm-2">
							<?php echo $this->Form->input('HospitalsData.price',
									array(
 										  'class'=>'form-control',
 										  'div'=>false, 
 										  'label'=>false,
 										  'type' => 'text',
 										  'readonly' => 'readonly'
 										  )
 									); 
		                  	?>
						</div>
					</div>