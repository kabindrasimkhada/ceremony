<h4 class="font-bold title-section text-primary"><?php echo __('Gedachtenisprentjes'); ?></h4>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Selecteer categorie'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('PrayerCardsData.prayer_category_type',array(
                                         'options'=>$prayerCardsDatas,
                                         'class'=>'form-control',
                                         'div'=>false,
                                         'label'=>false,
                                         'empty'=>'Selecteer',
                                         'default'=>'',
                                   	 ));
						?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Selecteer kaarten'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('PrayerCardsData.card_type',array(
                                         'options'=>'',
                                         'class'=>'form-control',
                                         'div'=>false,
                                         'label'=>false,
                                         'type' => 'select'
                                         
                                   	 ));
						?>
							
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Aantal'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('PrayerCardsData.number',
															array(
																	'class'=>'form-control',
																	'div'=>false,
																	'label'=>false,
																	'type' => 'text'
															)
													); 
							?>
						</div>
						<label class="col-sm-1 control-label"><?php echo __('Prijs'); ?></label>
						<div class="col-sm-2">
							<?php echo $this->Form->input('PrayerCardsData.price',
				    										array(
				    										'class'=>'form-control',
				    										'div'=>false,
				    										'label'=>false,
				    										'type' => 'text',
				    										'readonly' => 'readonly',

				    										)
				    									);
						    ?>
							<?php echo $this->Form->input('prayerhidden',array(
											'class'=>'form-control PrayerCardPrice',
											'div'=>false,
											'label'=>false,
											'type'=>'hidden'
										)
									);
								?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Bijzonderheden'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('PrayerCardsData.remarks',
											array(
												'class'=>'form-control',
												'div'=>false,'label'=>false,
												'type'=>'textarea',
												'rows'=> 4
												)
											);
							 ?>
						</div>
					</div>