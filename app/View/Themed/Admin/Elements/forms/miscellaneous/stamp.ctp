<h4 class="font-bold title-section text-primary"><?php echo __('Postzegels'); ?></h4>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Prijs Nederland'); ?></label>
						<div class="col-sm-2">
							<?php echo $this->Form->input('Stamp.price_netherland',
										  array(
		                                     		'options'=>isset($postzeals)?$postzeals:'',
			                                        'class'=>'form-control',
			                                        'div'=>false, 
			                                        'label'=>false,
													'empty'=>'Selecteer',
													'default'=>'1',
													'type'=>'select'
		                                        )
		                             	  );
							?>
						</div>
						<label class="col-sm-1 control-label"><?php echo __('Aantal'); ?></label>
						<div class="col-sm-1">
							<?php echo $this->Form->input('Stamp.number',
														array(
																'class'=>'form-control',
																'div'=>false,
																'label'=>false,
																'type' => 'text'
															)
														); 
							?>
						</div>
						<label class="col-sm-1 control-label"><?php echo __('Prijs'); ?></label>
						<div class="col-sm-2">
							<?php echo $this->Form->input('Stamp.price',
							    										array(
							    										'class'=>'form-control',
							    										'div'=>false,
							    										'label'=>false,
							    										'type' => 'text',
							    										'readonly' => 'readonly'
							    										)
							    									);
						    ?>
						</div>
					</div>
					