<h4 class="font-bold title-section text-primary"><?php echo __('Dragers / onderneming'); ?></h4>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Naam'); ?></label>
						<div class="col-sm-4">
								<?php echo $this->Form->input('CarriersData.name',array(
	                                         'options'=>$carriersDatas,
	                                         'class'=>'form-control',
	                                         'div'=>false,
	                                         'label'=>false,
	                                          'empty'=>'Selecteer',
                                              'default'=>'1',
	                                   	 ));
							    ?>
						</div>
					</div>
					<div class="form-group">
					        <label class="col-sm-2 control-label"><?php echo __('Aantal'); ?></label>
							<div class="col-sm-4">
								<?php echo $this->Form->input('CarriersData.number',
														array(	
																'class'=>'form-control',
																'div'=>false,
																'label'=>false,
																'type' => 'text'
													)
											);
							    ?>
							</div>
						
							<label class="col-sm-1 control-label"><?php echo __('Prijs'); ?></label>
							<div class="col-sm-2">
						 	<?php echo $this->Form->input('CarriersData.price',
						 								array(
					 										  'class'=>'form-control',
					 										  'div'=>false, 
					 										  'label'=>false,
					 										  'type' => 'text',
					 										  'readonly' => 'readonly',
					 										)
					 									); 
							?>

							<?php echo $this->Form->input('CarriersData.hidden',array('class'=>'form-control carrierPrice',
																	'div'=>false,
																	'label'=>false,
																	'type'=>'hidden'
																)
															);
							?>
								 
							</div>
					 
					</div>