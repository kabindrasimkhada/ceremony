<h4 class="font-bold title-section text-primary"><?php echo __('Dankbetuiging'); ?></h4>
					<div class="form-group">
						<div class="col-sm-6">
							<?php echo $this->Form->input('gratitude',array('class'=>'form-control','div'=>false,'label'=>false,'type'=>'textarea','rows'=> 4)); ?>
						</div>
						<label class="col-sm-1 control-label"><?php echo __('Prijs'); ?></label>
						<div class="col-sm-2">
							<?php echo $this->Form->input('gratitude_price',array(
																					'class'=>'form-control',
																					'div'=>false,
																					'type' => 'text',
																					'label'=>false
																				)
																			);
							 ?>
							 
						</div>
					</div>