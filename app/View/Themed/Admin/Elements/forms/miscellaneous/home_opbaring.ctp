<h4 class="font-bold title-section text-primary"><?php echo __('Thuisopbaring'); ?></h4>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Naam'); ?></label>
						<div class="col-sm-4">
								<?php echo $this->Form->input('Homeobaring.name',array(
	                                         'options'=>$homebaring,
	                                         'class'=>'form-control',
	                                         'div'=>false,
	                                         'label'=>false,
	                                         'empty'=>'Selecteer',
                                             'default'=>'',
	                                   	 ));
							    ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Aantal staandagen'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('Homeobaring.days',
									array(
											'class'=>'form-control',
											'div'=>false,
											'label'=>false,
											'type' => 'text'
										)
									); 
						   ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Prijs'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('Homeobaring.per_price',array(
		 										  'class'=>'form-control',
		 										  'div'=>false, 
		 										  'label'=>false,
		 										  'type' => 'text',
		 										  'readonly' => 'readonly'
		 										  )
		 									); 
		                  	?>
							<?php echo $this->Form->input('homeobaringhidden',array('class'=>'form-control homeobaringPrice',
																	'div'=>false,
																	'label'=>false,
																	'type'=>'hidden'
																)
															);
							?>
							
						</div>
						<!--  <label class="col-sm-1 control-label">Prijs</label>
						<div class="col-sm-2">
							<?php //echo $this->Form->input('number',array('class'=>'form-control','div'=>false,'label'=>false)); ?>
						</div>  -->
					</div>