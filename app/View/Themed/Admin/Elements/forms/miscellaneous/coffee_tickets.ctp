<h4 class="font-bold title-section text-primary"><?php echo __('Koffietafelkaartjes'); ?></h4>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Prijs met naam'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('CoffeeTicket.name_with_price',array(
															'class'=>'form-control',
															'div'=>false,
															'label'=>false,
                                                            'type'=>'text',
															'readonly' => 'readonly'
															)
														); 
							?>
                                                   
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Prijs zonder naam'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('CoffeeTicket.name_without_price',
									array(
											'class'=>'form-control',
											'div'=>false,
											'label'=>false,
                                     		'type'=>'text',
											'readonly' => 'readonly'
										)
									);
							 ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Met naam / zonder naam'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('CoffeeTicket.name',array(
                                                        'options'=>$coffeeRooms,
                                                        'class'=>'form-control',
                                                        'div'=>false,
                                                        'label'=>false,
                                                        'empty'=>'Selecteer',
                                                        'default'=>'',
                                                    ));
                            ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Aantal'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('CoffeeTicket.number',array(
																'class'=>'form-control',
																'div'=>false,
																'label'=>false,
																'type' => 'text'
															)
														);
						    ?>
						</div>
						<label class="col-sm-1 control-label"><?php echo __('Prijs'); ?></label>
						<div class="col-sm-2">
							 <?php echo $this->Form->input('CoffeeTicket.price',array(
		 										  'class'=>'form-control',
		 										  'div'=>false, 
		 										  'label'=>false,
		 										  'type' => 'text',
		 										   'readonly' => 'readonly'
		 										  )
		 									); 
								  ?>
						</div>
					</div>