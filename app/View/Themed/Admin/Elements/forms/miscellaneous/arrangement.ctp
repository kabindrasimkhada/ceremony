<h4 class="font-bold title-section text-primary"><?php echo __('Bloemstuk'); ?></h4>
					<?php $i=0;
						 while($i<=3){
					?>
						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo __('Bloemstuk'); ?></label>
							<div class="col-sm-4">
								<?php echo $this->Form->input("ArrangementData.{$i}.type",array(
	                                         //'options'=>$arrangements,
	                                         'class'=>'form-control',
	                                         'div'=>false,
	                                         'label'=>false,
	                                         'type' => 'text',
	                                         'empty'=>'Selecteer',
                                             'default'=>'0',
	                                   	 ));
							    ?>
							</div>
							<label class="col-sm-1 control-label"><?php echo __('Prijs'); ?></label>
							<div class="col-sm-2">
								<?php echo $this->Form->input("ArrangementData.{$i}.price",array(
		 										  'class'=>'form-control',
		 										  'div'=>false, 
		 										  'label'=>false,
		 										  'type' => 'text',
		 										  //'readonly' => 'readonly'
		 										  )
		 									); 
								  ?>
								
							</div>
						</div>
					
						<div class="form-group">
							<div class="col-sm-6">
								<?php echo $this->Form->input("ArrangementData.{$i}.remarks",array('class'=>'form-control','div'=>false,'label'=>false,'type' => 'textarea')); ?>
							</div>
						</div>
					<?php $i++;
					} ?>