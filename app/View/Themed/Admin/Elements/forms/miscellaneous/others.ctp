<h4 class="font-bold title-section text-primary"><?php echo __('Overige'); ?></h4>
<div class="form-group">
    <div class="col-sm-6">
							<?php echo $this->Form->input('miscellaneous',	
                                                                            array(
                                                                                            'class'=>'form-control',
                                                                                            'div'=>false,
                                                                                            'label'=>false,
                                                                                            'type'=>'textarea',
                                                                                            'rows'=> 4
                                                                                            )
                                                                                    ); 
							?>
    </div>
    <label class="col-sm-1 control-label"><?php echo __('Prijs'); ?></label>
    <div class="col-sm-2">
							<?php echo $this->Form->input('miscellaneous_price',
                                                                                        array('class'=>'form-control',
                                                                                        'div'=>false,
                                                                                        'label'=>false,
                                                                                        'type' => 'text'
                                                                                        )
                                                                             );
							 ?>
    </div>
</div>

<h4 class="font-bold title-section text-primary"><?php echo __('Boekje Dag lieve'); ?></h4>
<div class="form-group">
    <div class="col-sm-6"> 
							<?php echo $this->Form->input('booklet',	
										  array(
                                                                            'options'=>isset($booklets)?$booklets:'',
                                                                            'class'=>'form-control ',
                                                                            'div'=>false, 
                                                                            'label'=>false,
                                                                            'empty'=>'Selecteer',
                                                                            'default'=>'1',
                                                                            'type'=>'select'
                                                                    )
		                             	  );
							?>
    </div>
    <label class="col-sm-1 control-label"><?php echo __('Prijs'); ?></label>
    <div class="col-sm-2">
							<?php echo $this->Form->input('booklet_price',
                                                                                array(
                                                                                'class'=>'form-control',
                                                                                'div'=>false,
                                                                                'label'=>false,
                                                                                'type' => 'text',
                                                                                'readonly' => 'readonly'
                                                                                )
                                                                );
							 ?>
    </div>
</div>
<h4 class="font-bold title-section text-primary"><?php echo __('Gids na overlijden'); ?></h4>
<div class="form-group">
    <div class="col-sm-6">
							<?php echo $this->Form->input('guide_after_death',	
										  array(
		                                     		'options'=>isset($guide_after_death)?$guide_after_death:'',
			                                        'class'=>'form-control ',
			                                        'div'=>false, 
			                                        'label'=>false,
                                                                'empty'=>'Selecteer',
                                                                'default'=>'1',
                                                                'type'=>'select'
		                                        )
		                             	  );
							?>

    </div>
    <label class="col-sm-1 control-label"><?php echo __('Prijs'); ?></label>
    <div class="col-sm-2">
							<?php echo $this->Form->input('guide_after_death_price',
											array(
                                                                                            'class'=>'form-control',
                                                                                            'div'=>false,
                                                                                            'label'=>false,
                                                                                            'type' => 'text',
                                                                                            'readonly' => 'readonly'
                                                                                    )
										);
							 ?>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-6 control-label font-bold">
							<?php echo __('Voorganger uitvaart / crematie'); ?>
    </label>
    <label class="col-sm-1 control-label"><?php echo __('Prijs'); ?></label>
    <div class="col-sm-2">
							<?php echo $this->Form->input('cremation_price',
																array(
																		'class'=>'form-control',
																		'div'=>false,
																		'label'=>false,
																		'type' => 'text'
																	)
														);
							 ?>

    </div>
</div>
<div class="form-group">
    <label class="col-sm-6 control-label font-bold">
							<?php echo __('Hulp bij uitvaart'); ?>
    </label>
    <label class="col-sm-1 control-label"><?php echo __('Prijs'); ?></label>
    <div class="col-sm-2">
							<?php echo $this->Form->input('funeral_price',
																	array(
																		'class'=>'form-control',
																		'div'=>false,
																		'label'=>false,
																		'type' => 'text'
																	)
														);
							 ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-6 control-label font-bold">
							<?php echo __('Verzorging uitvaart en ondernemersloon'); ?>
    </label>
    <label class="col-sm-1 control-label"><?php echo __('Prijs'); ?></label>
    <div class="col-sm-2">
							<?php echo $this->Form->input('wage_price',
															array(
																'class'=>'form-control',
																'div'=>false,
																'label'=>false,
																'type' => 'text'
															)
														);
							 ?>
    </div>
</div>
