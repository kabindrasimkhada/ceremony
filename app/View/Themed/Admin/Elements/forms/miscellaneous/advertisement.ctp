<h4 class="font-bold title-section text-primary"><?php echo __('Advertentie'); ?></h4>
					<div class="form-group">
						<div class="col-sm-6">
							<?php echo $this->Form->input('advertisement',array('class'=>'form-control','div'=>false,'label'=>false,'type'=>'textarea','rows'=> 4)); ?>
						</div>
						<label class="col-sm-1 control-label"><?php echo __('Prijs'); ?></label>
						<div class="col-sm-2">
							<?php echo $this->Form->input('advertisement_price',
																				array(
																						'class'=>'form-control',
																						'div'=>false,
																						'label'=>false,
																						'type' => 'text'
									)
								);	
						     ?>
						</div>
					</div>