<h4 class="font-bold title-section text-primary"><?php echo __('Rouwbrieven'); ?></h4>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Selecteer categorie'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('FuneralLettersData.letter_name',array(
                                         'options'=>$funeralLettersDatas,
                                         'class'=>'form-control',
                                         'div'=>false,
                                         'label'=>false,
                                         'empty'=>'Selecteer',
                                         'default'=>'0',
                                   	 ));
						?>
							
						</div>
					</div>


					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Selecteer kaarten'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('FuneralLettersData.map_type',array(
                                         'options'=>'Selecteer kaarten',
                                         'class'=>'form-control',
                                         'div'=>false,
                                         'label'=>false,
                                         'empty'=>'Selecteer',
                                         'default'=>'0',
                                   	 ));
						?>
							
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Aantal'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('FuneralLettersData.number',
														array(
																'class'=>'form-control',
																'div'=>false,
																'label'=>false,
																'type' => 'text'
														)
												);
						 ?>
						</div>
						<label class="col-sm-1 control-label"><?php echo __('Prijs'); ?></label>
						<div class="col-sm-2">
							<?php echo $this->Form->input('FuneralLettersData.price',array(
		 										  'class'=>'form-control',
		 										  'div'=>false, 
		 										  'label'=>false,
		 										  'type' => 'text',
		 										  'readonly' => 'readonly'
		 										  )
		 								); 
							 ?>
							<?php echo $this->Form->input('funeralLettershidden',array(
																	'class'=>'form-control cardPrice',
																	'div'=>false,
																	'label'=>false,
																	'type'=>'hidden',

																)
															);
							?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Bijzonderheden'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('FuneralLettersData.remarks',array('class'=>'form-control','div'=>false,'label'=>false,'type'=>'textarea','rows'=> 4)); ?>
						</div>
						
					</div>