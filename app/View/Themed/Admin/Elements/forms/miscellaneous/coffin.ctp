<h4 class="font-bold title-section text-primary"><?php echo __('Grafkist type'); ?></h4>
	<div class="form-group">
	  <label class="col-sm-2 control-label"><?php echo __('Naam'); ?></label>
			<div class="col-sm-4">
				<?php echo $this->Form->input('CoffinsData.conffin_name',array(
                                         'options'=>$coffinsDatas,
                                         'class'=>'form-control',
                                         'div'=>false,
                                         'label'=>false,
                                         'empty'=>'Selecteer',
                                         'default'=>'0',
                                         'type'=>'select'
                                         
                                   	 ));
				?>
			</div>
	  <label class="col-sm-1 control-label"><?php echo __('Prijs'); ?></label>
      <div class="col-sm-2">
		<?php echo $this->Form->input('CoffinsData.price',array(
		 										  'class'=>'form-control',
		 										  'div'=>false, 
		 										  'label'=>false,
		 										  'type' => 'text',
		 										  'readonly' => 'readonly'
		 										  )
		 								); 
		 ?>
		</div>
  </div>