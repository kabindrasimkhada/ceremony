<h4 class="font-bold title-section text-primary"><?php echo __('Rouwcentrum'); ?></h4>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Naam'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('RouwcentrumsData.name',
													array(
							                            'options'=>$rouwcentrumsDatas,
							                            'class'=>'form-control',
							                            'div'=>false,
							                            'label'=>false,
							                            'empty'=>'Selecteer',
							                            'default'=>'1',
							                            )
													);
                          	?>
							
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Aantal staandagen'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('RouwcentrumsData.some_days',
		                                    array(
		                                        'class'=>'form-control',
		                                        'div'=>false,
		                                        'label'=>false,
		                                        'type' => 'text'
		                                    )
				                    ); 
        					?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo __('Prijs'); ?></label>
						<div class="col-sm-4">
							<?php echo $this->Form->input('RouwcentrumsData.price',
                                                            array(
                                                            'class'=>'form-control',
                                                            'div'=>false,
                                                            'label'=>false,
                                                            'readonly' => 'readonly',
                                                            'type' => 'text'

                                                            )
                                                    );
						    ?>
							<?php echo $this->Form->input('rowcentrumhidden',
													array(
													'class'=>'form-control rouwcentrumPrice',
													'div'=>false,
													'label'=>false,
													'type'=>'hidden'
												)
											);
							?>
						</div>
					<!-- 	< <label class="col-sm-1 control-label">Prijs</label>
						<div class="col-sm-2">
							<?php //echo $this->Form->input('RouwcentrumsData',array('class'=>'form-control','div'=>false,'label'=>false)); ?>
						</div>  -->
					</div>