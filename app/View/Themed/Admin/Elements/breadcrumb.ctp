<!-- Right side column. Contains the navbar and content of the page -->
			<aside class="right-side">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<?php if(isset($title) && !empty($title)):?>
						 <h1>
							<?php echo $title['title']; ?>
							<small><?php echo $title['explain']; ?></small>
						</h1>
					<?php endif; ?>	
					<ol class="breadcrumb">
						  <?php 
						  	 if(isset($breadCrumb) && !empty($breadCrumb)): 
						  		foreach($breadCrumb as $key=>$crumb):
	 							 	$seprator = !empty($breadCrumb[$key+1]) ? '>': '';
						   ?>
							  <li>
								 <a href="<?php echo $crumb['link'] ?>"><?php echo $crumb['name'];?></a>
						 	 </li>
						    
						<?php 	
								endforeach;
	                       endif;
						?> 
					</ol>
				</section>
		   </aside>     