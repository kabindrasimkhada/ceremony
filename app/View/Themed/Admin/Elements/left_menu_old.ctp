<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">                
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php echo $this->Html->image('../img/avatar.png', array('class' => 'img-circle')) ?>
            </div>
            <div class="pull-left info">
                <p><?php echo __('Hello, Admin') ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i>Online</a>
            </div>
        </div>
        <!-- search form -->
        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                    <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form> -->
        <!-- /.search form -->
        <?php
        $leftMenus = array();
        $defaultIcon = "fa-bars";
//        $leftMenus[] = array(
//            'label' => __('Dashboard'),
//            'link' => Router::url(array('controller' => 'users')),
//            'icon' => 'fa-dashboard'
//        );


            $leftMenus[] = array(
            'label' => __('Nieuw'),
            'link' => Router::url(array('controller' => 'clientDatas', 'action' => 'add')),
            'icon' => 'fa-user',
            'controller' => 'clients',

            );  

            $leftMenus[] =  array(
                                    'label' => __('concepten'),
                                    'link' => '#',
                                    'icon' => 'fa-lightbulb-o',
                                    'action' => 'admin_drafts',

                                    'childMenu' => array(
                                        array(
                                            'label' => __('Lijst concepten'),
                                            'link' => Router::url(array('controller' => 'clients', 'action' => 'drafts'))
                                        )
                                    ),
                                );

            $leftMenus[] =  array(
                            'label' => __('offertes'),
                            'link' => '#',
                            'icon' => 'fa-money',
                            'action' => 'admin_quotes',
                            'childMenu' => array(
                                array(
                                    'label' => __('Lijst offertes'),
                                    'link' => Router::url(array('controller' => 'clients', 'action' => 'quotes'))
                                )
                            ),
                        );

            $leftMenus[] =  array(
                            'label' => __('orders'),
                            'link' => '#',
                            'icon' => 'fa-file-text-o',
                            'action' => 'admin_orders',
                            'childMenu' => array(
                                array(
                                    'label' => __('Lijst orders'),
                                    'link' => Router::url(array('controller' => 'clients', 'action' => 'orders'))
                                )
                            ),
                        );

            $leftMenus[] = array(
            'label' => __('gebruikers'),
            'link' => '#',
            'icon' => 'fa-group',
            'controller' => 'users',
            'childMenu' => array(
                array(
                    'label' => __('Lijst gebruikers'),
                    'link' => Router::url(array('controller' => 'users', 'action' => 'index'))
                ),
                array(
                    'label' => __('Toevoegen gebruikers'),
                    'link' => Router::url(array('controller' => 'users', 'action' => 'addUsers')),
					'icon' => 'fa-plus'
                 )
               ),
            );

     

           $leftMenus[] = array(
                'label' => __('waardes'),
                'link' => '#',
                'icon' => 'fa-info-circle',
                'controller' => '',
                'childMenu' => array(
                                     array(
                                    'label' => __('Boekjes'),
                                    'link' => '#',
                                    'icon' => 'fa-book',
                                    'controller' => 'boekjes',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Lijst Boekjes'),
                                            'link' => Router::url(array('controller' => 'boekjes', 'action' => 'index', 'boekjes'))
                                        ),
                                        array(
                                            'label' => __('Toevoegen Boekjes'),
                                            'link' => Router::url(array('controller' => 'boekjes', 'action' => 'add'))
                                           )
                                        ),
                                    ),
                                    array(
                                        'label' => __('Kerkhoven'),
                                        'link' => '#',
                                        'icon' => 'fa-truck',
                                        'controller' => 'cemetries',
                                        'submenu' => array(
                                            array(
                                                'label' => __('Lijst Kerkhoven'),
                                                'link' => Router::url(array('controller' => 'cemetries', 'action' => 'index'))
                                            ),
                                            array(
                                                'label' => __('Toevoegen Kerkhoven'),
                                                'link' => Router::url(array('controller' => 'cemetries', 'action' => 'add'))
                                            )
                                        )
                                    ),
                                    array(
                                        'label' => __('doorgestuurd door'),
                                        'link' => '#',
                                        'icon' => 'fa-mail-forward',
                                        'controller' => 'forwardbies',
                                        'submenu' => array(
                                            array(
                                                'label' => __('Lijst doorgestuurd door'),
                                                'link' => Router::url(array('controller' => 'forwardbies', 'action' => 'index'))
                                            ),
                                            array(
                                                'label' => __('Toevoegen doorgestuurd door'),
                                                'link' => Router::url(array('controller' => 'forwardbies', 'action' => 'add'))
                                            )
                                        ),
                                    ),
                                    array(
                                        'label' => __('Doorsturen naar'),
                                        'link' => '#',
                                        'icon' => 'fa-send',
                                        'controller' => 'forwardTos',
                                        'submenu' => array(
                                            array(
                                                'label' => __('Lijst Of Doorsturen naar'),
                                                'link' => Router::url(array('controller' => 'forwardTos', 'action' => 'index'))
                                            ),
                                            array(
                                                'label' => __('Toevoegen  Doorsturen naar'),
                                                'link' => Router::url(array('controller' => 'forwardTos', 'action' => 'add'))
                                            )
                                        ),
                                    ),

                              
                            array(
                                    'label' => __('Rouwbrieven Typen'),
                                    'link' => '#',
                                    'icon' => 'fa-files-o',
                                    'controller' => 'funeralLetters',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Lijst Of Rouwbrieven'),
                                            'link' => Router::url(array('controller' => 'funeralLetters', 'action' => 'index'))
                                        ),
                                        array(
                                            'label' => __('Toevoegen  Rouwbrieven'),
                                            'link' => Router::url(array('controller' => 'funeralLetters', 'action' => 'add')),
											'icon' => 'fa-plus'
                                        )
                                    ),
                                ),
                             array(
                                    'label' => __('Kaarten'),
                                    'link' => '#',
                                    'icon' => ' fa-ticket',
                                    'controller' => 'funerlLetterCards',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Lijst Of Kaarten'),
                                            'link' => Router::url(array('controller' => 'funerlLetterCards', 'action' => 'index'))
                                        ),
                                        array(
                                            'label' => __('Toevoegen Kaarten'),
                                            'link' => Router::url(array('controller' => 'funerlLetterCards', 'action' => 'add')),
											'icon' => 'fa-plus'
                                        )
                                    ),
                                ),
                                 array(
                                    'label' => __('Koffietafelkaartje'),
                                    'link' => '#',
                                    'icon' => 'fa-coffee',
                                    'controller' => 'koffietafelkaartjes',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Lijst Of Koffietafelkaartje'),
                                            'link' => Router::url(array('controller' => 'koffietafelkaartjes', 'action' => 'index'))
                                        ),
                                        array(
                                            'label' => __('Toevoegen Koffietafelkaartje'),
                                            'link' => Router::url(array('controller' => 'koffietafelkaartjes', 'action' => 'add')),
											'icon' => 'fa-plus'
                                        )
                                    ),
                                ),
                               array(
                                    'label' => __('Gedachtenisprentjes'),
                                    'link' => '#',
                                    'icon' => 'fa-clipboard ',
                                    'controller' => 'prayerCardCategories',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Lijst Of Kaarten'),
                                            'link' => Router::url(array('controller' => 'prayerCardCategories', 'action' => 'index'))
                                        ),
                                        array(
                                            'label' => __('Toevoegen Kaarten'),
                                            'link' => Router::url(array('controller' => 'prayerCardCategories', 'action' => 'add')),
											'icon' => 'fa-plus'
                                        )
                                    ),
                                ),
                                 array(
                                    'label' => __('Gedachtenisprentjes Kaarten'),
                                    'link' => '#',
                                    'icon' => ' fa-file-text',
                                    'controller' => 'prayerCards',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Lijst Of Kaarten'),
                                            'link' => Router::url(array('controller' => 'prayerCards', 'action' => 'index'))
                                        ),
                                        array(
                                            'label' => __('Toevoegen Kaarten'),
                                            'link' => Router::url(array('controller' => 'prayerCards', 'action' => 'add')),
											'icon' => 'fa-plus'
                                        )
                                    ),
                                ),
                             array(
                                    'label' => __('Gereserveerd Types'),
                                    'link' => '#',
                                    'icon' => ' fa-edit',
                                    'controller' => 'reservedTypes',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Lijst Gereserveerd Types'),
                                            'link' => Router::url(array('controller' => 'reservedTypes', 'action' => 'index')),
                                        ),
                                        array(
                                            'label' => __('Toevoegen Gereserveerd Types'),
                                            'link' => Router::url(
                                                 array(
                                                        'controller' => 'reservedTypes',
                                                        'action' => 'add'
                                                )),
											'icon' => 'fa-plus'
                                        )
                                    )
                                ),
                              array(
                                    'label' => __('zorg Deceses'),
                                    'link' => '#',
                                    'icon' => ' fa-heart',
                                    'controller' => 'careDeceases',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Lijst zorg Deceses'),
                                            'link' => Router::url(array('controller' => 'careDeceases', 'action' => 'index'))
                                        ),
                                        array(
                                            'label' => __('Toevoegen zorg Deceses'),
                                            'link' => Router::url(array('controller' => 'careDeceases', 'action' => 'add')),
											'icon' => 'fa-plus'
                                        )
                                    ),
                                ),
                              array(
                                    'label' => __('thuis Opbarings'),
                                    'link' => '#',
                                    'icon' => ' fa-home',
                                    'controller' => 'homebarings',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Lijst thuis Opbarings'),
                                            'link' => Router::url(array('controller' => 'homebarings', 'action' => 'index'))
                                        ),
                                        array(
                                            'label' => __('Toevoegen thuis Opbarings'),
                                            'link' => Router::url(array('controller' => 'homebarings', 'action' => 'add')),
											'icon' => 'fa-plus'
                                        )
                                    ),
                                ),
                              array(
                                    'label' => __('ziekenhuizen'),
                                    'link' => '#',
                                    'icon' => ' fa-hospital-o',
                                    'controller' => 'Hospitals',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Lijst ziekenhuizen'),
                                            'link' => Router::url(array('controller' => 'hospitals', 'action' => 'index'))
                                        ),
                                        array(
                                            'label' => __('Toevoegen ziekenhuizen'),
                                            'link' => Router::url(array('controller' => 'hospitals', 'action' => 'add')),
											'icon' => 'fa-plus'
                                        )
                                    ),
                                ),
                              array(
                                    'label' => __('opnemen Types'),
                                    'link' => '#',
                                    'icon' => ' fa-save',
                                    'controller' => 'recordingTypes',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Lijst opnemen Types'),
                                            'link' => Router::url(array('controller' => 'recordingTypes', 'action' => 'index'))
                                        ),
                                        array(
                                            'label' => __('Toevoegen opnemen Types'),
                                            'link' => Router::url(array('controller' => 'recordingTypes', 'action' => 'add')),
											'icon' => 'fa-plus'
                                        )
                                    ),
                                ),
                               array(
                                    'label' => __('doodskisten'),
                                    'link' => '#',
                                    'icon' => ' fa-plus-square',
                                    'controller' => 'coffins',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Lijst doodskisten'),
                                            'link' => Router::url(array('controller' => 'coffins', 'action' => 'index'))
                                        ),
                                        array(
                                            'label' => __('Toevoegen doodskisten'),
                                            'link' => Router::url(array('controller' => 'coffins', 'action' => 'add')),
											'icon' => 'fa-plus'
                                        )
                                    ),
                                ),
                               array(
                                    'label' => __('Carriers'),
                                    'link' => '#',
                                    'icon' => ' fa-car',
                                    'controller' => 'Carriers',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Lijst Carriers'),
                                            'link' => Router::url(array('controller' => 'carriers', 'action' => 'index'))
                                        ),
                                        array(
                                            'label' => __('Toevoegen Carriers'),
                                            'link' => Router::url(array('controller' => 'carriers', 'action' => 'add'))
                                        ),
										'icon' => 'fa-plus'
                                    ),
                                ),
                              array(
                                    'label' => __('Rouwcentrums'),
                                    'link' => '#',
                                    'icon' => ' fa-cab',
                                    'controller' => 'Rouwcentrums',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Lijst Rouwcentrums'),
                                            'link' => Router::url(array('controller' => 'rouwcentrums', 'action' => 'index'))
                                        ),
                                        array(
                                            'label' => __('Toevoegen Rouwcentrums'),
                                            'link' => Router::url(array('controller' => 'rouwcentrums', 'action' => 'add')),
											'icon' => 'fa-plus'
                                        )
                                    ),
                                ),
                                array(
                                    'label' => __('Locatie'),
                                    'link' => '#',
                                    'icon' => ' fa-map-marker',
                                    'controller' => 'Locations',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Lijst Locatie'),
                                            'link' => Router::url(array('controller' => 'Locations', 'action' => 'index'))
                                        ),
                                        array(
                                            'label' => __('Toevoegen Locatie'),
                                            'link' => Router::url(array('controller' => 'Locations', 'action' => 'add')),
											'icon' => 'fa-plus'
                                        )
                                    ),
                                ),    
                                array(
                                    'label' => __('Regelingen'),
                                    'link' => '#',
                                    'icon' => ' fa-sitemap',
                                    'controller' => 'Arrangements',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Lijst Regelingen'),
                                            'link' => Router::url(array('controller' => 'arrangements', 'action' => 'index'))
                                        ),
                                        array(
                                            'label' => __('Toevoegen Regelingen'),
                                            'link' => Router::url(array('controller' => 'arrangements', 'action' => 'add')),
                                            'icon' => 'fa-plus'
                                        )
                                    ),
                                ),                          
                                array(
                                    'label' => __('stamps'),
                                    'link' => '#',
                                    'icon' => ' fa-map-marker',
                                    'controller' => 'postzeals',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Overzicht'),
                                            'link' => Router::url(array('controller' => 'postzeals', 'action' => 'index'))
                                        ),
                                        array(
                                            'label' => __('Toevoegen'),
                                            'link' => Router::url(array('controller' => 'postzeals', 'action' => 'add')),
                                            'icon' => 'fa-plus'
                                        )
                                    ),
                                ),
                                 array(
                                    'label' => __('booklets'),
                                    'link' => '#',
                                    'icon' => 'fa fa-book',
                                    'controller' => 'booklets',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Overzicht'),
                                            'link' => Router::url(array('controller' => 'booklets', 'action' => 'index'))
                                        ),
                                        array(
                                            'label' => __('Toevoege'),
                                            'link' => Router::url(array('controller' => 'booklets', 'action' => 'add')),
                                            'icon' => 'fa-plus'
                                        )
                                    ),
                                ),          
                                array(
                                    'label' => __('Guide After Death'),
                                    'link' => '#',
                                    'icon' => ' fa-xing',
                                    'controller' => 'guideAfterDeaths',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Overzicht'),
                                            'link' => Router::url(array('controller' => 'guideAfterDeaths', 'action' => 'index'))
                                        ),
                                        array(
                                            'label' => __('Toevoege'),
                                            'link' => Router::url(array('controller' => 'guideAfterDeaths', 'action' => 'add')),
                                            'icon' => 'fa-plus'
                                        )
                                    ),
                                ),
                                array(
                                    'label' => __('Undertaker'),
                                    'link' => '#',
                                    'icon' => ' fa-weibo',
                                    'controller' => 'undertakers',
                                    'submenu' => array(
                                        array(
                                            'label' => __('Overzicht'),
                                            'link' => Router::url(
                                                array(
                                                    'controller' => 'undertakers',
                                                    'action' => 'index'
                                                    )
                                                )
                                        ),
                                        array(
                                            'label' => __('Toevoege'),
                                            'link' => Router::url(
                                                array(
                                                'controller' => 'undertakers', 
                                                'action' => 'add'
                                            )
                                        ),
                                            'icon' => 'fa-plus'
                                      )
                                    ),
                                ),       
                              )
                            );
    
     
    ?>  


    <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">               <!--first level ul-->

            <?php foreach ($leftMenus as $leftMenu): //debug($leftMenus);die;?>
                <?php
                
                if (isset($this->request->params['controller'])) { 
                    if ($this->request->params['controller'] == 'users' && $this->action  != 'admin_dashboard') { 
                        
                        if ($this->request->params['action'] == 'admin_add_client' || $this->request->params['action'] == 'admin_edit_client') {
                            $paramsController = 'clients';
                        } else {
                            if (isset($this->request->params['pass'][0]) && $this->request->params['pass'][0] == 'clients') {
                                $paramsController = 'clients';
                            } else {
                                    if(isset($leftMenu['action']) && !empty($leftMenu['action'])){
                                            $paramsController = $this->request->params['action'];
                                            $chooser = $leftMenu['action'];
                                    }else{                        
                                            $paramsController = $this->request->params['controller'];
                                            $chooser = isset($leftMenu['childMenu']['controller'])?$leftMenu['childMenu']['controller']:$leftMenu['controller'];
                                  }
                            }
                        }
                    } elseif($this->action  != 'admin_dashboard') { 
                        if(isset($leftMenu['action']) && !empty($leftMenu['action'] )){
                                $paramsController = $this->request->params['action'];
                                $chooser = $leftMenu['action'];
                        }else{                    
                                $paramsController = $this->request->params['controller'];
                                $chooser = isset($leftMenu['childMenu']['controller'])?$leftMenu['childMenu']['controller']:$leftMenu['controller'];
                      }
                        
                    }
                   
                }
                
                //debug($paramsController);die;
                //$subController = isset($subMenu['controller'])?$subMenu['controller']:null;
                
               
              
            ?>
                <li class="<?php echo (isset($paramsController) && $paramsController == $chooser ) ? 'active' : '' ?> <?php echo (isset($leftMenu['childMenu'])) ? 'treeview' : ''; ?> ">
                    <a href="<?php echo $leftMenu['link'] ?>">
                        <i class="fa <?php echo isset($leftMenu['icon']) ? $leftMenu['icon'] : $defaultIcon; ?>"></i> <span><?php echo $leftMenu['label'] ?></span>
                 <?php echo (isset($leftMenu['childMenu'])) ? ' <i class="fa fa-angle-left pull-right"></i>' : ''; ?>
                    </a>
                     <?php if(isset($leftMenu['childMenu'])): ?>
                            <ul class="treeview-menu">   <!--second level ul-->
                                <?php foreach ($leftMenu['childMenu'] as $key => $subMenu):
                                            $subController = isset($subMenu['controller'])?$subMenu['controller']:null;
                                ?> 
                                        <li class="<?php echo (isset($paramsController) && $paramsController == $subController) ? 'active' : '' ?> <?php echo (isset($subMenu['submenu'])) ? 'treeview' : ''; ?>"> 
                                            <a href="<?php echo $subMenu['link'] ?>">
                                                 <i class="fa <?php echo isset($subMenu['icon']) ? $subMenu['icon'] : $defaultIcon; ?>"></i>
                                                  <span><?php echo $subMenu['label'] ?></span>
                                                 <?php echo (isset($subMenu['submenu'])) ? ' <i class="fa fa-angle-left pull-right"></i>' : ''; ?>
                                            </a>
                                               <?php if(isset($subMenu['submenu'])): ?>
                                                    <ul class="treeview-menu">   <!--third level ul-->
                                                      <?php foreach ($subMenu['submenu'] as $key => $subChildMenu):?> 
                                                           <li> 
                                                                 <a href="<?php echo $subChildMenu['link'] ?>"><i class="fa fa-angle-double-right"></i> <?php echo $subChildMenu['label'] ?></a>
                                                           </li>
                                                      <?php endforeach; ?>
                                                    </ul>
                                             <?php endif; ?>
                                        </li>
                               <?php endforeach; ?>
                            </ul>
                     <?php endif; ?>
                </li>
            <?php endforeach;?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<script>
    window.onload = function() {
      var activeNode = $('li.active');
      var menu = activeNode.parent().parent();
      var child = menu.children('a');
      menu.find('li.active').parent().slideDown(300, function() {
       menu.addClass('active');
       child.children('i.pull-right').addClass('fa-angle-down');
      });
    }
</script>