<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!-- header logo: style can be found in header.less -->
<header class="header">
    <!-- Add the class icon to your logo image or logo icon to add the margining -->
    <?php echo $this->Html->link(__('Ceromony Applications'), array('controller' => 'users', 'action' => 'dashboard'),array('class'=>'logo')); ?>
		
  
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>
    <?php $user = $this->Session->read('Auth.Admin'); ?>
    <div class="navbar-right">
        <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i>
                    <span><?php echo $user['username'] ?>  <i class="caret"></i></span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header bg-light-blue">
                        <?php echo $this->Html->image('avatar.png',array('class' => 'img-circle')) ?>
                        <p> 
                            <?php echo $user['username'] .' - '. $user['Group']['name'];   ?>
                            <small>Member since <?php echo $user['created'] ?> </small>
                        </p>
                    </li>
                    
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                             <?php echo $this->Html->link(__('Change Password'),array('controller' => 'users','action' => 'change_password'),array('class' => 'btn btn-default btn-flat')); ?>
                        </div>
                        <div class="pull-right">
                          
                            <?php echo $this->Html->link(__('Log Out'),array('controller' => 'users','action' => 'logout'),array('class' => 'btn btn-default btn-flat')); ?>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
</header>