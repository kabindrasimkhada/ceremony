<?php /* * */ ?>
<?php if (isset($errors) && !empty($errors)) { ?>
    <div class="box-body" id="messageElement" >
        <div class="callout callout-danger">
            <ul id="messageBox">
                <?php foreach ($errors as $field => $error) { ?>
                    <li><label for="<?php echo $field ?>" class="error"><?php echo $error[0]; ?></label></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <?php
} else {
    ?>
    <div class="box-body" id="messageElement" style="display:none">
        <div class="callout callout-danger">
            <ul id="messageBox">

            </ul>
        </div>
    </div>
    <?php
}?>