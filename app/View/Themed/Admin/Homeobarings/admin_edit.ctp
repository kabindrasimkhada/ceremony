<div class="homeobarings form">
<?php echo $this->Form->create('Homeobaring'); ?>
	<fieldset>
		<legend><?php echo __('Edit Homeobaring'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('days');
		echo $this->Form->input('per_price');
		echo $this->Form->input('total_price');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Homeobaring.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Homeobaring.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Homeobarings'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Miscellaneouses'), array('controller' => 'miscellaneouses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Miscellaneouse'), array('controller' => 'miscellaneouses', 'action' => 'add')); ?> </li>
	</ul>
</div>
