<div class="homeobarings index">
	<h2><?php echo __('Homeobarings'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('days'); ?></th>
			<th><?php echo $this->Paginator->sort('per_price'); ?></th>
			<th><?php echo $this->Paginator->sort('total_price'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($homeobarings as $homeobaring): ?>
	<tr>
		<td><?php echo h($homeobaring['Homeobaring']['id']); ?>&nbsp;</td>
		<td><?php echo h($homeobaring['Homeobaring']['name']); ?>&nbsp;</td>
		<td><?php echo h($homeobaring['Homeobaring']['days']); ?>&nbsp;</td>
		<td><?php echo h($this->App->formatPriceValue($homeobaring['Homeobaring']['per_price'])); ?>&nbsp;</td>
		<td><?php echo h($this->App->formatPriceValue($homeobaring['Homeobaring']['total_price'])); ?>&nbsp;</td>
		<td><?php echo h($homeobaring['Homeobaring']['created']); ?>&nbsp;</td>
		<td><?php echo h($homeobaring['Homeobaring']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $homeobaring['Homeobaring']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $homeobaring['Homeobaring']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $homeobaring['Homeobaring']['id']), null, __('Are you sure you want to delete # %s?', $homeobaring['Homeobaring']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Homeobaring'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Miscellaneouses'), array('controller' => 'miscellaneouses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Miscellaneouse'), array('controller' => 'miscellaneouses', 'action' => 'add')); ?> </li>
	</ul>
</div>
