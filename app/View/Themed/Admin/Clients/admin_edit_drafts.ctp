<div class="clients form">
<?php echo $this->Form->create('Client'); ?>
	<fieldset>
		<legend><?php echo __('Edit Client'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('client_data_id');
		echo $this->Form->input('decease_id');
		echo $this->Form->input('ceromony_info_id');
		echo $this->Form->input('entrepreneur_id');
		echo $this->Form->input('billing_address_id');
		echo $this->Form->input('farewell_service_id');
		echo $this->Form->input('picture_sound_id');
		echo $this->Form->input('coffee_room_id');
		echo $this->Form->input('miscellaneous_id');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
