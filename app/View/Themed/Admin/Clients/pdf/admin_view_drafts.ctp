<?php ob_start(); ?>
<?php $baseUrl = Router::url('/', true);?>
<?php $watermark = $baseUrl.'theme/Admin/img/watermark.jpg';?>
<html>
    <body>
        <style>
            body{
                color: #1A2B5C;
                font-family: sans-serif;
                padding:20px;
                background-image: url(<?php echo $watermark;?>);
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-position: center; 
                width: 100%; height: 100%;
            }
h1, h2, h3, h4, h5{
	font-family:sans-serif;
}
h1{
	font-size:1.1em;
}
h2{
	font-size:1.7em;
	
}
h3{
	font-size:1.1em;
	
}
h4{
	font-size:1.05em;
}
table{
	width:100%;
	margin:1em 0 2em 0;

}
table td, table th{
	font-size:0.8em;
	font-family:sans-serif;
	border:0px solid #000;
	text-align:left;
	padding:5px;
	vertical-align:middle;
}
table td{
	text-align:center;
}
.hr{
	border-top:0.5px solid #000000;
	margin:1em 0;
}
.clear { float: none; clear: both;}
.pdf-title { text-align: center; padding-bottom: 30px;}
h2.pdf-title span.pdf-title-tag { display: block; font-size: 13px; font-weight: normal;}
th.pdf-table-head {font-size: 12px; color: #999; padding-left: 5px; background-color: #1A2B5C; color:white;}
table.pdf-table tr td { text-align: left !important; font-size: 9px;}
table.table-header tr td { text-align: left;}
table.table-header tr td.table-right-td { padding-left: 370px;}
table h3.factuur { font-size: 24px;}
.factuur-left { padding-top: 26px;}
img { height: 80px; }
</style>
<div style="text-align:center;margin-top: 30px;">
    <img src="./theme/Admin/img/logo.jpg" alt="">
</div>
    <table class="table-header">
        <tr>
           <td class="factuur-left">
               <p>
                   <?php 
                   if(isset($Clientdatas['first_name'])){
                        echo $Clientdatas['first_name'].'<br/>';
                   }
                   ?>
                   <?php 
                   if(isset($Clientdatas['genus'])){
                       echo $Clientdatas['genus'];
                   }
                   ?>
               </p>
           </td>
           <td class="table-right-td">
               <h3 class="factuur"><?php echo __('Factuur'); ?></h3>
           </td>
        </tr>
        <tr>
           <td style="font-size:8px;">
               <p>
                <?php if(isset($Deceasesdatas['first_name']) && !empty($Deceasesdatas['first_name'])){ ?>
                   <?php echo $Deceasesdatas['first_name'];?><br/>
                <?php } ?>   
                <?php if(isset($Deceasesdatas['location']) && !empty($Deceasesdatas['location'])){ ?>
                    <?php echo $Deceasesdatas['location'];?>
                <?php } ?>
               </p>
           </td>
           <td class="table-right-td" style="font-size:8px;">
               <p>          
                <?php if(isset($Deceasesdatas['deathdate']) && !empty($Deceasesdatas['deathdate'])){ ?>
                    <?php echo dutchDateFormat($Deceasesdatas['deathdate']);?>
                <?php } ?>
               </p>
           </td>
        </tr>
        
    </table>
    
	<?php $map = array(
       	    'genus'=>__('Geslachtsnaam'),
       	    'genus_prefix'=>__('Voorvoegsels'),
       	    'first_name'=>__('Voornamen'),
       	    'nickname'=>__('Roepnaam'),
       	    'partner_name'=>__('Partnernaam'),
       	    'partner_prefix'=>__('Voorvoegsels'),
       	    'use_name'=>__('Naamgebruik'),
       	    'sex'=>__('Geslacht'),
       	    'BSN'=>__('BSN'),
       	    'brithdate'=>__('Geboortedatum'),
       	     'birthdate'=>__('Geboortedatum'),
       	    'birthplace'=>__('Geboorteplaats'),
       	    'zip_code'=>__('Postcode'),
       	    'number'=>__('Nummer'),
       	    'letter'=>__('Letter'),
       	    'add'=>__('Toev'),
       	    'tightening'=>__('Aand'),
       	    'street'=>__('Straat'),
       	    'location'=>__('Plaats (/land)'),
       	    'belief'=>__('Levensovertuiging'),
       	    'phone'=>__('Telefoon'),
       	    'mobile'=>__('Mobiel'),
       	    'email'=>__('E-mail adres'),
       	    'age' =>__( 'Leeftijd'),
			'martial_status' =>__( 'Burgerlijkestaat'),
			'father_name' =>__( 'Naam vader'),
			'mother_name' =>__( 'Naam moeder'),
			'wedding_booklet_included' =>__( 'Trouwboekje meegenomen'),
			'extract_death_certificate' =>__( 'Uitreksel overlijdensakte'),
			'name_of_declarant' =>__( 'Aangifte gedaan door (Naam aangever)'),
			'funerals' =>__( 'Uitvaartverzekering'),
			'forwarded_data_to' =>__( 'Gegevens doorsturen naar'),
       	    'relationship_to_deceased'=>__('Relatie tot overledene'),
       	    'crematorium' =>__( 'Crematorium'),
			'part_of' =>__( 'Onderdeel van'),
			'assignment_for' =>__( 'Opdracht voor'),
			'ceromony_date' =>__( 'Plechtigheidsdatum'),
			'theater' =>__( 'Aula'),
			'start_time' =>__( 'Aanvangstijd'),
			'duration' =>__( 'Tijdsduur'),
			'no_of_people' =>__( 'Aantal personen'),
			'att_info' =>__( 'Aanv. info'),
			'kistreg_nr' =>__( 'Kistreg.nr.'),
			'parish' =>__( 'Parochie'),
			'evening_vigil' =>__( 'Avondwake'),
			'funeral_chapel' =>__( 'Gebruik rouwkapel'),
			'ceromony_date' =>__( 'Plechtigheid datum'),
			'ceromony_time' =>__( 'Plechtigheid tijd'),
			'monthly_thoughts' =>__( 'Maandelijkse gedachten'),
			'cemetry' =>__( 'Begraafplaats'),
			'grave_number' =>__( 'Grafnummer'),
			'grafrechten' =>__( 'Grafrechten'),
			'complement_to_grafrechten' =>__( 'Grafrechten aanvullen tot'),
			'extends_to_ten_yrs' =>__( 'Grafrechten verlengen 10 jaar'),
			'remove_monuments' =>__( 'Monument verwijderen'),
			'grave_digging' =>__( 'Graf delven/dichten'),
			'funeral_company_type' =>__( 'Gegevens uitvaartonderneming'),
			'funeral_company' =>__( 'Uitvaartonderneming'),
			'funeral_caregiver' =>__( 'Uitvaartverzorger'),
			'relation' =>__( 'Relatie tot ovl.'),
			'reception' =>__( 'Ontvangst'),
			'goodbye_say' =>__( 'Vóór de afscheidsdienst afscheid nemen?'),
			'accompanied_by' =>__( 'Binnen begeleiden door'),
			'cross_post' =>__( 'Kruis plaatsen?'),
			'predecessor' =>__( 'Voorganger'),
			'last_to_leave' =>__( 'Familie als laatste de aula te verlaten?'),
			'floral' =>__( 'Bloemen'),
			'ramainder' =>__( 'Overige'),
			'reserve_seats' =>__( 'Plaatsen reserveren'),
			'reserve_spaces' =>__( 'Parkeerplaatsen reserveren'),
			'music_name' =>__( 'Muziek'),
			'image_recording' =>__( 'Wenst u beeldopname/PowerPoint te presenteren?'),
			'cd_dvd_recording' =>__( 'Wenst u een CD of DVD opname?'),
			'live_music' =>__( 'Live muziek'),
			'others' =>__( 'Overige'),
			'image_recording' =>__( 'beeldopname/PowerPoint te presenteren'),
			'neutral_backgrund_music' =>__( 'neutrale achtergrond muziek'),
			'no_of_people' =>__( 'Aantal personen'),
			'family_tbl_no' =>__( 'Familietafel aantal'),
			'moment_of_silence' =>__('Moment stilte/gebed'),
			'reserved' =>__( 'Gereserveerd'),
			'coffee_reserved' =>__( 'Koffietafel Gereserveerd'),
			'assignment_faxed'=>__('Opdracht gefaxt'),
			'deathdate'=>__('Overlijdensdatum'),
			'deathplace'=>__('Overlijdensplaats'),
			'pointoftime'=>__('Tijdstip'),
			'name'=>__('naam'),
			'Music'=>__('Muziek'),
			'advertisement'=>__('Advertentie'),
			'advertisement_price '=>__('Advertentie  Prijs'),
			'gratitude '=>__('Dankbetuiging'),
			'gratitude_price '=>__('Overlijdensdatum'),
			'casket_adornment'=>__('Kistversiering'),
			'casket_adornment_price'=>__('Kistversiering Prijs'),
			'miscellaneous '=>__('Diversen'),
			'miscellaneous_price '=>__('Diversen Prijs'),
			'booklet '=>__('Boekje Dag lieve'),
			'booklet_price '=>__('Boekje Dag lieve Prijs'),
			'guide_after_death'=>__('Gids na overlijden'),
			'forwarded_data_by' =>__('doorgezonden gegevens door'),
			'file_no' =>__('Dossier nr.'),
			'family_compensation' =>__('Familievergoeding'),
			'forwarded_by_status' =>__('doorgezonden gegevens door'),
			'funeral_status' =>__('doorgezonden gegevens door'),
			'reserve_seats_status' =>__('reserve zetels statuut'),
			'reserve_space_status' =>__('Parkeerplaatsen reserveren statuut'),
			);	

        $notRequired =	 array(
        						 'id',
        						'created',
        						'modified',
        						'ClientDatas',
        						'music_id',
        						'ceromony_data_id',
        						'chruch_data_id',
        						'cemetry_data_id',
        						'ClientData',
        						'CeromonyData',
        						'ChruchData',
        						'ChruchData',
        						'CemetryData'
        					);

        $yesNo = array(
        				'wedding_booklet_included',
        				'extract_death_certificate',
        				'funerals',
        				'forwarded_data_to',
        				'evening_vigil',
        				'funeral_chapel',
        				'monthly_thoughts',
        				'extends_to_ten_yrs',
        				'remove_monuments',
        				'grave_digging',
        				'assignment_faxed',
        				'goodbye_say',
        				'accompanied_by',
        				'predecessor',
        				'last_to_leave',
        				'image_recording',
        				'live_music',
        				'image_recording',
        				'live_music',
        				'moment_of_silence',
        				'reserved',
        				'cross_post'       
        	);
 ?>	

<!-- tab 9 -->
<?php
$Miscellaneousdatas_map = array(
    'conffin_name' => __('Naam'),
    'price' => __('Prijs'),
    'letter_name' => __('Rouwbrieven Naam'),
    'map_type' => __('Kaart'),
    'number' => __('Aantal'),
    'remarks' => __('Bijzonderheden'),
    'name' => __('Naam'),
    'name_with_price' => __('Prijs met naam'),
    'name_without_price' => __('Prijs zonder naam'),
    'price_per_piece' => __('Prijs per stuk'),
    'days' => __('Aantal staandagen'),
    'per_price' => __('Prijs per stuk'),
    'total_price' => __('Prijs'),
    'funerals_car_from' => __('Rouw auto van'),
    'funerals_car_to' => __('Rouw auto van Naar'),
    'price_netherland' => __('Prijs Nederland'),
    'advertisement_price' => __('Advertentie Prijs'),
    'gratitude' => __('Dankbetuiging'),
    'gratitude_price' => __('Dankbetuiging Prijs'),
    'miscellaneous_price' => __('Diversen Prijs'),
    'booklet' => __('Boekje Dag lieve'),
    'booklet_price' => __('Boekje Dag lieve Prijs'),
    'guide_after_death_price' => __('Gids na overlijden Prijs'),
    'cremation_price' => __('crematie Prijs'),
    'funeral_price' => __('uitvaart Prijs'),
    'wage_price' => __('Verzorging uitvaart en ondernemersloon Prijs'),
    'advertisement' => __('Advertentie'),
    'casket_adornment' => __('Kistversiering'),
    'casket_adornment_price' => __('Kistversiering Prijs'),
    'miscellaneous' => __('Diversen'),
    'guide_after_death' => __('Gids na overlijden'),
    'price_per_piecename' => __('Prijs per stuk'),
    'price_per_piecenumber' => __('Aantal'),
    'some_days' => __('Aantal staandagen'),
    'prayer_category_type' => __('Gedachtenisprentjes categorie'),
);
$notRequired = array(
    'coffins_data_id',
    'funeral_letters_data_id',
    'misboekjes_data_id',
    'stamp_id',
    'commemoration_id ',
    'homeobaring_id ',
    'hearse_id ',
    'carriers_data_id',
    'id',
    'prayer_cards_data_id',
    'coffee_ticket_id',
    'hospitals_data_id',
    'rouwcentrums_data_id',
    'care_decease_id',
    'arrangement_id',
    'florist_id',
    'created',
    'modified',
    'commemoration_id',
    'homeobaring_id',
    'hearse_id',
    'arrangement_data_id',
    'care_deceases_data_id',
);
$price_key = array(
    'funeral_price',
    'wage_price',
    'cremation_price',
    'booklet_price',
    'miscellaneous_price',
    'total_price',
    'per_price',
    'name_without_price',
    'name_with_price',
    'guide_after_death_price',
    'casket_adornment_price',
    'advertisement_price',
    'gratitude_price'
);
?>	

   
<?php if(!empty($coffinsData) || !empty($funeralLettersData) || !empty($PrayerCardsDatas) || !empty($misboekjesData) || !empty($coffeeTicket) || !empty($stamp) || !empty($commemoration) || !empty($hospitalsData) || !empty($rouwcentrum) || !empty($careDecease) || !empty($homeobaring) || !empty($hearse) || !empty($carriersData) || !empty($arrangements)  || !empty($Miscellaneousdatas)) :?>
<table cellpadding="0" cellspacing="0" class="pdf-table" >
    <tr>        
        <th class="pdf-table-head" width="70%"><?php echo __('Waarbij geleverd en verricht'); ?></th>
        <th class="pdf-table-head pdf-table-head2" width="15%"><?php echo __('Aantal'); ?></th>        
        <?php if(!isset($without_price)){ ?>
            <th class="pdf-table-head pdf-table-head2" width="15%"><?php echo __('Prijs'); ?></th>
        <?php } ?>

    </tr>
    
    <?php if (isset($coffinsData) && !empty($coffinsData) && isset($coffinsData['conffin_name'])): ?>
    <tr>
        <td width="70%"><?php echo $coffinsData['conffin_name'];?></td>
        <td width="15%"></td>                
        <?php if(!isset($without_price)){ ?>
            <td width="15%"><?php echo $this->App->formatPriceValue($coffinsData['price']);?></td>
        <?php } ?>
    </tr>	 
    <?php endif; ?>
    
    <?php if (isset($funeralLettersData) && !empty($funeralLettersData) && isset($funeralLettersData['letter_name'])): ?>
    <tr>
        <td width="70%"><?php echo $Miscellaneousdatas_map['letter_name'].' '.$funeralLettersData['letter_name'];?></td>
        <td width="15%"><?php echo $funeralLettersData['number'];?></td>
        <?php if(!isset($without_price)){ ?>
            <td width="15%"><?php echo $this->App->formatPriceValue($funeralLettersData['price']);?></td>
        <?php } ?>
    </tr>	 
    <?php endif; ?>

    <?php if (isset($PrayerCardsDatas) && !empty($PrayerCardsDatas) && isset($PrayerCardsDatas['prayer_category_type']) && isset($PrayerCardsDatas['card_type'])): ?>
        <tr>
            <td width="70%"><?php echo $Miscellaneousdatas_map['prayer_category_type'].' '.$PrayerCardsDatas['prayer_category_type'].', '.$PrayerCardsDatas['card_type'];;?></td>
            <td width="15%"><?php echo $PrayerCardsDatas['number'];?></td>
            <?php if(!isset($without_price)){ ?>
                <td width="15%"><?php echo $this->App->formatPriceValue($PrayerCardsDatas['price']);?></td>
            <?php } ?>
        </tr>	 
    <?php endif; ?>

    <?php if (isset($misboekjesData) && !empty($misboekjesData) && !empty($data) && isset($misboekjesData['price_per_piecename'])): ?>        
        <tr>
            <td width="70%"><?php echo $misboekjesData['price_per_piecename'];?></td>
            <td width="15%"><?php echo $misboekjesData['price_per_piecenumber'];?></td>
            <?php if(!isset($without_price)){ ?>
                <td width="15%"><?php echo $this->App->formatPriceValue($misboekjesData['total_price']);?></td>
            <?php } ?>
        </tr>	 
    <?php endif; ?>
        
    <?php if (isset($coffeeTicket) && !empty($coffeeTicket) && isset($coffeeTicket['name'])): ?>
        <tr>
            <td width="70%"><?php echo $coffeeTicket['name'];?></td>
            <td width="15%"><?php echo $coffeeTicket['number'];?></td>
            <?php if(!isset($without_price)){ ?>
                <td width="15%"><?php echo $this->App->formatPriceValue($coffeeTicket['price']);?></td>
            <?php } ?>
        </tr>	
    <?php endif; ?>

    <?php if (isset($stamp) && !empty($stamp) && isset($stamp['price_netherland'])): ?>
        <tr>
            <td width="70%"><?php echo __('Postzegels');?></td>
            <td width="15%"><?php echo $stamp['number'];?></td>
            <?php if(!isset($without_price)){ ?>
                <td width="15%"><?php echo $this->App->formatPriceValue($stamp['price']);?></td>
            <?php } ?>
        </tr>	
    <?php endif; ?>

    <?php if (isset($commemoration) && !empty($commemoration) && isset($commemoration['price_per_piece'])): ?>
        <tr>
            <td width="70%"><?php echo __('Condoleanceregister');?></td>
            <td width="15%"><?php echo $commemoration['number'];?></td>
            <?php if(!isset($without_price)){ ?>
                <td width="15%"><?php echo $this->App->formatPriceValue($commemoration['price']);?></td>
            <?php } ?>
        </tr>	
    <?php endif; ?>
        
    <?php if (isset($hospitalsData) && !empty($hospitalsData) && isset($hospitalsData['hospital_name'])): ?>
        <tr>
            <td width="70%"><?php echo __('Mortuarium ziekenhuis / verpleeghuis').', '.$hospitalsData['hospital_name'];?></td>
            <td width="15%"></td>
            <?php if(!isset($without_price)){ ?>
                <td width="15%"><?php echo $this->App->formatPriceValue($hospitalsData['price']);?></td>
            <?php } ?>
        </tr>	
    <?php endif; ?>

    <?php if (isset($rouwcentrum) && !empty($rouwcentrum) && isset($rouwcentrum['name'])): ?>
        <tr>
            <td width="70%"><?php echo __('Rouwcentrum').', '.$rouwcentrum['name'];?></td>
            <td width="15%"><?php echo $rouwcentrum['some_days'].' '; echo ($rouwcentrum['some_days'] >1)?__('dagen'):__('dag');?></td>
            <?php if(!isset($without_price)){ ?>
                <td width="15%"><?php echo $this->App->formatPriceValue($rouwcentrum['price']);?></td>
            <?php } ?>
        </tr>	
    <?php endif; ?>

    <?php if (isset($careDecease) && !empty($careDecease) && isset($careDecease['name'])): ?>
        <tr>
            <td width="70%"><?php echo $careDecease['name'];?></td>
            <td width="15%"></td>
            <?php if(!isset($without_price)){ ?>
                <td width="15%"><?php echo $this->App->formatPriceValue($careDecease['price']);?></td>
            <?php } ?>
        </tr>
    <?php endif; ?>
        
    <?php if (isset($homeobaring) && !empty($homeobaring) && isset($homeobaring['name'])): ?>
        <tr>
            <td width="70%"><?php echo __('Thuisopbaring').', '.$homeobaring['name'];?></td>
            <td width="15%"><?php echo $homeobaring['days'].' '; echo ($homeobaring['days'] >1)?__('dagen'):__('dag');?></td>
            <?php if(!isset($without_price)){ ?>
                <td width="15%"><?php echo $this->App->formatPriceValue($homeobaring['per_price']);?></td>
            <?php } ?>
        </tr>
    <?php endif; ?>

    <?php if (isset($hearse) && !empty($hearse) && isset($hearse['price'])): ?>
        <tr>
            <td width="70%">
                <?php echo __('Rouwauto');?><br/>
                    <?php if(isset($hearse['funerals_car_from'])){ ?>
                <?php echo $Miscellaneousdatas_map['funerals_car_from'].' : '.$hearse['funerals_car_from'];?><br/>
                <?php } ?>
                
                <?php if(isset($hearse['funerals_car_to'])) { ?>
                    <?php echo $Miscellaneousdatas_map['funerals_car_to'].' : '.$hearse['funerals_car_to'];?>
                                
                <?php } ?>
            </td>
            <td width="15%"></td>
            <?php if(!isset($without_price)){ ?>
                <td width="15%"><?php echo $this->App->formatPriceValue($hearse['price']);?></td>
            <?php } ?>
        </tr>
    <?php endif; ?>
        
    <?php if (isset($carriersData) && !empty($carriersData) && isset($carriersData['name'])): ?>
        <tr>            
            <td width="70%"><?php echo $carriersData['name'];?></td>
            <td><?php echo (!isset($carriersData['number']))?$carriersData['number']:0;?></td>
            <?php if(!isset($without_price)){ ?>
                <td width="15%"><?php echo $this->App->formatPriceValue($carriersData['price']);?></td>
            <?php } ?>
        </tr>
    <?php endif; ?>    

    <?php if (isset($arrangements) && !empty($arrangements)): ?>
        <?php foreach ($arrangements as $arrangement): ?>
            <?php if(!empty($arrangement['type']) || $arrangement['type'] != 0){ ?>
                <tr>            
                    <td width="70%"><?php echo __('Bloemstuk').', '.$arrangement['type'];?></td>
                    <td width="15%"></td>
                    <?php if(!isset($without_price)){ ?>
                        <td width="15%"><?php echo $this->App->formatPriceValue($arrangement['price']);?></td>
                    <?php } ?>
                </tr>
            <?php } elseif(empty($arrangement['type']) && $arrangement['price'] != 0) { ?>
                
                <tr>            
                    <td width="70%"><?php echo __('Bloemstuk');?></td>
                    <td width="15%"></td>
                    <?php if(!isset($without_price)){ ?>
                        <td width="15%"><?php echo $this->App->formatPriceValue($arrangement['price']);?></td>
                    <?php } ?>
                </tr>
                
            <?php }?>
        <?php endforeach; ?>		 
    <?php endif; ?>
        
        
        
    <?php     
    $miscellaneous_data = array(
        'advertisement',
        'gratitude',
        'casket_adornment',
        'miscellaneous',
        'booklet',
        'guide_after_death'
    );
    $miscellaneous_data_price = array(  
        'cremation_price',
        'funeral_price',
        'wage_price'
    );
    ?>        
    <?php if (isset($Miscellaneousdatas) && !empty($Miscellaneousdatas)): ?>
        <?php foreach ($miscellaneous_data as $data): ?>
            <?php if (isset($Miscellaneousdatas[$data]) && !empty($Miscellaneousdatas[$data])): ?>
                <tr>
                    <td width="70%">
                        <?php echo $Miscellaneousdatas_map[$data].', '.$Miscellaneousdatas[$data];?>
                    </td>
                    <td width="15%"></td>
                    <?php if(!isset($without_price)){ ?>
                        <td width="15%"><?php echo $this->App->formatPriceValue($Miscellaneousdatas[$data.'_price']);?></td>
                    <?php } ?>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>	
                
                       
	    <?php if(!isset($without_price)){ ?>        
            <?php foreach ($miscellaneous_data_price as $data): ?>
                <?php if (isset($Miscellaneousdatas[$data]) && !empty($Miscellaneousdatas[$data])): ?>
                    <tr>
                        <td width="70%"><?php echo $Miscellaneousdatas_map[$data];?></td>
                        <td width="15%"></td>
                        <td width="15%"><?php echo $this->App->formatPriceValue($Miscellaneousdatas[$data]);?></td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
	    <?php } ?>		
                
    <?php endif; ?>	   
                            
</table>


<?php if(!isset($without_price) && isset($Miscellaneousdatas['total_price']) && !empty($Miscellaneousdatas['total_price'])){ ?>
    <table class="table-header"> 
        <tr>
            <td width="85%"><strong><?php echo __('Door u te betalen');?></strong></td>
            <td width="15%"><?php echo $this->App->formatPriceValue($Miscellaneousdatas['total_price']);?></td>
        </tr>
    </table>  
<?php } ?> 

<?php endif; ?> 	 

    </body>
</html>
<?php ob_end_flush();?>