<div class="clients form">
<?php echo $this->Form->create('Client'); ?>
	<fieldset>
		<legend><?php echo __('Add Client'); ?></legend>
	<?php
		echo $this->Form->input('client_data_id');
		echo $this->Form->input('decease_id');
		echo $this->Form->input('ceromony_info_id');
		echo $this->Form->input('entrepreneur_id');
		echo $this->Form->input('billing_address_id');
		echo $this->Form->input('farewell_service_id');
		echo $this->Form->input('picture_sound_id');
		echo $this->Form->input('coffee_room_id');
		echo $this->Form->input('miscellaneous_id');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Clients'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Client Datas'), array('controller' => 'client_datas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client Data'), array('controller' => 'client_datas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Deceases'), array('controller' => 'deceases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Decease'), array('controller' => 'deceases', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ceromony Infos'), array('controller' => 'ceromony_infos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ceromony Info'), array('controller' => 'ceromony_infos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Entrepreneurs'), array('controller' => 'entrepreneurs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Entrepreneur'), array('controller' => 'entrepreneurs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Billing Addresses'), array('controller' => 'billing_addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Billing Address'), array('controller' => 'billing_addresses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Farewell Services'), array('controller' => 'farewell_services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Farewell Service'), array('controller' => 'farewell_services', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Picture Sounds'), array('controller' => 'picture_sounds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Picture Sound'), array('controller' => 'picture_sounds', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Coffee Rooms'), array('controller' => 'coffee_rooms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Coffee Room'), array('controller' => 'coffee_rooms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Miscellaneouses'), array('controller' => 'miscellaneouses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Miscellaneous'), array('controller' => 'miscellaneouses', 'action' => 'add')); ?> </li>
	</ul>
</div>
