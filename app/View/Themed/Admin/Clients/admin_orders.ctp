<?php if(!empty($clients) && isset($clients)):?>
<div class="row Orders">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <i class="glyphicon glyphicon-list-alt"></i>
                <h3 class="box-title"><?php echo __('Zoeken '); ?></h3>                                    
            </div><!-- /.box-header -->
            <!-- Flash Msg -->
            <?php echo $this->Form->create('Clients',array('class'=>'form-horizontal')); ?>
              <div class="col-sm-5">
                 <!--  <label for="name"><?php //echo __('Search By Name') ?></label> -->
                <?php
                echo $this->Form->input('searchByName', array(
                    'placeholder' => __('Search By Name'),
                    'class' => 'form-control',
                    'label'=>false
                ));
                ?>
            </div><!-- /.box-body -->
              <div class="col-sm-5">
                 <!--  <label for="name"><?php //echo __('Search By Data') ?></label> -->
                <?php
                echo $this->Form->input('searchByDate', array(
                    'placeholder' => __('Search By Date'),
                    'class' => 'form-control datepicker-control',
                    'label'=>false
                ));
                ?>
            </div><!-- /.box-body -->
                <div class="actions col-sm-2">
                    
                        <?php echo $this->Form->submit(__('Zoeken'),array('class'=>'btn btn-primary btn-block'));?>
                    
                </div>
        <?php echo $this->Form->end(); ?>
            <?php echo $this->Session->flash(); ?>           
            <?php //echo $this->Session->flash('auth'); ?>            <!--/Flash Msg -->
            
            <div class="box-body table-responsive">
              <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th><?php echo $this->Paginator->sort('id'); ?></th>
                                <th><?php echo __('klant naam'); ?></th>
                                <th><?php echo __('Geslachtsnaam'); ?></th>
                                <th><?php echo __('geboortedatum'); ?></th>
                                <th class="actions"><?php echo __('acties'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                         <?php foreach ($clients as $client): ?>
                                    <tr>
                                        <td><?php echo h($client['Client']['id']); ?>&nbsp;</td>
                                        <td>
                                            <?php echo $this->Html->link($client['ClientData']['first_name'], array('controller' => 'clients', 'action' => 'view', $client['Client']['id'])); ?>
                                        </td>
                                        <td>
                                            <?php echo __($client['ClientData']['genus']); ?>
                                        </td>
                                        <td>
                                            <?php echo __($client['ClientData']['birthdate']); ?>
                                        </td>
                                        <td class="actions">
                                        <?php echo $this->Html->link('<span class="fa fa-eye fa-fw"></span> '.__('Bekijk'), array(
                                                            'action' =>'view',
                                                            'orders',
                                                            $client['Client']['id']
                                                          ),array(
                                                             'class' => 'btn btn-sm btn-default',
                                                             'escape' => false
                                                        )
                                                    );
                                         ?>
                                         <?php echo $this->Html->link('<span class="fa fa-edit fa-fw"></span> '.__('Bewerken '), array(
                                                                                    'controller'=>'clientDatas',
                                                                                    'action'=>'edit',
                                                                                    $client['Client']['id'],
                                                                                    'admin' => true
                                                                                    ),array( 'class' => 'btn btn-sm btn-default', 'escape' => false)
                                                                    );
                                         ?>
                                        <?php echo $this->Form->postLink('<span class="fa fa-trash-o fa-fw"></span> '.__('Delete'), array(
                                                                'action' => 'delete',
                                                                 $client['Client']['id'],
                                                                  'orders' 
                                                                ),
                                                         array( 
                                                            'class' => 'btn btn-sm btn-default',
                                                             'escape' => false
                                                             ), 
                                                          __('Are you sure you want to delete # %s?',
                                                              $client['Client']['id']
                                                             )
                                                                                             
                                                        );
                                        ?>
                                         <?php echo $this->Html->link('<span class="fa  fa-file-pdf-o fa-fw"></span> '.__('Export PDF'), 
                                                                                    array(
                                                                                        'action' => 'view',
                                                                                        'with_price',
                                                                                        $client['Client']['id'],
                                                                                        'ext' => 'pdf'
                                                                                ),array( 'class' => 'btn btn-sm btn-default', 'escape' => false)
                                                                        ); 
                                         ?>
                                         
                                         <?php echo $this->Html->link('<span class="fa  fa-file-pdf-o fa-fw"></span> '.__('Export PDF Without price'), 
                                                                                        array(
                                                                                              'action' => 'view',
                                                                                              'with_out_price',
                                                                                               $client['Client']['id'],
                                                                                               'ext' => 'pdf'
                                                                                            ),array( 'class' => 'btn btn-sm btn-default', 'escape' => false)
                                                                          ); 
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                        </tbody>
                        <tfoot>
                            <tr>
                                <th><?php echo $this->Paginator->sort('id'); ?></th>
                                <th><?php echo __('klant naam'); ?></th>
                                <th><?php echo __('Geslachtsnaam'); ?></th>
                                <th><?php echo __('geboortedatum'); ?></th>
                                <th class="actions"><?php echo __('acties'); ?></th>
                            </tr>
                     </tfoot>
                </table>
                <?php echo $this->element('pagination'); ?>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<!--End Orders div-->
<?php else:?>
    <h3>There is no Orders</h3>
<?php endif;?>