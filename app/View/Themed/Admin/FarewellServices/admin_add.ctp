<?php echo $this->element('navigation', array('items' => 'tab6')); ?>	
<!-- Tab panes -->
<div class="tab-pane" id="tab6">
			<h1 class="title-large font-light"><?php echo __('Bijzonderheden afscheidsdienst');?></h1>
			<?php echo $this->Form->create('FarewellService',array( 
																	'class'=>'form-horizontal',
																	'novalidate' => true
																)
															);
			?>
				<div class="row">
					<div class="col-sm-7">
						<div class="form-group">
							<label class="control-label col-sm-5"><?php echo __('Ontvangst');?></label>
							<div class="col-sm-7">
								<?php echo $this->Form->input('reception',array('class'=>'form-control','div'=>false,'label'=>false)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-5"><?php echo __('Vóór de afscheidsdienst afscheid nemen');?> </label>
							<div class="col-sm-4">
								<?php echo $this->Form->input('goodbye_say',
							array(  
									'options'=>array('Nee','Ja'),
									'before'=>'<label class="radio-inline">',
									'type' => 'radio',
									'class' => 'radio-inline',
									'legend' => false,
									'div' => false,
   									'fieldset' => false,
   									 'separator'=>'</label><label class="radio-inline">',
   									'after' => '</label>',
								)
							 );
				 		?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-5"><?php echo __('Binnen begeleiden door');?></label>
							<div class="col-sm-4">
								<?php echo $this->Form->input('accompanied_by',
							array(  
									'options'=>array('Nee','Ja'),
									'before'=>'<label class="radio-inline">',
									'type' => 'radio',
									'class' => 'radio-inline',
									'legend' => false,
									'div' => false,
   									'fieldset' => false,
   									 'separator'=>'</label><label class="radio-inline">',
   									'after' => '</label>',
								)
							 );
				 		?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-5"><?php echo __('Kruis plaatsen');?></label>
							<div class="col-sm-4">
								<?php echo $this->Form->input('cross_post',
							array(  
									'options'=>array('Nee','Ja'),
									'before'=>'<label class="radio-inline">',
									'type' => 'radio',
									'class' => 'radio-inline',
									'legend' => false,
									'div' => false,
   									'fieldset' => false,
   									 'separator'=>'</label><label class="radio-inline">',
   									'after' => '</label>',
								)
							 );
				 		?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-5"><?php echo __('Voorganger');?></label>
							<div class="col-sm-4">
								<?php echo $this->Form->input('predecessor',
							array(  
									'options'=>array('Nee','Ja'),
									'before'=>'<label class="radio-inline">',
									'type' => 'radio',
									'class' => 'radio-inline',
									'legend' => false,
									'div' => false,
   									'fieldset' => false,
   									 'separator'=>'</label><label class="radio-inline">',
   									'after' => '</label>',
								)
							 );
				 		?>
								
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-5"><?php echo __('Familie als laatste de aula 
								te verlaten?');?></label>
							<div class="col-sm-4">
								<?php echo $this->Form->input('last_to_leave',
							array(  
									'options'=>array('Nee','Ja'),
									'before'=>'<label class="radio-inline">',
									'type' => 'radio',
									'class' => 'radio-inline',
									'legend' => false,
									'div' => false,
   									'fieldset' => false,
   									 'separator'=>'</label><label class="radio-inline">',
   									'after' => '</label>',
								)
							 );
				 		?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-5"><?php echo __('Bloemen');?></label>
							<div class="col-sm-7">
								<?php echo $this->Form->input('floral',array('class'=>'form-control','div'=>false,'label'=>false)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-5"><?php echo __('Overige');?></label>
							<div class="col-sm-7">
								<?php echo $this->Form->input('ramainder',array('class'=>'form-control','div'=>false,'label'=>false)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-5"><?php echo __('Plaatsen reserveren');?></label>
							<div class="col-sm-3">
								<?php echo $this->Form->input('reserve_seats_status',
							array(  
									'options'=>array('Nee','Ja'),
									'before'=>'<label class="radio-inline">',
									'type' => 'radio',
									'class' => 'radio-inline radioGroup1',
									'legend' => false,
									'div' => false,
   									'fieldset' => false,
   									 'separator'=>'</label><label class="radio-inline">',
   									'after' => '</label>',
								)
							 );
				 		?>
								
							</div>
							<div class="col-sm-4 dataradioGroup1">
								<div class="row">
									<label class="col-sm-3 control-label"><?php echo __('Aantal');?></label>
									<div class="col-sm-9">
										<?php echo $this->Form->input('reserve_seats',
												array(
													 	'class'=>'form-control',
													 	'div'=>false,
													 	'label'=>false,
													 	'type' => 'text'
													)
												);
									    ?>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-5"><?php echo __('Parkeerplaatsen reserveren');?></label>
							<div class="col-sm-3">
								<?php echo $this->Form->input('reserve_space_status',
							array(  
									'options'=>array('Nee','Ja'),
									'before'=>'<label class="radio-inline ">',
									'type' => 'radio',
									'class' => 'radio-inline radioGroup2',
									'legend' => false,
									'div' => false,
   									'fieldset' => false,
   									 'separator'=>'</label><label class="radio-inline">',
   									'after' => '</label>',
								)
							 );
				 		?>
							</div>
							<div class="col-sm-4 dataradioGroup2">
								<div class="row">
									<label class="col-sm-3 control-label"><?php echo __('Aantal');?></label>
									<div class="col-sm-9">
										<?php echo $this->Form->input('reserve_spaces',
													array(
														 	'class'=>'form-control',
														 	'div'=>false,
														 	'label'=>false,
														 	'type' => 'text'
														)
													);
									    ?>
									</div>
								</div>
							</div>
						</div>
						<div class="actions">
							<div class="pull-right">
								<?php echo $this->Form->submit(__('Opslaan'),array('class'=>'btn btn-primary'));?>

							</div>
						</div>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
		
</div>
<!-- end of 6th tab -->	

 <div id="bottom-nav">
 	<?php echo $this->element('bottom_nav'); ?>
 </div>