<div class="chruchDatas form">
<?php echo $this->Form->create('ChruchData'); ?>
	<fieldset>
		<legend><?php echo __('Add Chruch Data'); ?></legend>
	<?php
		echo $this->Form->input('client_datas_id');
		echo $this->Form->input('parish');
		echo $this->Form->input('evening_vigil');
		echo $this->Form->input('funeral_chapel');
		echo $this->Form->input('ceromony_date');
		echo $this->Form->input('ceromony_time');
		echo $this->Form->input('monthly_thoughts');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Chruch Datas'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Client Datas'), array('controller' => 'client_datas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client Datas'), array('controller' => 'client_datas', 'action' => 'add')); ?> </li>
	</ul>
</div>
