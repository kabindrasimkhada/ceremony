<div class="chruchDatas view">
<h2><?php echo __('Chruch Data'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($chruchData['ChruchData']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Client Datas'); ?></dt>
		<dd>
			<?php echo $this->Html->link($chruchData['ClientDatas']['id'], array('controller' => 'client_datas', 'action' => 'view', $chruchData['ClientDatas']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Parish'); ?></dt>
		<dd>
			<?php echo h($chruchData['ChruchData']['parish']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Evening Vigil'); ?></dt>
		<dd>
			<?php echo h($chruchData['ChruchData']['evening_vigil']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Funeral Chapel'); ?></dt>
		<dd>
			<?php echo h($chruchData['ChruchData']['funeral_chapel']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ceromony Date'); ?></dt>
		<dd>
			<?php echo h($chruchData['ChruchData']['ceromony_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ceromony Time'); ?></dt>
		<dd>
			<?php echo h($chruchData['ChruchData']['ceromony_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Monthly Thoughts'); ?></dt>
		<dd>
			<?php echo h($chruchData['ChruchData']['monthly_thoughts']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($chruchData['ChruchData']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($chruchData['ChruchData']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Chruch Data'), array('action' => 'edit', $chruchData['ChruchData']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Chruch Data'), array('action' => 'delete', $chruchData['ChruchData']['id']), null, __('Are you sure you want to delete # %s?', $chruchData['ChruchData']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Chruch Datas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Chruch Data'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Client Datas'), array('controller' => 'client_datas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client Datas'), array('controller' => 'client_datas', 'action' => 'add')); ?> </li>
	</ul>
</div>
