<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <!--Mobile first-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--IE Compatibility modes-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?php
        echo $this->Html->meta('icon');

        echo $this->Html->css(array('bootstrap.min', 'cleanadmin','font-awesome.min', 'layout'));

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        echo $this->Html->script(array('jquery.min.js', 'modernizr-build.min.js'));
        ?>
         <?php
        if (isset($language) && $language != '') {
            echo $this->Html->script(array('lang.nl'), array('fullBase' => true));
        }
        ?>
        <script type="text/javascript">
            var webpath = '<?php echo $this->webroot; ?>';
            var language = '<?php echo $language; ?>';
        </script>
    </head>
    <body class="skin-blue">
        <!-- Header ELement -->
        <?php echo $this->element('header'); ?>
        <!-- End Header ELement -->
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left Menu ELement -->
            <?php echo $this->element('left_menu'); ?>
            <!-- End Left Menu ELement -->

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">                
                <!-- Content Header (Page header) -->
                <?php /*
                <section class="content-header">
                    <h1>
                        <?php if(isset($this->request->params['controller'])){
                            echo ucwords($this->request->params['controller']). " " . __('Management');
                        }else{
                            echo __('Admin Management');
                        } ?>
                       
                    </h1>
                    <!--                    <ol class="breadcrumb">
                                            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                            <li><a href="#">Examples</a></li>
                                            <li class="active">Blank page</li>
                                        </ol>-->
                </section> */ ?>

                <!-- Main content -->
                <section class="content">
                    <?php echo $this->fetch('content'); ?>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->

        </div><!-- ./wrapper -->

        <?php
        echo $this->Html->script(array(
            'bootstrap.min.js',
            'jquery.validate.min.js',
            'cleanadmin.js',
            'main.js',
        ));
        ?>

        <?php echo $this->fetch('scriptBottom'); ?>
    </body>
</html>
