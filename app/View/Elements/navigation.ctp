<?php
$class = 'class="active"';
$client_id = isset($id) ? $id : null;
?>
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li <?php
    if ($items == 'tab1') {
        echo $class;
    }
    ?> data-rel='tab1' >
            <?php
            if (isset($client_id) && !empty($client_id)) {
                echo $this->Html->link(__('Opdrachtgever'), array(
                    'controller' => 'clientDatas',
                    'action' => 'edit',
                    'admin' => true,
                    $client_id
                        )
                );
            } else {
                echo $this->Html->link(__('Opdrachtgever'), array('controller' => 'clientDatas', 'action' => 'add'));
            }
            ?></li>
    <li <?php
            if ($items == 'tab2') {
                echo $class;
            }
            ?>>
            <?php
            if (isset($client_id) && !empty($client_id)) {
                echo $this->Html->link(__('Overledene'), array(
                    'controller' => 'deceases',
                    'action' => 'edit',
                    'admin' => true,
                    $client_id
                        )
                );
            } else {
                echo $this->Html->link(__('Overledene'), array('controller' => 'deceases', 'action' => 'add'));
            }
            ?>
    </li>
    <li <?php
            if ($items == 'tab3') {
                echo $class;
            }
            ?> >
            <?php
            if (isset($client_id) && !empty($client_id)) {
                echo $this->Html->link(__('Afscheid'), array(
                    'controller' => 'ceromonyInfos',
                    'action' => 'edit',
                    'admin' => true,
                    $client_id
                        )
                );
            } else {
                echo $this->Html->link(__('Afscheid'), array(
                    'controller' => 'ceromonyInfos',
                    'action' => 'add'
                        )
                );
            }
            ?>
    </li>
    <li <?php
            if ($items == 'tab4') {
                echo $class;
            }
            ?> >
            <?php
            if (isset($client_id) && !empty($client_id)) {
                echo $this->Html->link(__('Ondernemer'), array(
                    'controller' => 'entrepreneurs',
                    'action' => 'edit',
                    'admin' => true,
                    $client_id
                        )
                );
            } else {
                echo $this->Html->link(__('Ondernemer'), array(
                    'controller' => 'entrepreneurs',
                    'action' => 'add'
                        )
                );
            }
            ?>
    </li>
    <li <?php
            if ($items == 'tab6') {
                echo $class;
            }
            ?> >
            <?php
            if (isset($client_id) && !empty($client_id)) {
                echo $this->Html->link(__('Afscheidsdienst'), array(
                    'controller' => 'farewellServices',
                    'action' => 'edit',
                    'admin' => true,
                    $client_id
                        )
                );
            } else {
                echo $this->Html->link(__('Afscheidsdienst'), array(
                    'controller' => 'farewellServices',
                    'action' => 'add'
                        )
                );
            }
            ?>
    </li>
    <li <?php
            if ($items == 'tab7') {
                echo $class;
            }
            ?> >
            <?php
            if (isset($client_id) && !empty($client_id)) {
                echo $this->Html->link(__('Beeld en Geluid'), array(
                    'controller' => 'pictureSounds',
                    'action' => 'edit',
                    'admin' => true,
                    $client_id
                        )
                );
            } else {
                echo $this->Html->link(__('Beeld en Geluid'), array(
                    'controller' => 'pictureSounds',
                    'action' => 'add'
                        )
                );
            }
            ?>
    </li>
    <li <?php
            if ($items == 'tab8') {
                echo $class;
            }
            ?> >
            <?php
            if (isset($client_id) && !empty($client_id)) {
                echo $this->Html->link(__('Koffietafel'), array(
                    'controller' => 'coffeeRooms',
                    'action' => 'edit',
                    'admin' => true,
                    $client_id
                        )
                );
            } else {
                echo $this->Html->link(__('Koffietafel'), array(
                    'controller' => 'coffeeRooms',
                    'action' => 'add'
                        )
                );
            }
            ?>
    </li>
    <li <?php
            if ($items == 'tab9') {
                echo $class;
            }
            ?> >
    <?php
    if (isset($client_id) && !empty($client_id)) {
        echo $this->Html->link(__('Diversen'), array(
            'controller' => 'miscellaneouses',
            'action' => 'edit',
            'admin' => true,
            $client_id
                )
        );
    } else {
        echo $this->Html->link(__('Diversen'), array(
            'controller' => 'miscellaneouses',
            'action' => 'add'
                )
        );
    }
    ?>
    </li>
    <li <?php
            if ($items == 'tab10') {
                echo $class;
            }
            ?> >
<?php
if (isset($client_id) && !empty($client_id)) {
    echo $this->Html->link(__('overzicht'), array(
        'controller' => 'ClientDatas',
        'action' => 'overview',
        'admin' => true,
        $client_id
            )
    );
} else {
    echo $this->Html->link(__('overzicht'), array(
        'controller' => 'ClientDatas',
        'action' => 'overview',
            )
    );
}
?>
    </li>
</ul>
