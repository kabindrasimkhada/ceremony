<?php
class CreateBillingAddresesTables extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'billing_addresses' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 36, 'key' => 'primary'),
     				'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
     				'sex' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10),
     				'relation' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10),
     				'phone' => array('type' => 'integer', 'null'=>false,'default'=> null ,'length'=> 100),
     				'mobile' => array('type' => 'integer', 'null'=>false,'default'=> null ,'length'=> 100),
     				'zip_code' => array('type' => 'integer', 'null'=>false,'default'=> null ,'length'=> 60),
     				'number' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 36),
					'letter' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'add' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'tightening' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'street' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'location' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('column' => 'id', 'unique' => 1),
					),
					'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB'),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'billing_addresses'
			),
		),

	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
