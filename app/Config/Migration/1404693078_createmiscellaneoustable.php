<?php
class CreateMiscellaneousTable extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'miscellaneouses' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
     				'coffins_data_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11),
					'funeral_letters_data_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11),
					'prayer_cards_data_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11),
					'misboekjes_data_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11),
					'coffee_ticket_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11),
					'stamp_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11),
					'commemoration_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11),
					'hospitals_data_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11),
					'rouwcentrums_data_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11),
					'care_decease_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11),
					'homeobaring_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11),
					'hearse_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11),
					'carriers_data_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11),
					'arrangement_id'=> array('type' => 'text', 'null' => false, 'default' => null),
					'florist_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11),
					'advertisement'=> array('type' => 'text', 'null' => false, 'default' => null),
					'advertisement_price' => array('type' => 'float', 'null' => false, 'default' => null),
					'gratitude'=> array('type' => 'text', 'null' => false, 'default' => null),
					'gratitude_price' => array('type' => 'float', 'null' => false, 'default' => null),
					'casket_adornment'=> array('type' => 'text', 'null' => false, 'default' => null),
					'casket_adornment_price' => array('type' => 'float', 'null' => false, 'default' => null),
					'miscellaneous'=> array('type' => 'text', 'null' => false, 'default' => null),
					'miscellaneous_price' => array('type' => 'float', 'null' => false, 'default' => null),
					'booklet'=> array('type' => 'text', 'null' => false, 'default' => null),
					'booklet_price' => array('type' => 'float', 'null' => false, 'default' => null),
					'guide_after_death'=> array('type' => 'text', 'null' => false, 'default' => null),
					'guide_after_death_price' => array('type' => 'float', 'null' => false, 'default' => null),
					'cremation_price' => array('type' => 'float', 'null' => false, 'default' => null),
					'funeral_price' => array('type' => 'float', 'null' => false, 'default' => null),
					'wage_price' => array('type' => 'float', 'null' => false, 'default' => null),
					'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('column' => 'id', 'unique' => 1),
					),
					'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB'),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'miscellaneouses'
			),
		),

	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
