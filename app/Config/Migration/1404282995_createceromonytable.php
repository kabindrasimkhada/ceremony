<?php
class CreateCeromonyTable extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
	'up' => array(
			'create_table' => array(
				'ceromony_infos' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 36, 'key' => 'primary'),
					'ceromony_data_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 36),
					'chruch_data_id' =>array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 36),
					'cemetry_data_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 36),
					'assignment_faxed' => array('type'=>'string','null' => false,'default' => null,'length' => 50),
					'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('column' => 'id', 'unique' => 1),
					),
					'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB'),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'ceromony_infos'
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
