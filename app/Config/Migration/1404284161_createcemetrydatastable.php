<?php
class CreateCemetryDatasTable extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'cemetry_datas' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 36, 'key' => 'primary'),
					'cemetry' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'grave_number' =>array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20),
					'grafrechten' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20),
					'complement_to_grafrechten' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20),
					'extends_to_ten_yrs' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20),
					'remove_monuments' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20),
					'grave_digging' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20),
					'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('column' => 'id', 'unique' => 1),
					),
					'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB'),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'cemetry_datas'
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
