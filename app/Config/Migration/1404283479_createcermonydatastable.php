<?php
class CreateCermonyDatastable extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
	'up' => array(
			'create_table' => array(
				'ceromony_datas' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 36, 'key' => 'primary'),
					'crematorium' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'part_of' =>array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'assignment_for' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'ceromony_date' => array('type' => 'date', 'null' => true, 'default' => null),
					'theater' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'start_time' => array('type' => 'time', 'null' => true, 'default' => null),
					'duration' => array('type' => 'integer', 'null' => true, 'default' => null),
					'no_of_people' => array('type' => 'integer', 'null' => true, 'default' => null),
					'att_info' => array('type' => 'string', 'null' => true, 'default' => null,'length' => 50),
					'kistreg_nr' => array('type' => 'string', 'null' => true, 'default' => null,'length' => 50),
					'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('column' => 'id', 'unique' => 1),
					),
					'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB'),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'ceromony_datas'
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
