<?php
class CreateChruchesDatasTable extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'chruch_datas' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 36, 'key' => 'primary'),
					'parish' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'evening_vigil' =>array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20),
					'funeral_chapel' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20),
					'ceromony_date' => array('type' => 'date', 'null' => true, 'default' => null),
					'ceromony_time' => array('type' => 'time', 'null' => true, 'default' => null),
					'monthly_thoughts' => array('type' => 'string', 'null' => true, 'default' => null,'length' => 50),
					'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('column' => 'id', 'unique' => 1),
					),
					'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB'),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'chruch_datas'
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
