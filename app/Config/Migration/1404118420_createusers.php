<?php
class CreateUsers extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			 'create_table' => array(
			 	  'users' => array(
			 	  	  'id'		=>array(
				 	   	    'type'=> 'integer',
				 	   	    'null'=> 'false',
				 	   	    'default'=> null,
				 	   	    'length' =>36,
				 	   	    'key' =>'primary',
				 	   	    'auto-increment' => true
				 	   	),
			 	   	'first_name'	=> array(
			 	   	         'type'	=>'string',
			 	   	         'length' =>50,
			 	   	         'null'	=> false,
			 	   	         'default'=> null
			 	    	),
			 	    'last_name'	=> array(
			 	   	         'type'	=>'string',
			 	   	         'length' =>50,
			 	   	         'null'	=> false,
			 	   	         'default'=> null
			 	    	),
			 	    'email'=> array(
    						'type'  => 'string',
    						'null'	=> false,
    				  	),
			 	    'created' => array(
			 	   	        'type' => 'datetime'
			 	   	     ),
			 	   'modified' => array(
			 	   			'type' =>'datetime'
			 	   	),
			 	    'indexes' => array(
           				 'PRIMARY' => array(
			                'column' => 'id',
			                'unique' => 1
			            )
			        )
     			
			 	)
			 )
		),
		'down' => array(
			'drop_table'=> array('users')
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
