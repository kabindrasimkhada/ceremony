<?php
class CreateEntrepreneur extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'entrepreneurs' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 36, 'key' => 'primary'),
					'funeral_company_type' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'funeral_company' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'funeral_caregiver' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'zip_code' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 50),
					'number' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 36),
					'letter' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'add' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'tightening' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'street' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'location' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'phone' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20),
					'mobile' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 30),
					'email' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('column' => 'id', 'unique' => 1),
					),
					'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB'),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'entrepreneurs'
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
