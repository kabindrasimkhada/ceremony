<?php
class CreateClientData extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'client_datas' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 36, 'key' => 'primary'),
     				'genus' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'genus_prefixe'=> array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'first_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'nickname' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'partner_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'partner_prefix' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'use_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'sex' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10),
					'BSN' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'brithdate' => array('type' => 'date', 'null' => true, 'default' => null),
					'birthplace' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'zip_code' => array('type' => 'string', 'null'=>false,'default'=> null ,'length'=> 50),
					'number' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 36),
					'letter' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'street' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'location' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'phone' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 50),
					'mobile' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 50),
					'email' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'relationship_to_deceased'=> array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('column' => 'id', 'unique' => 1),
					),
					'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB'),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'client_datas'
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
