<?php
class CreateDeceasedTable extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'deceases' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 36, 'key' => 'primary'),
     				'genus' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'genus_prefixe'=> array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'first_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'nickname' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'partner_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'partner_prefix' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'use_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'sex' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10),
					'BSN' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'brithdate' => array('type' => 'date', 'null' => true, 'default' => null),
					'deathdate' => array('type' => 'date', 'null' => true, 'default' => null),
					'pointoftime' => array('type'=>'time', 'null' => true, 'default' => null),
					'birthplace' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'deathplace' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'zip_code' => array('type' => 'string', 'null'=>false,'default'=> null ,'length'=> 50),
					'number' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 36),
					'letter' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'add' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'tightening' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'street' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'location' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'belief' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'age' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 30),
					'martial_status' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'father_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'mother_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'wedding_booklet_included'=> array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'extract_death_certificate'=> array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'name_of_declarant'=> array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'funerals'=> array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'forwarded_data_to' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'forwarded_data_by' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
					'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('column' => 'id', 'unique' => 1),
					),
					'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB'),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'deceases'
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
