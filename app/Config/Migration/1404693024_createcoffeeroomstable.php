<?php
class CreateCoffeeRoomsTable extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'coffee_rooms' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 36, 'key' => 'primary'),
     				'duration' => array('type' => 'time', 'null' => false, 'default' => null),
     				'image_recording' => array('type' => 'boolean', 'null' => false, 'default' => null),
					'neutral_backgrund_music'=> array('type' => 'boolean', 'null' => false, 'default' => null),
					'no_of_people' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 25),
					'family_tbl_no' =>array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 25),
					'moment_of_silence' => array('type' => 'boolean', 'null' => false, 'default' => null),
					'reserved' =>  array('type' => 'boolean', 'null' => false, 'default' => null),
					'coffee_reserved' => array('type' => 'boolean', 'null' => false, 'default' => null),
					'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('column' => 'id', 'unique' => 1),
					),
					'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB'),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'coffee_rooms'
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
