<?php
class CreatePictureSoundsTable extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'picture_sounds' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 36, 'key' => 'primary'),
     				'music_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 36),
     				'image_recording' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50),
     				'cd_dvd_recording' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10),
     				'live_music' => array('type' => 'boolean', 'null' => false, 'default' => 0),
     				'others' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10),
     				'indexes' => array(
						'PRIMARY' => array('column' => 'id', 'unique' => 1),
					),
					'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB'),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'picture_sounds'
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
