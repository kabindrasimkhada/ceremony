<?php
App::uses('AppController', 'Controller');
/**
 * CoffinsDatas Controller
 *
 * @property CoffinsData $CoffinsData
 * @property PaginatorComponent $Paginator
 */
class CoffinsDatasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CoffinsData->recursive = 0;
		$this->set('coffinsDatas', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CoffinsData->exists($id)) {
			throw new NotFoundException(__('Invalid coffins data'));
		}
		$options = array('conditions' => array('CoffinsData.' . $this->CoffinsData->primaryKey => $id));
		$this->set('coffinsData', $this->CoffinsData->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CoffinsData->create();
			if ($this->CoffinsData->save($this->request->data)) {
				$this->Session->setFlash(__('The coffins data has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The coffins data could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CoffinsData->exists($id)) {
			throw new NotFoundException(__('Invalid coffins data'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CoffinsData->save($this->request->data)) {
				$this->Session->setFlash(__('The coffins data has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The coffins data could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CoffinsData.' . $this->CoffinsData->primaryKey => $id));
			$this->request->data = $this->CoffinsData->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CoffinsData->id = $id;
		if (!$this->CoffinsData->exists()) {
			throw new NotFoundException(__('Invalid coffins data'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CoffinsData->delete()) {
			$this->Session->setFlash(__('The coffins data has been deleted.'));
		} else {
			$this->Session->setFlash(__('The coffins data could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
