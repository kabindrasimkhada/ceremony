<?php
App::uses('AppController', 'Controller');
/**
 * Booklets Controller
 *
 * @property Booklet $Booklet
 * @property PaginatorComponent $Paginator
 */
class BookletsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Booklet->recursive = 0;
		$this->set('booklets', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Booklet->exists($id)) {
			throw new NotFoundException(__('Invalid booklet'));
		}
		$options = array('conditions' => array('Booklet.' . $this->Booklet->primaryKey => $id));
		$this->set('booklet', $this->Booklet->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Booklet->create();
			if ($this->Booklet->save($this->request->data)) {
				$this->Session->setFlash(__('The booklet has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The booklet could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Booklet->exists($id)) {
			throw new NotFoundException(__('Invalid booklet'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Booklet->save($this->request->data)) {
				$this->Session->setFlash(__('The booklet has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The booklet could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Booklet.' . $this->Booklet->primaryKey => $id));
			$this->request->data = $this->Booklet->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Booklet->id = $id;
		if (!$this->Booklet->exists()) {
			throw new NotFoundException(__('Invalid booklet'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Booklet->delete()) {
			$this->Session->setFlash(__('The booklet has been deleted.'));
		} else {
			$this->Session->setFlash(__('The booklet could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
