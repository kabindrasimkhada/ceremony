<?php
App::uses('AppController', 'Controller');
/**
 * ReservedTypes Controller
 *
 * @property ReservedType $ReservedType
 * @property PaginatorComponent $Paginator
 */
class ReservedTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->ReservedType->recursive = 0;
		$this->Paginator->settings = array(
								        'order'=>array('ReservedType.created'=>'DESC'),
								        'limit' => 10
									);
		$this->set('reservedTypes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->ReservedType->exists($id)) {
			throw new NotFoundException(__('Invalid reserved type'));
		}
		$options = array('conditions' => array('ReservedType.' . $this->ReservedType->primaryKey => $id));
		$this->set('reservedType', $this->ReservedType->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->ReservedType->create();
			if ($this->ReservedType->save($this->request->data)) {
				$this->Session->setFlash(__('The reserved type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The reserved type could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->ReservedType->exists($id)) {
			throw new NotFoundException(__('Invalid reserved type'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ReservedType->save($this->request->data)) {
				$this->Session->setFlash(__('The reserved type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The reserved type could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ReservedType.' . $this->ReservedType->primaryKey => $id));
			$this->request->data = $this->ReservedType->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->ReservedType->id = $id;
		if (!$this->ReservedType->exists()) {
			throw new NotFoundException(__('Invalid reserved type'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ReservedType->delete()) {
			$this->Session->setFlash(__('The reserved type has been deleted.'));
		} else {
			$this->Session->setFlash(__('The reserved type could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
