<?php
App::uses('AppController', 'Controller');
/**
 * ClientDatas Controller
 *
 * @property ClientData $ClientData
 * @property PaginatorComponent $Paginator
 */
class ClientDatasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');
    public $uses = array('Postcode','ClientData','FuneralLetter','PrayerCardCategory');


/**
 * add method
 *
 * @return void
 */
	public function admin_add($client_data_id=null,$Cid=null,$data = null){ 
	   	if ($this->request->is('post') || !empty($data)) {	
			$this->ClientData->create();
			if(!empty($data)){
				$this->request->data = $data;
				$this->ClientData->id = $client_data_id;
			}
			//debug($this->request->data['ClientData']);die;
			$val = $this->_emptyElementExists($this->request->data['ClientData']);
			if($val !== true){//if $val is false, no empty field
				$this->request->data['ClientData']['status'] = 1;
			}
            if($this->request->data['ClientData']['invoice'] == 1){ //when invoice is checked
                $val = $this->_emptyElementExists($this->request->data['BillingAddress']);
                if($val !== true){//if $val is false, no empty field
                    $this->request->data['BillingAddress']['status'] = 1;
                }
            }
            
			if ($this->ClientData->save($this->request->data)){   
                $billing_id = $this->ClientData->Client->BillingAddress->saveBillingAddress($this->request->data);
                if (empty($Cid)) {
                    $client_id = $this->ClientData->id;
                    $this->Client->save(
                            array(
                                'client_data_id' => $client_id,
                                'billing_address_id' => $billing_id,
                                'status' => '0'
                            )
                    );
                    $id = $this->Client->id;
                    $this->Session->setFlash(__('The client data has been saved.'));
                    return $this->redirect(array('controller' => 'deceases', 'action' => 'add', $id));
                } else {
                    $this->Client->id = $Cid;
                    $this->Client->save(
                            array(
                                'client_data_id' => $client_data_id,
                                'billing_address_id' => $billing_id
                            )
                    );
                    $id = $this->Client->id;
                    $this->Session->setFlash(__('The client data has been updated.'));
                    return $this->redirect(array('controller' => 'deceases', 'action' => 'edit', $id));
                }
						  
			} else {
				$this->Session->setFlash(__('The client data could not be saved. Please, try again.'));
			}
		}
		
	}



/**
 * overview method
 *
 * @return void
 */
	public function admin_overview($id = null){
	   if ($this->request->is('post')){
		      		$this->Client->create();
		      		$this->Client->id = $id;
		      		$status = $this->request->data['Client']['status'];
		      		if ($this->Client->saveField('status',$status)) {
						$this->Session->setFlash(__('The client status has been saved.'));
						return $this->redirect(array('action' => 'overview',$id));
					} else {
						$this->Session->setFlash(__('The client status could not be saved. Please, try again.'));
					}
				}
				$this->Client->recursive = 2;
				$client_info = $this->Client->find('first',array(
                    'conditions'=>array('Client.id'=>$id),
//                    'fields' => array(
//                        'ClientData.id',
//                        'ClientData.first_name',
//                        'ClientData.genus',
//                        'ClientData.street',
//                        'ClientData.zip_code',
//                        'ClientData.location',
//                        'Decease.first_name',
//                        'Decease.genus',
//                        'Decease.location',
//                        'Miscellaneouse.*'
//                    )
                ));
                $this->request->data['Client']['status'] = $client_info['Client']['status'];
				$Clientdatas = isset($client_info['ClientData'])?array_filter($client_info['ClientData']):null;
		   		$Deceasesdatas =  isset($client_info['Decease'])?array_filter($client_info['Decease']):null;
		   		$CeromonyInfodatas =  isset($client_info['CeromonyInfo'])?array_filter($client_info['CeromonyInfo']):null;
			    $assignment_faxed['assignment_faxed'] = isset($CeromonyInfodatas['assignment_faxed'])?$CeromonyInfodatas['assignment_faxed']:null;
			    $ceromonyDatas = isset($CeromonyInfodatas['CeromonyData'])?array_filter($CeromonyInfodatas['CeromonyData']):null;
			    $chruchDatas = isset($CeromonyInfodatas['ChruchData'])?array_filter($CeromonyInfodatas['ChruchData']):null;
			    $cemetryDatas = isset($CeromonyInfodatas['CemetryData'])?array_filter($CeromonyInfodatas['CemetryData']):null;
			    $Entrepreneurdatas =  isset($client_info['Entrepreneur'])?array_filter($client_info['Entrepreneur']):null;
			    $BillingAddressdatas =  isset($client_info['BillingAddress'])?array_filter($client_info['BillingAddress']):null;
			    $FarewellServicedatas =  isset($client_info['FarewellService'])?array_filter($client_info['FarewellService']):null;
			    $PictureSounddatas = isset( $client_info['PictureSound'])?array_filter( $client_info['PictureSound']):null;
			    $musics =  isset( $PictureSounddatas['Music']) ?$PictureSounddatas['Music']['music_name']:null;
			    if(!empty($musics)){
			    $musics = explode(',',$musics);
			    $musics = array_filter($musics);
			   	}
			    $CoffeeRoomdatas = isset($client_info['CoffeeRoom'])?array_filter($client_info['CoffeeRoom']):null;
			    $Miscellaneousdatas = isset($client_info['Miscellaneouse'])?array_filter($client_info['Miscellaneouse']):null;
			    //debug($Miscellaneousdatas);die;
			    $coffinsData = isset($Miscellaneousdatas['CoffinsData'])?array_filter($Miscellaneousdatas['CoffinsData']):null;
			    $funeralLettersData = isset($Miscellaneousdatas['FuneralLettersData'])?array_filter($Miscellaneousdatas['FuneralLettersData']):null;
				$this->FuneralLetter->recursive = -1;
				$letter_name = $this->FuneralLetter->find('first',
																			array(
																				'conditions'=>array('FuneralLetter.id' => isset($funeralLettersData['letter_name'])?$funeralLettersData['letter_name']:null),
																				'fields'   => 'letter_name'
													                        )
													                      );
				$funeralLettersData['letter_name'] = isset($letter_name['FuneralLetter']['letter_name'])?$letter_name['FuneralLetter']['letter_name']:null;
				$PrayerCardsDatas = isset($Miscellaneousdatas['PrayerCardsData'])?$Miscellaneousdatas['PrayerCardsData']:null;
			    $this->PrayerCardCategory->recursive = -1;
			    $prayerCategory = $this->PrayerCardCategory->find('first',
																		array(
																			'conditions'=>array(
																								'PrayerCardCategory.id' => isset($PrayerCardsDatas['prayer_category_type'])?$PrayerCardsDatas['prayer_category_type']:null
																							   ),
																			'fields'   => 'prayer_card'
												                        )	
															   );						
			$PrayerCardsDatas['prayer_category_type'] = isset($prayerCategory['PrayerCardCategory']['prayer_card'])?$prayerCategory['PrayerCardCategory']['prayer_card']:null;		
			$misboekjesData = isset($Miscellaneousdatas['MisboekjesData'])?array_filter($Miscellaneousdatas['MisboekjesData']):null;
		    $stamp = isset($Miscellaneousdatas['Stamp'])?array_filter($Miscellaneousdatas['Stamp']):null;
		    $commemoration = isset($Miscellaneousdatas['Commemoration'])?array_filter($Miscellaneousdatas['Commemoration']):null;
		    $hospitalsData =isset($Miscellaneousdatas['HospitalsData'])?array_filter($Miscellaneousdatas['HospitalsData']):null;
		    $rouwcentrum =isset($Miscellaneousdatas['RouwcentrumsData'])?array_filter($Miscellaneousdatas['RouwcentrumsData']):null;
		    $careDecease = isset($Miscellaneousdatas['CareDeceasesData'])?array_filter($Miscellaneousdatas['CareDeceasesData']):null;
		    $homeobaring = isset($Miscellaneousdatas['Homeobaring'])?array_filter($Miscellaneousdatas['Homeobaring']):null;
		    $hearse = isset($Miscellaneousdatas['Hearse'])?array_filter($Miscellaneousdatas['Hearse']):null;
		    $carriersData = isset($Miscellaneousdatas['CarriersData'])?array_filter($Miscellaneousdatas['CarriersData']):null;
		    $arrangements = isset($Miscellaneousdatas['ArrangementData'])?array_filter($Miscellaneousdatas['ArrangementData']):null;
		    $coffeeTicket = isset($Miscellaneousdatas['CoffeeTicket'])?array_filter($Miscellaneousdatas['CoffeeTicket']):null;
			$this->set(compact( 'Clientdatas',
		    					'Deceasesdatas',
		    					'CeromonyInfodatas',
		    					'Entrepreneurdatas',
		    					'FarewellServicedatas',
		    					'BillingAddressdatas',
		    					'FarewellServicedatas',
		    					'PictureSounddatas',
		    					'CoffeeRoomdatas',
		    					'Miscellaneousdatas',
		    					'musics',
		    					'coffinsData',
		    					'funeralLettersData',
		    					'PrayerCardsDatas',
		    					'misboekjesData',
		    					'stamp',
		    					'commemoration',
		    					'hospitalsData',
		    					'rouwcentrum',
		    					'careDecease',
		    					'homeobaring',
		    					'hearse',
		    					'carriersData',
		    					'arrangements',
		    					'coffeeTicket',
		    					'id'
	    					)
	                   );
    	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) { 
		   if (!$this->Client->exists($id)) {
					throw new NotFoundException(__('Invalid client data'));
			}
			$this->Client->Behaviors->attach("Containable");
	    	$this->Client->contain('ClientData','BillingAddress');
	    	$client_info = $this->Client->find('first',array(
                'conditions' => array(
                    'Client.id' => $id
                )
            ));
	    	$client_data_id = $client_info['ClientData']['id']; 
			if ($this->request->is(array('post', 'put'))){
		 		$this->admin_add($client_data_id,$id,$this->request->data);
		 	}
		 	//debug($client_info);die;
	    	$this->request->data = $client_info;
	    	$this->set(compact('id'));
			$this->render('admin_add');
	    }
		

	 public function  admin_dynamicLocationGeneartor(){
        $this->autoRender = false;
        if($this->request->isAjax()){
         if(isset($this->request->data) && !empty($this->request->data)){
            $postCode = str_replace(' ','',$this->request->data['postCode']);
            $code = substr($postCode,0,4);
            $char = strtoupper(substr($postCode,4,6));
            $postCode = $code . ' '. $char;
            if(!empty($postCode)){ 
                $street = $this->Postcode->find('all',
					                                  array(
					                                    'conditions'=>array('Postcode.postcode'=> $postCode),
					                                    'fields'   => array('street_name','city',)//'char','code')
					                                      )
					                           );
               
                echo json_encode(
                				  array(
                						'street_name'	=>	(isset($street) && !empty($street)) ? $street[0]['Postcode']['street_name'] : '',
                						'city' 		 	=>	(isset($street) && !empty($street)) ? $street[0]['Postcode']['city'] : '',
                						//'char' 		 	=>	$street[0]['Postcode']['char'],
                						//'code' 		 	=>	$street[0]['Postcode']['code']
                					   )
                				);
               
         
        }
      }
     }
   }

 

 /* Returns city that matches corresponding province.     
 */
    public function admin_getCities( $query ) {
     $this->layout = 'ajax';
     $this->autoRender = false;
     $postCodes = $this->Postcode->find('all', array(         
        'conditions' => array('Postcode.postcode like' => "%$query%" ),
        'fields' => array('DISTINCT(Postcode.postcode) AS postcode'),
        'limit' => 20
       )
      );
  
    $postCode = Set::classicExtract($postCodes, '{n}.Postcode.postcode');

     echo json_encode( $postCode );
    }

}
