<?php
App::uses('AppController', 'Controller');
/**
 * Deceases Controller
 *
 * @property Decease $Decease
 * @property PaginatorComponent $Paginator
 */
class DeceasesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');
    public $uses = array('ForwardBy','ForwardTo','Decease');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Decease->recursive = 0;
		$this->Paginator->settings = array(
								        'order'=>array('Decease.created'=>'DESC'),
								        'limit' => 10
									);
		$this->set('deceases', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Decease->exists($id)) {
			throw new NotFoundException(__('Invalid decease'));
		}
		$options = array('conditions' => array('Decease.' . $this->Decease->primaryKey => $id));
		$this->set('decease', $this->Decease->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add($id=null,$decease_id=null,$Cid=null,$data = null){
                if(is_null($id)){
			return $this->redirect(array('controller'=>'clientDatas','action' => 'add'));
		}
		if ($this->request->is('post') || !empty($data)) { 
			$this->Decease->create();
			if(!empty($data)){
				$this->request->data = $data;
				$this->Decease->id = $decease_id;
			}
			$death_certificate = $this->request->data['Decease']['extract_death_certificate'];
		    $death_certificate == 0 ? $this->Session->write('Decease.priceCertificate',1250):$this->Session->write('Decease.priceCertificate',null);
		    $val = $this->_emptyElementExists($this->request->data['Decease']);
			if($val !== true){
				$this->request->data['Decease']['status'] = 1;
			}
			if ($this->Decease->save($this->request->data)) {
				if(empty($Cid)){ 
				 	$decease_id = $this->Decease->id;
				 	$this->Client->id = $id;
				    $this->Client->saveField('decease_id',$decease_id);
				    $this->Session->setFlash(__('The decease has been saved.'));
					return $this->redirect(array('controller'=>'ceromonyInfos','action' => 'add',$this->Client->id));
			   }else{ 
			      $this->Client->id = $Cid;
			      $this->Session->setFlash(__('The decease data has been updated.'));
			      if(!isset($decease_id) && empty($decease_id)){
			      	$decease_id = $this->Decease->id;
			      	$this->Session->setFlash(__('The decease has been saved.'));
			      }
			      $this->Client->saveField(
				    	'decease_id',$decease_id
				    	);
			      $id = $this->Client->id;
			      return $this->redirect(
			      							array(
			      									'controller'=>'ceromonyInfos',
			      									'action' => 'edit',
			      									$id
			      								)
			      					);
			    }
			   
				
			} else {
				$this->Session->setFlash(__('The decease could not be saved. Please, try again.'));
			}
		}

		$forward_to_data = $this->ForwardTo->find('list',array('fields'=>array('name','name')));
		$forward_by_data = $this->ForwardBy->find('list',array('fields'=>array('name','name')));
		//debug($clientDatas);die;
		$this->set(compact('id','forward_to_data','forward_by_data'));
		
	}

/**
 * edit drafts method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null){ 
				   if (!$this->Client->exists($id)) {
							throw new NotFoundException(__('Invalid client data'));
					}
					$this->Client->Behaviors->attach("Containable");
			    	$this->Client->contain('Decease');
			    	$decease_info = $this->Client->find('first',array('conditions'=>array('Client.id'=>$id)));
			    	$decease_id = $decease_info['Decease']['id'];
			    	//debug($this->request->data);die; 
			    	if ($this->request->is(array('post', 'put'))){
				 		$this->admin_add('1',$decease_id,$id,$this->request->data );
				 	}
			    	//$this->Client->recursive = 2;

					//debug($client_info);die;
					//$clientDatas['ClientData'] = $client_info[0]['ClientData'];
					$forward_to_data = $this->ForwardTo->find('list',array('fields'=>array('name','name')));
					$forward_by_data = $this->ForwardBy->find('list',array('fields'=>array('name','name')));
					//debug($clientDatas);die;
					$this->set(compact('id','forward_to_data','forward_by_data'));
					$this->request->data = $decease_info;
					$this->render('admin_add');
			    }


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Decease->exists($id)) {
			throw new NotFoundException(__('Invalid decease'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Decease->save($this->request->data)) {
				$this->Session->setFlash(__('The decease has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The decease could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Decease.' . $this->Decease->primaryKey => $id));
			$this->request->data = $this->Decease->find('first', $options);
		}
		$clientDatas = $this->Decease->ClientDatum->find('list');
		$this->set(compact('clientDatas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Decease->id = $id;
		if (!$this->Decease->exists()) {
			throw new NotFoundException(__('Invalid decease'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Decease->delete()) {
			$this->Session->setFlash(__('The decease has been deleted.'));
		} else {
			$this->Session->setFlash(__('The decease could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
