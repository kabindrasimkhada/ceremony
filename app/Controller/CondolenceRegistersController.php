<?php
App::uses('AppController', 'Controller');
/**
 * CondolenceRegisters Controller
 *
 * @property CondolenceRegister $CondolenceRegister
 * @property PaginatorComponent $Paginator
 */
class CondolenceRegistersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->CondolenceRegister->recursive = 0;
		$this->set('condolenceRegisters', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->CondolenceRegister->exists($id)) {
			throw new NotFoundException(__('Invalid condolence register'));
		}
		$options = array('conditions' => array('CondolenceRegister.' . $this->CondolenceRegister->primaryKey => $id));
		$this->set('condolenceRegister', $this->CondolenceRegister->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->CondolenceRegister->create();
			if ($this->CondolenceRegister->save($this->request->data)) {
				$this->Session->setFlash(__('The condolence register has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The condolence register could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->CondolenceRegister->exists($id)) {
			throw new NotFoundException(__('Invalid condolence register'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CondolenceRegister->save($this->request->data)) {
				$this->Session->setFlash(__('The condolence register has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The condolence register could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CondolenceRegister.' . $this->CondolenceRegister->primaryKey => $id));
			$this->request->data = $this->CondolenceRegister->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->CondolenceRegister->id = $id;
		if (!$this->CondolenceRegister->exists()) {
			throw new NotFoundException(__('Invalid condolence register'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->CondolenceRegister->delete()) {
			$this->Session->setFlash(__('The condolence register has been deleted.'));
		} else {
			$this->Session->setFlash(__('The condolence register could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
