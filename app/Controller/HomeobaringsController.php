<?php
App::uses('AppController', 'Controller');
/**
 * Homeobarings Controller
 *
 * @property Homeobaring $Homeobaring
 * @property PaginatorComponent $Paginator
 */
class HomeobaringsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Homeobaring->recursive = 0;
		$this->set('homeobarings', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Homeobaring->exists($id)) {
			throw new NotFoundException(__('Invalid homeobaring'));
		}
		$options = array('conditions' => array('Homeobaring.' . $this->Homeobaring->primaryKey => $id));
		$this->set('homeobaring', $this->Homeobaring->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Homeobaring->create();
			if ($this->Homeobaring->save($this->request->data)) {
				$this->Session->setFlash(__('The homeobaring has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The homeobaring could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Homeobaring->exists($id)) {
			throw new NotFoundException(__('Invalid homeobaring'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Homeobaring->save($this->request->data)) {
				$this->Session->setFlash(__('The homeobaring has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The homeobaring could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Homeobaring.' . $this->Homeobaring->primaryKey => $id));
			$this->request->data = $this->Homeobaring->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Homeobaring->id = $id;
		if (!$this->Homeobaring->exists()) {
			throw new NotFoundException(__('Invalid homeobaring'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Homeobaring->delete()) {
			$this->Session->setFlash(__('The homeobaring has been deleted.'));
		} else {
			$this->Session->setFlash(__('The homeobaring could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
