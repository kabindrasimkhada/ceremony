<?php
App::uses('AppController', 'Controller');
/**
 * PrayerCategories Controller
 *
 * @property PrayerCategory $PrayerCategory
 * @property PaginatorComponent $Paginator
 */
class PrayerCategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->PrayerCategory->recursive = 0;
		$this->set('prayerCategories', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->PrayerCategory->exists($id)) {
			throw new NotFoundException(__('Invalid prayer category'));
		}
		$options = array('conditions' => array('PrayerCategory.' . $this->PrayerCategory->primaryKey => $id));
		$this->set('prayerCategory', $this->PrayerCategory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->PrayerCategory->create();
			if ($this->PrayerCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The prayer category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prayer category could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->PrayerCategory->exists($id)) {
			throw new NotFoundException(__('Invalid prayer category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->PrayerCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The prayer category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prayer category could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('PrayerCategory.' . $this->PrayerCategory->primaryKey => $id));
			$this->request->data = $this->PrayerCategory->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->PrayerCategory->id = $id;
		if (!$this->PrayerCategory->exists()) {
			throw new NotFoundException(__('Invalid prayer category'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->PrayerCategory->delete()) {
			$this->Session->setFlash(__('The prayer category has been deleted.'));
		} else {
			$this->Session->setFlash(__('The prayer category could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
