<?php
App::uses('AppController', 'Controller');
/**
 * Entrepreneurs Controller
 *
 * @property Entrepreneur $Entrepreneur
 * @property PaginatorComponent $Paginator
 */
class EntrepreneursController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');
	public $uses = array('Undertaker','Entrepreneur');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Entrepreneur->recursive = 0;
		$this->set('entrepreneurs', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Entrepreneur->exists($id)) {
			throw new NotFoundException(__('Invalid entrepreneur'));
		}
		$options = array('conditions' => array('Entrepreneur.' . $this->Entrepreneur->primaryKey => $id));
		$this->set('entrepreneur', $this->Entrepreneur->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add($id=null,$Entrepreneur_id=null,$Cid=null,$data = null){
                if(is_null($id)){
			return $this->redirect(array('controller'=>'clientDatas','action' => 'add'));
		}
		if ($this->request->is('post') || !empty($data)) {
			$this->Entrepreneur->create();
			if(!empty($data)){
				$this->request->data = $data;
				$this->Entrepreneur->id = $Entrepreneur_id;
			}
			$val = $this->_emptyElementExists($this->request->data['Entrepreneur']);
			if($val !== true){
				$this->request->data['Entrepreneur']['status'] = 1;
			}
			if ($this->Entrepreneur->save($this->request->data)) {
				if(empty($Cid)){
				 $entrepreneur_id = $this->Entrepreneur->id;
				//echo $Entrepreneur_id;die;
				$this->Client->id = $id;
			    $this->Client->saveField('entrepreneur_id',$entrepreneur_id);
				$this->Session->setFlash(__('The entrepreneur has been saved.'));
				return $this->redirect(array('controller'=>'farewellServices','action' => 'add',$this->Client->id));
			   }else{
			      $this->Client->id = $Cid;
			      $this->Session->setFlash(__('The entrepreneur data has been updated.'));
			      if(!isset($Entrepreneur_id) && empty($Entrepreneur_id)){
			      	$Entrepreneur_id = $this->Entrepreneur->id;
			      	$this->Session->setFlash(__('The entrepreneur has been saved.'));
				  }
			      $this->Client->saveField(
				    	'entrepreneur_id',$Entrepreneur_id
				    	);
			      $id = $this->Client->id;
			      return $this->redirect(array('controller'=>'farewellServices','action' => 'edit',$id));
			    }
			   
				
			} else {
				$this->Session->setFlash(__('The entrepreneur could not be saved. Please, try again.'));
			}
		}
		//$clientDatas = $this->Entrepreneur->ClientData->find('list');
		$this->set(compact('clientDatas'));
	}

/**
 * edit drafts method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
				   if (!$this->Client->exists($id)) {
							throw new NotFoundException(__('Invalid client data'));
					}
					$this->Client->Behaviors->attach("Containable");
			    	$this->Client->contain('Entrepreneur');
			    	$Entrepreneur_info = $this->Client->find('first',array('conditions'=>array('Client.id'=>$id)));
			    	$Entrepreneur_id = $Entrepreneur_info['Entrepreneur']['id']; 
					if ($this->request->is(array('post', 'put'))){
				 		$this->admin_add('1',$Entrepreneur_id,$id,$this->request->data);
				 	}
			    	//$this->Client->recursive = 2;

					//debug($client_info);die;
					//$clientDatas['ClientData'] = $client_info[0]['ClientData'];
					$this->request->data = $Entrepreneur_info;
					$this->set(compact('id'));
					$this->render('admin_add');
			    }


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Entrepreneur->exists($id)) {
			throw new NotFoundException(__('Invalid entrepreneur'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Entrepreneur->save($this->request->data)) {
				$this->Session->setFlash(__('The entrepreneur has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The entrepreneur could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Entrepreneur.' . $this->Entrepreneur->primaryKey => $id));
			$this->request->data = $this->Entrepreneur->find('first', $options);
		}
		$clientDatas = $this->Entrepreneur->ClientData->find('list');
		$this->set(compact('clientDatas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Entrepreneur->id = $id;
		if (!$this->Entrepreneur->exists()) {
			throw new NotFoundException(__('Invalid entrepreneur'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Entrepreneur->delete()) {
			$this->Session->setFlash(__('The entrepreneur has been deleted.'));
		} else {
			$this->Session->setFlash(__('The entrepreneur could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


 

 /* Returns name that matches corresponding character.     
 */
    public function admin_getUndertakerNames( $query ) {
     $this->layout = 'ajax';
     $this->autoRender = false;
     $names = $this->Undertaker->find('all', array(         
        'conditions' => array('Undertaker.name like' => "%$query%" ),
        'fields' => array('DISTINCT(Undertaker.name ) AS name'),
        'limit' => 20
       )
      );
  
    $names = Set::classicExtract($names, '{n}.Undertaker.name');

     echo json_encode( $names );
    }
}
