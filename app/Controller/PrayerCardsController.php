<?php
App::uses('AppController', 'Controller');
/**
 * PrayerCards Controller
 *
 * @property PrayerCard $PrayerCard
 * @property PaginatorComponent $Paginator
 */
class PrayerCardsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->PrayerCard->recursive = 0;
		$this->Paginator->settings = array(
								        'order'=>array('PrayerCard.created'=>'DESC'),
								        'limit' => 10
									);
		$this->set('prayerCards', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->PrayerCard->exists($id)) {
			throw new NotFoundException(__('Invalid prayer card'));
		}
		$options = array('conditions' => array('PrayerCard.' . $this->PrayerCard->primaryKey => $id));
		$this->set('prayerCard', $this->PrayerCard->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->PrayerCard->create();
			if ($this->PrayerCard->save($this->request->data)) {
				$this->Session->setFlash(__('The prayer card has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prayer card could not be saved. Please, try again.'));
			}
		}
		
		$prayerCardCategories = $this->PrayerCard->PrayerCardCategory->find('list',array('fields'=>array('id','prayer_card')));
		//debug($prayerCardCategories);die;
		$this->set(compact('prayerCardCategories'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->PrayerCard->exists($id)) {
			throw new NotFoundException(__('Invalid prayer card'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->PrayerCard->save($this->request->data)) {
				$this->Session->setFlash(__('The prayer card has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prayer card could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('PrayerCard.' . $this->PrayerCard->primaryKey => $id));
			$this->request->data = $this->PrayerCard->find('first', $options);
		}
		$prayerCardCategories = $this->PrayerCard->PrayerCardCategory->find('list',array('fields'=>array('id','prayer_card')));
		$this->set(compact('prayerCardCategories'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->PrayerCard->id = $id;
		if (!$this->PrayerCard->exists()) {
			throw new NotFoundException(__('Invalid prayer card'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->PrayerCard->delete()) {
			$this->Session->setFlash(__('The prayer card has been deleted.'));
		} else {
			$this->Session->setFlash(__('The prayer card could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
