<?php
App::uses('AppController', 'Controller');
/**
 * Arrangements Controller
 *
 * @property Arrangement $Arrangement
 * @property PaginatorComponent $Paginator
 */
class ArrangementsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function  admin_index() {
		$this->Arrangement->recursive = 0;
		$this->set('arrangements', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Arrangement->exists($id)) {
			throw new NotFoundException(__('Invalid arrangement'));
		}
		$options = array('conditions' => array('Arrangement.' . $this->Arrangement->primaryKey => $id));
		$this->set('arrangement', $this->Arrangement->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function  admin_add() {
		if ($this->request->is('post')) {
			$this->Arrangement->create();
			if ($this->Arrangement->save($this->request->data)) {
				$this->Session->setFlash(__('The arrangement has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The arrangement could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function  admin_edit($id = null) {
		if (!$this->Arrangement->exists($id)) {
			throw new NotFoundException(__('Invalid arrangement'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Arrangement->save($this->request->data)) {
				$this->Session->setFlash(__('The arrangement has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The arrangement could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Arrangement.' . $this->Arrangement->primaryKey => $id));
			$this->request->data = $this->Arrangement->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function  admin_delete($id = null) {
		$this->Arrangement->id = $id;
		if (!$this->Arrangement->exists()) {
			throw new NotFoundException(__('Invalid arrangement'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Arrangement->delete()) {
			$this->Session->setFlash(__('The arrangement has been deleted.'));
		} else {
			$this->Session->setFlash(__('The arrangement could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
