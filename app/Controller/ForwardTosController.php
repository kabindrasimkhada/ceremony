<?php
App::uses('AppController', 'Controller');
/**
 * ForwardTos Controller
 *
 * @property ForwardTo $ForwardTo
 * @property PaginatorComponent $Paginator
 */
class ForwardTosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->ForwardTo->recursive = 0;
		$this->Paginator->settings = array(
								        'order'=>array('ForwardTo.created'=>'DESC'),
								        'limit' => 10
									);
		$this->set('forwardTos', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->ForwardTo->exists($id)) {
			throw new NotFoundException(__('Invalid forward to'));
		}
		$options = array('conditions' => array('ForwardTo.' . $this->ForwardTo->primaryKey => $id));
		$this->set('forwardTo', $this->ForwardTo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->ForwardTo->create();
			if ($this->ForwardTo->save($this->request->data)) {
				$this->Session->setFlash(__('The forward to has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The forward to could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->ForwardTo->exists($id)) {
			throw new NotFoundException(__('Invalid forward to'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ForwardTo->save($this->request->data)) {
				$this->Session->setFlash(__('The forward to has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The forward to could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ForwardTo.' . $this->ForwardTo->primaryKey => $id));
			$this->request->data = $this->ForwardTo->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->ForwardTo->id = $id;
		if (!$this->ForwardTo->exists()) {
			throw new NotFoundException(__('Invalid forward to'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ForwardTo->delete()) {
			$this->Session->setFlash(__('The forward to has been deleted.'));
		} else {
			$this->Session->setFlash(__('The forward to could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
