<?php
App::uses('AppController', 'Controller');
/**
 * Rouwcentrums Controller
 *
 * @property Rouwcentrum $Rouwcentrum
 * @property PaginatorComponent $Paginator
 */
class RouwcentrumsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Rouwcentrum->recursive = 0;
		$this->Paginator->settings = array(
								        'order'=>array('Rouwcentrum.created'=>'DESC'),
								        'limit' => 10
									);
		$this->set('rouwcentrums', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Rouwcentrum->exists($id)) {
			throw new NotFoundException(__('Invalid rouwcentrum'));
		}
		$options = array('conditions' => array('Rouwcentrum.' . $this->Rouwcentrum->primaryKey => $id));
		$this->set('rouwcentrum', $this->Rouwcentrum->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Rouwcentrum->create();
			if ($this->Rouwcentrum->save($this->request->data)) {
				$this->Session->setFlash(__('The rouwcentrum has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The rouwcentrum could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Rouwcentrum->exists($id)) {
			throw new NotFoundException(__('Invalid rouwcentrum'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Rouwcentrum->save($this->request->data)) {
				$this->Session->setFlash(__('The rouwcentrum has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The rouwcentrum could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Rouwcentrum.' . $this->Rouwcentrum->primaryKey => $id));
			$this->request->data = $this->Rouwcentrum->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Rouwcentrum->id = $id;
		if (!$this->Rouwcentrum->exists()) {
			throw new NotFoundException(__('Invalid rouwcentrum'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Rouwcentrum->delete()) {
			$this->Session->setFlash(__('The rouwcentrum has been deleted.'));
		} else {
			$this->Session->setFlash(__('The rouwcentrum could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
