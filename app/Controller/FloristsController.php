<?php
App::uses('AppController', 'Controller');
/**
 * Florists Controller
 *
 * @property Florist $Florist
 * @property PaginatorComponent $Paginator
 */
class FloristsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Florist->recursive = 0;
		$this->Paginator->settings = array(
								        'order'=>array('Florist.created'=>'DESC'),
								        'limit' => 10
									);
		$this->set('florists', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Florist->exists($id)) {
			throw new NotFoundException(__('Invalid florist'));
		}
		$options = array('conditions' => array('Florist.' . $this->Florist->primaryKey => $id));
		$this->set('florist', $this->Florist->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Florist->create();
			if ($this->Florist->save($this->request->data)) {
				$this->Session->setFlash(__('The florist has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The florist could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Florist->exists($id)) {
			throw new NotFoundException(__('Invalid florist'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Florist->save($this->request->data)) {
				$this->Session->setFlash(__('The florist has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The florist could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Florist.' . $this->Florist->primaryKey => $id));
			$this->request->data = $this->Florist->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Florist->id = $id;
		if (!$this->Florist->exists()) {
			throw new NotFoundException(__('Invalid florist'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Florist->delete()) {
			$this->Session->setFlash(__('The florist has been deleted.'));
		} else {
			$this->Session->setFlash(__('The florist could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
