<?php
App::uses('AppController', 'Controller');
/**
 * PrayerCardsDatas Controller
 *
 * @property PrayerCardsData $PrayerCardsData
 * @property PaginatorComponent $Paginator
 */
class PrayerCardsDatasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->PrayerCardsData->recursive = 0;
		$this->set('prayerCardsDatas', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->PrayerCardsData->exists($id)) {
			throw new NotFoundException(__('Invalid prayer cards data'));
		}
		$options = array('conditions' => array('PrayerCardsData.' . $this->PrayerCardsData->primaryKey => $id));
		$this->set('prayerCardsData', $this->PrayerCardsData->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->PrayerCardsData->create();
			if ($this->PrayerCardsData->save($this->request->data)) {
				$this->Session->setFlash(__('The prayer cards data has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prayer cards data could not be saved. Please, try again.'));
			}
		}
		$prayerCategoies = $this->PrayerCardsData->PrayerCategoie->find('list');
		$this->set(compact('prayerCategoies'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->PrayerCardsData->exists($id)) {
			throw new NotFoundException(__('Invalid prayer cards data'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->PrayerCardsData->save($this->request->data)) {
				$this->Session->setFlash(__('The prayer cards data has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prayer cards data could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('PrayerCardsData.' . $this->PrayerCardsData->primaryKey => $id));
			$this->request->data = $this->PrayerCardsData->find('first', $options);
		}
		$prayerCategoies = $this->PrayerCardsData->PrayerCategoie->find('list');
		$this->set(compact('prayerCategoies'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->PrayerCardsData->id = $id;
		if (!$this->PrayerCardsData->exists()) {
			throw new NotFoundException(__('Invalid prayer cards data'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->PrayerCardsData->delete()) {
			$this->Session->setFlash(__('The prayer cards data has been deleted.'));
		} else {
			$this->Session->setFlash(__('The prayer cards data could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
