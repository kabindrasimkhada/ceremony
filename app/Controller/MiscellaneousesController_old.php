<?php
App::uses('AppController', 'Controller');
/**
 * Miscellaneouses Controller
 *
 * @property Miscellaneouse $Miscellaneouse
 * @property PaginatorComponent $Paginator
 */
class MiscellaneousesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');
    public $uses = array('Coffin','Boekje','FuneralLetter','ArrangementData','Koffietafelkaartje','CondolenceRegister','FunerlLetterCard','PrayerCardCategory','PrayerCard','Rouwcentrum','Carrier','Arrangement','Hospital','Miscellaneouse','CareDecease','Homebaring','CareDeceasesData','Postzeal','Booklet','GuideAfterDeath');
    private $keys = array(
                          'price',
                          'booklet_price',
                          'guide_after_death_price',
                          'cremation_price',
                          'funeral_price',
                          'wage_price',
                          'price_per_pieceprice',
                          'per_price',
                          'miscellaneous_price',
                          'casket_adornment_price',
                          'gratitude_price',
                          'advertisement_price',
                          'total_price'
                         
                );
    private $requiredIds =  array(
								    'id',
									'coffins_data_id',
									'funeral_letters_data_id',
									'prayer_cards_data_id',
									'misboekjes_data_id' ,
									'coffee_ticket_id',
									'stamp_id',
									'commemoration_id' ,
									'hospitals_data_id',
									'rouwcentrums_data_id',
									'care_deceases_data_id',
									'homeobaring_id',
									'hearse_id',
									'carriers_data_id',
								 );
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Miscellaneouse->recursive = 0;
		$this->set('miscellaneouses', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Miscellaneouse->exists($id)) {
			throw new NotFoundException(__('Invalid miscellaneouse'));
		}
		$options = array('conditions' => array('Miscellaneouse.' . $this->Miscellaneouse->primaryKey => $id));
		$this->set('miscellaneouse', $this->Miscellaneouse->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add($id=null,$Miscellaneouse_id=null,$Cid=null,$data = null){ 
		if(is_null($id)){
			return $this->redirect(
							array(
								'controller'=>'clientDatas',
								'action' => 'add'
							)
						);
		}
		if(isset($Cid) && !empty($Cid)){
			$conditions = array(
							'conditions' => array(
									'Miscellaneouse.id' => $Miscellaneouse_id,
								),
							'recursive' => -1
						);
			$ids = $this->Miscellaneouse->find('first',$conditions);
			$conditions = array(
							'fields' => array('ArrangementData.id','ArrangementData.miscellaneouse_id'),
							'conditions' => array(
									'ArrangementData.miscellaneouse_id' => $Miscellaneouse_id,
								),
							'recursive' => -1
						);
			$arrangementId = $this->Miscellaneouse->ArrangementData->find('all',$conditions);
			
		}
		
		$arrangementDatas = array();
		if($this->request->is('post')  || !empty($data)){ 
			$this->Miscellaneouse->create();
			if(!empty($data)){
				if(empty($arrangementId)){
				/*$i = 0;
				while($i <  4){
					$data['ArrangementData'][$i]['miscellaneouse_id'] = $Miscellaneouse_id;
					$i++;
				}*/
				foreach ($data['ArrangementData'] as $key => &$value) {
					//debug($value);die;
				 $value['miscellaneouse_id'] = $Miscellaneouse_id;
				}
			}
			debug($data);die;
				if(!empty($ids) && isset($ids)){
					$data['CoffinsData']['id'] = $ids['Miscellaneouse']['coffins_data_id'];
					$data['FuneralLettersData']['id'] = $ids['Miscellaneouse']['funeral_letters_data_id'];
					$data['MisboekjesData']['id'] = $ids['Miscellaneouse']['misboekjes_data_id'];
					$data['PrayerCardsData']['id'] = $ids['Miscellaneouse']['prayer_cards_data_id'];
					$data['CoffeeTicket']['id'] = $ids['Miscellaneouse']['coffee_ticket_id'];
					$data['Stamp']['id'] = $ids['Miscellaneouse']['stamp_id'];
					$data['Commemoration']['id'] = $ids['Miscellaneouse']['commemoration_id'];
					$data['HospitalsData']['id'] = $ids['Miscellaneouse']['hospitals_data_id'];
					$data['RouwcentrumsData']['id'] = $ids['Miscellaneouse']['rouwcentrums_data_id'];
					$data['CareDeceasesData']['id'] = $ids['Miscellaneouse']['care_deceases_data_id'];
					$data['Homeobaring']['id'] = $ids['Miscellaneouse']['homeobaring_id'];
					$data['Hearse']['id'] = $ids['Miscellaneouse']['hearse_id'];
					$data['CarriersData']['id'] = $ids['Miscellaneouse']['carriers_data_id'];
				}

				if ( isset($arrangementId) &&
					 is_array($arrangementId) &&
					 !empty($arrangementId)
				 )
				{
					foreach ($arrangementId as $key => $value) {
						$data['ArrangementData'][$key]['id'] = $value['ArrangementData']['id'];
						$data['ArrangementData'][$key]['miscellaneouse_id'] = $value['ArrangementData']['miscellaneouse_id'];
					}
				}
				$arrangementDatas = $data['ArrangementData'];
				$this->Miscellaneouse->id = $Miscellaneouse_id; 
				$this->request->data = $data;
			}

			$this->request->data['Miscellaneouse']['total_price'] = $this->_getTotalPrice($this->request->data);
			$this->_priceChecker($this->request->data);
			$this->request->data = $this->_priceChecker($this->request->data);
			if(empty($arrangementId)){
				unset($this->request->data['ArrangementData']);
			}
			$this->Miscellaneouse->create(); 
			if ($this->Miscellaneouse->saveAll($this->request->data)) {
				if(empty($Cid)){
					
					$miscellaneous_id = $this->Miscellaneouse->id;
					//echo $decease_id;die;
					$this->Client->id = $id;
				    $this->Client->saveField('miscellaneouse_id',$miscellaneous_id);
					$this->Session->setFlash(__('The miscellaneouse has been saved.'));
					return $this->redirect(array('controller'=>'ClientDatas','action' => 'overview',$this->Client->id));
			   }else{
			   	if(empty($arrangementId)){
			   	  $this->_saveAllArrangementDatas($arrangementDatas);
			    }  
			      $this->Client->id = $Cid;
			      $this->Session->setFlash(__('The miscellaneouse data has been updated.'));
			      if(!isset($Miscellaneouse_id) && empty($Miscellaneouse_id)){
			      	$Miscellaneouse_id =$this->Miscellaneouse->id;
			      	$this->Session->setFlash(__('The miscellaneouse has been saved.'));
				  }
			      $this->Client->saveField(
				    	'miscellaneouse_id',$Miscellaneouse_id
				    	);
			      $id = $this->Client->id;
			      return $this->redirect(array('controller'=>'ClientDatas','action' => 'overview',$id));
			    }
				
			} else {
				$this->Session->setFlash(__('The miscellaneouse could not be saved. Please, try again.'));
			}
		}
		$coffinsDatas = $this->Coffin->find('list',array('fields'=>array('conffin_name','conffin_name')));
		$bookejDatas = $this->Boekje->find('list',array('fields'=>array('name','name')));
		$condolencePricePerPiece = $this->CondolenceRegister->find('list',array('fields'=>array('price_per_piece','price_per_piece')));
		$funeralLettersDatas = $this->FuneralLetter->find('list',array('fields'=>array('id','letter_name')));
		//$funeralMapType = $this->FuneralLetter->find('list',array('fields'=>array('map_type','map_type')));
		$prayerCardsDatas = $this->PrayerCardCategory->find('list',array('fields'=>array('id','prayer_card')));
		$careDeceased = $this->CareDecease->find('list',array('fields'=>array('name','name')));
		$homebaring = $this->Homebaring->find('list',array('fields'=>array('name','name')));
		$hospitalsDatas = $this->Hospital->find('list',array('fields'=>array('hospital_name','hospital_name')));
		$rouwcentrumsDatas = $this->Rouwcentrum->find('list',array('fields'=>array('rouwcentrum_name','rouwcentrum_name')));
		$carriersDatas = $this->Carrier->find('list',array('fields'=>array('name','name')));
		$arrangements = $this->Arrangement->find('list',array('fields'=>array('arrangement_type','arrangement_type')));
		$coffeeRooms = $this->Koffietafelkaartje->find('list',array('fields'=>array(
																					'type','type',
																					)
																	)
													  );
		$postzeals = $this->Postzeal->find('list',array('fields'=>array('price','price')));
		$booklets = $this->Booklet->find('list',array('fields'=>array('name','name')));
		$guide_after_death = $this->GuideAfterDeath->find('list',array('fields'=>array('name','name')));
		//$florists = $this->Florist->find('list',array('fields'=>''));
		$this->set(compact('careDeceased','bookejDatas','condolencePricePerPiece','coffeeRooms','homebaring','coffinsDatas', 'funeralLettersDatas','funeralMapType', 'prayerCardsDatas', 'misboekjesDatas', 'coffeeTickets','prayerMapType','stamps', 'commemorations', 'hospitalsDatas', 'rouwcentrumsDatas', 'careDeceases', 'homeobarings', 'hearses', 'carriersDatas', 'arrangements', 'florists'));
	}

/**
 * edit drafts method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
				   if (!$this->Client->exists($id)) {
							throw new NotFoundException(__('Invalid client data'));
					}
					$this->Client->Behaviors->attach("Containable");
			    	$this->Client->contain(
			    		                   array('Miscellaneouse.CoffinsData',
			    		 						 'Miscellaneouse.FuneralLettersData',
			    		 						 'Miscellaneouse.PrayerCardsData',
			    		 						 'Miscellaneouse.MisboekjesData',
			    		 						 'Miscellaneouse.CoffeeTicket',
			    		 						 'Miscellaneouse.Stamp',
			    		 						 'Miscellaneouse.Commemoration',
			    		 						 //'Miscellaneouse.HospitalData',
			    		 						 //'Miscellaneouse.Rouwcentrum',
			    		 						 'Miscellaneouse.CareDeceasesData',
			    		 						 'Miscellaneouse.Homeobaring',
			    		 						 'Miscellaneouse.Hearse',
			    		 						 //'Miscellaneouse.CarrierData',
			    		 						 'Miscellaneouse.ArrangementData',
			    		 						 //'Miscellaneouse.Florist',
			    		 			)
			    		          );
			    	$miscellaneouseInfo = $this->Client->find('first',array('conditions'=>array('Client.id'=>$id)));
			    	$miscellaneouseInfo['CoffinsData']  = !empty($miscellaneouseInfo['Miscellaneouse']['CoffinsData'])?$miscellaneouseInfo['Miscellaneouse']['CoffinsData']:null;
			    	$miscellaneouseInfo['FuneralLettersData']  = !empty($miscellaneouseInfo['Miscellaneouse']['FuneralLettersData']) ?$miscellaneouseInfo['Miscellaneouse']['FuneralLettersData']:null;
			    	$miscellaneouseInfo['PrayerCardsData']  =  !empty($miscellaneouseInfo['Miscellaneouse']['PrayerCardsData']) ?$miscellaneouseInfo['Miscellaneouse']['PrayerCardsData']:null;
			    	$miscellaneouseInfo['MisboekjesData']  = !empty($miscellaneouseInfo['Miscellaneouse']['MisboekjesData']) ?$miscellaneouseInfo['Miscellaneouse']['MisboekjesData']:null;
			    	$miscellaneouseInfo['Stamp']  = !empty($miscellaneouseInfo['Miscellaneouse']['Stamp']) ?$miscellaneouseInfo['Miscellaneouse']['Stamp']:null;
			    	$miscellaneouseInfo['Commemoration']  = !empty($miscellaneouseInfo['Miscellaneouse']['Commemoration'] ) ?$miscellaneouseInfo['Miscellaneouse']['Commemoration'] :null;
			    	$miscellaneouseInfo['Rouwcentrum']  = !empty($miscellaneouseInfo['Miscellaneouse']['Rouwcentrum']) ?$miscellaneouseInfo['Miscellaneouse']['Rouwcentrum']:null;
			    	$miscellaneouseInfo['CareDeceasesData']  = !empty($miscellaneouseInfo['Miscellaneouse']['CareDeceasesData']) ?$miscellaneouseInfo['Miscellaneouse']['CareDeceasesData']:null;
			    	$miscellaneouseInfo['Homeobaring']  = !empty($miscellaneouseInfo['Miscellaneouse']['Homeobaring']) ?$miscellaneouseInfo['Miscellaneouse']['Homeobaring']:null;
			    	$miscellaneouseInfo['Hearse']  = !empty($miscellaneouseInfo['Miscellaneouse']['Hearse']) ?$miscellaneouseInfo['Miscellaneouse']['Hearse']:null;
			    	$miscellaneouseInfo['ArrangementData']  = !empty($miscellaneouseInfo['Miscellaneouse']['ArrangementData']) ?$miscellaneouseInfo['Miscellaneouse']['ArrangementData']:null;
			    	$miscellaneouseInfo['CoffeeTicket']  = !empty($miscellaneouseInfo['Miscellaneouse']['CoffeeTicket']) ?$miscellaneouseInfo['Miscellaneouse']['CoffeeTicket']:null;
			    	$miscellaneouse_id = $miscellaneouseInfo['Miscellaneouse']['id']; 
			    	$this->ArrangementData->recursive = -1;
			    	$arrangementIds = $this->ArrangementData->find('all',array(
	    														'conditions'=>array(
	    																			'ArrangementData.miscellaneouse_id'=>$miscellaneouse_id
	    																			),
	    														'fields' => 'id'
	    													)
	    									 );
			    	
			    	$ids =	$this->filter_ids($miscellaneouseInfo['Miscellaneouse']);
			    	$ids['arrangement_data_id'] = $arrangementIds;
			    	if ($this->request->is(array('post', 'put'))){
			    	    $data = $this->add_id($this->request->data,$ids);
						$this->admin_add('1',$miscellaneouse_id,$id,$this->request->data);
				 	}
					$this->request->data = $miscellaneouseInfo;
					$coffinsDatas = $this->Coffin->find('list',array('fields'=>array('conffin_name','conffin_name')));
					$bookejDatas = $this->Boekje->find('list',array('fields'=>array('name','name')));
					$condolencePricePerPiece = $this->CondolenceRegister->find('list',array('fields'=>array('price_per_piece','price_per_piece')));
					$funeralLettersDatas = $this->FuneralLetter->find('list',array('fields'=>array('id','letter_name')));
					$prayerCardsDatas = $this->PrayerCardCategory->find('list',array('fields'=>array('id','prayer_card')));
					$careDeceased = $this->CareDecease->find('list',array('fields'=>array('name','name')));
					$homebaring = $this->Homebaring->find('list',array('fields'=>array('name','name')));
					$hospitalsDatas = $this->Hospital->find('list',array('fields'=>array('hospital_name','hospital_name')));
					$rouwcentrumsDatas = $this->Rouwcentrum->find('list',array('fields'=>array('rouwcentrum_name','rouwcentrum_name')));
					$carriersDatas = $this->Carrier->find('list',array('fields'=>array('name','name')));
					$arrangements = $this->Arrangement->find('list',array('fields'=>array('arrangement_type','arrangement_type')));
					$coffeeRooms = $this->Koffietafelkaartje->find('list',array('fields'=>array(
																								'type','type',
																								)));
					$postzeals = $this->Postzeal->find('list',array('fields'=>array('price','price')));
					$booklets = $this->Booklet->find('list',array('fields'=>array('name','name')));
					$guide_after_death = $this->GuideAfterDeath->find('list',array('fields'=>array('name','name')));					
					//$florists = $this->Florist->find('list',array('fields'=>''));
					$this->set(compact('postzeals','booklets','guide_after_death','careDeceased','bookejDatas','condolencePricePerPiece','coffeeRooms','homebaring','coffinsDatas', 'funeralLettersDatas','funeralMapType', 'prayerCardsDatas', 'misboekjesDatas', 'coffeeTickets','prayerMapType','stamps', 'commemorations', 'hospitalsDatas', 'rouwcentrumsDatas', 'careDeceases', 'homeobarings', 'hearses', 'carriersDatas', 'arrangements', 'florists','id'));
								$this->render('admin_add');
						    }


/*
* Check the price
*/

   private function _priceChecker($data = null){
   	   $coffin = isset($data['CoffinsData']['conffin_name']) ? $data['CoffinsData']['conffin_name']:null;
       $coffeeTicket =  isset($data['CoffeeTicket']['name']) ? $data['CoffeeTicket']['name']:null;
       $coffeenumber =  isset($data['CoffeeTicket']['number']) ? $data['CoffeeTicket']['number']:null;
       $misboekjes = isset($data['MisboekjesData']['price_per_piecename']) ? $data['MisboekjesData']['price_per_piecename']:null;	 	
       $misboekjesnumber = isset($data['MisboekjesData']['price_per_piecenumber']) ? $data['MisboekjesData']['price_per_piecenumber']:null;
       $perStampPrice = isset($data['Stamp']['price_netherland']) ? $data['Stamp']['price_netherland']:null;	 	
       $stampnumber = isset($data['Stamp']['number']) ? $data['Stamp']['number']:null;	
       $commemorationPrice = isset($data['Commemoration']['price_per_piece']) ? $data['Commemoration']['price_per_piece']:null;	 	
       $commemorationNumber = isset($data['Commemoration']['number']) ? $data['Commemoration']['number']:null;
       $hospitalName = isset($data['Hospital']['hospital_name']) ? $data['Hospital']['hospital_name']:null;	 	
       $rowcentrumName = isset($data['RouwcentrumsData']['name']) ? $data['RouwcentrumsData']['name']:null; 
       $rowcentrumNumber = isset($data['RouwcentrumsData']['some_days']) ? $data['RouwcentrumsData']['some_days']:null; 	 		 	
       $careDeceaseName = isset($data['CareDeceasesData']['name']) ? $data['CareDeceasesData']['name']:null; 	 		 	
       $homeobaringName = isset($data['Homeobaring']['name']) ? $data['Homeobaring']['name']:null; 
       $homeobaringdays = isset($data['Homeobaring']['days']) ? $data['Homeobaring']['days']:null; 
       $carrierName = isset($data['CarriersData']['name']) ? $data['CarriersData']['name']:null; 
       $carriernumber = isset($data['CarriersData']['number']) ? $data['CarriersData']['number']:null; 
       $arrangementData = $data['ArrangementData'];
       $data['CoffinsData']['price'] = $this->admin_priceByAjax($coffin,'Coffin','conffin_name');
       $data['CoffeeTicket']['price'] = $this->admin_priceByAjax($coffeeTicket,'Koffietafelkaartje','type',$coffeenumber);
       $data['MisboekjesData']['total_price'] = $this->admin_priceByAjax($misboekjes,'Boekje','name',$misboekjesnumber);
       $data['Stamp']['price'] = $perStampPrice * $stampnumber;
       $data['Commemoration']['price'] =  $commemorationPrice * $commemorationNumber;
       $data['HospitalsData']['price'] =  $this->admin_priceByAjax($hospitalName,'Boekje','name');
       $data['CareDeceasesData']['price'] =  $this->admin_priceByAjax($careDeceaseName,'CareDecease','name');
       $data['Homeobaring']['per_price'] =  $this->admin_priceByAjax($homeobaringName,'Homebaring','name',$homeobaringdays);
       $data['CarriersData']['price'] =  $this->admin_priceByAjax($carrierName,'Carrier','name',$carriernumber);
       foreach($arrangementData as $key=>$arrangement){
       	   $type = isset($arrangement['type']) ? $arrangement['type']:null; 
       	   $data['ArrangementData'][$key]['price'] = $this->admin_priceByAjax($type,'Arrangement','arrangement_type');
       }
       return $data;
   }



/*
* calculate total price method
*
*
*/

	private function _getTotalPrice($datas = null)
	{
        $total = 0;
		if(isset($datas) && !empty($datas)){ //debug($datas);die;
			foreach($datas as $data){
				foreach($data as $key=>$value){
					if(in_array($key,$this->keys) &&  !empty($value)){ 
						//echo $total.'<br/>';
						if(is_array($value)){
							foreach($value as $v){
								$total += $v;
								//echo $total.'<br/>';
						 	}
						}else{
							$total += $value;
						}
					}
					
				}

			}
			// die;        
		}
       return $total;

	}
/**
 * save all arangement data manually method
 *
 * @throws NotFoundException
 * @param string $arrangementDatas
 * @return void
 */

	private function _saveAllArrangementDatas($arrangementDatas){
 	 if(!empty($arrangementDatas) && is_array($arrangementDatas)){
		foreach ($arrangementDatas as $key => $arrangementData) {
				$this->Miscellaneouse->ArrangementData->save($arrangementData);
			}
		}	
	}


	public function admin_priceByAjax($name='',$model='',$field='',$number='',$setter=null){
		$this->autoRender = false;
		if($this->request->isAjax()){
			 if(isset($this->request->data) && !empty($this->request->data)){
				 	$name = $this->request->data['name'];
				 	$model = $this->request->data['model'];
				 	$field =  $this->request->data['field'];
				    $number = isset($this->request->data['number'])?$this->request->data['number']:null;
				 	if(!empty($name)){ 
							$price = $this->$model->find('all',
																array(
																	'conditions'=>array($model.'.'.$field => $name),
																	'fields'   => 'price'
										                        )
															);
							
							
							$price = !empty($price)?(float) $price[0][$model]['price']:'';
							
							if(!empty($number)){
								$total = $price * $number;
							}else
							{
								$total =  $price;
							}
						}
					else{

						$total = '';
					}
                    $data['value'] = $total;
					echo json_encode($data);
			  }
			}
			elseif(!empty($name) && isset($name)){
				$name = $name;
				$model = $model;
				$field =  $field;
			    $number = $number;
			    if(!empty($name)){ 
							$price = $this->$model->find('all',
																array(
																	'conditions'=>array($model.'.'.$field => $name),
																	'fields'   => 'price'
										                        )
															);
							
							
							$price = !empty($price)?(float) $price[0][$model]['price']:'';
							
							if(!empty($number)){
								$total = $price * $number;
							}else
							{
								$total =  $price;
							}
						}
					else{

						$total = '';
					}
                    return $total;
			}
        }



      public function admin_findCardType()
      {
      		$this->layout = 'ajax';
      		 $this->autoRender = false;
			 if($this->request->isAjax()){
				 if(isset($this->request->data) && !empty($this->request->data)){
						 	$name = $this->request->data['name'];
						 	$model = $this->request->data['model'];
						 	$field =  $this->request->data['field'];
						 	if(!empty($name)){ 
									$cards = $this->$model->find('list',
																		array(
																			'conditions'=>array($model.'.'.$field => $name),
																			'fields'   => array('card','card')
												                        )
																	);
									
									$options = '';	
									$options .= "<option value=''>Selecteer</option>";								
									foreach($cards as $card) {										
										$options .= "<option value='".$card."'>" . $card . "</option>";
									}

							   	echo $options;
				           }
			 		}
       		   } 
          }

        private function add_id($datas = null,$ids= null){
        	
        	  $datas['CoffinsData']['id'] = isset($ids['coffins_data_id']) ? $ids['coffins_data_id'] : null;
        	  $datas['FuneralLettersData']['id'] = isset($ids['funeral_letters_data_id']) ? $ids['funeral_letters_data_id'] : null; 
        	  $datas['Miscellaneouse']['id'] = isset($ids['id']) ? $ids['id'] : null;
        	  $datas['PrayerCardsData']['id'] =isset($ids['prayer_cards_data_id']) ? $ids['prayer_cards_data_id'] : null; 
        	  $datas['MisboekjesData']['id'] = isset($ids['misboekjes_data_id']) ? $ids['misboekjes_data_id'] : null;
        	  $datas['CoffeeTicket']['id'] = isset($ids['coffee_ticket_id']) ? $ids['coffee_ticket_id'] : null;
        	  $datas['Stamp']['id'] = isset($ids['stamp_id']) ? $ids['stamp_id'] : null;
        	  $datas['Commemoration']['id'] = isset($ids['commemoration_id']) ? $ids['commemoration_id'] : null;
        	  $datas['HospitalsData']['id'] = isset($ids['hospitals_data_id']) ? $ids['hospitals_data_id'] : null;
        	  $datas['RouwcentrumsData']['id'] = isset($ids['rouwcentrums_data_id']) ? $ids['rouwcentrums_data_id'] : null;
        	  $datas['CareDeceasesData']['id'] = isset($ids['care_deceases_data_id']) ? $ids['care_deceases_data_id'] : null;
        	  $datas['Homeobaring']['id'] = isset($ids['homeobaring_id']) ? $ids['homeobaring_id'] : null;
        	  $datas['Hearse']['id'] = isset($ids['hearse_id']) ? $ids['hearse_id'] : null;
        	  $datas['CarriersData']['id'] = isset($ids['carriers_data_id']) ? $ids['carriers_data_id'] : null;
        	  $datas['ArrangementData'][0]['id'] = isset($ids['arrangement_data_id'][0]['ArrangementData']['id']) ? $ids['arrangement_data_id'][0]['ArrangementData']['id'] : null;
        	  $datas['ArrangementData'][1]['id'] = isset($ids['arrangement_data_id'][1]['ArrangementData']['id']) ? $ids['arrangement_data_id'][1]['ArrangementData']['id'] : null;
        	  $datas['ArrangementData'][2]['id'] = isset($ids['arrangement_data_id'][2]['ArrangementData']['id']) ? $ids['arrangement_data_id'][2]['ArrangementData']['id'] : null;
        	  $datas['ArrangementData'][3]['id'] = isset($ids['arrangement_data_id'][3]['ArrangementData']['id']) ? $ids['arrangement_data_id'][3]['ArrangementData']['id']: null;
        	  	        
            return $datas;	  	        
        }  

        private function filter_ids($datas = null)
        {

        	$ids = array();
        	if(!is_null($datas)){
        		foreach ($datas as $key=>$value) {
        			if(in_array($key,$this->requiredIds)){
        				if(!is_null($value) && !empty($value)){
        					$ids[$key]= $value; 
        				}
        			}

        		}
        	}
        	//array_splice($ids, 2, 0, $datas['id']);
        	return $ids;
        }  
   }
