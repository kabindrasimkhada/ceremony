<?php
App::uses('AppController', 'Controller');
/**
 * HospitalsDatas Controller
 *
 * @property HospitalsData $HospitalsData
 * @property PaginatorComponent $Paginator
 */
class HospitalsDatasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->HospitalsData->recursive = 0;
		$this->set('hospitalsDatas', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->HospitalsData->exists($id)) {
			throw new NotFoundException(__('Invalid hospitals data'));
		}
		$options = array('conditions' => array('HospitalsData.' . $this->HospitalsData->primaryKey => $id));
		$this->set('hospitalsData', $this->HospitalsData->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->HospitalsData->create();
			if ($this->HospitalsData->save($this->request->data)) {
				$this->Session->setFlash(__('The hospitals data has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The hospitals data could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->HospitalsData->exists($id)) {
			throw new NotFoundException(__('Invalid hospitals data'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->HospitalsData->save($this->request->data)) {
				$this->Session->setFlash(__('The hospitals data has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The hospitals data could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('HospitalsData.' . $this->HospitalsData->primaryKey => $id));
			$this->request->data = $this->HospitalsData->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->HospitalsData->id = $id;
		if (!$this->HospitalsData->exists()) {
			throw new NotFoundException(__('Invalid hospitals data'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->HospitalsData->delete()) {
			$this->Session->setFlash(__('The hospitals data has been deleted.'));
		} else {
			$this->Session->setFlash(__('The hospitals data could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
