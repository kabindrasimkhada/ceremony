<?php
App::uses('AppController', 'Controller');
/**
 * ChruchDatas Controller
 *
 * @property ChruchData $ChruchData
 * @property PaginatorComponent $Paginator
 */
class ChruchDatasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ChruchData->recursive = 0;
		$this->set('chruchDatas', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ChruchData->exists($id)) {
			throw new NotFoundException(__('Invalid chruch data'));
		}
		$options = array('conditions' => array('ChruchData.' . $this->ChruchData->primaryKey => $id));
		$this->set('chruchData', $this->ChruchData->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ChruchData->create();
			if ($this->ChruchData->save($this->request->data)) {
				$this->Session->setFlash(__('The chruch data has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The chruch data could not be saved. Please, try again.'));
			}
		}
		$clientDatas = $this->ChruchData->ClientData->find('list');
		$this->set(compact('clientDatas'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ChruchData->exists($id)) {
			throw new NotFoundException(__('Invalid chruch data'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ChruchData->save($this->request->data)) {
				$this->Session->setFlash(__('The chruch data has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The chruch data could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ChruchData.' . $this->ChruchData->primaryKey => $id));
			$this->request->data = $this->ChruchData->find('first', $options);
		}
		$clientDatas = $this->ChruchData->ClientData->find('list');
		$this->set(compact('clientDatas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ChruchData->id = $id;
		if (!$this->ChruchData->exists()) {
			throw new NotFoundException(__('Invalid chruch data'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ChruchData->delete()) {
			$this->Session->setFlash(__('The chruch data has been deleted.'));
		} else {
			$this->Session->setFlash(__('The chruch data could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
