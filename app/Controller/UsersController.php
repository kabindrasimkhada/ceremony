<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
   
	public $components = array('Paginator','Auth');

   public function beforeFilter(){
   	    parent::beforeFilter();
        $this->Auth->allow('*','admin_addUsers');
   }

/**
 * index method
 *
 * @return void
 */
    public function admin_dashboard() { 
           
        $title = array(
                    'title'=>__('Dashboard'),
                    'explain'=> __('bedieningspaneel')
                );
         $this->set('title',$title);
    }


/**
 * index method
 *
 * @return void
 */
	public function admin_index() { 
        $title = array(
                    'title'=>__('gebruikers'),
                    'explain'=>__('Lijst  gebruikers')
                );
        $breadCrumb = array(
                             array(
                                  'name' =>__('Dashboard'),
                                  'link' => Router::url(array('controller' => 'users', 'action' => 'dashboard'))
                              ),
                            array(
                                  'name' =>__('gebruikers'),
                                  'link' => Router::url(array('controller' => 'users'))
                              )
                        );
		$this->User->recursive = 0;
		$this->Paginator->settings = array(
                                        'order'=>array('User.created'=>'DESC'),
                                        'limit' => 10
                                    );
        $users = $this->Paginator->paginate();
        $this->set(compact('users','title','breadCrumb'));
       
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {  
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {

    			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
    			$this->request->data = $this->User->find('first', $options);
		}

	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * login method
 *
 * @throws NotFoundException
 *
 * @return void
 */

   public function admin_login(){
   	 if($this->request->is('post')){
   	   if($this->Auth->login()){
	   	    $this->set(compact('referer'));
	   	 	return $this->redirect('/admin');
   	 	 }else
   	 	 {
   	 	 	 $this->Session->setFlash(__('Invalid username/password combination'));
   	 	 	 $referer = $this->referer();
   	         $this->set(compact('referer'));
   	 	 }
   	 }
   	 
   	 $referer = $this->referer();
   	 $this->set(compact('referer'));
   	 $this->layout = 'login';
   }
/**
* Admin logout
* @return type
*/
    public function admin_logout() {

        $this->Session->setFlash(__('Good-Bye'), 'success_message');
        $this->redirect($this->Auth->logout());
    }

/**
* Add new user
* @return type
*/
    public function admin_addUsers(){
    	 $page_title = __('Add new Users');
    	 if($this->request->is('post')){
    	 	 if(!empty($this->request->data)){
    	 		$this->User->create();
    	 		if($this->User->save($this->request->data)){
    	 			$this->Session->setFlash(__('New Users added Successfully'),'success_message');
    	 			$this->redirect(array('action'=>'index'));

    	 		}else
    	 		{
    	 			$errors = $this->User->validationErrors;
    	 			$this->set('errors',$errors);
    	 			$this->Session->setFlash(__('The users cannot be added,Plz try again'),'error_message');

    	 		}
    	 	}
    	 	else{
    	 		$this->Session->setFlash(__('The users cannot be saved,Plz try again'),'error_message');
    	 	}
    	 }
          $title = array(
                    'title'=>__('gebruikers'),
                    'explain'=>__('Lijst  gebruikers')
                );
       
               $breadCrumb = array(
                                    array(
                                          'name' =>__('Dashboard'),
                                          'link' => Router::url(array('/'))
                                      ),
                                    array(
                                         'name' =>__('gebruikers'),
                                         'link' => Router::url(array('controller' => 'users', 'action' => 'index'))
                                      ),
                                    array(
                                        'name' =>__('toevoegen'),
                                        'link' => Router::url(array('controller' => 'users', 'action' => 'addUsers'))
                                    )
                                );
                $this->set(compact('title','breadCrumb'));
    }


/**
* Add new user
* @return type
*/
    public function admin_editUsers($id = null){
      $page_title = __('Edit Users');
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
           /* if ($this->request->data['User']['renew_password']) {
                $this->request->data['User']['password'] = $this->request->data['User']['renew_password'];
                unset($this->request->data['User']['renew_password']);
            }*/

            $this->User->id = $id;
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The Account Information is Updated.'), 'success_message');
                return $this->redirect(array('action' => 'index'));
            } else {
                $errors = $this->User->validationErrors;
                $this->set('errors', $errors);
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'error_message');
            }
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->request->data = $this->User->find('first', $options);
        $title = array(
                    'title'=>__('gebruikers'),
                    'explain'=>__('Lijst  gebruikers')
                );
       
       $breadCrumb = array(
                            array(
                                  'name' =>__('Dashboard'),
                                  'link' => Router::url(array('controller' => 'users', 'action' => 'dashboard'))
                                ),
                            array(

                                         'name' =>__('gebruikers'),
                                         'link' => Router::url(array('controller' => 'users', 'action' => 'index'))
                                 ),
                          );
        $this->set(compact('title','breadCrumb','page_title'));
       
    }    


     /**
     * Front end login method
     *
     * @return void
     */
    public function login() { 
         
        if ($this->Session->read('Auth.User')) {
            $this->Session->setFlash('You are logged in!');
            return $this->redirect('/admin');
            exit;
        }

        if (!$this->Auth->loggedIn() && $this->Cookie->read('rememberMe')) { 
            $cookie = $this->Cookie->read('rememberMe');

            if ($this->Auth->login($cookie['User'])) { 
                return $this->redirect('/admin');
                exit;
            } else {
                $this->Cookie->destroy('User'); # delete invalid cookie
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
        } else {
            if ($this->request->is('post')) {
                if ($this->Auth->login()) {
                    if (in_array($this->Auth->user('group_id'), array(1))) {
                        $this->Session->setFlash(__('Your username or password was incorrect.'), 'error_message');
                        return $this->redirect('login');
                    }
                    //Setting Cookie
                    if ($this->request->data['User']['remember_me'] == 1) {
                        // After what time frame should the cookie expire
                        $cookieTime = "12 months"; // You can do e.g: 1 week, 17 weeks, 14 days
                        // remove "remember me checkbox"
                        unset($this->request->data['User']['rememberMe']);
                        // hash the user's password
                        $this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);

                        // write the cookie
                        $this->Cookie->write('User', $this->request->data['User'], true, $cookieTime);
                    }

                    return $this->redirect('/admin');
                    exit;
                }
                $this->Session->setFlash(__('Your username or password was incorrect.'), 'error_message');
            }
        }
        $referer = $this->referer();
        $title_for_layout = __("Login");
        $this->set(compact('title_for_layout', 'referer'));

        $this->layout = 'login';
    }

    /**
     * function to login by url
     * @param type $keyhash 
     */
    public function url_login($keyhash = null) {
        $keyhash_parts = explode("#", $keyhash);
        $login_token = $keyhash_parts[0];
        $hash_token = $keyhash[1];
        $check_hash = $this->User->find('first', array(
            'conditions' => array('User.login_token' => $login_token)
        ));
        if ($login_token == "") {
            $this->Session->setFlash(__('No Login Token Detected'), 'error_message');
            $this->redirect($this->Auth->logout());
        }
        if ($hash_token == "") {
            $this->Session->setFlash(__('Failed to detect hash'), 'error_message');
            $this->redirect($this->Auth->logout());
        }
        if (empty($check_hash)) {
            $this->Session->setFlash(__('Invalid Access'), 'error_message');
            $this->redirect($this->Auth->logout());
        } else {
            if ($this->Auth->login(array('id' => $check_hash['User']['id'], 'group_id' => $check_hash['User']['group_id']))) {
                if (in_array($this->Auth->user('group_id'), array(1))) {
                    $this->Session->setFlash(__('Your username or password was incorrect.'), 'error_message');
                    $this->redirect($this->Auth->logout());
                }
                $keyhash = "";
                return $this->redirect(array('controller' => 'users', 'action' => 'index'));
            }
            exit;
        }
    }

    /**
     * Admin logout
     * @return void
     */
    public function logout() {
        $this->Session->setFlash(__('Good-Bye'), 'success_message');
        $this->Cookie->delete('User');
        $this->redirect($this->Auth->logout());
    }

 
    /**
     * Forgot Password
     * @return void
     */
    public function forgot_password() {
        $this->layout = 'login';
        if ($this->request->is('post')) {
            if (isset($this->request->data['User']['username']) && !empty($this->request->data['User']['username'])) {
                $user = $this->User->checkUsername($this->request->data['User']['username']);

                if ($user) {
                    $key = Security::hash(String::uuid(), 'sha512', true);
                    $hash = sha1($user['User']['username'] . rand(0, 100));
                    $url = Router::url(array('controller' => 'users', 'action' => 'reset'), true) . '/' . $key . '#' . $hash;

                    $this->loadModel('EmailTemplate');
                    $emailTemplate = $this->EmailTemplate->getEmailTemplate('forgot_password');
                    $content = $emailTemplate['EmailTemplate']['email_content'];
                    $content = str_replace('[#FULLNAME#]', $user['User']['first_name'] . " " . $user['User']['last_name'], $content);
                    $content = str_replace('[#RESET_LINK#]', $url, $content);

                    if ($this->User->saveUserField($user['User']['id'], 'token_hash', $key)) {
                        /* Send Forgot Password Email */
                        $options['from'] = $this->viewVars['siteEmail'];
                        $options['fromName'] = $this->viewVars['siteName'];
                        $options['to'] = $user['User']['username'];
                        $options['subject'] = $emailTemplate['EmailTemplate']['email_subject'];
                        $options['emailFormat'] = 'both';
                        if ($this->_sendResetEmail($options, $content)) {
                            $this->Session->setFlash(__('Please check your email to reset your password.'), 'success_message');
                        } else {
                            $this->Session->setFlash(__('There was some techincal problem. Please try again later.'), 'error_message');
                        }
                    } else {
                        $this->Session->setFlash(__('Error Generating Reset link.'), 'error_message');
                    }
                } else {
                    $this->Session->setFlash(__('Please provide your registered email address.'), 'error_message');
                }
            } else {
                $this->Session->setFlash(__('Please provide your email address.'), 'error_message');
            }
        }
        $title_for_layout = __('Forgot Password');
        $this->set(compact('title_for_layout'));
    }

    /**
     * Reset Password
     * @return void
     */
    public function reset($token = null) {
        $this->layout = "login";
        $this->User->recursive = -1;
        if (!empty($token)) {
            $user = $this->User->findBytoken_hash($token);

            if ($user) {
                $this->User->id = $user['User']['id'];

                if (($this->request->is('post'))) {
                    $this->User->data = $this->request->data;
                    $this->User->data['User']['username'] = $user['User']['username'];
                    $new_hash = sha1($user['User']['username'] . rand(0, 100)); //created token
                    $this->User->data['User']['token_hash'] = $new_hash;
                    pr($this->User->data);
                    if ($this->User->validates(array('fieldList' => array('password')))) {
                        if ($this->User->saveUser($this->User->data)) {
                            $this->Session->setFlash(__('Your password has been changed successfully.'), 'success_message');
                            $this->redirect(array('controller' => 'users', 'action' => 'login'));
                        }
                    } else {

                        $this->set('errors', $this->User->invalidFields());
                    }
                }
            } else {
                $this->Session->setFlash(__('Token Corrupted.The reset link work only for once.'), 'error_message');
            }
        } else {
            $this->redirect('/');
        }
    }

    /**
     * Sends the resend link email
     * @param string $to Receiver email address
     * @param array $options EmailComponent options
     * @return void
     */
    protected function _sendResetEmail($options, $content) {

        $Email = new CakeEmail();
        $Email->to($options['to'])
                ->from($options['from'])
                ->subject($options['subject'])
                ->emailFormat($options['emailFormat'])
                ->send($content);
        return true;
    }

    /**
     * Action for the total selection page
     * 
     * @param int $userID
     * 
     * @return void
     * */
    public function total($userID = null) {

        if (!$userID) {
            $userID = $this->Session->read('currentClientID');
        }
        $userData = $this->User->getTotalSelectionData($userID);
        //pr($userData);
        if ($this->User->checkSelectionCompletion($userID)) {
            $this->set('showTotal', 1);
        }
        $this->loadModel('Content');
        $content = $this->Content->findByCode('share_choice');

        $title_for_layout = __('Total overview');
        $this->set(compact('userData', 'title_for_layout', 'content'));
    }

   /**
     * Change Admin Password 
     *     
     * @return void
     */
    public function admin_change_password() {
        $page_title = __('Change Password');
        $this->User->id = $this->Session->read("Auth.User.id");
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }

        if( $this->request->is('post') ) {
            
            if(!empty($this->request->data['User']['old_password']) || !empty($this->request->data['User']['password'])){
                $this->User->validate = $this->User->changePasswordValidation;
                if ($this->User->save($this->request->data)) {
                    $this->Session->setFlash(__('The Admin password has been changed'), 'success_message');
                    $this->redirect(array('action' => 'index'));
                } else {                   
                    $this->Session->setFlash(__('Password Cannot be changed. Please, try again.'), 'error_message');
            }
           
           }

        }
        
    }      

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->User->delete()) {
            $this->Session->setFlash(__('The user has been deleted.'));
        } else {
            $this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }


}
