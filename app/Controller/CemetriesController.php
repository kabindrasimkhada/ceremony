<?php
App::uses('AppController', 'Controller');
/**
 * Cemetries Controller
 *
 * @property Cemetry $Cemetry
 * @property PaginatorComponent $Paginator
 */
class CemetriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Cemetry->recursive = 0;
		$this->set('cemetries', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Cemetry->exists($id)) {
			throw new NotFoundException(__('Invalid cemetry'));
		}
		$options = array('conditions' => array('Cemetry.' . $this->Cemetry->primaryKey => $id));
		$this->set('cemetry', $this->Cemetry->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Cemetry->create();
			if ($this->Cemetry->save($this->request->data)) {
				$this->Session->setFlash(__('The cemetry has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cemetry could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Cemetry->exists($id)) {
			throw new NotFoundException(__('Invalid cemetry'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Cemetry->save($this->request->data)) {
				$this->Session->setFlash(__('The cemetry has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cemetry could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Cemetry.' . $this->Cemetry->primaryKey => $id));
			$this->request->data = $this->Cemetry->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Cemetry->id = $id;
		if (!$this->Cemetry->exists()) {
			throw new NotFoundException(__('Invalid cemetry'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Cemetry->delete()) {
			$this->Session->setFlash(__('The cemetry has been deleted.'));
		} else {
			$this->Session->setFlash(__('The cemetry could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
