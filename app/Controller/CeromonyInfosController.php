<?php
App::uses('AppController', 'Controller');
/**
 * CeromonyInfos Controller
 *
 * @property CeromonyInfo $CeromonyInfo
 * @property PaginatorComponent $Paginator
 */
class CeromonyInfosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');
    var $uses = array('Church','Cemetry','CeromonyInfo');
   
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CeromonyInfo->recursive = 0;
		$this->set('ceromonyInfos', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CeromonyInfo->exists($id)) {
			throw new NotFoundException(__('Invalid ceromony info'));
		}
		$options = array('conditions' => array('CeromonyInfo.' . $this->CeromonyInfo->primaryKey => $id));
		$this->set('ceromonyInfo', $this->CeromonyInfo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add($id=null,$CeromonyInfo_id=null,$Cid=null,$data = null){
        if(is_null($id)){
			return $this->redirect(array('controller'=>'clientDatas','action' => 'add'));
		}        
		if ($this->request->is('post') || !empty($data)) { 
			$this->CeromonyInfo->create();
			if(!empty($CeromonyInfo_id)){                
                $ids = $this->CeromonyInfo->find('first',array('conditions'=>array('CeromonyInfo.id'=>$CeromonyInfo_id), 'recursive' => -1));
				$this->CeromonyInfo->id = $CeromonyInfo_id;
				$data['ChruchData']['id'] = $ids['CeromonyInfo']['chruch_data_id'];
				$data['CemetryData']['id'] = $ids['CeromonyInfo']['cemetry_data_id'];
				$data['CeromonyData']['id'] = $ids['CeromonyInfo']['ceromony_data_id'];
				$this->request->data = $data;
			}
			$val = $this->_emptyElementExists($this->request->data['CeromonyInfo']);
			if($val !== true){
				$this->request->data['CeromonyInfo']['status'] = 1;
			}
			
			if ($this->CeromonyInfo->saveAll($this->request->data)){
				if(empty($Cid)){
				$ceromony_info_id = $this->CeromonyInfo->id;
				//echo $CeromonyInfo_id;die;
				$this->Client->id = $id;
			    $this->Client->saveField('ceromony_info_id',$ceromony_info_id);
			    $this->Session->setFlash(__('The ceromony info has been saved.'));
				return $this->redirect(array('controller'=>'entrepreneurs' , 'action'=> 'add',$this->Client->id));
			   }else{
			      $this->Client->id = $Cid;
			      $this->Session->setFlash(__('The Ceromony info data has been updated.'));
			      if(!isset($CeromonyInfo_id) && empty($CeromonyInfo_id)){
			      	$CeromonyInfo_id = $this->CeromonyInfo->id;
			      	$this->Session->setFlash(__('The ceromony info has been saved.'));
				  }
				  $this->Client->saveField(
				    	'ceromony_info_id',$CeromonyInfo_id
				    	);
			      $id = $this->Client->id;
			      return $this->redirect(array('controller'=>'entrepreneurs','action' => 'edit',$id));
			    }
				
			} else {
				debug($this->CeromonyInfo->validationErrors);
				debug($this->ChruchData->validationErrors);
				debug($this->CeromonyData->validationErrors);
				die;
				$this->Session->setFlash(__('The ceromony info could not be saved. Please, try again.'));
			}
		}
		$churches = $this->Church->find('list',array('fields'=>array('chruch_name','chruch_name')));
		$cemetries = $this->Cemetry->find('list',array('fields'=>array('cemetry_name','cemetry_name')));
		//debug($cemetries);die;
		/*$clientDatas = $this->CeromonyInfo->ClientDatum->find('list');
		$ceromonyDatas = $this->CeromonyInfo->CeromonyDatum->find('list');
		$chruchDatas = $this->CeromonyInfo->ChruchDatum->find('list');
		$cemetryDatas = $this->CeromonyInfo->CemetryDatum->find('list');*/
		$this->set(compact('churches', 'cemetries','id'));
		
	}

/**
 * edit drafts method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
				   if (!$this->Client->exists($id)) {
							throw new NotFoundException(__('Invalid client data'));
					}
					$this->Client->Behaviors->attach("Containable");
			    	$this->Client->contain(array('CeromonyInfo.CeromonyData','CeromonyInfo.ChruchData','CeromonyInfo.CemetryData'));
			    	$ceromony_info = $this->Client->find('first',array('conditions'=>array('Client.id'=>$id)));
			    	$ceromony_data['CeromonyInfo']['assignment_faxed'] = $ceromony_info['CeromonyInfo']['assignment_faxed'];
			    	$ceromony_data['CeromonyData'] = !empty($ceromony_info['CeromonyInfo']['CeromonyData'])?$ceromony_info['CeromonyInfo']['CeromonyData']:null;
			    	$ceromony_data['ChruchData'] = !empty($ceromony_info['CeromonyInfo']['ChruchData'])?$ceromony_info['CeromonyInfo']['ChruchData']:null;
			    	$ceromony_data['CemetryData'] = !empty($ceromony_info['CeromonyInfo']['CemetryData']) ?	$ceromony_info['CeromonyInfo']['CemetryData'] :null;
			    	//debug($ceromony_data);die;
			    	$CeromonyInfo_id = $ceromony_info['CeromonyInfo']['id']; 
					if ($this->request->is(array('post', 'put'))){
				 		$this->admin_add('1',$CeromonyInfo_id,$id,$this->request->data );
				 	}
			    	//$this->Client->recursive = 2;
				 	$churches = $this->Church->find('list',array('fields'=>array('chruch_name','chruch_name')));
					$cemetries = $this->Cemetry->find('list',array('fields'=>array('cemetry_name','cemetry_name')));
					//debug($client_info);die;
					//$clientDatas['ClientData'] = $client_info[0]['ClientData'];
					$this->request->data = $ceromony_data;
					$this->set(compact('churches', 'cemetries','id'));
					$this->render('admin_add');
			    }


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CeromonyInfo->exists($id)) {
			throw new NotFoundException(__('Invalid ceromony info'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CeromonyInfo->save($this->request->data)) {
				$this->Session->setFlash(__('The ceromony info has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ceromony info could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CeromonyInfo.' . $this->CeromonyInfo->primaryKey => $id));
			$this->request->data = $this->CeromonyInfo->find('first', $options);
		}
		$clientDatas = $this->CeromonyInfo->ClientDatum->find('list');
		$ceromonyDatas = $this->CeromonyInfo->CeromonyDatum->find('list');
		$chruchDatas = $this->CeromonyInfo->ChruchDatum->find('list');
		$cemetryDatas = $this->CeromonyInfo->CemetryDatum->find('list');
		$this->set(compact('clientDatas', 'ceromonyDatas', 'chruchDatas', 'cemetryDatas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CeromonyInfo->id = $id;
		if (!$this->CeromonyInfo->exists()) {
			throw new NotFoundException(__('Invalid ceromony info'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->CeromonyInfo->delete()) {
			$this->Session->setFlash(__('The ceromony info has been deleted.'));
		} else {
			$this->Session->setFlash(__('The ceromony info could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
