<?php
App::uses('AppController', 'Controller');
/**
 * FunerlLetterCards Controller
 *
 * @property FunerlLetterCard $FunerlLetterCard
 * @property PaginatorComponent $Paginator
 */
class FunerlLetterCardsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->FunerlLetterCard->recursive = 0;
		$this->Paginator->settings = array(
								        'order'=>array('FunerlLetterCard.created'=>'DESC'),
								        'limit' => 10
									);
		$this->set('funerlLetterCards', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->FunerlLetterCard->exists($id)) {
			throw new NotFoundException(__('Invalid funerl letter card'));
		}
		$options = array('conditions' => array('FunerlLetterCard.' . $this->FunerlLetterCard->primaryKey => $id));
		$this->set('funerlLetterCard', $this->FunerlLetterCard->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->FunerlLetterCard->create();
			if ($this->FunerlLetterCard->save($this->request->data)) {
				$this->Session->setFlash(__('The funerl letter card has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The funerl letter card could not be saved. Please, try again.'));
			}
		}
		$funeralLetters = $this->FunerlLetterCard->FuneralLetter->find('list',array('fields'=>array('id','letter_name')));
		$this->set(compact('funeralLetters'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->FunerlLetterCard->exists($id)) {
			throw new NotFoundException(__('Invalid funerl letter card'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->FunerlLetterCard->save($this->request->data)) {
				$this->Session->setFlash(__('The funerl letter card has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The funerl letter card could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FunerlLetterCard.' . $this->FunerlLetterCard->primaryKey => $id));
			$this->request->data = $this->FunerlLetterCard->find('first', $options);
		}
		$funeralLetters = $this->FunerlLetterCard->FuneralLetter->find('list',array('fields'=>array('id','letter_name')));
		$this->set(compact('funeralLetters'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->FunerlLetterCard->id = $id;
		if (!$this->FunerlLetterCard->exists()) {
			throw new NotFoundException(__('Invalid funerl letter card'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->FunerlLetterCard->delete()) {
			$this->Session->setFlash(__('The funerl letter card has been deleted.'));
		} else {
			$this->Session->setFlash(__('The funerl letter card could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
