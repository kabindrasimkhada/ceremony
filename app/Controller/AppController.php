<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
   public $components = array(
        'Auth' => array(
            'authorize' => array(
                'Actions' => array('actionPath' => 'controllers')
            )
        ),
        'Session',
        'Cookie',
        'RequestHandler'
    );
  public $uses = array('Client');
  public $helpers = array('Html', 'Form', 'Session');

  public function beforeFilter(){
       
       Configure::write('Config.language', 'nld');
       
        
        $this->set('language', Configure::read('Config.language'));

        if(isset($this->request->params['admin'])){
        	$this->theme = 'Admin';
        	//Auth component
        	$this->Auth->allow('login','logout');
        	$this->Auth->authenticate = array(
  						'Form' => array(
  							 'fields' => array('username'=>'username','password'=>'password'),
  							)
              );
  		   $this->Auth->loginAction = array(
  		   				'controller' => 'users',
  		   				'action'	=> 'login',
                'admin'=>true
  		   	      );
  		   $this->Auth->logoutRedirect = array(
                      'controller' => 'users',
  		   			      	'action'	=> 'index',
                      'admin'=>true
  		   			);
  		   $this->Auth->flash['element'] = 'error_message';
        
        }else
        {
        	//$this->theme = "FrontEnd";
         // $this->Auth->allow('index','add','edit','delete','overview');
          //$this->redirect('controller'=>'clientdatas','action'=>'index');
          // set cookie options
            $this->Cookie->httpOnly = true;
            $this->theme = "FrontEnd";
            AuthComponent::$sessionKey = 'Auth.User';
            $this->Auth->authenticate = array('Form' => array('recursive' => -1));
            $this->Auth->allow('login', 'logout', 'forgot_password', 'reset','addUsers','editUsers');
            //$this->Auth->allow();
            //Configure AuthComponent
            $this->Auth->loginAction = array(
                'controller' => 'users',
                'action' => 'login'
            );
            $this->Auth->logoutRedirect = array(
                'controller' => 'users',
                'action' => 'login'
            );
            $this->Auth->loginRedirect = array(
                'controller' => 'clientdatas',
                'action' => 'add'
            );
            $this->Auth->flash['element'] = 'error_message';
            $this->Auth->authError = __('Please log in first to perform this action.');
            $baseURL = Router::url('/', true);       
        }
      }

      public function beforeRender()
      {
        $this->set('title_for_layout','Ceremony Application');
        
      }

     /**
     * empty field checker method
     *
     * 
     * @param array $params
     * @return boolean
     */
      protected function _emptyElementExists($arr) {
        return array_search("", $arr) !== false;
      }
     

    

}
