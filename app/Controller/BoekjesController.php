<?php
App::uses('AppController', 'Controller');
/**
 * Boekjes Controller
 *
 * @property Boekje $Boekje
 * @property PaginatorComponent $Paginator
 */
class BoekjesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');
	
/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Boekje->recursive = 0;
		$this->set('boekjes', $this->Paginator->paginate());
		
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Boekje->exists($id)) {
			throw new NotFoundException(__('Invalid boekje'));
		}
		$options = array('conditions' => array('Boekje.' . $this->Boekje->primaryKey => $id));
		$this->set('boekje', $this->Boekje->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Boekje->create();
			if ($this->Boekje->save($this->request->data)) {
				$this->Session->setFlash(__('The boekje has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The boekje could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Boekje->exists($id)) {
			throw new NotFoundException(__('Invalid boekje'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Boekje->save($this->request->data)) {
				$this->Session->setFlash(__('The boekje has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The boekje could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Boekje.' . $this->Boekje->primaryKey => $id));
			$this->request->data = $this->Boekje->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Boekje->id = $id;
		if (!$this->Boekje->exists()) {
			throw new NotFoundException(__('Invalid boekje'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Boekje->delete()) {
			$this->Session->setFlash(__('The boekje has been deleted.'));
		} else {
			$this->Session->setFlash(__('The boekje could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
