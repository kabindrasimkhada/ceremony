<?php
App::uses('AppController', 'Controller');
/**
 * RecordingTypes Controller
 *
 * @property RecordingType $RecordingType
 * @property PaginatorComponent $Paginator
 */
class RecordingTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->RecordingType->recursive = 0;
		$this->Paginator->settings = array(
								        'order'=>array('RecordingType.created'=>'DESC'),
								        'limit' => 10
									);
		$this->set('recordingTypes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->RecordingType->exists($id)) {
			throw new NotFoundException(__('Invalid recording type'));
		}
		$options = array('conditions' => array('RecordingType.' . $this->RecordingType->primaryKey => $id));
		$this->set('recordingType', $this->RecordingType->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RecordingType->create();
			if ($this->RecordingType->save($this->request->data)) {
				$this->Session->setFlash(__('The recording type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The recording type could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->RecordingType->exists($id)) {
			throw new NotFoundException(__('Invalid recording type'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RecordingType->save($this->request->data)) {
				$this->Session->setFlash(__('The recording type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The recording type could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RecordingType.' . $this->RecordingType->primaryKey => $id));
			$this->request->data = $this->RecordingType->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->RecordingType->id = $id;
		if (!$this->RecordingType->exists()) {
			throw new NotFoundException(__('Invalid recording type'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->RecordingType->delete()) {
			$this->Session->setFlash(__('The recording type has been deleted.'));
		} else {
			$this->Session->setFlash(__('The recording type could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
