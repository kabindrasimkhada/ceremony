<?php
App::uses('AppController', 'Controller');
/**
 * PrayerCardCategories Controller
 *
 * @property PrayerCardCategory $PrayerCardCategory
 * @property PaginatorComponent $Paginator
 */
class PrayerCardCategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->PrayerCardCategory->recursive = 0;
		$this->Paginator->settings = array(
								        'order'=>array('PrayerCardCategory.created'=>'DESC'),
								        'limit' => 10
									);
		$this->set('prayerCardCategories', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->PrayerCardCategory->exists($id)) {
			throw new NotFoundException(__('Invalid prayer card category'));
		}
		$options = array('conditions' => array('PrayerCardCategory.' . $this->PrayerCardCategory->primaryKey => $id));
		$this->set('prayerCardCategory', $this->PrayerCardCategory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->PrayerCardCategory->create();
			if ($this->PrayerCardCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The prayer card category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prayer card category could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->PrayerCardCategory->exists($id)) {
			throw new NotFoundException(__('Invalid prayer card category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->PrayerCardCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The prayer card category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prayer card category could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('PrayerCardCategory.' . $this->PrayerCardCategory->primaryKey => $id));
			$this->request->data = $this->PrayerCardCategory->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->PrayerCardCategory->id = $id;
		if (!$this->PrayerCardCategory->exists()) {
			throw new NotFoundException(__('Invalid prayer card category'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->PrayerCardCategory->delete()) {
			$this->Session->setFlash(__('The prayer card category has been deleted.'));
		} else {
			$this->Session->setFlash(__('The prayer card category could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
