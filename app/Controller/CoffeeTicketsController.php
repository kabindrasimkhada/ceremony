<?php
App::uses('AppController', 'Controller');
/**
 * CoffeeTickets Controller
 *
 * @property CoffeeTicket $CoffeeTicket
 * @property PaginatorComponent $Paginator
 */
class CoffeeTicketsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->CoffeeTicket->recursive = 0;
		$this->set('coffeeTickets', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->CoffeeTicket->exists($id)) {
			throw new NotFoundException(__('Invalid coffee ticket'));
		}
		$options = array('conditions' => array('CoffeeTicket.' . $this->CoffeeTicket->primaryKey => $id));
		$this->set('coffeeTicket', $this->CoffeeTicket->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->CoffeeTicket->create();
			if ($this->CoffeeTicket->save($this->request->data)) {
				$this->Session->setFlash(__('The coffee ticket has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The coffee ticket could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->CoffeeTicket->exists($id)) {
			throw new NotFoundException(__('Invalid coffee ticket'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CoffeeTicket->save($this->request->data)) {
				$this->Session->setFlash(__('The coffee ticket has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The coffee ticket could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CoffeeTicket.' . $this->CoffeeTicket->primaryKey => $id));
			$this->request->data = $this->CoffeeTicket->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->CoffeeTicket->id = $id;
		if (!$this->CoffeeTicket->exists()) {
			throw new NotFoundException(__('Invalid coffee ticket'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CoffeeTicket->delete()) {
			$this->Session->setFlash(__('The coffee ticket has been deleted.'));
		} else {
			$this->Session->setFlash(__('The coffee ticket could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
