<?php
App::uses('AppController', 'Controller');
/**
 * GuideAfterDeaths Controller
 *
 * @property GuideAfterDeath $GuideAfterDeath
 * @property PaginatorComponent $Paginator
 */
class GuideAfterDeathsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->GuideAfterDeath->recursive = 0;
		$this->set('guideAfterDeaths', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->GuideAfterDeath->exists($id)) {
			throw new NotFoundException(__('Invalid guide after death'));
		}
		$options = array('conditions' => array('GuideAfterDeath.' . $this->GuideAfterDeath->primaryKey => $id));
		$this->set('guideAfterDeath', $this->GuideAfterDeath->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->GuideAfterDeath->create();
			if ($this->GuideAfterDeath->save($this->request->data)) {
				$this->Session->setFlash(__('The guide after death has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The guide after death could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->GuideAfterDeath->exists($id)) {
			throw new NotFoundException(__('Invalid guide after death'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->GuideAfterDeath->save($this->request->data)) {
				$this->Session->setFlash(__('The guide after death has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The guide after death could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('GuideAfterDeath.' . $this->GuideAfterDeath->primaryKey => $id));
			$this->request->data = $this->GuideAfterDeath->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->GuideAfterDeath->id = $id;
		if (!$this->GuideAfterDeath->exists()) {
			throw new NotFoundException(__('Invalid guide after death'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->GuideAfterDeath->delete()) {
			$this->Session->setFlash(__('The guide after death has been deleted.'));
		} else {
			$this->Session->setFlash(__('The guide after death could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
