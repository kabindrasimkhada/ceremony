<?php
App::uses('AppController', 'Controller');
/**
 * Stamps Controller
 *
 * @property Stamp $Stamp
 * @property PaginatorComponent $Paginator
 */
class StampsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Stamp->recursive = 0;
		$this->Paginator->settings = array(
								        'order'=>array('Stamp.created'=>'DESC'),
								        'limit' => 10
									);
		$this->set('stamps', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Stamp->exists($id)) {
			throw new NotFoundException(__('Invalid stamp'));
		}
		$options = array('conditions' => array('Stamp.' . $this->Stamp->primaryKey => $id));
		$this->set('stamp', $this->Stamp->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Stamp->create();
			if ($this->Stamp->save($this->request->data)) {
				$this->Session->setFlash(__('The stamp has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The stamp could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Stamp->exists($id)) {
			throw new NotFoundException(__('Invalid stamp'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Stamp->save($this->request->data)) {
				$this->Session->setFlash(__('The stamp has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The stamp could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Stamp.' . $this->Stamp->primaryKey => $id));
			$this->request->data = $this->Stamp->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Stamp->id = $id;
		if (!$this->Stamp->exists()) {
			throw new NotFoundException(__('Invalid stamp'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Stamp->delete()) {
			$this->Session->setFlash(__('The stamp has been deleted.'));
		} else {
			$this->Session->setFlash(__('The stamp could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
