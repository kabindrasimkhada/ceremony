<?php
App::uses('AppController', 'Controller');
/**
 * Undertakers Controller
 *
 * @property Undertaker $Undertaker
 * @property PaginatorComponent $Paginator
 */
class UndertakersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Undertaker->recursive = 0;
		$this->set('undertakers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Undertaker->exists($id)) {
			throw new NotFoundException(__('Invalid undertaker'));
		}
		$options = array('conditions' => array('Undertaker.' . $this->Undertaker->primaryKey => $id));
		$this->set('undertaker', $this->Undertaker->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Undertaker->create();
			if ($this->Undertaker->save($this->request->data)) {
				$this->Session->setFlash(__('The undertaker has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The undertaker could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Undertaker->exists($id)) {
			throw new NotFoundException(__('Invalid undertaker'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Undertaker->save($this->request->data)) {
				$this->Session->setFlash(__('The undertaker has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The undertaker could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Undertaker.' . $this->Undertaker->primaryKey => $id));
			$this->request->data = $this->Undertaker->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Undertaker->id = $id;
		if (!$this->Undertaker->exists()) {
			throw new NotFoundException(__('Invalid undertaker'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Undertaker->delete()) {
			$this->Session->setFlash(__('The undertaker has been deleted.'));
		} else {
			$this->Session->setFlash(__('The undertaker could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
