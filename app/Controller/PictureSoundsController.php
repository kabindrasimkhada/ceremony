<?php
App::uses('AppController', 'Controller');
/**
 * PictureSounds Controller
 *
 * @property PictureSound $PictureSound
 * @property PaginatorComponent $Paginator
 */
class PictureSoundsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');
	public $uses = array('RecordingType','PictureSound');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->PictureSound->recursive = 0;
		$this->set('pictureSounds', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->PictureSound->exists($id)) {
			throw new NotFoundException(__('Invalid picture sound'));
		}
		$options = array('conditions' => array('PictureSound.' . $this->PictureSound->primaryKey => $id));
		$this->set('pictureSound', $this->PictureSound->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add($id=null,$PictureSound_id=null,$Cid=null,$data = null){
                if(is_null($id)){
			return $this->redirect(array('controller'=>'clientDatas','action' => 'add'));
		}
		if ($this->request->is('post') || !empty($data)) {
			$this->PictureSound->create();
			if(!empty($data)){
				$this->request->data = $data;
				$music['music_name'] = $this->request->data('Music');
				$this->request->data['Music'] = null;
				//debug($this->spilitArraytoString($data['music_name']));die;
				$this->request->data['Music']['music_name'] = implode(',',$this->spilitArraytoString($music['music_name']));
				$this->PictureSound->id = $PictureSound_id;
			}
		   //debug($this->request->data);
			$music['music_name'] = $this->request->data('Music');
			$this->request->data['Music'] = null;
			//debug($this->spilitArraytoString($data['music_name']));die;
			$this->request->data['Music']['music_name'] = implode(',',$this->spilitArraytoString($music['music_name']));
			$val = $this->_emptyElementExists($this->request->data['PictureSound']);
			if($val !== true){
				$this->request->data['PictureSound']['status'] = 1;
			}
			if ($this->PictureSound->saveAll($this->request->data)) {
				if(empty($Cid)){
				 	
					$picture_sound_id = $this->PictureSound->id;
					//echo $decease_id;die;
					$this->Client->id = $id;
				    $this->Client->saveField('picture_sound_id',$picture_sound_id);
					$this->Session->setFlash(__('The picture sound has been saved.'));
					return $this->redirect(array('controller'=>'coffeeRooms','action' => 'add',$this->Client->id));
			   }else{
			      $this->Client->id = $Cid;
			      $this->Session->setFlash(__('The picture sound data has been updated.'));
			      if(!isset($PictureSound_id) && empty($PictureSound_id)){
			      	$PictureSound_id =$this->PictureSound->id;
			      	$this->Session->setFlash(__('The picture sound has been saved.'));
				  }
			      $this->Client->saveField(
				    	'picture_sound_id',$PictureSound_id
				    	);
			      $id = $this->Client->id;
			      return $this->redirect(array('controller'=>'coffeeRooms','action' => 'edit',$id));
			    }
				
			} else {
				$this->Session->setFlash(__('The picture sound could not be saved. Please, try again.'));
			}
		}
		$recording_types = $this->RecordingType->find('list',
													   array(
													   	     'fields'=>array(
													   	     	            'name','name'
													   	     	  )
													   	     )
													   );
		
		//$musics = $this->PictureSound->Music->find('list');
		$this->set(compact('recording_types'));
	}

/**
 * edit drafts method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
				   if (!$this->Client->exists($id)) {
							throw new NotFoundException(__('Invalid client data'));
					}
					$this->Client->Behaviors->attach("Containable");
			    	$this->Client->contain(array('PictureSound.Music'));
			    	$pictureSound_info = $this->Client->find('first',array('conditions'=>array('Client.id'=>$id)));
			    	//$pictureSound_info['Music']=$pictureSound_info['PictureSound']['Music'];
			    	if(array_key_exists('Music', $pictureSound_info['PictureSound'])){
			    	$musics = $pictureSound_info['PictureSound']['Music']['music_name'];
			    	$musics = explode(',',$musics);
				    	foreach($musics as $key=>$music){
	                        $pictureSound_info['Music']["music_name[$key]"] = $music;
				    	}
			    	}
			       	$PictureSound_id = $pictureSound_info['PictureSound']['id']; 
					if ($this->request->is(array('post', 'put'))){
				 		$this->admin_add('1',$PictureSound_id,$id,$this->request->data);
				 	}
			    	//$this->Client->recursive = 2;

					//debug($client_info);die;
					//$clientDatas['ClientData'] = $client_info[0]['ClientData'];
					$this->request->data = $pictureSound_info;
					$recording_types = $this->RecordingType->find('list',
																	   array(
																	   	     'fields'=>array('name','name')
																	   	     )
														   );
				
					$this->set(compact('recording_types','id'));
					$this->render('admin_add');
			    }


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->PictureSound->exists($id)) {
			throw new NotFoundException(__('Invalid picture sound'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->PictureSound->save($this->request->data)) {
				$this->Session->setFlash(__('The picture sound has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The picture sound could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('PictureSound.' . $this->PictureSound->primaryKey => $id));
			$this->request->data = $this->PictureSound->find('first', $options);
		}
		$musics = $this->PictureSound->Music->find('list');
		$this->set(compact('musics'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->PictureSound->id = $id;
		if (!$this->PictureSound->exists()) {
			throw new NotFoundException(__('Invalid picture sound'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->PictureSound->delete()) {
			$this->Session->setFlash(__('The picture sound has been deleted.'));
		} else {
			$this->Session->setFlash(__('The picture sound could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	/*/**
 * helper method
 *
 * 
 * @param array $data
 * @return string seperated by comma
 */

	public function spilitArraytoString($data)
	{
		$required = array();
		foreach ($data as $key => $value) {
			$required[]= $value;
		}
		return $required;
	}
}
