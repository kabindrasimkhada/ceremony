<?php
App::uses('AppController', 'Controller');
/**
 * MisboekjesDatas Controller
 *
 * @property MisboekjesData $MisboekjesData
 * @property PaginatorComponent $Paginator
 */
class MisboekjesDatasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->MisboekjesData->recursive = 0;
		$this->set('misboekjesDatas', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->MisboekjesData->exists($id)) {
			throw new NotFoundException(__('Invalid misboekjes data'));
		}
		$options = array('conditions' => array('MisboekjesData.' . $this->MisboekjesData->primaryKey => $id));
		$this->set('misboekjesData', $this->MisboekjesData->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->MisboekjesData->create();
			if ($this->MisboekjesData->save($this->request->data)) {
				$this->Session->setFlash(__('The misboekjes data has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The misboekjes data could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->MisboekjesData->exists($id)) {
			throw new NotFoundException(__('Invalid misboekjes data'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MisboekjesData->save($this->request->data)) {
				$this->Session->setFlash(__('The misboekjes data has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The misboekjes data could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MisboekjesData.' . $this->MisboekjesData->primaryKey => $id));
			$this->request->data = $this->MisboekjesData->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->MisboekjesData->id = $id;
		if (!$this->MisboekjesData->exists()) {
			throw new NotFoundException(__('Invalid misboekjes data'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->MisboekjesData->delete()) {
			$this->Session->setFlash(__('The misboekjes data has been deleted.'));
		} else {
			$this->Session->setFlash(__('The misboekjes data could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
