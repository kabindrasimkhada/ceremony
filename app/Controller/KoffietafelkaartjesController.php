<?php
App::uses('AppController', 'Controller');
/**
 * Koffietafelkaartjes Controller
 *
 * @property Koffietafelkaartje $Koffietafelkaartje
 * @property PaginatorComponent $Paginator
 */
class KoffietafelkaartjesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Koffietafelkaartje->recursive = 0;
		$this->Paginator->settings = array(
								        'order'=>array('Koffietafelkaartje.created'=>'DESC'),
								        'limit' => 10
									);
		$this->set('koffietafelkaartjes', $this->Paginator->paginate());
	}


/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Koffietafelkaartje->create();
			if ($this->Koffietafelkaartje->save($this->request->data)) {
				$this->Session->setFlash(__('The koffietafelkaartje has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The koffietafelkaartje could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) { 
		if (!$this->Koffietafelkaartje->exists($id)) {
			throw new NotFoundException(__('Invalid koffietafelkaartje'));
		}
		if ($this->request->is(array('post', 'put'))){ 
			if ($this->Koffietafelkaartje->save($this->request->data)) {
				$this->Session->setFlash(__('The koffietafelkaartje has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The koffietafelkaartje could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Koffietafelkaartje.' . $this->Koffietafelkaartje->primaryKey => $id));
			$this->request->data = $this->Koffietafelkaartje->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Koffietafelkaartje->id = $id;
		if (!$this->Koffietafelkaartje->exists()) {
			throw new NotFoundException(__('Invalid koffietafelkaartje'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Koffietafelkaartje->delete()) {
			$this->Session->setFlash(__('The koffietafelkaartje has been deleted.'));
		} else {
			$this->Session->setFlash(__('The koffietafelkaartje could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
