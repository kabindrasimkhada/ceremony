<?php
App::uses('AppController', 'Controller');
/**
 * Homebarings Controller
 *
 * @property Homebaring $Homebaring
 * @property PaginatorComponent $Paginator
 */
class HomebaringsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Homebaring->recursive = 0;
		$this->Paginator->settings = array(
								        'order'=>array('Homebaring.created'=>'DESC'),
								        'limit' => 10
									);
		$this->set('homebarings', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Homebaring->exists($id)) {
			throw new NotFoundException(__('Invalid homebaring'));
		}
		$options = array('conditions' => array('Homebaring.' . $this->Homebaring->primaryKey => $id));
		$this->set('homebaring', $this->Homebaring->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Homebaring->create();
			if ($this->Homebaring->save($this->request->data)) {
				$this->Session->setFlash(__('The homebaring has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The homebaring could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Homebaring->exists($id)) {
			throw new NotFoundException(__('Invalid homebaring'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Homebaring->save($this->request->data)) {
				$this->Session->setFlash(__('The homebaring has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The homebaring could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Homebaring.' . $this->Homebaring->primaryKey => $id));
			$this->request->data = $this->Homebaring->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Homebaring->id = $id;
		if (!$this->Homebaring->exists()) {
			throw new NotFoundException(__('Invalid homebaring'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Homebaring->delete()) {
			$this->Session->setFlash(__('The homebaring has been deleted.'));
		} else {
			$this->Session->setFlash(__('The homebaring could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
