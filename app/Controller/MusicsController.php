<?php
App::uses('AppController', 'Controller');
/**
 * Musics Controller
 *
 * @property Music $Music
 * @property PaginatorComponent $Paginator
 */
class MusicsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Music->recursive = 0;
		$this->set('musics', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Music->exists($id)) {
			throw new NotFoundException(__('Invalid music'));
		}
		$options = array('conditions' => array('Music.' . $this->Music->primaryKey => $id));
		$this->set('music', $this->Music->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Music->create();
			if ($this->Music->save($this->request->data)) {
				$this->Session->setFlash(__('The music has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The music could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Music->exists($id)) {
			throw new NotFoundException(__('Invalid music'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Music->save($this->request->data)) {
				$this->Session->setFlash(__('The music has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The music could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Music.' . $this->Music->primaryKey => $id));
			$this->request->data = $this->Music->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Music->id = $id;
		if (!$this->Music->exists()) {
			throw new NotFoundException(__('Invalid music'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Music->delete()) {
			$this->Session->setFlash(__('The music has been deleted.'));
		} else {
			$this->Session->setFlash(__('The music could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
