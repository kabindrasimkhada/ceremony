<?php
App::uses('AppController', 'Controller');
/**
 * CoffeeRooms Controller
 *
 * @property CoffeeRoom $CoffeeRoom
 * @property PaginatorComponent $Paginator
 */
class CoffeeRoomsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');
    public $uses = array('ReservedType','CoffeeRoom');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CoffeeRoom->recursive = 0;
		$this->set('coffeeRooms', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CoffeeRoom->exists($id)) {
			throw new NotFoundException(__('Invalid coffee room'));
		}
		$options = array('conditions' => array('CoffeeRoom.' . $this->CoffeeRoom->primaryKey => $id));
		$this->set('coffeeRoom', $this->CoffeeRoom->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add($id=null,$CoffeeRoom_id=null,$Cid=null,$data = null){
                if(is_null($id)){
			return $this->redirect(array('controller'=>'clientDatas','action' => 'add'));
		}
		if ($this->request->is('post') || !empty($data)) {
			$this->CoffeeRoom->create();
			if(array_key_exists('number',$this->request->data['CoffeeRoom']) && array_key_exists('letter',$this->request->data['CoffeeRoom'])){
			$this->request->data['CoffeeRoom']['number'] = is_array($this->request->data['CoffeeRoom']['number'])?implode(',',$this->request->data['CoffeeRoom']['number']):null;
                        $this->request->data['CoffeeRoom']['letter'] = is_array($this->request->data['CoffeeRoom']['letter'])?implode(',',$this->request->data['CoffeeRoom']['letter']):null;
			}
			if(!empty($data)){
				$this->request->data = $data;
				if(array_key_exists('number',$this->request->data['CoffeeRoom']) && array_key_exists('letter',$this->request->data['CoffeeRoom'])){
				$this->request->data['CoffeeRoom']['number'] = is_array($this->request->data['CoffeeRoom']['number'])?implode(',',$this->request->data['CoffeeRoom']['number']):null;
           		$this->request->data['CoffeeRoom']['letter'] = is_array($this->request->data['CoffeeRoom']['letter'])?implode(',',$this->request->data['CoffeeRoom']['letter']):null;
				}
				$this->CoffeeRoom->id = $CoffeeRoom_id;
			}
			$val = $this->_emptyElementExists($this->request->data['CoffeeRoom']);
			if($val !== true){
				$this->request->data['CoffeeRoom']['status'] = 1;
			}
			if ($this->CoffeeRoom->save($this->request->data)) {
				if(empty($Cid)){
				 	$coffee_room_id = $this->CoffeeRoom->id;
				//echo $CoffeeRoom_id;die;
				$this->Client->id = $id;
			    $this->Client->saveField('coffee_room_id',$coffee_room_id);
				$this->Session->setFlash(__('The coffee room data has been saved.'));
				return $this->redirect(array('controller'=>'miscellaneouses','action' => 'add',$this->Client->id));
			   }else{
			      $this->Client->id = $Cid;
			      $this->Session->setFlash(__('The coffee room data has been updated.'));
			      if(!isset($CoffeeRoom_id) && empty($CoffeeRoom_id)){
			      	$CoffeeRoom_id =$this->CoffeeRoom->id;
			      	$this->Session->setFlash(__('The coffee room data has been saved.'));
				  }
			      $this->Client->saveField(
				    	'coffee_room_id',$CoffeeRoom_id
				    	);
			      $id = $this->Client->id;
			      
			      return $this->redirect(array('controller'=>'miscellaneouses','action' => 'edit',$id));
			    }
				
			} else {
				$this->Session->setFlash(__('The coffee room could not be saved. Please, try again.'));
			}
		}
		$reserved_types = $this->ReservedType->find('list',array('fields'=>array('name','name')));
		$this->set(compact('reserved_types'));
	}
/**
 * edit drafts method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
				   if (!$this->Client->exists($id)) {
							throw new NotFoundException(__('Invalid client data'));
					}
					$this->Client->Behaviors->attach("Containable");
			    	$this->Client->contain('CoffeeRoom');
			    	$CoffeeRoom_info = $this->Client->find('first',array(
			    															'conditions'=>array(
			    																				'Client.id'=>$id
			    																				)
			    														)
			   											);
			    	$CoffeeRoom_id = $CoffeeRoom_info['CoffeeRoom']['id']; 
					if ($this->request->is(array('post', 'put'))){
				 		$this->admin_add('1',$CoffeeRoom_id,$id,$this->request->data);
				 	}
			    	$this->request->data = $CoffeeRoom_info;
			    	$numbers = !empty($this->request->data['CoffeeRoom']['number']) ? explode(',',$this->request->data['CoffeeRoom']['number']):null ;
			    	$letters = !empty($this->request->data['CoffeeRoom']['letter']) ? explode(',',$this->request->data['CoffeeRoom']['letter']):null ;
			    	if($numbers !== null){
				    	foreach($numbers as $key=>$number){
				    		$dynamicDatas[$key]['number'] = $number;
				    		$dynamicDatas[$key]['letter'] = $letters[$key];
				    		
				    	}
				    }
			    	$reserved_types = $this->ReservedType->find('list',array('fields'=>array('name','name')));
					$this->set(compact('reserved_types','dynamicDatas','id'));
					$this->render('admin_add');
			    }	

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CoffeeRoom->exists($id)) {
			throw new NotFoundException(__('Invalid coffee room'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CoffeeRoom->save($this->request->data)) {
				$this->Session->setFlash(__('The coffee room has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The coffee room could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CoffeeRoom.' . $this->CoffeeRoom->primaryKey => $id));
			$this->request->data = $this->CoffeeRoom->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CoffeeRoom->id = $id;
		if (!$this->CoffeeRoom->exists()) {
			throw new NotFoundException(__('Invalid coffee room'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CoffeeRoom->delete()) {
			$this->Session->setFlash(__('The coffee room has been deleted.'));
		} else {
			$this->Session->setFlash(__('The coffee room could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
