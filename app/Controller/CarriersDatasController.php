<?php
App::uses('AppController', 'Controller');
/**
 * CarriersDatas Controller
 *
 * @property CarriersData $CarriersData
 * @property PaginatorComponent $Paginator
 */
class CarriersDatasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CarriersData->recursive = 0;
		$this->set('carriersDatas', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CarriersData->exists($id)) {
			throw new NotFoundException(__('Invalid carriers data'));
		}
		$options = array('conditions' => array('CarriersData.' . $this->CarriersData->primaryKey => $id));
		$this->set('carriersData', $this->CarriersData->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CarriersData->create();
			if ($this->CarriersData->save($this->request->data)) {
				$this->Session->setFlash(__('The carriers data has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The carriers data could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CarriersData->exists($id)) {
			throw new NotFoundException(__('Invalid carriers data'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CarriersData->save($this->request->data)) {
				$this->Session->setFlash(__('The carriers data has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The carriers data could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CarriersData.' . $this->CarriersData->primaryKey => $id));
			$this->request->data = $this->CarriersData->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CarriersData->id = $id;
		if (!$this->CarriersData->exists()) {
			throw new NotFoundException(__('Invalid carriers data'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->CarriersData->delete()) {
			$this->Session->setFlash(__('The carriers data has been deleted.'));
		} else {
			$this->Session->setFlash(__('The carriers data could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
