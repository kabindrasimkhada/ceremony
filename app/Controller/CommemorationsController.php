<?php
App::uses('AppController', 'Controller');
/**
 * Commemorations Controller
 *
 * @property Commemoration $Commemoration
 * @property PaginatorComponent $Paginator
 */
class CommemorationsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Commemoration->recursive = 0;
		$this->set('commemorations', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Commemoration->exists($id)) {
			throw new NotFoundException(__('Invalid commemoration'));
		}
		$options = array('conditions' => array('Commemoration.' . $this->Commemoration->primaryKey => $id));
		$this->set('commemoration', $this->Commemoration->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Commemoration->create();
			if ($this->Commemoration->save($this->request->data)) {
				$this->Session->setFlash(__('The commemoration has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The commemoration could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Commemoration->exists($id)) {
			throw new NotFoundException(__('Invalid commemoration'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Commemoration->save($this->request->data)) {
				$this->Session->setFlash(__('The commemoration has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The commemoration could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Commemoration.' . $this->Commemoration->primaryKey => $id));
			$this->request->data = $this->Commemoration->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Commemoration->id = $id;
		if (!$this->Commemoration->exists()) {
			throw new NotFoundException(__('Invalid commemoration'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Commemoration->delete()) {
			$this->Session->setFlash(__('The commemoration has been deleted.'));
		} else {
			$this->Session->setFlash(__('The commemoration could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
