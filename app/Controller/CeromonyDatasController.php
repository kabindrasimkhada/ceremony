<?php
App::uses('AppController', 'Controller');
/**
 * CeromonyDatas Controller
 *
 * @property CeromonyData $CeromonyData
 * @property PaginatorComponent $Paginator
 */
class CeromonyDatasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CeromonyData->recursive = 0;
		$this->set('ceromonyDatas', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CeromonyData->exists($id)) {
			throw new NotFoundException(__('Invalid ceromony data'));
		}
		$options = array('conditions' => array('CeromonyData.' . $this->CeromonyData->primaryKey => $id));
		$this->set('ceromonyData', $this->CeromonyData->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CeromonyData->create();
			
			if ($this->CeromonyData->save($this->request->data)) {
				$this->Session->setFlash(__('The ceromony data has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ceromony data could not be saved. Please, try again.'));
			}
		}
		$clientDatas = $this->CeromonyData->ClientData->find('list');
		$this->set(compact('clientDatas'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CeromonyData->exists($id)) {
			throw new NotFoundException(__('Invalid ceromony data'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CeromonyData->save($this->request->data)) {
				$this->Session->setFlash(__('The ceromony data has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ceromony data could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CeromonyData.' . $this->CeromonyData->primaryKey => $id));
			$this->request->data = $this->CeromonyData->find('first', $options);
		}
		$clientDatas = $this->CeromonyData->ClientData->find('list');
		$this->set(compact('clientDatas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CeromonyData->id = $id;
		if (!$this->CeromonyData->exists()) {
			throw new NotFoundException(__('Invalid ceromony data'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->CeromonyData->delete()) {
			$this->Session->setFlash(__('The ceromony data has been deleted.'));
		} else {
			$this->Session->setFlash(__('The ceromony data could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
