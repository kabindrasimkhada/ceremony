<?php
App::uses('AppController', 'Controller');
/**
 * FarewellServices Controller
 *
 * @property FarewellService $FarewellService
 * @property PaginatorComponent $Paginator
 */
class FarewellServicesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FarewellService->recursive = 0;
		$this->set('farewellServices', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FarewellService->exists($id)) {
			throw new NotFoundException(__('Invalid farewell service'));
		}
		$options = array('conditions' => array('FarewellService.' . $this->FarewellService->primaryKey => $id));
		$this->set('farewellService', $this->FarewellService->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add($id=null,$FarewellService_id=null,$Cid=null,$data = null){
                if(is_null($id)){
			return $this->redirect(array('controller'=>'clientDatas','action' => 'add'));
		}
		if ($this->request->is('post') || !empty($data)) {
			$this->FarewellService->create();
			if(!empty($data)){
				$this->request->data = $data;
				$this->FarewellService->id = $FarewellService_id;
			}
			$val = $this->_emptyElementExists($this->request->data['FarewellService']);
			if($val !== true){
				$this->request->data['FarewellService']['status'] = 1;
			}
			if ($this->FarewellService->save($this->request->data)){
				if(empty($Cid)){
				 	
					$farewell_service_id = $this->FarewellService->id;
					//echo $FarewellService_id;die;
					$this->Client->id = $id;
				    $this->Client->saveField('farewell_service_id',$farewell_service_id);
					$this->Session->setFlash(__('The farewell service has been saved.'));
					return $this->redirect(
						                    array('controller'=>'pictureSounds',
						                    	     'action' => 'add',
						                    	     $this->Client->id,
						                   	 )
						                  );
			   }else{
			      $this->Client->id = $Cid;
			      $this->Session->setFlash(__('The farewell data has been updated.'));
			      if(!isset($FarewellService_id) && empty($FarewellService_id)){
			      	$FarewellService_id = $this->FarewellService->id;
			      	$this->Session->setFlash(__('The farewell service has been saved.'));
				  }
			      $this->Client->saveField(
				    	'farewell_service_id',$FarewellService_id
				    	);
			      $id = $this->Client->id;
			      return $this->redirect(array('controller'=>'pictureSounds','action' => 'edit',$id));
			    }
				
			} else {
				$this->Session->setFlash(__('The farewell service could not be saved. Please, try again.'));
			}
		}
		//$clientDatas = $this->FarewellService->ClientData->find('list');
		$this->set(compact('clientDatas'));
	}
/**
 * edit drafts method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null){
				   if (!$this->Client->exists($id)) {
							throw new NotFoundException(__('Invalid client data'));
					}
					$this->Client->Behaviors->attach("Containable");
			    	$this->Client->contain('FarewellService');
			    	$FarewellService_info = $this->Client->find('first',array('conditions'=>array('Client.id'=>$id)));
			    	$FarewellService_id = $FarewellService_info['FarewellService']['id']; 
					if ($this->request->is(array('post', 'put'))){
				 		$this->admin_add('1',$FarewellService_id,$id,$this->request->data);
				 	}
			    	//$this->Client->recursive = 2;

					//debug($client_info);die;
					//$clientDatas['ClientData'] = $client_info[0]['ClientData'];
					$this->request->data = $FarewellService_info;
					$this->set(compact('id'));
					$this->render('admin_add');
			    }


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FarewellService->exists($id)) {
			throw new NotFoundException(__('Invalid farewell service'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->FarewellService->save($this->request->data)) {
				$this->Session->setFlash(__('The farewell service has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The farewell service could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FarewellService.' . $this->FarewellService->primaryKey => $id));
			$this->request->data = $this->FarewellService->find('first', $options);
		}
		$clientDatas = $this->FarewellService->ClientData->find('list');
		$this->set(compact('clientDatas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->FarewellService->id = $id;
		if (!$this->FarewellService->exists()) {
			throw new NotFoundException(__('Invalid farewell service'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->FarewellService->delete()) {
			$this->Session->setFlash(__('The farewell service has been deleted.'));
		} else {
			$this->Session->setFlash(__('The farewell service could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
