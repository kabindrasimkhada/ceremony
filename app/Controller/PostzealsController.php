<?php
App::uses('AppController', 'Controller');
/**
 * Postzeals Controller
 *
 * @property Postzeal $Postzeal
 * @property PaginatorComponent $Paginator
 */
class PostzealsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Postzeal->recursive = 0;
		$this->set('postzeals', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Postzeal->exists($id)) {
			throw new NotFoundException(__('Invalid postzeal'));
		}
		$options = array('conditions' => array('Postzeal.' . $this->Postzeal->primaryKey => $id));
		$this->set('postzeal', $this->Postzeal->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Postzeal->create();
			if ($this->Postzeal->save($this->request->data)) {
				$this->Session->setFlash(__('The postzeal has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The postzeal could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Postzeal->exists($id)) {
			throw new NotFoundException(__('Invalid postzeal'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Postzeal->save($this->request->data)) {
				$this->Session->setFlash(__('The postzeal has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The postzeal could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Postzeal.' . $this->Postzeal->primaryKey => $id));
			$this->request->data = $this->Postzeal->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Postzeal->id = $id;
		if (!$this->Postzeal->exists()) {
			throw new NotFoundException(__('Invalid postzeal'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Postzeal->delete()) {
			$this->Session->setFlash(__('The postzeal has been deleted.'));
		} else {
			$this->Session->setFlash(__('The postzeal could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
