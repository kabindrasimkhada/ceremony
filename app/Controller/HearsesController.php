<?php
App::uses('AppController', 'Controller');
/**
 * Hearses Controller
 *
 * @property Hearse $Hearse
 * @property PaginatorComponent $Paginator
 */
class HearsesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Hearse->recursive = 0;
		$this->Paginator->settings = array(
								        'order'=>array('Hearse.created'=>'DESC'),
								        'limit' => 10
									);
		$this->set('hearses', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Hearse->exists($id)) {
			throw new NotFoundException(__('Invalid hearse'));
		}
		$options = array('conditions' => array('Hearse.' . $this->Hearse->primaryKey => $id));
		$this->set('hearse', $this->Hearse->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Hearse->create();
			if ($this->Hearse->save($this->request->data)) {
				$this->Session->setFlash(__('The hearse has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The hearse could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Hearse->exists($id)) {
			throw new NotFoundException(__('Invalid hearse'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Hearse->save($this->request->data)) {
				$this->Session->setFlash(__('The hearse has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The hearse could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Hearse.' . $this->Hearse->primaryKey => $id));
			$this->request->data = $this->Hearse->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Hearse->id = $id;
		if (!$this->Hearse->exists()) {
			throw new NotFoundException(__('Invalid hearse'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Hearse->delete()) {
			$this->Session->setFlash(__('The hearse has been deleted.'));
		} else {
			$this->Session->setFlash(__('The hearse could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
