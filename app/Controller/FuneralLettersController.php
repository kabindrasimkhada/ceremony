<?php
App::uses('AppController', 'Controller');
/**
 * FuneralLetters Controller
 *
 * @property FuneralLetter $FuneralLetter
 * @property PaginatorComponent $Paginator
 */
class FuneralLettersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function  admin_index() {
		$this->FuneralLetter->recursive = 0;
		$this->Paginator->settings = array(
								        'order'=>array('FuneralLetter.created'=>'DESC'),
								        'limit' => 10
									);
		$this->set('funeralLetters', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function  admin_view($id = null) {
		if (!$this->FuneralLetter->exists($id)) {
			throw new NotFoundException(__('Invalid funeral letter'));
		}
		$options = array('conditions' => array('FuneralLetter.' . $this->FuneralLetter->primaryKey => $id));
		$this->set('funeralLetter', $this->FuneralLetter->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function  admin_add() {
		if ($this->request->is('post')) {
			$this->FuneralLetter->create();
			if ($this->FuneralLetter->save($this->request->data)) {
				$this->Session->setFlash(__('The funeral letter has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The funeral letter could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function  admin_edit($id = null) {
		if (!$this->FuneralLetter->exists($id)) {
			throw new NotFoundException(__('Invalid funeral letter'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->FuneralLetter->save($this->request->data)) {
				$this->Session->setFlash(__('The funeral letter has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The funeral letter could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FuneralLetter.' . $this->FuneralLetter->primaryKey => $id));
			$this->request->data = $this->FuneralLetter->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function  admin_delete($id = null) {
		$this->FuneralLetter->id = $id;
		if (!$this->FuneralLetter->exists()) {
			throw new NotFoundException(__('Invalid funeral letter'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->FuneralLetter->delete()) {
			$this->Session->setFlash(__('The funeral letter has been deleted.'));
		} else {
			$this->Session->setFlash(__('The funeral letter could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
