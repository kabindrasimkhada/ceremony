<?php
App::uses('AppController', 'Controller');
/**
 * FuneralLettersDatas Controller
 *
 * @property FuneralLettersData $FuneralLettersData
 * @property PaginatorComponent $Paginator
 */
class FuneralLettersDatasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FuneralLettersData->recursive = 0;
		$this->set('funeralLettersDatas', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FuneralLettersData->exists($id)) {
			throw new NotFoundException(__('Invalid funeral letters data'));
		}
		$options = array('conditions' => array('FuneralLettersData.' . $this->FuneralLettersData->primaryKey => $id));
		$this->set('funeralLettersData', $this->FuneralLettersData->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->FuneralLettersData->create();
			if ($this->FuneralLettersData->save($this->request->data)) {
				$this->Session->setFlash(__('The funeral letters data has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The funeral letters data could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FuneralLettersData->exists($id)) {
			throw new NotFoundException(__('Invalid funeral letters data'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->FuneralLettersData->save($this->request->data)) {
				$this->Session->setFlash(__('The funeral letters data has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The funeral letters data could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FuneralLettersData.' . $this->FuneralLettersData->primaryKey => $id));
			$this->request->data = $this->FuneralLettersData->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->FuneralLettersData->id = $id;
		if (!$this->FuneralLettersData->exists()) {
			throw new NotFoundException(__('Invalid funeral letters data'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->FuneralLettersData->delete()) {
			$this->Session->setFlash(__('The funeral letters data has been deleted.'));
		} else {
			$this->Session->setFlash(__('The funeral letters data could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
