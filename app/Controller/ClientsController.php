<?php
App::uses('AppController', 'Controller');
/**
 * Clients Controller
 *
 * @property Client $Client
 * @property PaginatorComponent $Paginator
 */
class ClientsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth','Mpdf.Mpdf');

/**
 * drafts method
 *
 * @return void
 */
	public function admin_drafts(){ 
		$title = array(
                    'title'=>'concepten',
                    'explain'=>'Lijst concepten'
                );
        $breadCrumb = array(
	    					array(
	                              'name' =>__('Dashboard'),
	                              'link' => Router::url(array('controller' => 'users', 'action' => 'dashboard'))
	                        	),
                            array(
                                  'name' =>'concepten',
                                  'link' => Router::url(array('controller' => 'clients','action'=>'drafts'))
                                )
                        );
		$this->Client->recursive = 1;
		$conditions = array();
		$search['ClientData.first_name'] = !empty($this->request->data['Clients']['searchByName']) ? $this->request->data['Clients']['searchByName'] : '';
		$search['Client.created'] = !empty($this->request->data['Clients']['searchByDate']) ? $this->request->data['Clients']['searchByDate'] : '';
		foreach($search as $key => $value) {
				if( !empty($value) ) {
					$conditions[$key] = $value;
				}
			}
	 	$conditions['Client.status'] = 0;
		$this->Paginator->settings = array(
									        'conditions' => $conditions,
									        'order'=>array('Client.created'=>'DESC'),
									        'fields' =>array('Client.id','ClientData.first_name','ClientData.genus','ClientData.birthdate'),
									        'limit' => 10
    									);
		 //debug($this->Paginator->paginate());die;
		 $clients =  $this->Paginator->paginate();
		 $this->set(compact('clients','title','breadCrumb'));
	}
/**
 * orders method
 *
 * @return void
 */
	public function admin_orders(){ 
		
		$this->Client->recursive = 1;
		$search['ClientData.first_name'] = !empty($this->request->data['Clients']['searchByName']) ? $this->request->data['Clients']['searchByName'] : '';
		$search['Client.created'] = !empty($this->request->data['Clients']['searchByDate']) ? $this->request->data['Clients']['searchByDate'] : '';

			foreach($search as $key => $value) {
				if( !empty($value) ) {
					$conditions[$key] = $value;
				}
			}
		$conditions['Client.status'] = 1;
		 $this->Paginator->settings = array(
        'conditions' => $conditions,
        'order'=>array('Client.created'=>'DESC'),
        'fields' =>array('Client.id','ClientData.first_name','ClientData.genus','ClientData.birthdate'),
        'limit' => 10
    	);
		$clients =  $this->Paginator->paginate();
		$title = array(
            'title'=>'Orders',
            'explain'=>'Lijst Orders'
        );
         $breadCrumb = array(
	         					array(
		                              'name' =>__('Dashboard'),
		                              'link' => Router::url(array('controller' => 'users', 'action' => 'dashboard'))
		                        	),
	                            array(
	                                  'name' =>'Orders',
	                                  'link' => Router::url(array('controller' => 'clients','action'=>'orders'))
	                              )
                        );
		$this->set(compact('clients','title','breadCrumb'));
	}

/**
 * quotes method
 *
 * @return void
 */
	public function admin_quotes() { 
		$this->Client->recursive = 1;
		$search['ClientData.first_name'] = !empty($this->request->data['Clients']['searchByName']) ? $this->request->data['Clients']['searchByName'] : '';
		$search['Client.created'] = !empty($this->request->data['Clients']['searchByDate']) ? $this->request->data['Clients']['searchByDate'] : '';

			foreach($search as $key => $value) {
				if( !empty($value) ) {
					$conditions[$key] = $value;
				}
			}
			$conditions['Client.status'] = 2;
			 $this->Paginator->settings = array(
	        'conditions' => $conditions,
	        'order'=>array('Client.created'=>'DESC'),
	        'fields' =>array('Client.id','ClientData.first_name','ClientData.genus','ClientData.birthdate'),
	        'limit' => 10
	    	);
			
		$clients =  $this->Paginator->paginate();
		$title = array(
            'title'=>'offertes',
            'explain'=>'Lijst offertes'
        ); 
        $breadCrumb = array(
        					array(
	                              'name' =>__('Dashboard'),
	                              'link' => Router::url(array('controller' => 'users', 'action' => 'dashboard'))
	                        	),
                            array(
                                  'name' =>'offertes',
                                  'link' => Router::url(array('controller' => 'clients','action'=>'quotes'))
                              )
                        );
		$this->set(compact('clients','title','breadCrumb'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($condition=null,$id = null,$pdf = null) { 
	   if(is_numeric($condition)){
		   $id = $condition;
	    } 

		if (!$this->Client->exists($id)) {
			throw new NotFoundException(__('Invalid client'));
		}
		switch($condition){
		   case 'drafts':
				 $breadCrumb = array(
	    					array(
	                              'name' =>__('Dashboard'),
	                              'link' => Router::url(array('controller' => 'users', 'action' => 'dashboard'))
	                        	),
                            array(
                                  'name' =>__('concepten'),
                                  'link' => Router::url(array('controller' => 'clients','action'=>'drafts'))
                                )
                        );
				break;
		   case 'quotes':
					$breadCrumb = array(
			    					array(
			                              'name' =>__('Dashboard'),
			                              'link' => Router::url(array('controller' => 'users', 'action' => 'dashboard'))
			                        	),
		                            array(
		                                  'name' =>__('Offertes'),
		                                  'link' => Router::url(array('controller' => 'clients','action'=>'quotes'))
		                                )
		                        );
					break;
		   case 'orders':
				   $breadCrumb = array(
					    					array(
					                              'name' =>__('Dashboard'),
					                              'link' => Router::url(array('controller' => 'users', 'action' => 'dashboard'))
					                        	),
				                            array(
				                                  'name' =>__('Bestellingen'),
				                                  'link' => Router::url(array('controller' => 'clients','action'=>'quotes'))
				                                )
				                        );
					break;	
        }

		if(!is_numeric($condition)){
		$params = array(
					        'download' => true,
					        'name' => 'example.pdf',
					        'paperOrientation' => 'portrait',
					        'paperSize' => 'legal'
    						);
  		$this->set($params);
  	   }
  	  
		   	$this->Client->recursive = 2;
            $client_info = $this->Client->find('all', array(
                'conditions' => array(
                    'Client.id' => $id
                ),
//                'fields' => array(
//                    'ClientData.id',
//                    'ClientData.first_name',
//                    'ClientData.genus',
//                    'ClientData.street',
//                    'ClientData.zip_code',
//                    'ClientData.location',
//                    'Decease.first_name',
//                    'Decease.genus',
//                    'Decease.location',
//                    'Decease.deathdate',
//                    'Miscellaneouse.*'
//                )
            ));
			if($condition == 'with_out_price'){
				$without_price = true;
			}
			$Clientdatas = isset($client_info[0]['ClientData'])?array_filter($client_info[0]['ClientData']):null;
		    $Deceasesdatas =  isset($client_info[0]['Decease'])?array_filter($client_info[0]['Decease']):null;
		    $CeromonyInfodatas =  isset($client_info[0]['CeromonyInfo'])?array_filter($client_info[0]['CeromonyInfo']):null;
		    $assignment_faxed['assignment_faxed'] = isset($CeromonyInfodatas['assignment_faxed'])?$CeromonyInfodatas['assignment_faxed']:null;
		    $ceromonyDatas = isset($CeromonyInfodatas['CeromonyData'])?array_filter($CeromonyInfodatas['CeromonyData']):null;
		    $chruchDatas = isset($CeromonyInfodatas['ChruchData'])?array_filter($CeromonyInfodatas['ChruchData']):null;
		    $cemetryDatas = isset($CeromonyInfodatas['CemetryData'])?array_filter($CeromonyInfodatas['CemetryData']):null;
		    $Entrepreneurdatas =  isset($client_info[0]['Entrepreneur'])?array_filter($client_info[0]['Entrepreneur']):null;
		    $BillingAddressdatas =  isset($client_info[0]['BillingAddress'])?array_filter($client_info[0]['BillingAddress']):null;
		    $FarewellServicedatas =  isset($client_info[0]['FarewellService'])?array_filter($client_info[0]['FarewellService']):null;
		    $PictureSounddatas = isset( $client_info[0]['PictureSound'])?array_filter( $client_info[0]['PictureSound']):null;
		    $musics =  isset( $PictureSounddatas['Music']) ?$PictureSounddatas['Music']['music_name']:null;
		    if(!empty($musics)){
		    $musics = explode(',',$musics);
		    $musics = array_filter($musics);
		   	}
		    $CoffeeRoomdatas = isset($client_info[0]['CoffeeRoom'])?array_filter($client_info[0]['CoffeeRoom']):null;
		    $Miscellaneousdatas = isset($client_info[0]['Miscellaneouse'])?array_filter($client_info[0]['Miscellaneouse']):null;
		    $coffinsData = isset($Miscellaneousdatas['CoffinsData'])?array_filter($Miscellaneousdatas['CoffinsData']):null;
			$funeralLettersData = isset($Miscellaneousdatas['FuneralLettersData'])?array_filter($Miscellaneousdatas['FuneralLettersData']):null;
		    $misboekjesData = isset($Miscellaneousdatas['MisboekjesData'])?array_filter($Miscellaneousdatas['MisboekjesData']):null;
		    $stamp = isset($Miscellaneousdatas['Stamp'])?array_filter($Miscellaneousdatas['Stamp']):null;
		    $commemoration = isset($Miscellaneousdatas['Commemoration'])?array_filter($Miscellaneousdatas['Commemoration']):null;
		    $rouwcentrum =isset($Miscellaneousdatas['RouwcentrumsData'])?array_filter($Miscellaneousdatas['RouwcentrumsData']):null;
		    $careDecease = isset($Miscellaneousdatas['CareDeceasesData'])?array_filter($Miscellaneousdatas['CareDeceasesData']):null;
		    $homeobaring = isset($Miscellaneousdatas['Homeobaring'])?array_filter($Miscellaneousdatas['Homeobaring']):null;
		    $hearse = isset($Miscellaneousdatas['Hearse'])?array_filter($Miscellaneousdatas['Hearse']):null;
		    $arrangements = isset($Miscellaneousdatas['ArrangementData'])?array_filter($Miscellaneousdatas['ArrangementData']):null;
		    $coffeeTicket = isset($Miscellaneousdatas['CoffeeTicket'])?array_filter($Miscellaneousdatas['CoffeeTicket']):null;
		    $carriersData = isset($Miscellaneousdatas['CarriersData'])?array_filter($Miscellaneousdatas['CarriersData']):null;
		    $hospitalsData =isset($Miscellaneousdatas['HospitalsData'])?array_filter($Miscellaneousdatas['HospitalsData']):null;
		    $PrayerCardsDatas = isset($Miscellaneousdatas['PrayerCardsData'])?$Miscellaneousdatas['PrayerCardsData']:null;
		    $this->set( 
		    	         compact(
		    	               'Clientdatas',
		    	               'Deceasesdatas',
		    	               'CeromonyInfodatas',
		    	               'Entrepreneurdatas',
		    	               'FarewellServicedatas',
		    	               'BillingAddressdatas',
		    	               'FarewellServicedatas',
		    	               'PictureSounddatas',
		    	               'CoffeeRoomdatas',
		    	               'Miscellaneousdatas',
		    	               'without_price',
		    	               'florist',
		    	               'arrangements',
		    	               'hearse',
		    	               'homeobaring',
		    	               'careDecease',
		    	               'rouwcentrum',
		    	               'commemoration',
		    	               'stamp',
		    	               'misboekjesData',
		    	               'funeralLettersData',
		    	               'coffinsData',
		    	               'ceromonyDatas',
		    	               'chruchDatas',
		    	               'cemetryDatas',
		    	               'musics',
		    	               'assignment_faxed',
		    	               'coffeeTicket',
		    	               'breadCrumb',
		    	               'carriersData',
		    	               'hospitalsData',
		    	               'PrayerCardsDatas'
		    	            )
				    );
			$this->render('admin_view_drafts');
            $view = new View($this, false);
		    $html = $view->render('admin_view_drafts', 'default');
           
	}


/**
 * pdf export method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */	

public function admin_export_pdf($id){
    $this->Client->recursive = 2;
	$client_info = $this->Client->find('all',array(
												'conditions'=>array('Client.id'=>$id)
											 )
    								  );
	$this->render('admin_view_drafts');
}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null,$params=null) {	
		$this->Client->id = $id;
		if (!$this->Client->exists()) {
			throw new NotFoundException(__('Invalid client'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Client->delete()) {
			$this->Session->setFlash(__('The client has been deleted.'));
		} else {
			$this->Session->setFlash(__('The client could not be deleted. Please, try again.'));
		}
		switch($params){
		   case 'drafts':
			return $this->redirect(array('action' => 'drafts','admin'=>'true'));
			break;
		   case 'quotes':
			return $this->redirect(array('action' => 'quotes','admin'=>'true'));
			break;
		   case 'orders':
			return $this->redirect(array('action' => 'orders','admin'=>'true'));
			break;	
        }

	}

/**
 * drafts method
 *
 * @return void
 */
	public function drafts() {
		$this->Client->recursive = 1;
		$conditions = array();
		$search['ClientData.first_name'] = !empty($this->request->data['Clients']['searchByName']) ? $this->request->data['Clients']['searchByName'] : '';
		$search['ClientData.created'] = !empty($this->request->data['Clients']['created']) ? $this->request->data['Clients']['searchByDate'] : '';

			foreach($search as $key => $value) {
				if( !empty($value) ) {
					$conditions[$key] = $value;
				}
			}
		$conditions['Client.status'] = 0;
		 $this->Paginator->settings = array(
        'conditions' => $conditions,
        'order'=>array('Client.created'=>'DESC'),
        'fields' =>array('Client.id','ClientData.first_name','ClientData.email','ClientData.birthdate','FarewellService.reception','Decease.deathdate'),
        'limit' => 10
    	);
		$breadCrumb = array(
	    					array(
	                              'name' =>__('Dashboard'),
	                              'link' => Router::url(array('controller' => 'users', 'action' => 'dashboard'))
	                        	),
                            array(
                                  'name' =>'concepten',
                                  'link' => Router::url(array('controller' => 'clients','action'=>'drafts'))
                                )
                        ); 

		 //debug($this->Paginator->paginate());die;
		$this->set('clients', $this->Paginator->paginate());
	}
/**
 * orders method
 *
 * @return void
 */
	public function orders() {
		$this->Client->recursive = 1;
		$search['ClientData.first_name'] = !empty($this->request->data['Clients']['searchByName']) ? $this->request->data['Clients']['searchByName'] : '';
		$search['ClientData.created'] = !empty($this->request->data['Clients']['created']) ? $this->request->data['Clients']['searchByDate'] : '';

			foreach($search as $key => $value) {
				if( !empty($value) ) {
					$conditions[$key] = $value;
				}
			}
		$conditions['Client.status'] = 1;
		 $this->Paginator->settings = array(
        'conditions' => $conditions,
        'order'=>array('Client.created'=>'DESC'),
        'fields' =>array('Client.id','ClientData.first_name','ClientData.email','ClientData.birthdate','FarewellService.reception','Decease.deathdate'),
        'limit' => 10
    	);
		 $breadCrumb = array(
	    					array(
	                              'name' =>__('Dashboard'),
	                              'link' => Router::url(array('controller' => 'users', 'action' => 'dashboard'))
	                        	),
                            array(
                                  'name' =>__('concepten'),
                                  'link' => Router::url(array('controller' => 'clients','action'=>'drafts'))
                                )
                        ); 


		$this->set('clients', $this->Paginator->paginate());
	}

/**
 * quotes method
 *
 * @return void
 */
	public function quotes() {
		$this->Client->recursive = 1;
		$search['ClientData.first_name'] = !empty($this->request->data['Clients']['searchByName']) ? $this->request->data['Clients']['searchByName'] : '';
		$search['ClientData.created'] = !empty($this->request->data['Clients']['created']) ? $this->request->data['Clients']['searchByDate'] : '';

			foreach($search as $key => $value) {
				if( !empty($value) ) {
					$conditions[$key] = $value;
				}
			}
			$conditions['Client.status'] = 2;
			 $this->Paginator->settings = array(
	        'conditions' => $conditions,
	        'order'=>array('Client.created'=>'DESC'),
	        'fields' =>array('Client.id','ClientData.first_name','ClientData.email','ClientData.birthdate','FarewellService.reception','Decease.deathdate'),
	        'limit' => 10
	    	);
			 $breadCrumb = array(
	    					array(
	                              'name' =>__('Dashboard'),
	                              'link' => Router::url(array('controller' => 'users', 'action' => 'dashboard'))
	                        	),
                            array(
                                  'name' =>__('concepten'),
                                  'link' => Router::url(array('controller' => 'clients','action'=>'drafts'))
                                )
                        ); 

			$this->set('clients', $this->Paginator->paginate());


			
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view_drafts($condition=null,$id = null,$pdf = null) {
	    if(is_numeric($condition)){
		   $id = $condition;
	    } 
		if (!$this->Client->exists($id)) {
			throw new NotFoundException(__('Invalid client'));
		}
		if(!is_numeric($condition)){
		$params = array(
					        'download' => true,
					        'name' => 'example.pdf',
					        'paperOrientation' => 'portrait',
					        'paperSize' => 'legal'
    						);
  		$this->set($params);
  	   }

	   	$this->Client->recursive = 2;
		$client_info = $this->Client->find('all',array(
														'conditions'=>array(
															               'Client.id'=>$id
															               )
													 )
												);
	if($condition == 'with_out_price'){
		$without_price = true;
	}	
	
	$Clientdatas = isset($client_info[0]['ClientData'])?$client_info[0]['ClientData']:null;
    $Deceasesdatas =  isset($client_info[0]['Decease'])?$client_info[0]['Decease']:null;
    $CeromonyInfodatas =  isset($client_info[0]['CeromonyInfo'])?$client_info[0]['CeromonyInfo']:null;
    $assignment_faxed['assignment_faxed'] = isset($CeromonyInfodatas['assignment_faxed'])?$CeromonyInfodatas['assignment_faxed']:null;
    $ceromonyDatas = isset($CeromonyInfodatas['CeromonyData'])?$CeromonyInfodatas['CeromonyData']:null;
    $chruchDatas = isset($CeromonyInfodatas['ChruchData'])?$CeromonyInfodatas['ChruchData']:null;
    $cemetryDatas = isset($CeromonyInfodatas['CemetryData'])?$CeromonyInfodatas['CemetryData']:null;
    $Entrepreneurdatas =  isset($client_info[0]['Entrepreneur'])?$client_info[0]['Entrepreneur']:null;
    $BillingAddressdatas =  isset($client_info[0]['BillingAddress'])?$client_info[0]['BillingAddress']:null;
    $FarewellServicedatas =  isset($client_info[0]['FarewellService'])?$client_info[0]['FarewellService']:null;
    $PictureSounddatas = isset( $client_info[0]['PictureSound'])? $client_info[0]['PictureSound']:null;
    $musics =  isset( $PictureSounddatas['Music']) ?$PictureSounddatas['Music']['music_name']:null;
    if(!empty($musics)){
    $musics = explode(',',$musics);
   	}
    $CoffeeRoomdatas = isset($client_info[0]['CoffeeRoom'])?$client_info[0]['CoffeeRoom']:null;
    $Miscellaneousdatas = isset($client_info[0]['Miscellaneouse'])?$client_info[0]['Miscellaneouse']:null;
    //debug($Miscellaneousdatas);die;
    $coffinsData = isset($Miscellaneousdatas['CoffinsData'])?$Miscellaneousdatas['CoffinsData']:null;
	$funeralLettersData = isset($Miscellaneousdatas['FuneralLettersData'])?$Miscellaneousdatas['FuneralLettersData']:null;
    $misboekjesData = isset($Miscellaneousdatas['MisboekjesData'])?$Miscellaneousdatas['MisboekjesData']:null;
    $stamp = isset($Miscellaneousdatas['Stamp'])?$Miscellaneousdatas['Stamp']:null;
    $commemoration = isset($Miscellaneousdatas['Commemoration'])?$Miscellaneousdatas['Commemoration']:null;
    $rouwcentrum =isset($Miscellaneousdatas['RouwcentrumsData'])?$Miscellaneousdatas['RouwcentrumsData']:null;
    $careDecease = isset($Miscellaneousdatas['CareDecease'])?$Miscellaneousdatas['CareDecease']:null;;
    $homeobaring = isset($Miscellaneousdatas['Homeobaring'])?$Miscellaneousdatas['Homeobaring']:null;
    $hearse = isset($Miscellaneousdatas['Hearse'])?$Miscellaneousdatas['Hearse']:null;
    $arrangement = isset($Miscellaneousdatas['Arrangement'])?$Miscellaneousdatas['Arrangement']:null;
    $florist = isset($Miscellaneousdatas['Florist'])?$Miscellaneousdatas['Florist']:null;
    $this->set( 
    	         compact(
    	               'Clientdatas',
    	               'Deceasesdatas',
    	               'CeromonyInfodatas',
    	               'Entrepreneurdatas',
    	               'FarewellServicedatas',
    	               'BillingAddressdatas',
    	               'FarewellServicedatas',
    	               'PictureSounddatas',
    	               'CoffeeRoomdatas',
    	               'Miscellaneousdatas',
    	               'without_price',
    	               'florist',
    	               'arrangement',
    	               'hearse',
    	               'homeobaring',
    	               'careDecease',
    	               'rouwcentrum',
    	               'commemoration',
    	               'stamp',
    	               'misboekjesData',
    	               'funeralLettersData',
    	               'coffinsData',
    	               'ceromonyDatas',
    	               'chruchDatas',
    	               'cemetryDatas',
    	               'musics',
    	               'assignment_faxed'
    	            )
		    );
	$view = new View($this, false);
    $html = $view->render('view_drafts', 'default');
	}


/**
 * pdf export method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */	

public function export_pdf($id){
    $this->Client->recursive = 2;
	$client_info = $this->Client->find('all',array(
												'conditions'=>array('Client.id'=>$id)
											 )
    								  );
	$this->render('admin_view_drafts');
}
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit_drafts($id = null) {
		if (!$this->Client->exists($id)) {
			throw new NotFoundException(__('Invalid client'));
		}
		return $this->redirect(array('controller'=>'clientDatas','action' => 'add','admin'=>false,$id));
		$this->set(compact('clientDatas', 'deceases', 'ceromonyInfos', 'entrepreneurs', 'billingAddresses', 'farewellServices', 'pictureSounds', 'coffeeRooms', 'miscellaneouses'));
	}


}
