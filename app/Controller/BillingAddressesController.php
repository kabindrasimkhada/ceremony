<?php
App::uses('AppController', 'Controller');
/**
 * BillingAddresses Controller
 *
 * @property BillingAddress $BillingAddress
 * @property PaginatorComponent $Paginator
 */
class BillingAddressesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');
	

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->BillingAddress->recursive = 0;
		$this->set('billingAddresses', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->BillingAddress->exists($id)) {
			throw new NotFoundException(__('Invalid billing address'));
		}
		$options = array('conditions' => array('BillingAddress.' . $this->BillingAddress->primaryKey => $id));
		$this->set('billingAddress', $this->BillingAddress->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
    /*
	public function admin_add($id=null,$BillingAddress_id=null,$Cid=null,$data = null){
                if(is_null($id)){
			return $this->redirect(array('controller'=>'clientDatas','action' => 'add'));
		}
		if ($this->request->is('post') || !empty($data)) {
			$this->BillingAddress->create();
			if(!empty($data)){
				$this->request->data = $data;
				$this->BillingAddress->id = $BillingAddress_id;
			}
			$val = $this->_emptyElementExists($this->request->data['BillingAddress']);
			if($val !== true){
				$this->request->data['BillingAddress']['status'] = 1;
			}
			if ($this->BillingAddress->save($this->request->data)) {
				if(empty($Cid)){
				 	$billing_address_id = $this->BillingAddress->id;
				//echo $BillingAddress_id;die;
				$this->Client->id = $id;
			    $this->Client->saveField('billing_address_id',$billing_address_id);
				$this->Session->setFlash(__('The billing address has been saved.'));
				return $this->redirect(array('controller'=>'farewellServices','action' => 'add',$this->Client->id));
			   }else{
			      $this->Client->id = $Cid;
			      $this->Session->setFlash(__('The billing address has been saved.'));
			      if(!isset($BillingAddress_id) && empty($BillingAddress_id)){
			      	$BillingAddress_id = $this->BillingAddress->id;
			      	$this->Session->setFlash(__('The billing address has been saved.'));
				  }
			      $this->Client->saveField(
				    	'billing_address_id',$BillingAddress_id
				    	);
			      $id = $this->Client->id;
			      $this->Session->setFlash(__('The BillingAddress data has been updated.'));
			      return $this->redirect(array('controller'=>'farewellServices','action' => 'edit',$id));
			    }
			
			} else {
				$this->Session->setFlash(__('The billing address could not be saved. Please, try again.'));
			}
		}
		//$clientDatas = $this->BillingAddress->ClientData->find('list');
		$this->set(compact('clientDatas'));
	}
/**
 * edit drafts method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    /*
	public function admin_edit($id = null) {
				   if (!$this->Client->exists($id)) {
							throw new NotFoundException(__('Invalid client data'));
					}
					$this->Client->Behaviors->attach("Containable");
			    	$this->Client->contain('BillingAddress');
			    	$BillingAddress_info = $this->Client->find('first',array('conditions'=>array('Client.id'=>$id)));
			    	$BillingAddress_id = $BillingAddress_info['BillingAddress']['id']; 
					if ($this->request->is(array('post', 'put'))){
				 		$this->admin_add('1',$BillingAddress_id,$id,$this->request->data);
				 	}
			    	//$this->Client->recursive = 2;

					//debug($client_info);die;
					//$clientDatas['ClientData'] = $client_info[0]['ClientData'];
					$this->request->data = $BillingAddress_info;
					$this->set(compact('id'));
					$this->render('admin_add');
			    }



/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->BillingAddress->exists($id)) {
			throw new NotFoundException(__('Invalid billing address'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->BillingAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The billing address has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The billing address could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('BillingAddress.' . $this->BillingAddress->primaryKey => $id));
			$this->request->data = $this->BillingAddress->find('first', $options);
		}
		$clientDatas = $this->BillingAddress->ClientData->find('list');
		$this->set(compact('clientDatas'));
	}


/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->BillingAddress->id = $id;
		if (!$this->BillingAddress->exists()) {
			throw new NotFoundException(__('Invalid billing address'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->BillingAddress->delete()) {
			$this->Session->setFlash(__('The billing address has been deleted.'));
		} else {
			$this->Session->setFlash(__('The billing address could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
