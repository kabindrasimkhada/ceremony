<?php
App::uses('AppController', 'Controller');
/**
 * ForwardBies Controller
 *
 * @property ForwardBy $ForwardBy
 * @property PaginatorComponent $Paginator
 */
class ForwardBiesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->ForwardBy->recursive = 0;
		$this->Paginator->settings = array(
								        'order'=>array('ForwardBy.created'=>'DESC'),
								        'limit' => 10
									);
		$this->set('forwardBies', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->ForwardBy->exists($id)) {
			throw new NotFoundException(__('Invalid forward by'));
		}
		$options = array('conditions' => array('ForwardBy.' . $this->ForwardBy->primaryKey => $id));
		$this->set('forwardBy', $this->ForwardBy->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->ForwardBy->create();
			if ($this->ForwardBy->save($this->request->data)) {
				$this->Session->setFlash(__('The forward by has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The forward by could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->ForwardBy->exists($id)) {
			throw new NotFoundException(__('Invalid forward by'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ForwardBy->save($this->request->data)) {
				$this->Session->setFlash(__('The forward by has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The forward by could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ForwardBy.' . $this->ForwardBy->primaryKey => $id));
			$this->request->data = $this->ForwardBy->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->ForwardBy->id = $id;
		if (!$this->ForwardBy->exists()) {
			throw new NotFoundException(__('Invalid forward by'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ForwardBy->delete()) {
			$this->Session->setFlash(__('The forward by has been deleted.'));
		} else {
			$this->Session->setFlash(__('The forward by could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
