<?php
App::uses('AppController', 'Controller');
/**
 * Coffins Controller
 *
 * @property Coffin $Coffin
 * @property PaginatorComponent $Paginator
 */
class CoffinsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function  admin_index() {
		$this->Coffin->recursive = 0;
		$this->Paginator->settings = array(
								        'order'=>array('Coffin.created'=>'DESC'),
								        'limit' => 10
									);
		$this->set('coffins', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function  admin_view($id = null) {
		if (!$this->Coffin->exists($id)) {
			throw new NotFoundException(__('Invalid coffin'));
		}
		$options = array('conditions' => array('Coffin.' . $this->Coffin->primaryKey => $id));
		$this->set('coffin', $this->Coffin->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function  admin_add() {
		if ($this->request->is('post')) {
			$this->Coffin->create();
			if ($this->Coffin->save($this->request->data)) {
				$this->Session->setFlash(__('The coffin has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The coffin could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function  admin_edit($id = null) {
		if (!$this->Coffin->exists($id)) {
			throw new NotFoundException(__('Invalid coffin'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Coffin->save($this->request->data)) {
				$this->Session->setFlash(__('The coffin has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The coffin could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Coffin.' . $this->Coffin->primaryKey => $id));
			$this->request->data = $this->Coffin->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function  admin_delete($id = null) {
		$this->Coffin->id = $id;
		if (!$this->Coffin->exists()) {
			throw new NotFoundException(__('Invalid coffin'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Coffin->delete()) {
			$this->Session->setFlash(__('The coffin has been deleted.'));
		} else {
			$this->Session->setFlash(__('The coffin could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
