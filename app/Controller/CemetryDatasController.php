<?php
App::uses('AppController', 'Controller');
/**
 * CemetryDatas Controller
 *
 * @property CemetryData $CemetryData
 * @property PaginatorComponent $Paginator
 */
class CemetryDatasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CemetryData->recursive = 0;
		$this->set('cemetryDatas', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CemetryData->exists($id)) {
			throw new NotFoundException(__('Invalid cemetry data'));
		}
		$options = array('conditions' => array('CemetryData.' . $this->CemetryData->primaryKey => $id));
		$this->set('cemetryData', $this->CemetryData->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CemetryData->create();
			if ($this->CemetryData->save($this->request->data)) {
				$this->Session->setFlash(__('The cemetry data has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cemetry data could not be saved. Please, try again.'));
			}
		}
		$clientDatas = $this->CemetryData->ClientData->find('list');
		$this->set(compact('clientDatas'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CemetryData->exists($id)) {
			throw new NotFoundException(__('Invalid cemetry data'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CemetryData->save($this->request->data)) {
				$this->Session->setFlash(__('The cemetry data has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cemetry data could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CemetryData.' . $this->CemetryData->primaryKey => $id));
			$this->request->data = $this->CemetryData->find('first', $options);
		}
		$clientDatas = $this->CemetryData->ClientData->find('list');
		$this->set(compact('clientDatas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CemetryData->id = $id;
		if (!$this->CemetryData->exists()) {
			throw new NotFoundException(__('Invalid cemetry data'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->CemetryData->delete()) {
			$this->Session->setFlash(__('The cemetry data has been deleted.'));
		} else {
			$this->Session->setFlash(__('The cemetry data could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
