<?php
App::uses('AppController', 'Controller');
/**
 * CareDeceases Controller
 *
 * @property CareDecease $CareDecease
 * @property PaginatorComponent $Paginator
 */
class CareDeceasesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Auth');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->CareDecease->recursive = 0;
	
		$this->set('careDeceases', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->CareDecease->exists($id)) {
			throw new NotFoundException(__('Invalid care decease'));
		}
		$options = array('conditions' => array('CareDecease.' . $this->CareDecease->primaryKey => $id));
		$this->set('careDecease', $this->CareDecease->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->CareDecease->create();
			if ($this->CareDecease->save($this->request->data)) {
				$this->Session->setFlash(__('The care decease has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The care decease could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->CareDecease->exists($id)) {
			throw new NotFoundException(__('Invalid care decease'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CareDecease->save($this->request->data)) {
				$this->Session->setFlash(__('The care decease has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The care decease could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CareDecease.' . $this->CareDecease->primaryKey => $id));
			$this->request->data = $this->CareDecease->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->CareDecease->id = $id;
		if (!$this->CareDecease->exists()) {
			throw new NotFoundException(__('Invalid care decease'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->CareDecease->delete()) {
			$this->Session->setFlash(__('The care decease has been deleted.'));
		} else {
			$this->Session->setFlash(__('The care decease could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
