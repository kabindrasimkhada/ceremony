<?php
App::uses('AppModel', 'Model');
/**
 * Client Model
 *
 * @property ClientData $ClientData
 * @property Decease $Decease
 * @property CeromonyInfo $CeromonyInfo
 * @property Entrepreneur $Entrepreneur
 * @property BillingAddress $BillingAddress
 * @property FarewellService $FarewellService
 * @property PictureSound $PictureSound
 * @property CoffeeRoom $CoffeeRoom
 * @property Miscellaneouse $Miscellaneouse
 */
class Client extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'ClientData' => array(
			'className' => 'ClientData',
			'foreignKey' => 'client_data_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Decease' => array(
			'className' => 'Decease',
			'foreignKey' => 'decease_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CeromonyInfo' => array(
			'className' => 'CeromonyInfo',
			'foreignKey' => 'ceromony_info_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Entrepreneur' => array(
			'className' => 'Entrepreneur',
			'foreignKey' => 'entrepreneur_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'BillingAddress' => array(
			'className' => 'BillingAddress',
			'foreignKey' => 'billing_address_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'FarewellService' => array(
			'className' => 'FarewellService',
			'foreignKey' => 'farewell_service_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'PictureSound' => array(
			'className' => 'PictureSound',
			'foreignKey' => 'picture_sound_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CoffeeRoom' => array(
			'className' => 'CoffeeRoom',
			'foreignKey' => 'coffee_room_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Miscellaneouse' => array(
			'className' => 'Miscellaneouse',
			'foreignKey' => 'miscellaneouse_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
