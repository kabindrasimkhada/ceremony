<?php
App::uses('AppModel', 'Model');
/**
 * PictureSound Model
 *
 * @property Music $Music
 */
class PictureSound extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Music' => array(
			'className' => 'Music',
			'foreignKey' => 'music_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
