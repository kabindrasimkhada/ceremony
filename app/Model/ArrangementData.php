<?php
App::uses('AppModel', 'Model');
/**
 * ArrangementData Model
 *
 */
class ArrangementData extends AppModel {

	public $belongsTo = array(
		'Miscellaneouse' => array(
			'className' => 'Miscellaneouse',
			'foreignKey' => 'miscellaneouse_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
