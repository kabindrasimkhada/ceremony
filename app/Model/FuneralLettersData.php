<?php
App::uses('AppModel', 'Model');
/**
 * FuneralLettersData Model
 *
 * @property Miscellaneouse $Miscellaneouse
 */
class FuneralLettersData extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Miscellaneouse' => array(
			'className' => 'Miscellaneouse',
			'foreignKey' => 'funeral_letters_data_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
