<?php
App::uses('AppModel', 'Model');
/**
 * CoffeeTicket Model
 *
 * @property Miscellaneouse $Miscellaneouse
 */
class CoffeeTicket extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Miscellaneouse' => array(
			'className' => 'Miscellaneouse',
			'foreignKey' => 'coffee_ticket_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
