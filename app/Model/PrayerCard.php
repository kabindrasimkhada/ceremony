<?php
App::uses('AppModel', 'Model');
/**
 * PrayerCard Model
 *
 * @property PrayerCardCategory $PrayerCardCategory
 */
class PrayerCard extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
  var $displayFields = 'prayer_card';
	public $validate = array(
		'prayer_card_category_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'card' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'price' => array(
			'decimal' => array(
				'rule' => array('decimal'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'PrayerCardCategory' => array(
			'className' => 'PrayerCardCategory',
			'foreignKey' => 'prayer_card_category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
