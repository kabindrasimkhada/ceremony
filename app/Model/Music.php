<?php
App::uses('AppModel', 'Model');
/**
 * Music Model
 *
 * @property PictureSound $PictureSound
 */
class Music extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'PictureSound' => array(
			'className' => 'PictureSound',
			'foreignKey' => 'music_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
