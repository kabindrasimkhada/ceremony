<?php
App::uses('AppModel', 'Model');
/**
 * CeromonyInfo Model
 *
 * @property ClientData $ClientData
 * @property CeromonyData $CeromonyData
 * @property ChruchData $ChruchData
 * @property CemetryData $CemetryData
 */
class CeromonyInfo extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	/*public $validate = array(
		'client_data_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'ceromony_data_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'chruch_data_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cemetry_data_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'assignment_faxed' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
*/
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'ClientData' => array(
			'className' => 'ClientData',
			'foreignKey' => 'client_data_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CeromonyData' => array(
			'className' => 'CeromonyData',
			'foreignKey' => 'ceromony_data_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ChruchData' => array(
			'className' => 'ChruchData',
			'foreignKey' => 'chruch_data_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CemetryData' => array(
			'className' => 'CemetryData',
			'foreignKey' => 'cemetry_data_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
