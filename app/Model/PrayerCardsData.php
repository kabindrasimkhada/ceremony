<?php
App::uses('AppModel', 'Model');
/**
 * PrayerCardsData Model
 *
 * @property PrayerCategoies $PrayerCategoies
 * @property Miscellaneouse $Miscellaneouse
 */
class PrayerCardsData extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'PrayerCategoies' => array(
			'className' => 'PrayerCategoies',
			'foreignKey' => 'prayer_categoies_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Miscellaneouse' => array(
			'className' => 'Miscellaneouse',
			'foreignKey' => 'prayer_cards_data_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
