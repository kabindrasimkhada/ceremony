<?php
App::uses('AppModel', 'Model');
/**
 * BillingAddress Model
 *
 * @property ClientDatas $ClientDatas
 */
class BillingAddress extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	/*public $validate = array(
		'client_datas_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'sex' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'relation' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'zip_code' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'number' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'letter' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'add' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tightening' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'street' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'location' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);*/

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'ClientDatas' => array(
			'className' => 'ClientDatas',
			'foreignKey' => 'client_datas_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
    
    
    /**
     * function to save billing address from client data controller
     * @param array $data
     * @param int $editID
     * @return int
     */
    public function saveBillingAddress($data,$editID = false){
        if($editID){
            $this->id = $editID;
        }else{
            $this->create();            
        }
       
        if($data['ClientData']['invoice'] == 0){
            $data['BillingAddress']['name'] = $data['ClientData']['first_name'];
            $data['BillingAddress']['sex'] = $data['ClientData']['sex'];
            $data['BillingAddress']['phone'] = $data['ClientData']['phone'];
            $data['BillingAddress']['mobile'] =  $data['ClientData']['mobile'];
            $data['BillingAddress']['zip_code'] = $data['ClientData']['zip_code'];
            $data['BillingAddress']['number'] = $data['ClientData']['number'];
            $data['BillingAddress']['letter'] = $data['ClientData']['letter'];
            $data['BillingAddress']['street'] = $data['ClientData']['street'];
            $data['BillingAddress']['location'] = $data['ClientData']['location'];         
        }
        
        if($this->save($data['BillingAddress'])){
            return $this->id;
        }else{
            return 0;
        }
        
        
    }
}
