<?php
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
/**
 * User Model
 *
 */
class User extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'username';
  
	public function beforeSave($options = array()){
	   parent::beforeSave();
	   if(isset($this->data[$this->alias]['password']))
	   {
		 $passwordHasher = new SimplePasswordHasher();
		 $this->data[$this->alias]['password'] = $passwordHasher->hash(
							 $this->data[$this->alias]['password']
					);
	   }
	   return true;
   }

   public $validate = array(
			
			'first_name' => array(
				  'notEmpty' => array(
	                    'rule' => array('notEmpty'),
	                   	'message' => 'Firstname is required',
	            	    'required' => true,
		            ),
				),
			'last_name' => array(
				   'notEmpty' => array(
	                    'rule' => array('notEmpty'),
	                   	'message' =>'Lastname is required',
	            	    'required' => true,
		            ),
				),

			'email' =>array(
					'notEmpty' => array(
			            'rule' => 'notEmpty',
			            'message' => 'Provide an email address'
			        ),
			        'validEmailRule' => array(
			            'rule' => array('email'),
			            'message' => 'Invalid email address'
			        ),
			        'uniqueEmailRule' => array(
			            'rule' => 'isUnique',
			            'message' => 'Email already registered'
			        )
			   ),

		   'username' => array(
				  'alphaNumeric' => array(
						'rule' => 'alphaNumeric',
						'required' => true,
						'message' => 'Only alphaNumeric value is alloweds'
					),
				  'uniqueUsername' => array(
				  		'rule' => 'isUnique',
				  		'message' => 'Username already exist'
				  	)
				),
		 'password' => array(
				   'notEmpty' => array(
						'rule' => array('notEmpty'),
						'message' => 'Password is required.',  
						'allowEmpty' => false          
					)
				)
			
			);

 public $changePasswordValidation = array(
		'old_password' => array(            
			'checkOldPassword' => array(
				'rule' => array('checkOldPassword'),
				'allowEmpty' => false,
				'message' => 'Old password is incorrect!',
				//'on' => 'reset',
			),
		),
		'password' => array(
		   'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Password is required.',  
				'allowEmpty' => false          
			)
		),
		'confirm_password' => array(            
			'match' => array(
				'rule' => array('match', 'password'),
				'allowEmpty' => false,
				'message' => 'Password and Confirm Password must match.',
				'on' => 'reset',
			),
		),
	);
  
/**
 * 
 * Matches two fields to check if they are equal.
 * @param string $field, $compare_field
 */
 public function match($field = array(), $compare_field = null) {
	 foreach ($field as $key => $value) {
		 $v1 = trim($value);
		 $v2 = trim($this->data[$this->name][$compare_field]);
		 if ($v1 !== $v2) {
			 return FALSE;
		 } else {
			 continue;
		 }
	 }
	 return TRUE;
 }

  /**
   * Validate check old password.
   */
  public function checkOldPassword( $old_password ) { 
	
	  if ($this->field('password') === AuthComponent::password($this->data[$this->alias]['old_password'])) {     
	   	return true;
	  }

	  return false;
  }

	 /**
	 * Validate ckeck old password.
	 */
	public function checkpassword($password) {
		if ($this->field('password') == AuthComponent::password($this->data[$this->alias]['old_password'])) {
			return true;
		}
		return false;
	}

	/**
	 * Validate password match.
	 */
	public function checkPasswordMatch($newPassword) {
		if (isset($this->data[$this->alias]['new_password']) AND isset($this->data[$this->alias]['new_password_confirm'])) {
			if (!Validation::notEmpty($this->data[$this->alias]['new_password']) AND !Validation::notEmpty($this->data[$this->alias]['new_password_confirm'])) {
				return true;
			}
			// continue
		} else if (!isset($this->data[$this->alias]['new_password']) AND !isset($this->data[$this->alias]['new_password_confirm'])) {
			return true;
		} else {
			return false;
		}

		$res = $newPassword['new_password'] === $this->data[$this->alias]['new_password_confirm'];

		if ($res === true) {
			$this->data[$this->alias]['password'] = $newPassword['new_password'];
			return true;
		}
		return false;
	}


}
