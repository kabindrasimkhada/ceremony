<?php
App::uses('AppModel', 'Model');
/**
 * ClientData Model
 *
 */
class ClientData extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'genus' => array(
			'alphanumeric' => array(
				'rule' => array('alphanumeric',array('maxLength','80')),
				'message' => 'vereist',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
			),
		);
		/*'genus_prefixe' => array(
			'alphanumeric' => array(
				'rule' => array('alphanumeric',array('maxLength','80')),
				'message' => 'Only alphabets and numerical values are valid',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'first_name' => array(
			'alphanumeric' => array(
				'rule' => array('alphanumeric',array('maxLength','80')),
				'message' => 'Only alphabets and numerical values are valid',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'nickname' => array(
			'alphanumeric' => array(
				'rule' => array('alphanumeric',array('maxLength','80')),
				'message' => 'Only alphabets and numerical values are valid',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'partner_name' => array(
			'alphanumeric' => array(
				'rule' => array('alphanumeric',array('maxLength','80')),
				'message' => 'Only alphabets and numerical values are valid',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'partner_prefix' => array(
			'alphanumeric' => array(
				'rule' => array('alphanumeric',array('maxLength','80')),
				'message' => 'Only alphabets and numerical values are valid',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'use_name' => array(
			'alphanumeric' => array(
				'rule' => array('alphanumeric',array('maxLength','80')),
				'message' => 'Only alphabets and numerical values are valid',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'sex' => array(
			'alphanumeric' => array(
				'rule' => array('alphanumeric',array('maxLength','8')),
				'message' => 'Only alphabets and numerical values are valid',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'BSN' => array(
			'alphanumeric' => array(
				'rule' => array('alphanumeric',array('maxLength','80')),
				'message' => 'Only alphabets and numerical values are valid',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'birthplace' => array(
			'alphanumeric' => array(
				'rule' => array('alphanumeric',array('maxLength','8')),
				'message' => 'Only alphabets and numerical values are valid',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'zip_code' => array(
			'postal' => array(
				'rule' => array('postal',null,'us'),
				'message' => 'Your zip isnot in the correct format',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'number' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'letter' => array(
			'alphanumeric' => array(
				'rule' => array('alphanumeric', array('maxLength','8')),
				'message' => 'Maximum 8 characters are allowed',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'street' => array(
			'alphanumeric' => array(
				'rule' => array('alphanumeric', array('maxLength','8')),
				'message' => 'Maximum 8 characters are allowed',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'location' => array(
			'alphanumeric' => array(
				'rule' => array('alphanumeric', array('maxLength','8')),
				'message' => 'Maximum 8 characters are allowed',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'phone' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Please enter a valid phone number',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'mobile' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Please enter a valid mobile number',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'email' => array(
		    	'rule' => 'email',
		    	'required' => true,
		    	'message' => 'Please enter a valid email address.'
   		 ),
		'relationship_to_deceased' => array(
			'alphanumeric' => array(
				'rule' => array('alphanumeric', array('maxLength','8')),
				'message' => 'Maximum 8 characters are allowed',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);*/
    
    
    public $hasMany = array(
		'Client' => array(
			'className' => 'Client',
			'foreignKey' => 'client_data_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
    );
    
}
