$('document').ready(function(){
      $('ul.childMenu').hide();

      $('.datepicker-control').datepicker({
        'format':'yyyy-mm-dd'
      }); 

      $('li.parentWithChild').hover(function(){
          $(this).children().slideDown();
        },function(){
          $('li > ul.childMenu').slideUp();
     });


/*
* Dynamically add an input into the CoffeeRooms tabs
*/

  $('.glyphicon-plus').click(function(){
    var html = '<tr class="dynamic-input">';
    html += '<td><label>Aantal</label>';
    html += '<input id="CoffeeRoomNumber" class="form-control" type="text" name="data[CoffeeRoom][number][]">';
    html += '</td>';
    html += '<td colspan="2"><label>Omschrijving</label>';
    html += '<input id="CoffeeRoomNumber" class="form-control" type="text" name="data[CoffeeRoom][letter][]">';
    html += '</tr>';
    
    $('table tr:first-child').after(html);
  });

  $('.glyphicon-remove').click(function(){
    var input = $('.dynamic-input').length;
    if(input !== 0){
      $('table tr:nth-last-child(2)').remove();
    }else
    {
     alert("There is nothing to remove"); 
    }
    });      

      $('#CoffinsDataConffinName').change(function(){
             var name = $(this).val();
             var model = 'Coffin';
             var idPrice = 'CoffinsDataPrice';
             var field = 'conffin_name';
            // alert(val);
            ajaxCall(name,model,idPrice,field);
             
      });  

       $('#FuneralLettersDataLetterName').change(function(){
             var name = $(this).val();
             var model = 'FunerlLetterCard';
             var cardField = 'FuneralLettersDataMapType';
             var field = 'funeral_letter_id';
            // alert(val);
            findCardType(name,model,cardField,field);
             
      });  


});

function change(name, model) {
  ajaxCall()
}
/******Function for ajax Call to find price ********/

function  ajaxCall(name, model,inputPriceField,field,number){
           if(typeof number == 'undefined'){
            number = 1;
           }
           $.ajax({
                  type:'POST',
                  url: 'priceByAjax',
                  dataType:'html',
                  data:{name:name,model:model,number:number,field:field},
                  success:function(html){
                    $('#'+inputPriceField).val(html);
                  }
            });
}


/******Function for ajax Call to find card type ********/

function findCardType(name,model,card,field)
{
             $.ajax({
                  type:'POST',
                  url: 'findCardType',
                  dataType:'html',
                  data:{name:name,model:model,field:field},
                  success:function(response){                     
                      $('#FuneralLettersDataMapType').html(response);
                  }
            });
}
/**Custom function for manipulation of radio ***/

    $('div.dataradioGroup1').hide();
    $('div.dataradioGroup2').hide();
    $('div.dataradioGroup3').hide();
    $('document').ready(function(){
    	  $('input.radioGroup1').change(function(){
    	       var data = $(this).val();
    	       toggleDataField('dataradioGroup1',data);
	          
        });

    	$('input.group2').change(function(){
    		$('div.data1').fadeOut();
    		var  data = $(this).closest('.form-group').find('.data1');
    		data.fadeIn();
          });
        $('input.radioGroup2').change(function(){
    	       var data = $(this).val();
    	       toggleDataField('dataradioGroup2',data);
	          
        });
          $('input.radioGroup3').change(function(){
    	       var data = $(this).val();
    	       toggleDataField('dataradioGroup3',data);
	          
        });
    });


    // function for manipulating radio buttons

    function toggleDataField(valueField,data)
    {
           	  if(data == 1){
           	  	$('div.'+valueField).fadeIn();
           	  }else
           	  {
           	  	$('div.'+valueField).fadeOut();
           	  }

    }
