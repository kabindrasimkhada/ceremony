$(function() {
    var dropped = false;
    var moveto;
    $("#sortable").sortable({
        stop: function(event, ui) {
            if (dropped) {
                var itemID = ui.item.attr('id');
                var sortedIDs = $("#sortable").sortable("toArray");
                jQuery.ajax({
                    type: 'POST',
                    url: webpath + 'admin/categories/ajax_change_page',
                    data: {'ids': sortedIDs, 'id': itemID, 'page': page, 'moveTo': moveto, 'model': modelName},
                    success: function(response) {
                        dropped = false;
                        location.reload();
                    }
                })

            }
            else {
                var info = $("#sortable").sortable("serialize");
                var sortedIDs = $("#sortable").sortable("toArray");
                jQuery.ajax({
                    type: 'POST',
                    url: webpath + 'admin/categories/ajax_update_order',
                    data: {'ids': sortedIDs, 'page': page, 'model': modelName},
                    success: function(response) {
                        dropped = false;
                         location.reload();
                    }
                })
            }
        }
    });

    $(".droppable").droppable({
        drop: function(event, ui) {
            if ($(this).attr('id') == "nextPage") {
                moveto = "nextPage";
            } else {
                moveto = "prevPage";
            }
            dropped = true;
        }
    });

    var fixHelper = function(e, ui) {
        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;
    };

    $("#sortable tbody").sortable({
        helper: fixHelper
    }).disableSelection();

});
