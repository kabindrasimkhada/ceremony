var lang = new Array();

lang["Please enter your password."] = "Vul uw wachtwoord in.";
lang["Please enter the 6 character password."] = "Het wachtwoord dient te bestaan uit minimaal 6 tekens.";
lang["Please enter the confirm password."] = "Bevestig het wachtwoord.";
lang["Password and Confirm password do not match"] = "De wachtwoorden komen niet overheen";
lang["Please enter your email."] = "Vul uw e-mailadres in.";

lang['Please enter your first name.']="Vul uw voornaam in.";
lang['Please enter your last name.']="Vul uw achternaam in.";
lang['Please enter a shortened version of youtube URL']="Vul een Youtube adres in";
lang['Please enter Profile Image.']="Upload een profielfoto.";
lang['Please enter floor number.']="Vul de verdieping in.";
lang['Please enter House number.']="Vul een huisnummer in.";
lang['Please enter the contract number']="Vul het contractnummer in";
lang['Please enter Apartment Name.']="Vul de appartementnaam in.";
lang['Please enter Apartment Image.']="Upload een afbeelding van het appartement.";
lang['Please Enter Area.']="Voer het aantal vierkante meters in.";
lang['Please enter Category Specification Name.']="Vul een naam in.";
lang['Please upload Category Image.']="Upload een afbeelding van de categorie.";
lang['Please Enter number']="Voer een nummer in";
lang['Please enter code.']="Voer een code in.";
lang['Please enter subject.']="Voer het onderwerp in.";
lang['Please enter content.']="Voer de inhoud in.";
lang['Please enter Title.']="Voer een titel in.";
lang['Please Enter Supplier Name.']="Voer de naam van de leverancier in.";
lang['Please enter Product Name.']="Voer de naam van het product in.";
lang['Please enter Product Photo.']="Upload een productafbeeling.";
lang['Please enter Package Name.']="Voer een pakketnaam in.";
lang['Please enter Package Photo.']="Upload een pakketafbeeling.";
lang['Please enter email Template name.']="Voer een templatenaam in.";
lang['Please enter your site name.']="Voer een naam van de website in.";
lang['Please enter your twitter url.']="Voer uw twitter url in.";
lang['lease enter your facebook url.']="Voer uw facebook url in.";
lang['Please enter your youtube url.']="Voer uw youtube url.";
lang['Please enter Short Decription.']="Voer de korte omschrijving in.";
lang['Please enter description.']="Voer een omschrijving in.";
lang['Please enter the price.']="Voer de prijs in.";
lang['Please enter the numeric value for price.']="Vul een numerieke waarde voor de prijs in.";
lang["Please enter receipent's name."]="Voer een naam in";
lang['Please enter email address.']="Voer een geldig e-mailadres in";
lang['Please enter the valid email address.']="Voer een geldig e-mailadres in.";
lang['Please Enter Question.']="Voer vraag.";
lang['Please Enter Answer.']="Voer antwoord.";
lang['Please enter your username.']="Voer uw naam in";
lang['Please enter  message.']="Voer een bericht in";
lang['Please enter your email']="Uw heeft geen juist e-mailadres ingevuld";
lang['Please Enter Tel Number']="Uw heeft geen juist telefoonummer ingevuld";
lang['Please enter the contract number.']="Voer een geldig contract nummer in";
lang['Warning: All the choices made for this category will be lost.']="Let op: Keuzes die gemaakt zijn en te maken hebben met deze indeling zullen verloren gaan.";
lang['Continue']="Bevestig";
lang['Cancel']="Annuleren";

//Langugae translation
function translate(string) {
    if (string) {
        if (language == 'nld') {
            if (lang.hasOwnProperty(string)) {
                return lang[string];
            }
        }
        return string;
    }
}
