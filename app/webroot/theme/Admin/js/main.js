/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


jQuery(function() {



    //Youtube URL validation REGEX
    jQuery.validator.addMethod("youtube_URL", function(value, element) {
        var p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/;
        var p1 = /http:\/\/(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/watch(?:\?|#\!)v=)([\w-]{11}).*/gi;
        if (value.match(p)) {
            return RegExp.$1;
        }
        else if (value.match(p1)) {
            return RegExp.$1;
        }
        else {
            return false;
        }
        // return (value.match(p)) ?RegExp.$1 : false;
    }, translate('Please enter valid YouTube link. In YouTube try clicking on Share and copy paste the shortened link.'));

//Validation for Change Admin Password

    jQuery('#UserAdminChangePasswordForm').validate({
        focusInvalid: false,
        rules: {
            "data[User][password]": {
                required: true,
                minlength: 6
            },
            "data[User][new_password]": {
                required: true,
                minlength: 6
            },
            "data[User][new_password_confirm]": {
                required: true,
                minlength: 6
            }
        },
        messages: {
            "data[User][password]": {
                required: translate("Please enter your password."),
                minlength: translate("Please enter atleast 6 character.")

            },
            "data[User][new_password]": {
                required: translate("Please enter your new password."),
                minlength: translate("Please enter atleast 6 character.")

            },
            "data[User][new_password_confirm]": {
                required: translate("Please enter your new confirm password."),
                minlength: translate("Please enter atleast 6 character.")

            }
        },
        errorPlacement: function(error, element) {
            // Need to add this function to remove the error default placement
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });

// Validation for Change Admin Password End

    //Validation for Account Manager start

    jQuery('#UserAdminAddAccountManagerForm,#UserAdminEditAccountManagerForm').validate({
        focusInvalid: false,
        rules: {
            "data[User][first_name]": "required",
            "data[User][last_name]": "required",
            "data[User][username]": {
                required: true,
                email: true
            },
            "data[User][password]": {
                required: true,
                minlength: 6
            },
            "data[User][video]": {
                required: true,
                youtube_URL: true

            },
            "data[User][photo]": {
                required: function() {
                    return (jQuery('#AcoumtManagerImage').val() < 1);
                }
            }
        },
        messages: {
            "data[User][first_name]": translate("Please enter your first name."),
            "data[User][last_name]": translate("Please enter your last name."),
            "data[User][username]": {
                required: translate("Please enter your email."),
                email: translate("Please enter your email.")
            },
            "data[User][password]": {
                required: translate("Please enter your password."),
                minlength: translate("Please enter atleast 6 character.")

            },
            "data[User][video]": {
                required: translate("Please enter a shortened version of youtube URL"),
                youtube_URL: translate("Please enter a shortened version of youtube URL")
            },
            "data[User][photo]": {
                required: translate("Please enter Account Manager Photo.")
            }
        },
        errorPlacement: function(error, element) {
            // Need to add this function to remove the error default placement
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });

    //Validation for Account Manager End

    //Validation for Client  Manager start

    jQuery('#UserAdminAddClientForm,#UserAdminEditClientForm').validate({
        focusInvalid: false,
        rules: {
            "data[User][last_name]": "required",
            "data[User][floor]": "required",
            "data[User][house_number]": "required",
            "data[User][username]": {
                required: true,
                email: true
            },
            "data[User][password]": {
                required: true,
                minlength: 6
            },
            "data[User][contract_number]": {
                required: true
            },
        },
        messages: {
            "data[User][last_name]": translate("Please enter your last name."),
            "data[User][floor]": translate("Please enter floor number."),
            "data[User][house_number]": translate("Please enter House number."),
            "data[User][username]": {
                required: translate("Please enter your email."),
                email: translate("Please enter your email.")
            },
            "data[User][password]": {
                required: translate("Please enter your password."),
                minlength: translate("Please enter atleast 6 character.")

            },
            "data[User][contract_number]": {
                required: translate("Please enter the contract number."),
            },
        },
        errorPlacement: function(error, element) {
            // Need to add this function to remove the error default placement
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });

//Validation for Client Manager End

    //Validation for Apartment Type start

    jQuery('#ApartmentTypeAdminAddForm,#ApartmentTypeAdminEditForm').validate({
        focusInvalid: false,
        rules: {
            "data[ApartmentType][name]": "required",
            "data[ApartmentType][area]": {
                required: true,
                number: true
            },
        },
        messages: {
            "data[ApartmentType][name]": translate("Please enter Apartment Name."),
            "data[ApartmentType][area]": {
                required: translate("Please Enter Area"),
                number: translate("Please Enter number")
            }


        },
        errorPlacement: function(error, element) {
            // Need to add this function to remove the error default placement
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });

// Validation for Apartment Type End

    //Validation for Category  start

    jQuery('#CategosryAdminAddForm,#CategoryAdminEditForm').validate({
        focusInvalid: false,
        rules: {
            "data[Category][name]": "required",
            "data[Category][layout_image]": {
                required: function() {
                    return (jQuery('#CategoryImage').val() < 1);
                }
            }
        },
        messages: {
            "data[Category][name]": translate("Please enter Apartment Name."),
            "data[Category][layout_image]": {
                required: translate("Please upload Category Image.")
            }
        },
        errorPlacement: function(error, element) {
            // Need to add this function to remove the error default placement
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });

// Validation for Category  End

    //Validation for Category Specifications start

    jQuery('#CategorySpecificationAdminAddForm,#CategorySpecificationAdminEditForm').validate({
        focusInvalid: false,
        rules: {
            "data[CategorySpecification][name]": "required"
        },
        messages: {
            "data[CategorySpecification][name]": translate("Please enter Category Specification Name.")
        },
        errorPlacement: function(error, element) {
            // Need to add this function to remove the error default placement
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });

// Validation for Category  Specifications End



    //Validation for Product  start

    jQuery('#ProductAdminAddForm,#ProductAdminEditForm').validate({
        focusInvalid: false,
        rules: {
            "data[Product][name]": "required",
            "data[Product][package_id]": "required",
            "data[Product][photo]": {
                required: function() {
                    return (jQuery('#categorySpecType').val() < 1);
                }
            }
        },
        messages: {
            "data[Product][name]": translate("Please enter Product Name."),
            "data[Product][package_id]": translate("Please enter Package Name."),
            "data[Product][photo]": {
                required: translate("Please enter Product Photo.")
            }
        },
        errorPlacement: function(error, element) {
            // Need to add this function to remove the error default placement
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });

    jQuery('#ProductCheckboxAdminAddForm,#ProductCheckboxAdminEditForm').validate({
        focusInvalid: false,
        rules: {
            "data[Product][name]": "required",
            "data[Product][package_id]": "required"
        },
        messages: {
            "data[Product][name]": translate("Please enter Product Name."),
            "data[Product][package_id]": translate("Please enter Package Name."),
        },
        errorPlacement: function(error, element) {
            // Need to add this function to remove the error default placement
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });

// Validation for Product  End
// 
    //Validation for Site Settings  start

    jQuery('#SiteSettingAdminEditForm').validate({
        focusInvalid: false,
        rules: {
            "data[SiteSetting][site_name]": "required",
            "data[SiteSetting][twitter]": "required",
            "data[SiteSetting][facebook]": "required",
            "data[SiteSetting][youtube]": "required",
            "data[SiteSetting][site_email]": {
                required: true,
                email: true
            }
        },
        messages: {
            "data[SiteSetting][site_name]": translate("Please enter your site name."),
            "data[SiteSetting][twitter]": translate("Please enter your twitter url."),
            "data[SiteSetting][facebook]": translate("Please enter your facebook url."),
            "data[SiteSetting][youtube]": translate("Please enter your youtube url."),
            "data[SiteSetting][site_email]": {
                required: translate("Please enter your email."),
                email: translate("Please enter your valid email.")
            }
        },
        errorPlacement: function(error, element) {
            // Need to add this function to remove the error default placement
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });

// Validation for site settings End

//Validation for Email Tempalte Start
    jQuery('#EmailTemplateAdminAddForm,#EmailTemplateAdminEditForm').validate({
        focusInvalid: false,
        rules: {
            "data[EmailTemplate][name]": "required",
            "data[EmailTemplate][code]": "required",
            "data[EmailTemplate][email_subject]": "required",
            "data[EmailTemplate][email_content]": "required"
        },
        messages: {
            "data[EmailTemplate][name]": translate("Please enter email Template name."),
            "data[EmailTemplate][code]": translate("Please enter code."),
            "data[EmailTemplate][email_subject]": translate("Please enter subject."),
            "data[EmailTemplate][email_content]": translate("Please enter content.")
        },
        errorPlacement: function(error, element) {
            // Need to add this function to remove the error default placement
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });

    // Validation for Email Template End

    // Validation for Content Managent Start

    jQuery('#ContentAdminAddForm,#ContentAdminEditForm').validate({
        focusInvalid: false,
        rules: {
            "data[Content][title]": "required",
            "data[Content][code]": "required",
            "data[Content][content]": "required"
        },
        messages: {
            "data[Content][title]": translate("Please enter Title."),
            "data[Content][code]": translate("Please enter Code."),
            "data[Content][content]": translate("Please enter Content.")
        },
        errorPlacement: function(error, element) {
            // Need to add this function to remove the error default placement
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });
    // Validation for content management End


    // Validation for Content Supplier Start

    jQuery('#SupplierAdminAddForm,#SupplierAdminEditForm').validate({
        focusInvalid: false,
        rules: {
            "data[Supplier][name]": "required"
        },
        messages: {
            "data[Supplier][name]": translate("Please Enter Supplier Name.")
        },
        errorPlacement: function(error, element) {
            // Need to add this function to remove the error default placement
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });
    // Validation for supplier End


    // Validation for Faqs Managent Start

    jQuery('#FaqAdminAddForm,#FaqAdminEditForm').validate({
        focusInvalid: false,
        rules: {
            "data[Faq][question]": "required",
            "data[Faq][answer]": "required"
        },
        messages: {
            "data[Faq][question]": translate("Please Enter Question."),
            "data[Faq][answer]": translate("Please Enter Answer.")
        },
        errorPlacement: function(error, element) {
            // Need to add this function to remove the error default placement
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });
    // Validation for Faqs management End
    // 
    //Validation for Package  start

    jQuery('#PackageAdminAddForm,#PackageAdminEditForm').validate({
        focusInvalid: false,
        rules: {
            "data[Package][name]": "required",
            "data[Package][short_description]": "required",
            "data[Package][description]": "required",
            "data[Package][price]": {
                required: true,
                number: true
            },
            "data[Package][photo]": {
                required: function() {
                    return (jQuery('#PackageImage').val() < 1);
                }
            }
        },
        messages: {
            "data[Package][name]": translate("Please enter Package Name."),
            "data[Package][short_description]": translate("Please enter Short Decription."),
            "data[Package][description]": translate("Please enter description."),
            "data[Package][price]": {
                required: translate("Please enter the price."),
                number: translate("Please enter the numeric value for price.")
            },
            "data[Package][photo]": {
                required: translate("Please enter Package Photo.")
            }
        },
        errorPlacement: function(error, element) {
            // Need to add this function to remove the error default placement
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });

// Validation for Package  End


    //Sample validation code
    jQuery('#UserAdminAddAccountManagerForm').validate({
        rules: {
            "data[User][password]": {
                required: true,
                minlength: 6
            },
            "data[User][password]": {
                required: true,
                minlength: 6
            },
            "data[User][username]": {
                required: true,
                email: true
            },
            "data[User][password]": {
                required: true,
                minlength: 6
            }
        },
        messages: {
            "data[User][username]": {
                required: translate("Please enter your email."),
                email: translate("Please enter your email.")
            },
            "data[User][password]": {
                required: translate("Please enter your password."),
                minlength: translate("Please enter atleast 6 character.")

            }
        },
        errorPlacement: function(error, element) {
            // Need to add this function to remove the error default placement
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function(event, validator) {
            jQuery("#messageElement").show().delay(5000).fadeOut();
        }
    });
    // To change monthly price at Product add edit page
    jQuery('.monthly_price').on('ifChecked', function(event) {
        jQuery("#ProductMonthlyPrice").show();
    });
    jQuery('.monthly_price').on('ifUnchecked', function(event) {
        jQuery("#ProductMonthlyPrice").hide();
        jQuery("#ProductMonthlyPrice").val('');
    });

    // To change is indeling in category add / edit page
    jQuery('.is_indeling').on('ifChecked', function(event) {
        // alert(1);
        jQuery("#has_package_div").hide();
        jQuery('#haspackage').iCheck('uncheck');
    });
    jQuery('.is_indeling').on('ifUnchecked', function(event) {
        jQuery("#has_package_div").show();

    });


// To select the checkbox for upload category images in ApartmentType add/edit pages 
    jQuery('.category-checkbox').on('ifChecked', function(event) {
        var id = jQuery(this).attr("id");
        var chkID = id - 1;
        var indeling = jQuery("#isIndeling_" + id).val();
        if (indeling == 1) {
            jQuery(this).parents(".checkbox").append("<input type='file' name='data[Category][" + id + "][layout_image]' class='form-control' id=img-" + id + " />");
        }
        else {
            jQuery(this).parents(".checkbox").append('<div class="checkbox"id="chk-' + id + '"><input type="checkbox" name="data[Category][' + id + '][is_optional_type]" class="form-control dyn-check" value="1" /><label for="CategoryCategoryId">' + translate("Optional") + '</label></div>');
        }
        jQuery(".dyn-check").iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal'
        });
    });
    jQuery('.category-checkbox').on('ifUnchecked', function(event) {
        var id = jQuery(this).attr("id");
        var imgid = "img-" + jQuery(this).attr("id");
        var chkboxId = "#chk-" + jQuery(this).attr("id");
        var indeling = jQuery("#isIndeling_" + id).val();
        if (indeling == 1) {
            jQuery(this).parents(".checkbox").find("input[id=" + imgid + "]").remove();
        }
        else {
            jQuery(chkboxId).remove();
            jQuery(this).parents(".checkbox").find("input[id=" + chkboxId + "]").remove();
        }

    });



// to change imageType entry Type at Product add edit page
    jQuery('#categorySpecType').change(function(e) {
        e.preventDefault();
        var productType = jQuery('#ProductProductType');
        var imageDiv = jQuery('.imageType');
        var checkboxDiv = jQuery('.checkboxType');
        var actionBtn = jQuery('.box-footer');
        var formDiv = jQuery('.form-show');
        var selectField = jQuery('#ProductCategorySpecificationId option:selected');
        var currentSelection = jQuery('#categorySpecType option:selected').text();
        var optionType = jQuery('#categorySpecType option:selected').attr('rel');
        var selectedId = jQuery(this).val();
        productType.val(optionType);
        if (optionType == 1) {
            imageDiv.hide();
            checkboxDiv.show();
            actionBtn.show();
            formDiv.show();
            selectField.text(currentSelection);
            selectField.val(selectedId);
        } else if (optionType == 0) {
            imageDiv.show();
            // checkboxDiv.hide();
            actionBtn.show();
            formDiv.show();

            selectField.text(currentSelection);
            selectField.val(selectedId);
        } else {
            imageDiv.hide();
            checkboxDiv.hide();
            actionBtn.hide();
            formDiv.hide();
        }
        if (typeof(optionType) == "undefined") {
            actionBtn.show();
        }
    });

    //Image checkbox product page
    jQuery('.image-radio').click(function() {
        var rel = jQuery(this).attr('rel');
        jQuery('.image-products-' + rel).parent().parent().removeClass('default');
        //jQuery(this).addClass('default');
        jQuery(this).parent().parent().addClass('default');
    });

    //Ajax request for filtering category specification on basis of selected category
    function categoryChangeEvent() {
        var selectedCategory = jQuery('#CategorySpecificationCategoryId').val();
        var categorySpecificationID = jQuery('#CategorySpecificationId').val();
        if (typeof(categorySpecificationID) == "undefined") {
            categorySpecificationID = 0;
        }
        jQuery('.category-specifications').html('Loading...');
        jQuery.ajax({
            type: 'GET',
            url: webpath + 'categories/ajax_get_category_specifications/' + selectedCategory + "/" + categorySpecificationID,
            success: function(response) {
                // console.log(response)
                jQuery('.category-specifications').html(response);
            }
        })
    }

    jQuery('#CategorySpecificationCategoryId').bind('change', function() {
        categoryChangeEvent();
    });


    if (typeof(initializeCategoryChangeEvent) != "undefined") {
        categoryChangeEvent();
    }

});

