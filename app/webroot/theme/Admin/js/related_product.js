function get_options(num) {
    var newOptions = [];
    for (var key in options) {
        if (key == num) {
            newOptions.push(options[key]);
        }
    }
    return newOptions;
}
var options = relatedProduct;
var selectedoptionID = selectedID;
var selectedProductID = selectedProductID;

jQuery(document).ready(function() {
    categorySpecificationChangeEvent();
//    var inputOption = get_options(selectedoptionID);
//    jQuery(".related_products").show();
//    var parent = jQuery('.related_product');
//    parent.append("<option value='0'>None</option>");
//    if (inputOption.length) {
//        parent.html('');
//        for (var i = 0; i < inputOption.length; i++) {
//            for (var key in inputOption[i]) {
//                if (inputOption[i][key]['id'] == selectedProductID) {
//                    var selected = "selected=selected";
//                }
//                else {
//                    var selected = "";
//                }
//                parent.append("<option " + selected + " value='" + inputOption[i][key]['id'] + "'>" + inputOption[i][key]['name'] + "</option>");
//
//            }
//
//        }
//    } else {
//        parent.html('');
//        parent.append("<option value='0'>None</option>");
//        jQuery(".related_products").hide();
//    }
    jQuery(".category_specification").change(function() {
        var inputOption = get_options(jQuery(this).val());
        jQuery(".related_products").show();
        var parent = jQuery('.related_product');
        parent.append("<option value='0'>None</option>");
        if (inputOption.length) {
            parent.html('');
            for (var i = 0; i < inputOption.length; i++) {
                for (var key in inputOption[i]) {
                    // console.log(inputOption[i][key]['name']);
                    parent.append("<option value='" + inputOption[i][key]['id'] + "'>" + inputOption[i][key]['name'] + "</option>");
                    categorySpecificationChangeEvent();

                }

            }
        } else {
            parent.html('');
            parent.append("<option value='0'>None</option>");
        }
    });


    // categorySpecificationChangeEvent();   

    jQuery('#categorySpecType').change(function() {
        newSelectionType = jQuery(this).find('option:selected').attr('rel');
        thisSelectedVal = jQuery(this).find('option:selected').val();

        isIndeling = jQuery(this).find('option:selected').data('is_indeling');

        if (isIndeling == 0) {
            jQuery('#linked_category').hide();
        } else {
            jQuery('#linked_category').show();
        }
        if (typeof(newSelectionType) != "undefined") {
            if (newSelectionType[0] == 0) {
                categorySpecificationChangeEvent();
            }
        }
        updatePackageList(thisSelectedVal);
    });

    jQuery('#ProductCategorySpecificationId').change(function() {
        thisSelectedVal = jQuery(this).find('option:selected').val();
        updatePackageList(thisSelectedVal);
    });

});

function categorySpecificationChangeEvent() {
    var categorySpecificationID = jQuery('#categorySpecType').find('option:selected').val();
    jQuery.ajax({
        type: 'GET',
        url: webpath + 'products/ajax_get_parent_products/' + categorySpecificationID,
        dataType: 'JSON',
        success: function(response) {
            if (response.success == 1) {
                html = '';
                jQuery.each(response.products, function(index, value) {                   
                    if(selectedProductID == index ){
                        html += '<option value="' + index + '" selected="selected">' + value + '</option>';
                        selectedProductID = '';
                    }else{
                        html += '<option value="' + index + '">' + value + '</option>';
                    }
                    
                });
                jQuery('#related_product').html('');
                jQuery('#related_product').html(html);
            } else {
                html = '<option value = "0">None</opton>';
                jQuery('#related_product').html(html);
            }
        }
    })
}

function updatePackageList(categorySpecificationID) {
    jQuery.ajax({
        type: 'GET',
        url: webpath + 'packages/get_packages_of_category_specification/' + categorySpecificationID,
        dataType: 'JSON',
        success: function(response) {
            html = '';
            jQuery.each(response, function(index, value) {
                html += '<option value = "' + index + '">' + value + '</option>';
            });
            jQuery('#ProductPackageId').html(html);
        }
    })
}
    