

$('document').ready(function(){
      $('ul.childMenu').hide();
      showDatePicker('.datepicker-control');
      showDatePicker('#DeceaseDeathdate');
      showDatePicker('#ChruchDataCeromonyDate');

     $('.datepicker-control').pickadate({
        format:'yyyy-mm-dd',
        monthsFull: ['januari','februari','maart','april','mei','juni','juli','augustus','september','october','november','december'],
        monthsShort: ['jan.','feb.','maart','apr.','mei','juni','juli','aug.','sept.','oct.','nov.','dec.'],
        weekdaysFull: ['zondag','maandag','dinsdag','woensdag','donderdag','vrijdagdag','zaterdag'],
        weekdaysShort: ['Zon','Ma','Di','Woe','Do','Vrij','Zat'],
      });
      var activeTab = $('li.active').data('rel');
      if(activeTab == 'tab1' && action == 'admin_add'){ 
        $('li.active').siblings('li').children('a').removeAttr('href');
        $('a.arrow').hide();
        //console.log(others);
      }
      

       /*
      * Auto populate the street name and city according to zipcode and street number in ClientData tab
      */

      $('.glyphicon-search').on('click',function(){ 
          var postcode = $('.postcode').val();
          if(validatPostCode(postcode) === true){
             generateStreetnLocation(postcode,'street','place');
          }else{
            alert('Enter valid Post code');
          }
          
        
      });

      /*
      * Auto populate the street name and city according to zipcode and street number in Decease tab
      */

      /*$('#DeceaseZipCode').on('change',function(){ 
          var postcode = $(this).val();
          if(validatPostCode(postcode) === true){
             generateStreetnLocation(postcode,'DeceaseStreet','DeceaseLocation');
          }else{
            alert('Enter valid Post code');
          }
          
        
      });*/

       
      /*Auto populate the street name and city according to zipcode and street number in Entrepreneurs tab*/
      

      /*$('#EntrepreneurZipCode').on('change',function(){ 
          var postcode = $(this).val();
          if(validatPostCode(postcode) === true){
             generateStreetnLocation(postcode,'EntrepreneurStreet','EntrepreneurLocation');
          }else{
            alert('Enter valid Post code');
          }
          
        
      });*/


      

      /*
      * Dynamically add an input into the CoffeeRooms tabs
      */

        $('.glyphicon-plus').click(function(){
          var html = '<tr class="dynamic-input">';
          html += '<td>';
          html += '<input placeholder="Aantal" id="CoffeeRoomNumber" class="form-control" type="text" name="data[CoffeeRoom][number][]">';
          html += '</td>';
          html += '<td>';
          html += '<input placeholder="Omschrijving" id="CoffeeRoomNumber" class="form-control" type="text" name="data[CoffeeRoom][letter][]">';
		  html += '</td>';
		  html += '<td class="text-center" valign="middle" style="vertical-align:middle;">';
          html += '<a href="#" class="text-danger">';
		  html += '<span class="glyphicon glyphicon-remove">';
		  html += '</span>';
		  html += '</a>';
		  html += '</td>';
		  html += '</tr>';
          
          $('table tr:first-child').after(html);
        });

        $('table').on('click','.dynamic-input .glyphicon-remove', function(){
          var input = $('.dynamic-input').length;
          if(input !== 0){ 
		  
            $(this).closest('tr.dynamic-input').remove();
          }else
          {
           alert("There is nothing to remove"); 
          }
        });      

      $('li.parentWithChild').hover(function(){
          $(this).children().slideDown();
        },function(){
          $('li > ul.childMenu').slideUp();
     });

      $('#CoffinsDataConffinName').change(function(){
             var name = $(this).val();
             var model = 'Coffin';
             var idPrice = 'CoffinsDataPrice';
             var field = 'conffin_name';
             ajaxCall(name,model,idPrice,field);
             
      });  

       $('#HospitalsDataHospitalName').change(function(){
             var name = $(this).val();
             var model = 'Hospital';
             var idPrice = 'HospitalsDataPrice';
             var field = 'hospital_name';
            // alert(val);
            ajaxCall(name,model,idPrice,field);
             
      });  

       $('#RouwcentrumsDataName').change(function(){
             var name = $(this).val();
             $('#RouwcentrumsDataPrice').val('');
             $('#RouwcentrumsDataSomeDays').val('');
             var model = 'Rouwcentrum';
             var idPrice = 'RouwcentrumsDataPrice';
             var field = 'rouwcentrum_name';
             var number = 1;
             var hidden = 'rouwcentrumPrice';
            // alert(val);
            ajaxCall(name,model,idPrice,field,number,hidden);
             
      });   


        $('#RouwcentrumsDataSomeDays').change(function(){
          var per_price = $('.rouwcentrumPrice').val();
         
          //var len = $('#FuneralLettersDataPrice').val().length;
          var number = $('#RouwcentrumsDataSomeDays').val(); 
          if(number === ''){
             number = 1;
          }
          if(per_price !== ''){
            var total = per_price * number;
            $('#RouwcentrumsDataPrice').val(total);
          }
         
      });

      $('#StampNumber').change(function(){
          var per_price = $('#StampPriceNetherland').val();
          if(per_price === ''){
             per_price = 0;
          }
          var number = $('#StampNumber').val();
          if(per_price !== ''){
            var total = per_price * number;
            $('#StampPrice').val(total);
          }
      });

      
       $('#CareDeceasesDataName').change(function(){
             var name = $(this).val();
             var model = 'CareDecease';
             var idPrice = 'CareDeceasesDataPrice';
             var field = 'name';
            // alert(val);
            ajaxCall(name,model,idPrice,field);
             
      });    

        //Coffee Ticket events 
        $('#CoffeeTicketName').change(function(){
             var name = $(this).val();
             var model = 'Koffietafelkaartje';
             if(name == 'Zonder naam'){
                var idPrice = 'CoffeeTicketNameWithoutPrice';
                $('#CoffeeTicketNameWithPrice').val('');
                $('#CoffeeTicketNumber').val('');
                $('#CoffeeTicketPrice').val('');
             }else if(name == ''){
                $('#CoffeeTicketNameWithPrice').val('');
                $('#CoffeeTicketNameWithoutPrice').val('');
                $('#CoffeeTicketNumber').val('');
                $('#CoffeeTicketPrice').val('');
               }
             else{
                 var idPrice = 'CoffeeTicketNameWithPrice';
                  $('#CoffeeTicketNameWithoutPrice').val('');
                  $('#CoffeeTicketNumber').val('');
                  $('#CoffeeTicketPrice').val('');
             }
            var field = 'type';
            var number = 1;
            ajaxCall(name,model,idPrice,field,number);
             
      });   


        $('#CoffeeTicketNumber').change(function(){
          var name = $('#CoffeeTicketName option:selected').val();
          if(name == 'Zonder naam'){ 
               var per_price = $('#CoffeeTicketNameWithoutPrice').val();
           }else{
                per_price = $('#CoffeeTicketNameWithPrice').val();
            }
         var number = $('#CoffeeTicketNumber').val();  
         if(per_price !== ''){
            var total = per_price * number;
            $('#CoffeeTicketPrice').val(total);
          }
         
      });


     // HomeobaringName sections

        
      $('#HomeobaringName').change(function(){
             var name = $(this).val();
             $('#HomeobaringPerPrice').val('');
             $('#HomeobaringDays').val('');
             var model = 'Homebaring';
             var idPrice = 'HomeobaringPerPrice';
             var field = 'name';
             var number = 1;
             var hidden = 'homeobaringPrice';
            // alert(val);
            ajaxCall(name,model,idPrice,field,number,hidden);
             
      });   


        $('#HomeobaringDays').change(function(){
          var per_price = $('.homeobaringPrice').val();
         
          //var len = $('#FuneralLettersDataPrice').val().length;
          var number = $('#HomeobaringDays').val();
          if(number === ''){
             number = 1;
          }
          if(per_price !== ''){
            var total = per_price * number;
            $('#HomeobaringPerPrice').val(total);
          }
         
      });

// Boekje section 
    $('#MisboekjesDataPricePerPiecename').change(function(){
            var name = $(this).val();
            $('#MisboekjesDataPricePerPiecenumber').val('');
            $('#MisboekjesDataTotalPrice').val('');
            var model = 'Boekje';
             var idPrice = 'MisboekjesDataPricePerPiece';
             var field = 'name';
             var number = 1;
             var hidden = 'MisboekjesDataPricePerPiece';
            // alert(val);
            ajaxCall(name,model,idPrice,field,number,hidden);
             
      });   


    $('#MisboekjesDataPricePerPiecenumber').change(function(){ 
          var per_price = $('#MisboekjesDataPricePerPiece').val(); 
          //var len = $('#FuneralLettersDataPrice').val().length;
          var number = $(this).val();  
          number = number === ''?1:number;
          if(per_price !== ''){
            var total = per_price * number;
           
            $('#MisboekjesDataTotalPrice').val(total);
          }
         
      }); 

     $('#CarriersDataName').change(function(){
             var name = $(this).val();
             if(name == ''){ 
               $('#CarriersDataPrice').val('');
               $('#MiscellaneouseNumber').val('');
            }
             var model = 'Carrier';
             var idPrice = 'CarriersDataPrice';
             var field = 'name';
             var number = 1;
             var hidden = 'carrierPrice';
            // alert(val);
            ajaxCall(name,model,idPrice,field,number,hidden);
             
      });   

      $('#CommemorationNumber').change(function(){
            var number = $(this).val();
            var perPrice = $('#CommemorationPricePerPiece').val();
            number = (number === '') ? 1 : number;
            var total = perPrice * number;
            total = total == 0 ? '' : total;
            $('#CommemorationPrice').val(total);
      });  

       $('#CommemorationPricePerPiece').change(function(){
            $('#CommemorationNumber').val('');
            $('#CommemorationPrice').val('');
            var perPrice = $(this).val();
            var number = $('#CommemorationNumber').val();
            number = (number === '') ? 1 : number;
            var total = perPrice * number;
            total = total == 0 ? '' : total;
            $('#CommemorationPrice').val(total);
            
              
             

      });  

        $('#CarriersDataNumber').change(function(){
          var per_price = $('.carrierPrice').val();
          var number = $('#CarriersDataNumber').val();
          number = (number === '') ? 1 : number;
          if(per_price !== ''){
            var total = per_price * number;
            $('#CarriersDataPrice').val(total);
          }
         
      });
       
       $('#ArrangementData0Type').change(function(){
             var name = $(this).val();
             var model = 'Arrangement';
             var idPrice = 'ArrangementData0Price';
             var field = 'arrangement_type';
            // alert(val);
            ajaxCall(name,model,idPrice,field);
             
      });  

       $('#ArrangementData1Type').change(function(){
             var name = $(this).val();
             var model = 'Arrangement';
             var idPrice = 'ArrangementData1Price';
             var field = 'arrangement_type';
            // alert(val);
            ajaxCall(name,model,idPrice,field);
             
      });  

       $('#ArrangementData2Type').change(function(){
             var name = $(this).val();
             var model = 'Arrangement';
             var idPrice = 'ArrangementData2Price';
             var field = 'arrangement_type';
            // alert(val);
            ajaxCall(name,model,idPrice,field);
             
      });  


       $('#ArrangementData3Type').change(function(){
             var name = $(this).val();
             var model = 'Arrangement';
             var idPrice = 'ArrangementData3Price';
             var field = 'arrangement_type';
            // alert(val);
            ajaxCall(name,model,idPrice,field);
             
      });  

      

       $('#FuneralLettersDataLetterName').change(function(){
             var name = $(this).val();
             if(name == ''){ 
               $('#FuneralLettersDataNumber').val('');
               $('#FuneralLettersDataPrice').val('');
             }
             var model = 'FunerlLetterCard';
             var cardField = 'FuneralLettersDataMapType';
             var field = 'funeral_letter_id';
            // alert(val);
            findCardType(name,model,cardField,field);
             
      });  

        $('#FuneralLettersDataMapType').change(function(){
             var name = $(this).val();
             $('#FuneralLettersDataNumber').val('');
             $('#FuneralLettersDataPrice').val('');
             var model = 'FunerlLetterCard';
             var cardField = 'FuneralLettersDataPrice';
             var field = 'card';
             var number = $('#FuneralLettersDataNumber').val(); 
             var hidden = 'cardPrice';            
            // alert(val);
            ajaxCall(name,model,cardField,field,number,hidden);
             
      }); 

      $('#FuneralLettersDataNumber').change(function(){
          var per_price = $('.cardPrice').val();
         
          //var len = $('#FuneralLettersDataPrice').val().length;
          var number = $('#FuneralLettersDataNumber').val();
          number = number === '' ? 1 : number;
          if(per_price !== ''){
            var total = per_price * number;
            $('#FuneralLettersDataPrice').val(total);
          }
         
      }); 


         $('#PrayerCardsDataPrayerCategoryType').change(function(){
             var name = $(this).val();
             $('#PrayerCardsDataNumber').val('');
             $('#PrayerCardsDataPrice').val('');
             var model = 'PrayerCard';
             var cardField = 'PrayerCardsDataCardType';
             var field = 'prayer_card_category_id';
             
            // var hidden = 'cardPrice';            
            // alert(val);
            findCardType(name,model,cardField,field);
             
      }); 

        $('#PrayerCardsDataCardType').change(function(){
             var name = $(this).val();
             $('#PrayerCardsDataNumber').val('');
             $('#PrayerCardsDataPrice').val('');
             var model = 'PrayerCard';
             var cardField = 'PrayerCardsDataPrice';
             var field = 'card';
             var number = $('#PrayerCardsDataNumber').val(); 
             var hidden = 'PrayerCardPrice';            
            // alert(val);
            ajaxCall(name,model,cardField,field,number,hidden);
             
      });    

       $('#PrayerCardsDataNumber').change(function(){
          var per_price = $('.PrayerCardPrice').val();
         
          //var len = $('#FuneralLettersDataPrice').val().length;
          var number = $('#PrayerCardsDataNumber').val();
          number = number === ''?1:number;
          if(per_price !== ''){
            var total = per_price * number;
            $('#PrayerCardsDataPrice').val(total);
          }
         
      }); 

    ///////////// POSTZEAL/STAMPS SECTION ////////////////// 
    
     $('#StampNumber').change(function(){
            var number = $(this).val();
            var perPrice = $('#StampPriceNetherland').val();
            number = (number === '') ? 1 : number;
            var total = perPrice * number;
            total = total == 0 ? '' : total;
            $('#StampPrice').val(total);
      });  

       $('#StampPriceNetherland').change(function(){
            $('#StampNumber').val('');
            $('#StampPrice').val('');
            var perPrice = $(this).val();
            var number = $('#StampNumber').val();
            number = (number === '') ? 1 : number;
            var total = perPrice * number;
            total = total == 0 ? '' : total;
            $('#StampPrice').val(total);
            
              
             

      });  


    ///////////// BOOKLET SECTION //////////////////

      $('#MiscellaneouseBooklet').change(function(){
             var name = $(this).val();
             var model = 'Booklet';
             var cardField = 'MiscellaneouseBookletPrice';
             var field = 'name';
             // alert(val);
            ajaxCall(name,model,cardField,field);
             
      }); 

     ///////////// GUIDE AFTER DEATH SECTION //////////////////
    
      $('#MiscellaneouseGuideAfterDeath').change(function(){
             var name = $(this).val();
             var model = 'GuideAfterDeath';
             var cardField = 'MiscellaneouseGuideAfterDeathPrice';
             var field = 'name';
             // alert(val);
            ajaxCall(name,model,cardField,field);
             
      });  

//    
      $('.arrow').on('click',function(){ //alert('hell');
           var direction = $(this).data('rel');
           var curtElem = $('li.active');
          if(direction == 'right'){
                if($(curtElem).is(':last-child')){
                   var first =  $(curtElem).parent('ul').find('li:first-child').children('a');
                    var href = first.attr("href");
                    window.location.replace(href);
                }else{
                   var nextElem = $(curtElem).next().children('a');
                   var href = nextElem.attr("href");
                  window.location.replace(href);
              }
           }else
           {
             if($(curtElem).is(':first-child')){
                    var last =  $(curtElem).parent('ul').find('li:last-child').children('a');
                    var href = last.attr("href");
                    window.location.replace(href);
                }else{
                  var prevElem = $(curtElem).prev().children('a');
                  var href = prevElem.attr("href");
                  window.location.replace(href);
            }
          }
       
          //var nextElem = curtElem.next();
          //$('li.active').next().trigger('click');
      });

    getAutocompleteList('.postcode', webpath + 'admin/clientDatas/getCities/');
    getAutocompleteList('.undertakerName', webpath + 'admin/Entrepreneurs/getUndertakerNames/' , 2);

});

/******Function for ajax Call to find price ********/

function  ajaxCall(name,model,inputPriceField,field,number,hidden){
           if(typeof number == 'undefined'){
            number = 1;
           }
           if(typeof number  == 'undefined'){
            hidden = '';
           }
           $.ajax({
                 type:'POST',
                  url: priceUrl, 
                  dataType:'json',
                  data:{name:name,model:model,number:number,field:field},
                  success:function(html){
                   $('#'+inputPriceField).val(html.value);
                    if(hidden != ''){
                       $('.'+hidden).val(html.value);
                     }
                  }
            });
}


/******Function for ajax Call to find card type ********/

function findCardType(name,model,card,field)
{           $.ajax({
                  type:'POST',
                  url: cartUrl,
                  dataType:'html',
                  data:{name:name,model:model,field:field},
                  success:function(response){                     
                      $('#'+card).html(response);
                  }
            });
}
/**Custom function for manipulation of radio ***/

    $('div.dataradioGroup1').hide();
    $('div.dataradioGroup2').hide();
    $('div.dataradioGroup3').hide();
    $('document').ready(function(){
    	  $('input.radioGroup1').change(function(){
    	       var data = $(this).val();
    	       toggleDataField('dataradioGroup1',data);
	          
        });

    /*	$('input.group2').change(function(){
    		$('div.data1').fadeOut();
    		var  data = $(this).closest('.form-group').find('.data1');
    		data.fadeIn();
          });*/
        $('input.radioGroup2').change(function(){
    	       var data = $(this).val();
    	       toggleDataField('dataradioGroup2',data);
	          
        });
          $('input.radioGroup3').change(function(){
    	       var data = $(this).val();
    	       toggleDataField('dataradioGroup3',data);
	          
        });
    });


    // function for manipulating radio buttons

    function toggleDataField(valueField,data)
    {
           	  if(data == 1){
           	  	$('div.'+valueField).fadeIn();
           	  }else
           	  {
           	  	$('div.'+valueField).fadeOut();
           	  }

    }

//function for datepicker
   function showDatePicker(element){
     $(element).pickadate({
        format:'yyyy-mm-dd',
        monthsFull: ['januari','februari','maart','april','mei','juni','juli','augustus','september','october','november','december'],
        monthsShort: ['jan.','feb.','maart','apr.','mei','juni','juli','aug.','sept.','oct.','nov.','dec.'],
        weekdaysFull: ['zondag','maandag','dinsdag','woensdag','donderdag','vrijdagdag','zaterdag'],
        weekdaysShort: ['Zon','Ma','Di','Woe','Do','Vrij','Zat'],
        
      });
   }
 // function  to for validating postCodes

 function  validatPostCode(postCode){
    var regex = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i;
    return regex.test(postCode);
 }

 //function to generate street and location

 function generateStreetnLocation(postCode , streetId , placeId ){
      $.ajax({
                type:'POST',
                url: dynamicLocationGeneartor,
                dataType:'json',
                data:{postCode:postCode},
                success:function(response){
                   if(response.street_name == '' || response.city == ''){
                    var message = '<label class="control-label col-sm-2 error">Wrong postcode</label>';
                    $('.error').remove();
                    $('.message').append(message);
                    $('.'+streetId).val('');
                    $('.'+placeId).val('');
                   }else{                 
                    $('.'+streetId).val(response.street_name);
                    $('.'+placeId).val(response.city);
                  }
                   // $('#'+streetId).val(response.char);
                   // $('#'+streetId).val(response.code);
                }
            });

 }




//for autocomplete
 function getAutocompleteList( elm, url, minLength ) {
  if( typeof minLength === 'undefined' ) {
    minLength = 3;
  }
  $( elm ).autocomplete({
    source: function( request, response ) {       
      getSource(url, request.term, response);
    },
    minLength: minLength
  });

}

function getSource( url, term, response ) {

  $.ajax({
      url: url + term,
      dataType: "json",
      
      success: function( data ) {           
        response( data );
      }
    });
  
}