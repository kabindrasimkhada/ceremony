

jQuery('document').ready(function(){
      jQuery('ul.childMenu').hide();

      ///input field show and hide according to status of radio buttons

      inputFieldViewerByradioStatus("input.radioGroup1:checked",'.dataradioGroup1');
      inputFieldViewerByradioStatus('input.radioGroup2:checked','.dataradioGroup2');

      showDatePicker('.datepicker-control');
      showDatePicker('#DeceaseDeathdate');
      showDatePicker('#ChruchDataCeromonyDate');

     jQuery('.datepicker-control').pickadate({
        format:'yyyy-mm-dd',
        monthsFull: ['januari','februari','maart','april','mei','juni','juli','augustus','september','october','november','december'],
        monthsShort: ['jan.','feb.','maart','apr.','mei','juni','juli','aug.','sept.','oct.','nov.','dec.'],
        weekdaysFull: ['zondag','maandag','dinsdag','woensdag','donderdag','vrijdagdag','zaterdag'],
        weekdaysShort: ['Zon','Ma','Di','Woe','Do','Vrij','Zat'],
      });
      var activeTab = jQuery('li.active').data('rel');
      if(activeTab == 'tab1' && action == 'admin_add'){ 
        jQuery('li.active').siblings('li').children('a').removeAttr('href');
        jQuery('a.arrow').hide();
        //console.log(others);
      }
      

       /*
      * Auto populate the street name and city according to zipcode and street number in ClientData tab
      */

      jQuery('.glyphicon-search').on('click',function(){ 
          var postcode = jQuery('.postcode').val();
          if(validatPostCode(postcode) === true){
             generateStreetnLocation(postcode,'street','place');
          }else{
            alert('Enter valid Post code');
          }
          
        
      });

      /*
      * Auto populate the street name and city according to zipcode and street number in Decease tab
      */

      /*jQuery('#DeceaseZipCode').on('change',function(){ 
          var postcode = jQuery(this).val();
          if(validatPostCode(postcode) === true){
             generateStreetnLocation(postcode,'DeceaseStreet','DeceaseLocation');
          }else{
            alert('Enter valid Post code');
          }
          
        
      });*/

       
      /*Auto populate the street name and city according to zipcode and street number in Entrepreneurs tab*/
      

      /*jQuery('#EntrepreneurZipCode').on('change',function(){ 
          var postcode = jQuery(this).val();
          if(validatPostCode(postcode) === true){
             generateStreetnLocation(postcode,'EntrepreneurStreet','EntrepreneurLocation');
          }else{
            alert('Enter valid Post code');
          }
          
        
      });*/


      

      /*
      * Dynamically add an input into the CoffeeRooms tabs
      */

        jQuery('.glyphicon-plus').click(function(){
          var html = '<tr class="dynamic-input">';
          html += '<td>';
          html += '<input placeholder="Aantal" id="CoffeeRoomNumber" class="form-control" type="text" name="data[CoffeeRoom][number][]">';
          html += '</td>';
          html += '<td>';
          html += '<input placeholder="Omschrijving" id="CoffeeRoomNumber" class="form-control" type="text" name="data[CoffeeRoom][letter][]">';
		  html += '</td>';
		  html += '<td class="text-center" valign="middle" style="vertical-align:middle;">';
          html += '<a href="#" class="text-danger">';
		  html += '<span class="glyphicon glyphicon-remove">';
		  html += '</span>';
		  html += '</a>';
		  html += '</td>';
		  html += '</tr>';
          
          jQuery('table tr:first-child').after(html);
        });

        jQuery('table').on('click','.dynamic-input .glyphicon-remove', function(){
          var input = jQuery('.dynamic-input').length;
          if(input !== 0){ 
		  
            jQuery(this).closest('tr.dynamic-input').remove();
          }else
          {
           alert("There is nothing to remove"); 
          }
        });      

      jQuery('li.parentWithChild').hover(function(){
          jQuery(this).children().slideDown();
        },function(){
          jQuery('li > ul.childMenu').slideUp();
     });

      jQuery('#CoffinsDataConffinName').change(function(){
             var name = jQuery(this).val();
             var model = 'Coffin';
             var idPrice = 'CoffinsDataPrice';
             var field = 'conffin_name';
             ajaxCall(name,model,idPrice,field);
             
      });  

       jQuery('#HospitalsDataHospitalName').change(function(){
             var name = jQuery(this).val();
             var model = 'Hospital';
             var idPrice = 'HospitalsDataPrice';
             var field = 'hospital_name';
            // alert(val);
            ajaxCall(name,model,idPrice,field);
             
      });  

       jQuery('#RouwcentrumsDataName').change(function(){
             var name = jQuery(this).val();
             jQuery('#RouwcentrumsDataPrice').val('');
             jQuery('#RouwcentrumsDataSomeDays').val('');
             var model = 'Rouwcentrum';
             var idPrice = 'RouwcentrumsDataPrice';
             var field = 'rouwcentrum_name';
             var number = 1;
             var hidden = 'rouwcentrumPrice';
            // alert(val);
            ajaxCall(name,model,idPrice,field,number,hidden);
             
      });   


        jQuery('#RouwcentrumsDataSomeDays').change(function(){
          var per_price = jQuery('.rouwcentrumPrice').val();
         
          //var len = jQuery('#FuneralLettersDataPrice').val().length;
          var number = jQuery('#RouwcentrumsDataSomeDays').val(); 
          if(number === ''){
             number = 1;
          }
          if(per_price !== ''){
            var total = per_price * number;
            jQuery('#RouwcentrumsDataPrice').val(total);
          }
         
      });

      jQuery('#StampNumber').change(function(){
          var per_price = jQuery('#StampPriceNetherland').val();
          if(per_price === ''){
             per_price = 0;
          }
          var number = jQuery('#StampNumber').val();
          if(per_price !== ''){
            var total = per_price * number;
            jQuery('#StampPrice').val(total);
          }
      });

      
       jQuery('#CareDeceasesDataName').change(function(){
             var name = jQuery(this).val();
             var model = 'CareDecease';
             var idPrice = 'CareDeceasesDataPrice';
             var field = 'name';
            // alert(val);
            ajaxCall(name,model,idPrice,field);
             
      });    

        //Coffee Ticket events 
        jQuery('#CoffeeTicketName').change(function(){
             var name = jQuery(this).val();
             var model = 'Koffietafelkaartje';
             if(name == 'Zonder naam'){
                var idPrice = 'CoffeeTicketNameWithoutPrice';
                jQuery('#CoffeeTicketNameWithPrice').val('');
                jQuery('#CoffeeTicketNumber').val('');
                jQuery('#CoffeeTicketPrice').val('');
             }else if(name == ''){
                jQuery('#CoffeeTicketNameWithPrice').val('');
                jQuery('#CoffeeTicketNameWithoutPrice').val('');
                jQuery('#CoffeeTicketNumber').val('');
                jQuery('#CoffeeTicketPrice').val('');
               }
             else{
                 var idPrice = 'CoffeeTicketNameWithPrice';
                  jQuery('#CoffeeTicketNameWithoutPrice').val('');
                  jQuery('#CoffeeTicketNumber').val('');
                  jQuery('#CoffeeTicketPrice').val('');
             }
            var field = 'type';
            var number = 1;
            ajaxCall(name,model,idPrice,field,number);
             
      });   


        jQuery('#CoffeeTicketNumber').change(function(){
          var name = jQuery('#CoffeeTicketName option:selected').val();
          if(name == 'Zonder naam'){ 
               var per_price = jQuery('#CoffeeTicketNameWithoutPrice').val();
           }else{
                per_price = jQuery('#CoffeeTicketNameWithPrice').val();
            }
         var number = jQuery('#CoffeeTicketNumber').val();  
         if(per_price !== ''){
            var total = per_price * number;
            jQuery('#CoffeeTicketPrice').val(total);
          }
         
      });


     // HomeobaringName sections

        
      jQuery('#HomeobaringName').change(function(){
             var name = jQuery(this).val();
             jQuery('#HomeobaringPerPrice').val('');
             jQuery('#HomeobaringDays').val('');
             var model = 'Homebaring';
             var idPrice = 'HomeobaringPerPrice';
             var field = 'name';
             var number = 1;
             var hidden = 'homeobaringPrice';
            // alert(val);
            ajaxCall(name,model,idPrice,field,number,hidden);
             
      });   


        jQuery('#HomeobaringDays').change(function(){
          var per_price = jQuery('.homeobaringPrice').val();
         
          //var len = jQuery('#FuneralLettersDataPrice').val().length;
          var number = jQuery('#HomeobaringDays').val();
          if(number === ''){
             number = 1;
          }
          if(per_price !== ''){
            var total = per_price * number;
            jQuery('#HomeobaringPerPrice').val(total);
          }
         
      });

// Boekje section 
    jQuery('#MisboekjesDataPricePerPiecename').change(function(){
            var name = jQuery(this).val();
            jQuery('#MisboekjesDataPricePerPiecenumber').val('');
            jQuery('#MisboekjesDataTotalPrice').val('');
            var model = 'Boekje';
             var idPrice = 'MisboekjesDataPricePerPiece';
             var field = 'name';
             var number = 1;
             var hidden = 'MisboekjesDataPricePerPiece';
            // alert(val);
            ajaxCall(name,model,idPrice,field,number,hidden);
             
      });   


    jQuery('#MisboekjesDataPricePerPiecenumber').change(function(){ 
          var per_price = jQuery('#MisboekjesDataPricePerPiece').val(); 
          //var len = jQuery('#FuneralLettersDataPrice').val().length;
          var number = jQuery(this).val();  
          number = number === ''?1:number;
          if(per_price !== ''){
            var total = per_price * number;
           
            jQuery('#MisboekjesDataTotalPrice').val(total);
          }
         
      }); 

     jQuery('#CarriersDataName').change(function(){
             var name = jQuery(this).val();
             if(name == ''){ 
               jQuery('#CarriersDataPrice').val('');
               jQuery('#MiscellaneouseNumber').val('');
            }
             var model = 'Carrier';
             var idPrice = 'CarriersDataPrice';
             var field = 'name';
             var number = 1;
             var hidden = 'carrierPrice';
            // alert(val);
            ajaxCall(name,model,idPrice,field,number,hidden);
             
      });   

      jQuery('#CommemorationNumber').change(function(){
            var number = jQuery(this).val();
            var perPrice = jQuery('#CommemorationPricePerPiece').val();
            number = (number === '') ? 1 : number;
            var total = perPrice * number;
            total = total == 0 ? '' : total;
            jQuery('#CommemorationPrice').val(total);
      });  

       jQuery('#CommemorationPricePerPiece').change(function(){
            jQuery('#CommemorationNumber').val('');
            jQuery('#CommemorationPrice').val('');
            var perPrice = jQuery(this).val();
            var number = jQuery('#CommemorationNumber').val();
            number = (number === '') ? 1 : number;
            var total = perPrice * number;
            total = total == 0 ? '' : total;
            jQuery('#CommemorationPrice').val(total);
            
              
             

      });  

        jQuery('#CarriersDataNumber').change(function(){
          var per_price = jQuery('.carrierPrice').val();
          var number = jQuery('#CarriersDataNumber').val();
          number = (number === '') ? 1 : number;
          if(per_price !== ''){
            var total = per_price * number;
            jQuery('#CarriersDataPrice').val(total);
          }
         
      });
       
       jQuery('#ArrangementData0Type').change(function(){
             var name = jQuery(this).val();
             var model = 'Arrangement';
             var idPrice = 'ArrangementData0Price';
             var field = 'arrangement_type';
            // alert(val);
            ajaxCall(name,model,idPrice,field);
             
      });  

       jQuery('#ArrangementData1Type').change(function(){
             var name = jQuery(this).val();
             var model = 'Arrangement';
             var idPrice = 'ArrangementData1Price';
             var field = 'arrangement_type';
            // alert(val);
            ajaxCall(name,model,idPrice,field);
             
      });  

       jQuery('#ArrangementData2Type').change(function(){
             var name = jQuery(this).val();
             var model = 'Arrangement';
             var idPrice = 'ArrangementData2Price';
             var field = 'arrangement_type';
            // alert(val);
            ajaxCall(name,model,idPrice,field);
             
      });  


       jQuery('#ArrangementData3Type').change(function(){
             var name = jQuery(this).val();
             var model = 'Arrangement';
             var idPrice = 'ArrangementData3Price';
             var field = 'arrangement_type';
            // alert(val);
            ajaxCall(name,model,idPrice,field);
             
      });  

      

       jQuery('#FuneralLettersDataLetterName').change(function(){
             var name = jQuery(this).val();
             if(name == ''){ 
               jQuery('#FuneralLettersDataNumber').val('');
               jQuery('#FuneralLettersDataPrice').val('');
             }
             var model = 'FunerlLetterCard';
             var cardField = 'FuneralLettersDataMapType';
             var field = 'funeral_letter_id';
            // alert(val);
            findCardType(name,model,cardField,field);
             
      });  

        jQuery('#FuneralLettersDataMapType').change(function(){
             var name = jQuery(this).val();
             jQuery('#FuneralLettersDataNumber').val('');
             jQuery('#FuneralLettersDataPrice').val('');
             var model = 'FunerlLetterCard';
             var cardField = 'FuneralLettersDataPrice';
             var field = 'card';
             var number = jQuery('#FuneralLettersDataNumber').val(); 
             var hidden = 'cardPrice';            
            // alert(val);
            ajaxCall(name,model,cardField,field,number,hidden);
             
      }); 

      jQuery('#FuneralLettersDataNumber').change(function(){
          var per_price = jQuery('.cardPrice').val();
         
          //var len = jQuery('#FuneralLettersDataPrice').val().length;
          var number = jQuery('#FuneralLettersDataNumber').val();
          number = number === '' ? 1 : number;
          if(per_price !== ''){
            var total = per_price * number;
            jQuery('#FuneralLettersDataPrice').val(total);
          }
         
      }); 


         jQuery('#PrayerCardsDataPrayerCategoryType').change(function(){
             var name = jQuery(this).val();
             jQuery('#PrayerCardsDataNumber').val('');
             jQuery('#PrayerCardsDataPrice').val('');
             var model = 'PrayerCard';
             var cardField = 'PrayerCardsDataCardType';
             var field = 'prayer_card_category_id';
             
            // var hidden = 'cardPrice';            
            // alert(val);
            findCardType(name,model,cardField,field);
             
      }); 

        jQuery('#PrayerCardsDataCardType').change(function(){
             var name = jQuery(this).val();
             jQuery('#PrayerCardsDataNumber').val('');
             jQuery('#PrayerCardsDataPrice').val('');
             var model = 'PrayerCard';
             var cardField = 'PrayerCardsDataPrice';
             var field = 'card';
             var number = jQuery('#PrayerCardsDataNumber').val(); 
             var hidden = 'PrayerCardPrice';            
            // alert(val);
            ajaxCall(name,model,cardField,field,number,hidden);
             
      });    

       jQuery('#PrayerCardsDataNumber').change(function(){
          var per_price = jQuery('.PrayerCardPrice').val();
         
          //var len = jQuery('#FuneralLettersDataPrice').val().length;
          var number = jQuery('#PrayerCardsDataNumber').val();
          number = number === ''?1:number;
          if(per_price !== ''){
            var total = per_price * number;
            jQuery('#PrayerCardsDataPrice').val(total);
          }
         
      }); 

    ///////////// POSTZEAL/STAMPS SECTION ////////////////// 
    
     jQuery('#StampNumber').change(function(){
            var number = jQuery(this).val();
            var perPrice = jQuery('#StampPriceNetherland').val();
            number = (number === '') ? 1 : number;
            var total = perPrice * number;
            total = total == 0 ? '' : total;
            jQuery('#StampPrice').val(total);
      });  

       jQuery('#StampPriceNetherland').change(function(){
            jQuery('#StampNumber').val('');
            jQuery('#StampPrice').val('');
            var perPrice = jQuery(this).val();
            var number = jQuery('#StampNumber').val();
            number = (number === '') ? 1 : number;
            var total = perPrice * number;
            total = total == 0 ? '' : total;
            jQuery('#StampPrice').val(total);
            
              
             

      });  


    ///////////// BOOKLET SECTION //////////////////

      jQuery('#MiscellaneouseBooklet').change(function(){
             var name = jQuery(this).val();
             var model = 'Booklet';
             var cardField = 'MiscellaneouseBookletPrice';
             var field = 'name';
             // alert(val);
            ajaxCall(name,model,cardField,field);
             
      }); 

     ///////////// GUIDE AFTER DEATH SECTION //////////////////
    
      jQuery('#MiscellaneouseGuideAfterDeath').change(function(){
             var name = jQuery(this).val();
             var model = 'GuideAfterDeath';
             var cardField = 'MiscellaneouseGuideAfterDeathPrice';
             var field = 'name';
             // alert(val);
            ajaxCall(name,model,cardField,field);
             
      });  

//    
      jQuery('.arrow').on('click',function(){ //alert('hell');
           var direction = jQuery(this).data('rel');
           var curtElem = jQuery('li.active');
          if(direction == 'right'){
                if(jQuery(curtElem).is(':last-child')){
                   var first =  jQuery(curtElem).parent('ul').find('li:first-child').children('a');
                    var href = first.attr("href");
                    window.location.replace(href);
                }else{
                   var nextElem = jQuery(curtElem).next().children('a');
                   var href = nextElem.attr("href");
                  window.location.replace(href);
              }
           }else
           {
             if(jQuery(curtElem).is(':first-child')){
                    var last =  jQuery(curtElem).parent('ul').find('li:last-child').children('a');
                    var href = last.attr("href");
                    window.location.replace(href);
                }else{
                  var prevElem = jQuery(curtElem).prev().children('a');
                  var href = prevElem.attr("href");
                  window.location.replace(href);
            }
          }
       
          //var nextElem = curtElem.next();
          //jQuery('li.active').next().trigger('click');
      });

    getAutocompleteList('.postcode', webpath + 'admin/clientDatas/getCities/');
    getAutocompleteList('.undertakerName', webpath + 'admin/Entrepreneurs/getUndertakerNames/' , 2);

});

/******Function for ajax Call to find price ********/

function  ajaxCall(name,model,inputPriceField,field,number,hidden){
           if(typeof number == 'undefined'){
            number = 1;
           }
           if(typeof number  == 'undefined'){
            hidden = '';
           }
           jQuery.ajax({
                 type:'POST',
                  url: priceUrl, 
                  dataType:'json',
                  data:{name:name,model:model,number:number,field:field},
                  success:function(html){
                   jQuery('#'+inputPriceField).val(html.value);
                    if(hidden != ''){
                       jQuery('.'+hidden).val(html.value);
                     }
                  }
            });
}


/******Function for ajax Call to find card type ********/

function findCardType(name,model,card,field)
{           jQuery.ajax({
                  type:'POST',
                  url: cartUrl,
                  dataType:'html',
                  data:{name:name,model:model,field:field},
                  success:function(response){                     
                      jQuery('#'+card).html(response);
                  }
            });
}
/**Custom function for manipulation of radio ***/

    jQuery('div.dataradioGroup1').hide();
    jQuery('div.dataradioGroup2').hide();
    jQuery('div.dataradioGroup3').hide();
    jQuery('document').ready(function(){
    	  jQuery('input.radioGroup1').change(function(){
    	       var data = jQuery(this).val();
    	       toggleDataField('dataradioGroup1',data);
	          
        });

    /*	jQuery('input.group2').change(function(){
    		jQuery('div.data1').fadeOut();
    		var  data = jQuery(this).closest('.form-group').find('.data1');
    		data.fadeIn();
          });*/
        jQuery('input.radioGroup2').change(function(){
    	       var data = jQuery(this).val();
    	       toggleDataField('dataradioGroup2',data);
	          
        });
          jQuery('input.radioGroup3').change(function(){
    	       var data = jQuery(this).val();
    	       toggleDataField('dataradioGroup3',data);
	          
        });
    });


    // function for manipulating radio buttons

    function toggleDataField(valueField,data)
    {
           	  if(data == 1){
           	  	jQuery('div.'+valueField).fadeIn();
           	  }else
           	  {
           	  	jQuery('div.'+valueField).fadeOut();
           	  }

    }

//function for datepicker
   function showDatePicker(element){
     jQuery(element).pickadate({
        format:'yyyy-mm-dd',
        monthsFull: ['januari','februari','maart','april','mei','juni','juli','augustus','september','october','november','december'],
        monthsShort: ['jan.','feb.','maart','apr.','mei','juni','juli','aug.','sept.','oct.','nov.','dec.'],
        weekdaysFull: ['zondag','maandag','dinsdag','woensdag','donderdag','vrijdagdag','zaterdag'],
        weekdaysShort: ['Zon','Ma','Di','Woe','Do','Vrij','Zat'],
        selectYears: 100,

      });
   }
 // function  to for validating postCodes

 function  validatPostCode(postCode){
    var regex = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}jQuery/i;
    return regex.test(postCode);
 }

 //function to generate street and location

 function generateStreetnLocation(postCode , streetId , placeId ){
      jQuery.ajax({
                type:'POST',
                url: dynamicLocationGeneartor,
                dataType:'json',
                data:{postCode:postCode},
                success:function(response){
                   if(response.street_name == '' || response.city == ''){
                    var message = '<label class="control-label col-sm-2 error">Wrong postcode</label>';
                    jQuery('.error').remove();
                    jQuery('.message').append(message);
                    jQuery('.'+streetId).val('');
                    jQuery('.'+placeId).val('');
                   }else{                 
                    jQuery('.'+streetId).val(response.street_name);
                    jQuery('.'+placeId).val(response.city);
                  }
                   // jQuery('#'+streetId).val(response.char);
                   // jQuery('#'+streetId).val(response.code);
                }
            });

 }




//for autocomplete
 function getAutocompleteList( elm, url, minLength ) {
  if( typeof minLength === 'undefined' ) {
    minLength = 3;
  }
  jQuery( elm ).autocomplete({
    source: function( request, response ) {       
      getSource(url, request.term, response);
    },
    minLength: minLength
  });

}

function getSource( url, term, response ) {

  jQuery.ajax({
      url: url + term,
      dataType: "json",
      
      success: function( data ) {           
        response( data );
      }
    });
  
}


function inputFieldViewerByradioStatus(radioStatus,inputFieldDiv)
{
  //input:radio[class=radioGroup1]:checked
  var radioChecker = jQuery(radioStatus).val();
      if(radioChecker == 1){
        jQuery(inputFieldDiv).show();
      }

}